#ifndef CM_POIS_NONATOMIC_OPS_H_
#define CM_POIS_NONATOMIC_OPS_H_


#define EXTRA_BLOCK_SPACE (sizeof(BlockMetaData))

inline BlockMetaData *getMetaDataFromBlock(void *block, size_t objSize) {
    return (BlockMetaData *) (block + objSize - sizeof(BlockMetaData));
}

inline BLKPtr getBlockFromMetaData(BlockMetaData *md, size_t objSize) {
    BLKPtr res = (void *)md - (objSize - sizeof(BlockMetaData));
    return res;
}


////////////////////////////////////////
// operations for malloc
////////////////////////////////////////

#define _CM_OPS_MALLOC_POISON \
    BlockMetaData *blkMD = getMetaDataFromBlock(block, objSize); \
    blkMD->guard = 1; \
    blkMD->next = NULL;


////////////////////////////////////////
// operations for remote freeing
////////////////////////////////////////

inline void remoteFree(BLKPtr block, SBMetaData *sbmd) {

    // read the current valid guard
    unsigned long guard = sbmd->md.remoteList.guard;

    // calculate ptr addr using guard (to enforce RD->RD ordering)
    // guard's LSB is guaranteed to be 0
    BLKPtr *ListAddr = (void**)(&sbmd->md.remoteList.next) + (guard & 0x1);

    // read the list head pointer
    BLKPtr rl = *ListAddr;

    // write both to the current block
    BlockMetaData *blkMD = getMetaDataFromBlock(block, sbmd->md.objSize);
    blkMD->guard = guard;
    blkMD->next = rl;

    // add the block to the remote list
    sbmd->md.remoteList.next = block;
}


////////////////////////////////////////
// operations for collecting remote
// lists
////////////////////////////////////////

inline BLKPtr swapAndConditionRemoteList(SBMetaData *sbmd) {

    unsigned long oldGuard;
    BLKPtr newList = sbmd->md.remoteList.next;
    size_t objSize = sbmd->md.objSize;

    // check and exchange the remote list head if not NULL
    if (newList != NULL) {
        // atomically exchange the remote list head
        newList = __sync_lock_test_and_set(&sbmd->md.remoteList.next, NULL);

        // update the version
        unsigned long guard;
        guard = oldGuard = sbmd->md.remoteList.guard;
        guard += 2;
        sbmd->md.remoteList.guard = guard;

        // condition the remote list
        BLKPtr curObj = newList;
        while (curObj != NULL) {
            BlockMetaData *blkMD = getMetaDataFromBlock(curObj, objSize);
            unsigned long blkGuard = blkMD->guard;

            // if the guard is invalid, annul the next pointer
            // even when the guard is valid, it is possible that the pointer is invalid
            // but that does not need special handling, as an invalid pointer is NULL anyway
            if (blkGuard != oldGuard) {
                blkMD->next = NULL;
            }

            curObj = blkMD->next;

#ifdef USE_STATISTICS
            heap.stats->recoveredRemoteBlks += 1;
#endif
        }
    }

    return newList;
}


////////////////////////////////////////
// misc. operations
////////////////////////////////////////

// setting guard to 0 ensures that "invalid" is initialized to 0
// because it is never touched later, it'll remain 0 forever
#define _CM_OPS_INIT_SB \
    sbmd->md.remoteList.next = NULL; \
    sbmd->md.remoteList.guard = 0;

#endif /* CM_POIS_NONATOMIC_OPS_H_ */

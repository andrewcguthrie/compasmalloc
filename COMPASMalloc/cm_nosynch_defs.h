#ifndef CM_NOSYNCH_DEFS_H_
#define CM_NOSYNCH_DEFS_H_

typedef struct BlockMetaData {
    void *next;
} BlockMetaData;

#endif /* CM_NOSYNCH_DEFS_H_ */

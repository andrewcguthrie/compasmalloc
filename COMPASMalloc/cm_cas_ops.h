#ifndef CM_CAS_OPS_H_
#define CM_CAS_OPS_H_

#define EXTRA_BLOCK_SPACE (0)

inline BlockMetaData *getMetaDataFromBlock(BLKPtr block, size_t objSize) {
    return (BlockMetaData *) block;
}

inline BLKPtr getBlockFromMetaData(BlockMetaData *md, size_t objSize) {
    return (BLKPtr) md;
}


////////////////////////////////////////
// operations for malloc
////////////////////////////////////////

#define _CM_OPS_MALLOC_POISON   /*nothing*/


////////////////////////////////////////
// operations for remote freeing
////////////////////////////////////////

inline void remoteFree(BLKPtr block, SBMetaData *sbmd) {
    bool success;
    do {
        BLKPtr rl = sbmd->md.remoteList.next;
        getMetaDataFromBlock(block, sbmd->md.objSize)->next = rl;

        // Note: there should be a write barrier here
        // the assumption is that __sync_bool_compare_and_swap includes that fence
        // if not, we should insert one manually
        success = __sync_bool_compare_and_swap(&sbmd->md.remoteList.next, rl,  block);
    } while (!success);
}

////////////////////////////////////////
// operations for collecting remote
// lists
////////////////////////////////////////

inline BLKPtr swapAndConditionRemoteList(SBMetaData *sbmd) {

    // check and swap the list head
    BLKPtr newList = sbmd->md.remoteList.next;
    if (newList != NULL) {
        newList = __sync_lock_test_and_set(&sbmd->md.remoteList.next, NULL);
    }

    return newList;
}

////////////////////////////////////////
// misc. operations
////////////////////////////////////////

#define _CM_OPS_INIT_SB \
    sbmd->md.remoteList.next = NULL;


#endif /* CM_CAS_OPS_H_ */

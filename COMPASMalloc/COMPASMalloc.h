#ifndef COMPASMALLOC
#define COMPASMALLOC

#include <pthread.h>
#include <cassert>
#include <cstdint>
#include <cstdio>
#include <cstring>
#include <sys/mman.h>


// high-level configuration definitions
// define CM_NOSYNC to enable no_sync version
// define CM_PTHREAD to enable pthread version
// define CM_CAS to enable the cas version (cas is used to atomically place a block on the remoteList)
// define CM_POIS_NONATOMIC to enable poisoned w/ non-atomic-tuple version

#if !(defined(CM_NOSYNC) || defined(CM_PTHREAD) || defined(CM_CAS) || defined(CM_POIS_NONATOMIC))
#error "Please choose a valid COMPASMalloc flavor"
#endif


namespace COMPASMalloc {

// forward type declaration(s)
typedef struct LocalHeap;
typedef struct BlockMetaData;
typedef union SBMetaData;

/** Enumeration of all the various size classes */
enum SizeClass {
    SCERR       = -1,     // Error (less than 1)
    SC1B        = 0,      // 1 bytes - unused
    SC2B,                 // 2 bytes - unused
    SC4B,                 // 4 bytes - unused
    SC8B,                 // 8 bytes
    SC16B,                // 16 bytes
    SC32B,                // 32 bytes
    SC64B,                // 64 bytes
    SC128B,               // 128 bytes
    SC256B,               // 256 bytes
    SC512B,               // 512 bytes
    SC1024B,              // 1024 bytes
    SC2048B,              // 2048 bytes
    SC4096B,              // 4096 bytes
    SCLRG                 // Large (greater than 4096)
};

// constant declarations
#define NUM_SIZE_CLASS          ((int)SCLRG)
#define PAGE_SIZE               4096
#define SB_SIZE                 (2*PAGE_SIZE)
#define MMAP(size)              mmap(0,size,PROT_READ|PROT_WRITE,MAP_ANON|MAP_PRIVATE,-1,0)
#define SUPERBLOCK_BITMASK      ((0UL - 1UL) & ~0x1FFFUL)
#define META_DATA_BLOCK_SIZE    128

// just to syntactically indicate whether a pointer points to a block or superblock
typedef void* SBPtr;
typedef void* BLKPtr;


////////////////////////////////////////////////////////////////////////////////
// statistics
////////////////////////////////////////////////////////////////////////////////

#ifdef USE_STATISTICS

#define NUM_STATISTICS_BINS 17 // A modestly sized prime number

typedef struct HeapStatistics {
    pthread_t owningThread;
    unsigned long mallocCount;
    unsigned long localFreeCount;
    unsigned long remoteFreeCount;
    unsigned long recoveredRemoteBlks;
    unsigned long swapCount;
    unsigned long numSuperblocksAllocated;
} HeapStatistics;

// Array of all heaps arrays (use modulo p to distribute evenly)
static HeapStatistics heapStats[NUM_STATISTICS_BINS];

#endif


////////////////////////////////////////////////////////////////////////////////
// data types
////////////////////////////////////////////////////////////////////////////////

#if defined(CM_NOSYNC)
#include "./cm_nosynch_defs.h"
#elif defined(CM_PTHREAD)
#include "./cm_pthread_defs.h"
#elif defined(CM_CAS)
#include "./cm_cas_defs.h"
#elif defined(CM_POIS_NONATOMIC)
#include "./cm_pois_nonatomic_defs.h"
#endif


/**
 * Data structure to store SB metadata. For now, always placed
 * at the end of the corresponding SB. @cl should make sure the 
 * structure is padded to a cache line boundary.
 * 
 * Invariants:
 *      @remoteList is only added to by remote processors
 */
typedef union SBMetaData {
    struct {
    volatile BlockMetaData remoteList;
    SizeClass sizeClass;
    size_t objSize;                         /** always equal to 1 << sizeClass */
    LocalHeap *heap;
    SBMetaData *nextSB;
#ifdef CM_PTHREAD
    pthread_mutex_t mutex;
#endif
    } md;
    char cl[META_DATA_BLOCK_SIZE];
} SBMetaData;


/**
 * Data structure to store local heap metadata.
 *
 * Invariants:
 *      Each entry in @tlabs is a vanilla pointer to a block of the appropriate size class
 *      Elements on a tlab list are linked together using vanilla pointers
 *      Pointer validity is checked for when moving objects from a remoteList to the tlab
 */
typedef struct LocalHeap {
    pthread_t owningThread;
    SBMetaData *superblocks[NUM_SIZE_CLASS];
    SBMetaData *superblocksTail[NUM_SIZE_CLASS];
    BLKPtr tlabs[NUM_SIZE_CLASS];
#ifdef USE_STATISTICS
    HeapStatistics *stats;
#endif
} LocalHeap;


////////////////////////////////////////////////////////////////////////////////
// implementation
////////////////////////////////////////////////////////////////////////////////

/**
 * Thread-local heap (initialize to NULL)
 */
thread_local LocalHeap heap;


#if defined(CM_NOSYNC)
#include "./cm_nosynch_ops.h"
#elif defined(CM_PTHREAD)
#include "./cm_pthread_ops.h"
#elif defined(CM_CAS)
#include "./cm_cas_ops.h"
#elif defined(CM_POIS_NONATOMIC)
#include "./cm_pois_nonatomic_ops.h"
#endif

// function stubs
void * initializeSuperblock(SizeClass sizeClass);
void constructLinkedList(void *sbAddr, SizeClass sizeClass);
bool findNonEmptySB(SizeClass sizeClass);
void initializeNewHeap();
BLKPtr mallocForSizeClass(SizeClass sizeClass);
void addNewSuperBlock(SizeClass sizeClass);


// Each superblock's metadata will be placed at the end of it
#define SB_META_DATA_OFFSET (SB_SIZE - sizeof(SBMetaData))

inline SBMetaData *getMetaDataFromSuperblock(void *sbAddr) {
  return (SBMetaData *)(sbAddr + SB_META_DATA_OFFSET);
}
inline void *getSuperblockFromMetaData(SBMetaData *sbmd) {
  return ((void*)(sbmd) - SB_META_DATA_OFFSET);
}


inline SizeClass getSizeClass(size_t sz) {
    assert(0 < sz && sz <= 4096-EXTRA_BLOCK_SPACE);

    // account for the metadata space
    sz += EXTRA_BLOCK_SPACE;

    // round to the next multiple of 8
    unsigned n = ((sz + 7) >> 3) << 3;

    // find the (smallest power of 2 larger than or equal to sz) - 1
    n -= 1;
    n |= n >> 1;
    n |= n >> 2;
    n |= n >> 4;
    n |= n >> 8;

    // count the number of 1s in n to find the index; there can be at most 12
    static int ONES_COUNT[16] = {0, 1, 0, 2, 0, 0, 0, 3, 0, 0, 0, 0, 0, 0, 0, 4};

    int res = 0;
    res += ONES_COUNT[(n & 0xF00) >> 8];
    res += ONES_COUNT[(n & 0x0F0) >> 4];
    res += ONES_COUNT[(n & 0x00F)];

    return (SizeClass) res;
}


/**
 * Check to see if current thread has a heap
 * Should be called by all interface functions
 */
inline void checkHeap() {
    if (heap.owningThread == 0)
        initializeNewHeap();
}


void * _malloc(size_t sz) {
    assert(sz > 0 && sz <= 4096);

    checkHeap();

    //printf("[DEBUG] In _malloc. sz:%lu, sizeof(pthread_mutex_t):%lu, sizeof(SBMetaData):%lu, heap.owningThread:%lu\n",
    // sz, sizeof(pthread_mutex_t), sizeof(SBMetaData), (unsigned long)heap.owningThread);

    if (sz <= 0) {
        printf("[FATAL] Non-positive size class in malloc\n");
        exit(1);
    }
    else if (sz >= 4096) {
        printf("[FATAL] Large size class in malloc; currently not supported\n");
        exit(1);
    }
    else {
        SizeClass sc = getSizeClass(sz);
        return mallocForSizeClass(sc);
    }
}


BLKPtr mallocForSizeClass(SizeClass sizeClass) {
    void *sbAddr = NULL;
    int index = (int) sizeClass;
    size_t objSize = 1 << sizeClass;

    // anything on the tlab?
    if (heap.tlabs[index] == NULL) {

        if (heap.superblocks[index] == NULL) {
            // allocate a new superblock and add all of its blocks to the TLAB
            addNewSuperBlock(sizeClass);
        }
        else {
            // try to pull free blocks from a superblock into the TLAB
            bool success = findNonEmptySB(sizeClass);
            if (! success) {
                // allocate a new superblock and add all of its blocks to the TLAB
                addNewSuperBlock(sizeClass);
            }
        }
    }

    // pop the head off the TLAB
    BLKPtr block = heap.tlabs[index];
    heap.tlabs[index] = getMetaDataFromBlock(block, objSize)->next;

    // poison the object before returning (if necessary)
    _CM_OPS_MALLOC_POISON;

#ifdef USE_STATISTICS
    heap.stats->mallocCount += 1;
#endif

    return block;
}


void _free(void *block) {
    SBPtr sbAddr = (SBPtr)((unsigned long)block & SUPERBLOCK_BITMASK);
    SBMetaData *sbmd = getMetaDataFromSuperblock(sbAddr);

#ifdef USE_STATISTICS
    checkHeap();
#endif

    LocalHeap *owner = sbmd->md.heap;
    if (owner == NULL) {
        printf("[FATAL] _free call made to invalid addr: %p\n", block);
        exit(1);
    }

    // Now we check if the owner of this superblock is the current thread
    if (owner == &heap) {
        // we are the owner; push this block onto the head of the TLAB
        int index = (int) sbmd->md.sizeClass;
        getMetaDataFromBlock(block, sbmd->md.objSize)->next = heap.tlabs[index];
        heap.tlabs[index] = block;

#ifdef USE_STATISTICS
        heap.stats->localFreeCount += 1;
#endif
    }
    else {
        // We are remote; push the block on to the SB's remoteList
        remoteFree(block, sbmd);

#ifdef USE_STATISTICS
        heap.stats->remoteFreeCount += 1;
#endif
    }

    return;
}


/**
 * If the current thread does not have it's own heap, this function allocates one from the manager's
 * mem pool and then initializes all of the fields to their proper values.
 *
 * If the manager instance doesn't exist, then create it (thereby allocating all of the heap mem we will need)
 */
void initializeNewHeap() {

    // Initialize the entries of our new heap
    heap.owningThread = pthread_self();
    for (unsigned i = 0; i < NUM_SIZE_CLASS; ++i) {
        heap.superblocks[i] = NULL;
        heap.superblocksTail[i] = NULL;
        heap.tlabs[i] = NULL;
    }

#ifdef USE_STATISTICS
    // Initialize statistics for this heap
    unsigned statisticsIndex = heap.owningThread % NUM_STATISTICS_BINS;
    HeapStatistics *stats = &heapStats[statisticsIndex];
    heap.stats = stats;
    memset(stats, 0, sizeof(HeapStatistics));
    stats->owningThread = heap.owningThread;
#endif

    return;
}


/**
 * Allocate a new superblock, and it to the heap, and put all of its objects on the tlab.
 */
void addNewSuperBlock(SizeClass sizeClass) {

    // allocate
    int index = (int) sizeClass;
    void *sbAddr = initializeSuperblock(sizeClass);

    // add the blocks to the tlab
    constructLinkedList(sbAddr, sizeClass);
    heap.tlabs[index] = sbAddr;

    // append the new superblock to the list
    SBMetaData *sbmd = getMetaDataFromSuperblock(sbAddr);
    if (NULL == heap.superblocks[index]) {
        heap.superblocks[index] = sbmd;
    }
    else {
        assert(NULL != heap.superblocksTail[index]);
        heap.superblocksTail[index]->md.nextSB = sbmd;
    }
    heap.superblocksTail[index] = sbmd;

#ifdef USE_STATISTICS
    heap.stats->numSuperblocksAllocated += 1;
#endif
}


/**
 * MMAP's a new superblock, constructs the free deque, and sets the meta-data to
 * the appropriate initial values
 */
void * initializeSuperblock(SizeClass sizeClass) {
    void *sbAddr;

    sbAddr = MMAP(SB_SIZE);
    if (sbAddr == NULL) {
        printf("[FATAL] MMAP call to allocate new superblock failed\n");
        exit(1);
    }

    if ((unsigned long)sbAddr & 0x1000UL) {
        munmap(sbAddr, SB_SIZE);
        sbAddr = MMAP(3*PAGE_SIZE);
        if ((unsigned long)sbAddr & 0x1000UL) {
            munmap(sbAddr, 1*PAGE_SIZE);
            sbAddr += PAGE_SIZE;
        }
        else {
            munmap(sbAddr + SB_SIZE, PAGE_SIZE);
        }
    }

    // common initialization
    SBMetaData *sbmd = getMetaDataFromSuperblock(sbAddr);
    sbmd->md.sizeClass = sizeClass;
    sbmd->md.objSize = 1 << sizeClass;
    sbmd->md.heap = &heap;
    sbmd->md.nextSB = NULL;

    // flavor-specific initialization
    _CM_OPS_INIT_SB;

    return sbAddr;
}


/**
 * given a superblock address, this constructs a linked list where the "next" pointer is
 * within the memory of the current block. Returns the head of the list.
 */
void constructLinkedList(void *sbAddr, SizeClass sizeClass) {
    int index = (int) sizeClass;
    unsigned objSize = 1 << index;

    unsigned temp;
    for (temp = 0 ; temp + 2*objSize <= SB_META_DATA_OFFSET; temp += objSize) {
        // current and next blocks
        void *block = sbAddr + temp;
        void *nextBlock = sbAddr + temp + objSize;

        // current should point to next
        getMetaDataFromBlock(block, objSize)->next = nextBlock;
    }

    // NULLing the end of the linked list to be explicit
    getMetaDataFromBlock(sbAddr + temp, objSize)->next = NULL;
}


/**
 * Find the first non-empty free list among the superblock of @sizeClass.
 * Move all superblocks (including the one found) to the tail of the SB list.
 * Condition the list found and put all the retrievable blocks on the tlab.
 */
bool findNonEmptySB(SizeClass sizeClass) {
    int index = (int) sizeClass;
    SBMetaData **listHead = &heap.superblocks[index];
    SBMetaData **listTail = &heap.superblocksTail[index];

    SBMetaData *origHead = *listHead;

    // loop until we find a non-empty remote list
    // or exhaust the list
    SBMetaData *sbmd = *listHead;
    SBMetaData *newTail = NULL;
    BLKPtr newList = NULL;

    while ((NULL == newList) && (NULL != sbmd)) {

        // check the remoteList of sbmd and return it if NULL
        newList = swapAndConditionRemoteList(sbmd);

        // move the SB to the tail
        newTail = sbmd;

        // proceed to the next superblock in the list
        sbmd = sbmd->md.nextSB;
    }

    // update the list pointers if the list is going to change
    if (newTail != NULL && newTail->md.nextSB != NULL) {
        *listHead = newTail->md.nextSB;

        // it is important to do the following in this order
        // (*listTail) and newTail might be the same objects if the list has only one element
        (*listTail)->md.nextSB = origHead;
        newTail->md.nextSB = NULL;
        *listTail = newTail;
    }

    // add all the retrievable elements on the remote list to the tlab
    if (newList != NULL) {
        heap.tlabs[index] = newList;
    }
    
#ifdef USE_STATISTICS
    heap.stats->swapCount += newList ? 1 : 0;
#endif

    return (newList != NULL);
}

void _printStats() {
#ifdef USE_STATISTICS
    HeapStatistics overall;
    memset(&overall, 0, sizeof(overall));

    for (unsigned i = 0; i < NUM_STATISTICS_BINS; i++) {
        HeapStatistics &stats = heapStats[i];
        if (stats.owningThread != 0) {
            overall.mallocCount += stats.mallocCount;
            overall.localFreeCount += stats.localFreeCount;
            overall.remoteFreeCount += stats.remoteFreeCount;
            overall.recoveredRemoteBlks += stats.recoveredRemoteBlks;
            overall.swapCount += stats.swapCount;
            overall.numSuperblocksAllocated += stats.numSuperblocksAllocated;
        }
    }

    printf("Malloc Count: %lu\n", overall.mallocCount);
    printf("Local-Free Count: %lu\n", overall.localFreeCount);
    printf("Remote-Free Count: %lu\n", overall.remoteFreeCount);
    printf("Recovered-Remote Count: %lu\n", overall.recoveredRemoteBlks);
    printf("Swap Count: %lu\n", overall.swapCount);
    printf("Superblocks Allocated: %lu\n", overall.numSuperblocksAllocated);
    //printf("Leaked Count: %d\n", overall.mallocCount - (overall.localFreeCount + overall.remoteFreeCount));
#endif
}

}

#endif // #ifndef COMPASMALLOC

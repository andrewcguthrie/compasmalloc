import os
import sys

Platforms = ['x86-64', 'x86-32', 'ARM-32', 'ARM-64']
Versions = ['cas', 'pthread', 'poisNA'] # , 'NoSynch']
# Benchmarks = ['asynchPmC', 'semisynchPmC']
Benchmarks = ['synchPC', 'asynchPC', 'semisynchPC', 'asynchPmC', 'semisynchPmC']
BuildDir = 'bin'

BuildCommands = {'x86-64':'g++ -std=c++11 -O3 -DNDEBUG', 
                 'x86-32':'g++ -m32 -std=c++11 -O3 -DNDEBUG', 
                 'ARM-32':'/var/services/homes/aguthrie/Research/NDK/toolchains/android/arm/bin/arm-linux-androideabi-g++ -std=c++11 -march=armv7-a --sysroot=/var/services/homes/aguthrie/Research/NDK/toolchains/android/arm/sysroot/ -O3 -DNDEBUG', 
                 'ARM-64':'/var/services/homes/aguthrie/Research/NDK/toolchains/android/aarch64/bin/aarch64-linux-android-g++ -std=c++11 -march=armv8-a --sysroot=/var/services/homes/aguthrie/Research/NDK/toolchains/android/aarch64/sysroot/ -O3 -DNDEBUG -fpie -fpic -fPIE -pie'}

ReturnToParentDir = os.path.join('..', '..', '..')

BuildFile_x86 = {'synchPC':os.path.join(ReturnToParentDir, 'simple_affinity.cpp'), 'asynchPC':os.path.join(ReturnToParentDir, 'AsynchProducerConsumer.cpp'), 'semisynchPC':os.path.join(ReturnToParentDir, 'SemiSynchProducerConsumer.cpp'), 'asynchPmC':os.path.join(ReturnToParentDir, 'AsynchProducerMultiConsumer.cpp'), 'semisynchPmC':os.path.join(ReturnToParentDir, 'SemiSynchProducerMultiConsumer.cpp')}

BuildFile_ARM_32 = {'synchPC':os.path.join(ReturnToParentDir, 'ProducerConsumer.cpp'), 'asynchPC':os.path.join(ReturnToParentDir, 'AsynchProducerConsumer_ARM32.cpp'), 'semisynchPC':os.path.join(ReturnToParentDir, 'SemiSynchProducerConsumer_ARM32.cpp'), 'asynchPmC':os.path.join(ReturnToParentDir, 'AsynchProducerMultiConsumer_ARM32.cpp'), 'semisynchPmC':os.path.join(ReturnToParentDir, 'SemiSynchProducerMultiConsumer_ARM32.cpp')}

BuildFile_ARM_64 = {'synchPC':os.path.join(ReturnToParentDir, 'ProducerConsumer64.cpp'), 'asynchPC':os.path.join(ReturnToParentDir, 'AsynchProducerConsumer_ARM64.cpp'), 'semisynchPC':os.path.join(ReturnToParentDir, 'SemiSynchProducerConsumer_ARM64.cpp'), 'asynchPmC':os.path.join(ReturnToParentDir, 'AsynchProducerMultiConsumer_ARM64.cpp'), 'semisynchPmC':os.path.join(ReturnToParentDir, 'SemiSynchProducerMultiConsumer_ARM64.cpp')}

BuildFileByPlatform = {'x86-64':BuildFile_x86, 'x86-32':BuildFile_x86, 'ARM-32':BuildFile_ARM_32, 'ARM-64':BuildFile_ARM_64}

Includes = {'cas':'-DCM_CAS', 'pthread':'-DCM_PTHREAD', 'poisNA':'-DCM_POIS_NONATOMIC'}

CommandsRun = []

def buildBenchmarks(platform, useStatistics, useLimits):
    for version in Versions:
#        os.mkdir('%s' % (version))
#        os.chdir('%s' % (version))
        for benchmark in Benchmarks:
            buildCmd = '%s' % (BuildCommands[platform])
            buildTarget = '%s_%s_%s' % (benchmark, version, platform)
            if useStatistics:
                buildCmd += ' -DUSE_STATISTICS'
                buildTarget += '_stats'
            if useLimits:
                buildCmd += ' -DUSE_SUPERBLOCK_LIMITS'
                buildTarget += '_limits'
            buildCmd += ' %s %s -o %s' % (Includes[version], BuildFileByPlatform[platform][benchmark], buildTarget)
            if 'x86' in platform:
                buildCmd += ' -lpthread -fpermissive'
            print '[INFO] Running Build Command for %s, %s, %s, Statistics: %s, Limits: %s\n%s' % (benchmark, version, platform, useStatistics, useLimits, buildCmd)
            os.system(buildCmd)
            CommandsRun.append(buildCmd)
#        os.chdir('..')
    return

def main():
    os.system('rm -rf %s/' % (BuildDir))
    os.system('mkdir %s' % (BuildDir))
    os.chdir('%s' % (BuildDir)) # cd into bin/
    for platform in Platforms:
        os.mkdir('%s' % (platform))
        os.chdir('%s' % (platform)) # cd into platform/
        os.mkdir('Stats')
        os.chdir('Stats')           # cd into Stats
        buildBenchmarks(platform, True, False)
        os.chdir('..')              # cd out of Stats
        os.mkdir('NoStats')
        os.chdir('NoStats')         # cd into NoStats
        buildBenchmarks(platform, False, False)
        os.chdir('..')              # cd out of NoStats
        os.chdir('..')              # cd out of platform
    os.chdir('..') # cd out of bin/

    for line in CommandsRun:
        print line
    return

if __name__ == '__main__':
    main()

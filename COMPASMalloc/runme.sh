#!/bin/sh

NOW=`date +%m-%d-%H:%M`

mkdir -p $NOW
RESULTS=$NOW/results.csv
RESULTS_LIMITS=$NOW/results_limits.csv
STATS=$NOW/stats.csv
STATS_LIMITS=$NOW/stats_limits.csv
BIN_DIR=bin
NOSTATNOLIMIT_DIR=NoStats
STATNOLIMIT_DIR=Stats
NOSTAT_DIR=NoStats-Limits
STAT_DIR=Stats
# PLATFORM='x86-64'

echo -n > $RESULTS
echo -n > $STATS
echo -n > $RESULTS_LIMITS
echo -n > $STATS_LIMITS

for PLATFORM in x86-64 x86-32; do
for version in poisNA; do
#for version in cas poisNA pthread; do
#for version in poisNA pthread; do
# for BIN in asynchPC; synchPC; do

# No Statistics
#for rep in 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27 28 29 30; do
#    BIN=$BIN_DIR/$PLATFORM/$NOSTATNOLIMIT_DIR/synchPC_${version}_$PLATFORM
#    ./$BIN 10000 10000 6 >> $RESULTS
#done
#for rep in 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27 28 29 30; do
#    BIN=$BIN_DIR/$PLATFORM/$NOSTATNOLIMIT_DIR/asynchPC_${version}_$PLATFORM
#    ./$BIN 10000 10000 6 1 1 0 0 >> $RESULTS
#done
#for rep in 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27 28 29 30; do
#    BIN=$BIN_DIR/$PLATFORM/$NOSTATNOLIMIT_DIR/semisynchPC_${version}_$PLATFORM
#    ./$BIN 10000 10000 6 1 1 0 126 0 >> $RESULTS
#done


# Statistics
for rep in 1 2 3 4 5 6 7 8 9 10; do
    BIN=$BIN_DIR/$PLATFORM/$STAT_DIR/synchPC_${version}_${PLATFORM}_stats
    ./$BIN 10000 10000 6 >> $STATS
done
for rep in 1 2 3 4 5 6 7 8 9 10; do
    BIN=$BIN_DIR/$PLATFORM/$STAT_DIR/asynchPC_${version}_${PLATFORM}_stats
    ./$BIN 10000 10000 6 1 1 0 0 >> $STATS
done
for rep in 1 2 3 4 5 6 7 8 9 10; do
    BIN=$BIN_DIR/$PLATFORM/$STAT_DIR/semisynchPC_${version}_${PLATFORM}_stats
    ./$BIN 10000 10000 6 1 1 0 126 0 >> $STATS
done



# done
done #version
done #PLATFORM

///-*-C++-*-//////////////////////////////////////////////////////////////////
//
// OpenMalloc Benchmark #1: Simple Producer Consumer Model
//
//
//////////////////////////////////////////////////////////////////////////////


/**
 * @file simple.cpp
 *
 * Simple.cpp creates two threads, in each iteration, one thread mallocs X 
 * objects of size N, and the second thread frees those same X objects.
 * The goal of this is to make it so that the memory allocator cannot rely
 * exclusively on thread local storage (and thus use not synchronization primitives)
 *
*/

#ifndef _REENTRANT
#define _REENTRANT
#endif

#include <iostream>
#include <thread>
#include <chrono>
#include <new>

using namespace std;
using namespace std::chrono;

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sched.h>
#include <sys/sysinfo.h> // Added for CPU Count
#include "COMPASMalloc.h"


int niterations = 50;	// Default number of iterations.
int nobjects = 30000;  // Default number of objects.
int size = 1;

/*
void * operator new (size_t size)
{
  printf("[DEBUG] new Call in ProdCons of size: %u\n", size);
  if (size == 512)
    {
  void *p = COMPASMalloc::_malloc(size);
  
  if (p == NULL)
    {
      printf("[FATAL] _malloc returned a NULL pointer\n");
      exit(1);
    }
  return p;
    }
  else
    {
      return malloc(size);
    }
}

void operator delete (void *p)
{
  COMPASMalloc::_free(p);
}
*/

class Foo {
public:
  Foo (void)
    : x (21),
      y (87)
    {}

  int x;
  int y;
};


static int cpu_to_assign = 0;
static Foo **a; // static array to be passed back and forth between producer and consumer
static pthread_barrier_t barrier1, barrier2; // we need two barriers in order for this benchmark to work properly

/**
 * assignThreadToCPU takes in two arguments: thread_id, and cpu_id
 *
 * cpu_id should be > 0. Any values greater than number of configured processors will be modded down
 * thread_id can be either 0 (for current thread), or > 0 for another (currently running) thread
 */
int assignThreadToCPU(pthread_t thread_id, int cpu_id)
{
	int cpuMap[32] = {0, 2, 4, 6, 8, 10, 12, 14, 1, 3, 5, 7, 9, 11, 13, 15, 16, 18, 20, 22, 24, 26, 28, 30, 17, 19, 21, 23, 25, 27, 29, 31};
	// Probably a good idea to sanitize the inputs
	if(cpu_id < 0) // check if less than 0
	{
		printf("Error: Improper CPU id (< 0)\n");
		return -1;
	}
	// if this is greater than the maximum number of processors this system is CONFIGURED for,
	// (since there can be offline processors)
	// then we simply do a modulo operation instead of returning error, in effect 
	// performing a round-robin assignment
	if(cpu_id >= sysconf(_SC_NPROCESSORS_CONF)) 
	{
		cpu_id = cpu_id % sysconf(_SC_NPROCESSORS_CONF); // ensures cpu_id in [0, max-1]
	}
	// index into our array
	cpu_id = cpuMap[cpu_id];

	pthread_t thread_to_set = thread_id;
	// If thread_id == 0, then simply set the affinity of the current thread to desired CPU
	if(thread_id == 0)
	{
		thread_to_set = pthread_self(); // this is the tid of current thread
	}
	
	fprintf(stderr, "Setting Thread #%lu to CPU #%d\n", thread_to_set, cpu_id);

	// fetch our cpu_set_t var (for the given CPU)
	cpu_set_t cpu_set;
	CPU_ZERO(&cpu_set);
	CPU_SET(cpu_id, &cpu_set);
	
	// and finally set the thread affinity
	int return_value = pthread_setaffinity_np(thread_to_set, sizeof(cpu_set_t), &cpu_set);
	
	// make sure we didnt goof along the way 
	if(return_value < 0)
	{
		printf("Error: Setting thread affinity failed\n");
		return -1;
	}
	return 0;
}

void producer()
{
	assignThreadToCPU(0, 0); // assign producer thread to core 0
	int outer, inner;
	a = new Foo * [nobjects];
	for(outer = 0; outer < niterations; ++outer)
	{
	  //	   printf("_malloc Iteration: %d\n", outer);
		for(inner = 0; inner < nobjects; ++inner)
		{
		  // printf("$[simple_affinity]$ malloc() *START* <tid:%lu>\n", pthread_self());

		  // printf("[DEBUG] Addr received from _malloc: %p {ProdCons}\n", addr);
		  a[inner] = (Foo *) COMPASMalloc::_malloc(sizeof(Foo) * size);		  
		  new (a[inner]) Foo[size];
			assert (a[inner]);
		}
		// now we wait at the first barrier 
		// this kicks off the consumer
		pthread_barrier_wait(&barrier1);
		// and then wait for the consumer to finish
		pthread_barrier_wait(&barrier2);
	}
}

void consumer()
{
	assignThreadToCPU(0, 1); // assign consumer thread to core 2
	int outer, inner;
	// a is explicitly constructed in producer... this just overwrites it. BAD!
	// 	a = new Foo * [nobjects];
	for(outer = 0; outer < niterations; ++outer)
	{
		// now we wait at the first barrier 
		// for the producer to finish
	  pthread_barrier_wait(&barrier1);
	  // printf("_free Iteration: %d\n", outer);

		for(inner = 0; inner < nobjects; ++inner)
		{
		  // printf("$[simple_affinity]$ free() *START* <tid:%lu>\n", pthread_self());
		  COMPASMalloc::_free(a[inner]);
		  //delete[] a[inner];
			// printf("$[simple_affinity]$ free() *END* <tid:%lu>\n", pthread_self());
		}
		// consumer is finished, kick off the producer
		pthread_barrier_wait(&barrier2);
	}
	// and finally delete the entire array
	delete [] a;
}


int main (int argc, char * argv[])
{
  thread ** threads;
  

  if (argc >= 2) {
    niterations = atoi(argv[1]);
  }

  if (argc >= 3) {
    nobjects = atoi(argv[2]);
  }

  if (argc >= 4) {
    size = atoi(argv[3]);
  }

  fprintf (stderr, "[INFO] Running simple producer consumer for 2 threads, %d iterations, %d objects, and %d size...\n", niterations, nobjects, size);

  // initialize our barriers
  pthread_barrier_init(&barrier1, NULL, 2); // takes two threads to trip this barrier

  pthread_barrier_init(&barrier2, NULL, 2); // takes two threads to trip this barrier

  threads = new thread*[2];

  high_resolution_clock t;
  auto start = t.now();

  int i;
  threads[0] = new thread(producer);
  threads[1] = new thread(consumer);

  for (i = 0; i < 2; i++) {
    threads[i]->join();
  }

  auto stop = t.now();
  auto elapsed = duration_cast<duration<double>>(stop - start);

  cout << "Time elapsed = " << elapsed.count() << endl;

  COMPASMalloc::_printStats();

  delete [] threads;

  return 0;
}

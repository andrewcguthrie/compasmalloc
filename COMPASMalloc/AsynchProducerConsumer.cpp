///-*-C++-*-//////////////////////////////////////////////////////////////////
//
// OpenMalloc Benchmark #3: Asynchronous Producer Consumer Model
//
//
//////////////////////////////////////////////////////////////////////////////


/**
 * @file AsynchProducerConsumer.cpp
 *
 * AsynchProducerConsumer.cpp creates two threads, in each iteration, one thread mallocs X 
 * objects of size N, and the second thread frees those same X objects.
 * The goal of this is to induce contention in the internal data structures 
 * of the malloc algorithm under testing.
 *
*/

#ifndef _REENTRANT
#define _REENTRANT
#endif

#include <iostream>
#include <thread>
#include <chrono>
#include <new>


#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sched.h>
#include <sys/sysinfo.h> // Added for CPU Count

#include "ReaderWriterQueue.h"
// #include <boost/lockfree/queue.hpp> // Added for Lock-Free Queue
#include "COMPASMalloc.h"

using namespace std;
using namespace std::chrono;
// using namespace boost::lockfree;
using namespace moodycamel;


int nwork = 0;
int niterations = 50;	// Default number of iterations.
int nobjects = 30000;  // Default number of objects.
int size = 1;
int nproducers = 1;
int nconsumers = 1;
int ndelay = 0; // delay in microseconds between producer and consumer init

class Foo {
public:
  Foo (void)
    : x (21),
      y (87)
    {}

  int x;
  int y;
};

typedef struct Block {
  Foo *data;
  Block *prev;
} Block;


#define WORK_MODULO 5  // Every work iteration, we add between 0 and 5-1 to Foo->x
static int cpu_to_assign = 0;
static int workSum = 0;
static ReaderWriterQueue<Foo *> fooQueue(1024*1024);



/**
 * assignThreadToCPU takes in two arguments: thread_id, and cpu_id
 *
 * cpu_id should be > 0. Any values greater than number of configured processors will be modded down
 * thread_id can be either 0 (for current thread), or > 0 for another (currently running) thread
 */
int assignThreadToCPU(pthread_t thread_id, int cpu_id)
{
	int cpuMap[32] = {0, 2, 4, 6, 8, 10, 12, 14, 1, 3, 5, 7, 9, 11, 13, 15, 16, 18, 20, 22, 24, 26, 28, 30, 17, 19, 21, 23, 25, 27, 29, 31};
	// Probably a good idea to sanitize the inputs
	if(cpu_id < 0) // check if less than 0
	{
		printf("Error: Improper CPU id (< 0)\n");
		return -1;
	}
	// if this is greater than the maximum number of processors this system is CONFIGURED for,
	// (since there can be offline processors)
	// then we simply do a modulo operation instead of returning error, in effect 
	// performing a round-robin assignment
	if(cpu_id >= sysconf(_SC_NPROCESSORS_CONF)) 
	{
		cpu_id = cpu_id % sysconf(_SC_NPROCESSORS_CONF); // ensures cpu_id in [0, max-1]
	}
	// index into our array
	cpu_id = cpuMap[cpu_id];

	pthread_t thread_to_set = thread_id;
	// If thread_id == 0, then simply set the affinity of the current thread to desired CPU
	if(thread_id == 0)
	{
		thread_to_set = pthread_self(); // this is the tid of current thread
	}
	
	fprintf(stderr, "Setting Thread #%lu to CPU #%d\n", thread_to_set, cpu_id);

	// fetch our cpu_set_t var (for the given CPU)
	cpu_set_t cpu_set;
	CPU_ZERO(&cpu_set);
	CPU_SET(cpu_id, &cpu_set);
	
	// and finally set the thread affinity
	int return_value = pthread_setaffinity_np(thread_to_set, sizeof(cpu_set_t), &cpu_set);
	
	// make sure we didnt goof along the way 
	if(return_value < 0)
	{
		printf("Error: Setting thread affinity failed\n");
		return -1;
	}
	return 0;
}


void producer()
{
	assignThreadToCPU(0, cpu_to_assign++); // assign producer thread
	int outer, inner, workOuter, workInner;
	for(outer = 0; outer < niterations; ++outer)
	{
	  // printf("_malloc Iteration: %d\n", outer);
		for(inner = 0; inner < nobjects / nproducers; ++inner)
		{
			Foo *a = (Foo *) COMPASMalloc::_malloc(sizeof(Foo) * size);		  
			new (a) Foo[size];
			assert (a);
			for(workOuter = 0; workOuter < nwork; ++workOuter)
			  {
			    for(workInner = 0; workInner < size; ++workInner)
			      {
				a[workInner].x += (a[workInner].x + a[workInner].y) % WORK_MODULO;
			      }
			  }

			while (! fooQueue.try_enqueue(a))
			{
			  // printf("[AsynchProducerConsumer] push operation failed on outer:%d inner:%d\n", outer, inner);
			}
		}
	}
}

void consumer()
{
	assignThreadToCPU(0, cpu_to_assign++); // assign consumer thread
	int outer, inner, workInner;
	for(outer = 0; outer < niterations; ++outer)
	{
	  //  printf("_free Iteration: %d\n", outer);
		for(inner = 0; inner < nobjects / nconsumers; ++inner)
		{
			Foo *a;
			while (!fooQueue.try_dequeue(a))
			{
			  //	   printf("[AsynchProducerConsumer] pop operation failed on outer:%d inner:%d\n", outer, inner);
			}
			if (nwork > 0)
			  {
			    for(workInner = 0; workInner < size; ++workInner)
			      {
				workSum += a[workInner].x;
			      }
			  }

				COMPASMalloc::_free(a);
		}
	}
	// and finally delete the entire queue
	//	delete &fooQueue;
}


int main (int argc, char * argv[])
{
  thread ** producers;
  thread ** consumers;

  if (argc >= 2) {
    niterations = atoi(argv[1]);
  }

  if (argc >= 3) {
    nobjects = atoi(argv[2]);
  }

  if (argc >= 4) {
    size = atoi(argv[3]);
  }

  if (argc >= 5) {
    nproducers = atoi(argv[4]);
  }

  if (argc >= 6) {
    nconsumers = atoi(argv[5]);
  }

if (argc >= 7) {
	ndelay = atoi(argv[6]);
}


 if (argc >= 8) {
   nwork = atoi(argv[7]);
 }


 fprintf (stderr, "[INFO] Running asynchronous producer consumer for %d producers, %d consumers, %d iterations, %d objects, %d size and %d work loops  with %dms delay...\n", nproducers, nconsumers, niterations, nobjects, size, nwork, ndelay/1000);
  producers = new thread*[nproducers];
  consumers = new thread*[nconsumers];

  high_resolution_clock t;
  auto start = t.now();

  int i;
  for (i = 0; i < nproducers; ++i)
    {
      producers[i] = new thread(producer);
    }
  // sleeping 1ms so producers can have output ready for consumers
  // usleep(2500);
  //  if (ndelay > 0)
  // {
      usleep(ndelay);
      // }
  
  for (i = 0; i < nconsumers; ++i)
    {
      consumers[i] = new thread(consumer);
    }
  // threads[0] = new thread(producer);
  // threads[1] = new thread(consumer);

  for (i = 0; i < nproducers; ++i) {
    producers[i]->join();
  }
  for (i = 0; i < nconsumers; ++i) {
    consumers[i]->join();
  }

  auto stop = t.now();
  auto elapsed = duration_cast<duration<double>>(stop - start);

  cerr << "Time elapsed = " << elapsed.count() << endl;
  cout << "Time elapsed = " << elapsed.count() << endl;

  if (nwork > 0)
    {
      cerr << "[INFO] Work Sum = " << workSum << endl;
    }
  cout << "Work Done: " << nwork << endl;
  cout << argv[0] << ", " << argv[1] << ", " << argv[2] << ", " << argv[3] << ", " << argv[4] << ", " << argv[5] << ", " << argv[6] << ", " << argv[7] << endl;

  COMPASMalloc::_printStats();

  cout << "BENCHMARK END ------------------------------" << endl;

  delete [] producers;
  delete [] consumers;

  return 0;
}


#ifndef CM_NOSYNCH_OPS_H_
#define CM_NOSYNCH_OPS_H_

#define EXTRA_BLOCK_SPACE (0)

inline BlockMetaData *getMetaDataFromBlock(BLKPtr block, size_t objSize) {
    return (BlockMetaData *) block;
}

inline BLKPtr getBlockFromMetaData(BlockMetaData *md, size_t objSize) {
    return (BLKPtr) md;
}


////////////////////////////////////////
// operations for malloc
////////////////////////////////////////

#define _CM_OPS_MALLOC_POISON   /*nothing*/


////////////////////////////////////////
// operations for remote freeing
////////////////////////////////////////

inline void remoteFree(BLKPtr block, SBMetaData *sbmd) {
    BLKPtr rl = sbmd->md.remoteList.next;
    getMetaDataFromBlock(block, sbmd->md.objSize)->next = rl;
    sbmd->md.remoteList.next = block;
}

////////////////////////////////////////
// operations for collecting remote
// lists
////////////////////////////////////////

inline BLKPtr swapAndConditionRemoteList(SBMetaData *sbmd) {

    // check the swap the list head
    BLKPtr newList = sbmd->md.remoteList.next;
    if (newList != NULL) {
        sbmd->md.remoteList.next = NULL;
    }

    return newList;
}


////////////////////////////////////////
// misc. operations
////////////////////////////////////////

#define _CM_OPS_INIT_SB \
    sbmd->md.remoteList.next = NULL;


#endif /* CM_NOSYNCH_OPS_H_ */

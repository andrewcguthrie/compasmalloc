#ifndef CM_POIS_NONATOMIC_DEFS_H_
#define CM_POIS_NONATOMIC_DEFS_H_

/**
 * @guard: a valid guard is always an even number; 1 is the poisoned value
 */
typedef struct BlockMetaData {
    unsigned long guard;
    void *next;
} BlockMetaData;

#endif /* CM_POIS_NONATOMIC_DEFS_H_ */

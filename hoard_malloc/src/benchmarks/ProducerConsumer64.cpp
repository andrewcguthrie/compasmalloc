// Android-related headers
#include <jni.h>
#include <android/log.h>
///-*-C++-*-//////////////////////////////////////////////////////////////////
//
// OpenMalloc Benchmark #1: Simple Producer Consumer Model
//
//
//////////////////////////////////////////////////////////////////////////////


/**
 * @file ProducerConsumer.cpp
 *
 * ProducerConsumer.cpp creates two threads, in each iteration, one thread mallocs X
 * objects of size N, and the second thread frees those same X objects.
 * The goal of this is to make it so that the memory allocator cannot rely
 * exclusively on thread local storage (and thus be forced to use synchronization primitives)
 *
 */

#ifndef _REENTRANT
#define _REENTRANT
#endif

#include <pthread.h>
#include <time.h>
#include <errno.h>

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sched.h>
#include <sys/sysinfo.h> // Added for CPU Count
#include <sys/syscall.h> // Added for syscall(...)

namespace Hoard {

extern "C" {

  void * xxmalloc (size_t sz);

  void xxfree (void * ptr);

}

}

/*
#define CPU_SETSIZE 1024
#define __NCPUBITS  (8 * sizeof (unsigned long))
typedef struct
{
   unsigned long __bits[CPU_SETSIZE / __NCPUBITS];
} cpu_set_t;

#define CPU_SET(cpu, cpusetp) \
   ((cpusetp)->__bits[(cpu)/__NCPUBITS] |= (1UL << ((cpu) % __NCPUBITS)))
#define CPU_ZERO(cpusetp) \
  memset((cpusetp), 0, sizeof(cpu_set_t))
*/

int niterations = 50;	// Default number of iterations.
int nobjects = 30000;  // Default number of objects.
int size = 1;

static int bigProcessor;

class Foo {
public:
  Foo (void)
    : x (21),
      y (87)
    {}

  int x;
  int y;
};

// Replace chrono::highResolutionTimer with time.h functionality
static double getTimeOfDay_ms(void) {

    struct timespec res;
    clock_gettime(CLOCK_REALTIME, &res);
    return 1000.0 * res.tv_sec + (double) res.tv_nsec / 1e6;

}

// Workaround to achieve barrier functionality
static pthread_mutex_t	lock;
// This is the boolean we write to in the Producer thread
static volatile bool isLockedProducer;
// This is the boolean we write to in the Consumer thread
static volatile bool isLockedConsumer;

static Foo **a; // static array to be passed back and forth between producer and consumer

struct timespec tim, tim2;


/**
 * assignThreadToCPU takes in two arguments: thread_id, and cpu_id
 *
 * cpu_id should be > 0. Any values greater than number of configured processors will be modded down
 * thread_id can be either 0 (for current thread), or > 0 for another (currently running) thread
 */
int assignThreadToCPU(pthread_t thread_id, int cpu_id)
{
	int cpuMap[32] = {0, 1};

	if (bigProcessor) {
	  cpuMap[0] = 4;
	  cpuMap[1] = 5;
	}


	// Probably a good idea to sanitize the inputs
	if(cpu_id < 0) // check if less than 0
	{
		printf("[ProducerConsumer] Error: Improper CPU id (< 0)\n");
		// __android_log_print(ANDROID_LOG_DEBUG, "[ProducerConsumer]", "Error: Improper CPU id (< 0)\n");
		return -1;
	}
	// if this is greater than the maximum number of processors this system is CONFIGURED for,
	// (since there can be offline processors)
	// then we simply do a modulo operation instead of returning error, in effect
	// performing a round-robin assignment
	if(cpu_id >= sysconf(_SC_NPROCESSORS_CONF))
	{
		cpu_id = cpu_id % sysconf(_SC_NPROCESSORS_CONF); // ensures cpu_id in [0, max-1]
	}
	// index into our array
	cpu_id = cpuMap[cpu_id];

	pthread_t thread_to_set = thread_id;
	// If thread_id == 0, then simply set the affinity of the current thread to desired CPU
	if(thread_id == 0)
	{
		thread_to_set = pthread_self(); // this is the tid of current thread
	}


	// fetch our cpu_set_t var (for the given CPU)
	cpu_set_t cpu_set_var;
	CPU_ZERO(&cpu_set_var);
	CPU_SET(cpu_id, &cpu_set_var);

	// For some reason, this is mapping to {1, 2} instead of {0, 1}
// 	cpu_set_var.__bits = cpu_set_var.__bits - 1;


	printf("[ProducerConsumer] Setting Thread #%lu to CPU #%d\n", thread_to_set, cpu_id);


	// and finally set the thread affinity
	// int return_value = pthread_setaffinity_np(thread_to_set, sizeof(cpu_set_t), &cpu_set);
	// pass in 0 to explicitly have this refer to the caller thread
	int return_value = syscall(__NR_sched_setaffinity, 0, sizeof(cpu_set_t), &cpu_set_var);

	// make sure we didnt goof along the way
	if(return_value < 0)
	{
		printf("[ProducerConsumer] Error: Setting thread affinity failed: %s\n", strerror(errno));
		// __android_log_print(ANDROID_LOG_DEBUG, "[ProducerConsumer]", "Error: Setting thread affinity failed\n");
		return -1;
	}
	return 0;
}

void * producer(void * in)
{
	assignThreadToCPU(0, 0); // assign producer thread to core 0
	int outer, inner;
	a = new Foo * [nobjects];
	for(outer = 0; outer < niterations; ++outer)
	{
		// Using locking to achieve barrier functionality
		while (isLockedConsumer)
		{
			// Spin until lock is released
			int temp = 0;
		}
		pthread_mutex_lock(&lock);
		// printf("Producer is setting lock\n");
		isLockedProducer = true;
		pthread_mutex_unlock(&lock);

		for(inner = 0; inner < nobjects; ++inner)
		{
			a[inner] = (Foo *) Hoard::xxmalloc(size);
			// new (a[inner]) Foo[size];
			// a[inner] = new Foo[size];
			assert (a[inner]);
		}


		// release the lock for consumer, set for producer
		pthread_mutex_lock(&lock);
		isLockedProducer = false;
		isLockedConsumer = true;
		pthread_mutex_unlock(&lock);

	}
	return NULL;
}

void * consumer(void * in)
{

	assignThreadToCPU(0, 1); // assign consumer thread to core 2
	int outer, inner;
	a = new Foo * [nobjects];
	for(outer = 0; outer < niterations; ++outer)
	{
		while (isLockedProducer)
		{
			// Spin until lock is released
			int temp = 0;
		}
		pthread_mutex_lock(&lock);
		// printf("Consumer is setting lock\n");
		isLockedConsumer = true;
		pthread_mutex_unlock(&lock);

		for(inner = 0; inner < nobjects; ++inner)
		{
			Hoard::xxfree(((void *) a[inner]));
			//delete[] a[inner];
		}
		// release the lock
		pthread_mutex_lock(&lock);
		isLockedConsumer = false;
		isLockedProducer = true;
		pthread_mutex_unlock(&lock);
	}
	// and finally delete the entire array
	delete [] a;
	return NULL;
}


int main (int argc, char * argv[])
{

  if (argc >= 2) {
    niterations = atoi(argv[1]);
  }

  if (argc >= 3) {
    nobjects = atoi(argv[2]);
  }

  if (argc >= 4) {
    size = atoi(argv[3]);
  }

  if (argc >= 5) {
    if (atoi(argv[4])) {
      bigProcessor = true;
    } else {
      bigProcessor = false;
    }
  } else {
    bigProcessor = false;
  }


  printf("[ProducerConsumer] Running simple producer consumer for 2 threads, %d iterations, %d objects, and %d size... bigProcessor: %u\n", niterations, nobjects, size, bigProcessor);


  // Want to have Producer always start first
  isLockedProducer = true;
  isLockedConsumer = false;

  double start = getTimeOfDay_ms();


  pthread_t producerThread, consumerThread;
  pthread_create(&producerThread, NULL, producer, NULL);
  pthread_create(&consumerThread, NULL, consumer, NULL);

  pthread_join(producerThread, NULL);
  pthread_join(consumerThread, NULL);

  double end = getTimeOfDay_ms();
  double elapsed = (end - start)/1000.0; // elapsed runtime in sec

  printf("[ProducerConsumer] Time elapsed = %f\n", elapsed);

  return 0;
}

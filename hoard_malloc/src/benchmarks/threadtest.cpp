///-*-C++-*-//////////////////////////////////////////////////////////////////
//
// Hoard: A Fast, Scalable, and Memory-Efficient Allocator
//        for Shared-Memory Multiprocessors
// Contact author: Emery Berger, http://www.cs.utexas.edu/users/emery
//
// Copyright (c) 1998-2000, The University of Texas at Austin.
//
// This library is free software; you can redistribute it and/or modify
// it under the terms of the GNU Library General Public License as
// published by the Free Software Foundation, http://www.fsf.org.
//
// This library is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Library General Public License for more details.
//
//////////////////////////////////////////////////////////////////////////////


/**
 * @file threadtest.cpp
 *
 * This program does nothing but generate a number of kernel threads
 * that allocate and free memory, with a variable
 * amount of "work" (i.e. cycle wasting) in between.
*/

#ifndef _REENTRANT
#define _REENTRANT
#endif

#include <iostream>
#include <thread>
#include <chrono>

using namespace std;
using namespace std::chrono;

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/sysinfo.h> // Added for CPU Count
#include <unistd.h>
#include <sched.h>
#define _GNU_SOURCE // Needed for CPU_SET



// #include "COMPASMalloc_noSynch_v2_3.h"

namespace Hoard {

extern "C" {

  void * xxmalloc (size_t sz);

  void xxfree (void * ptr);

}

}


int niterations = 50;	// Default number of iterations.
int nobjects = 30000;  // Default number of objects.
int nthreads = 1;	// Default number of threads.
int work = 0;		// Default number of loop iterations.
int size = 1;


class Foo {
public:
  Foo (void)
    : x (14),
      y (29)
    {}

  int x;
  int y;
};


static int cpu_to_assign = 0;

/**
 * assignThreadToCPU takes in two arguments: thread_id, and cpu_id
 *
 * cpu_id should be > 0. Any values greater than number of configured processors will be modded down
 * thread_id can be either 0 (for current thread), or > 0 for another (currently running) thread
 */
int assignThreadToCPU(pthread_t thread_id, int cpu_id)
{
  int cpuMap[32] = {0, 2, 4, 6, 8, 10, 12, 14, 1, 3, 5, 7, 9, 11, 13, 15, 16, 18, 20, 22, 24, 26, 28, 30, 17, 19, 21, 23, 25, 27, 29, 31};
  // Probably a good idea to sanitize the inputs
  if(cpu_id < 0) // check if less than 0
    {
      printf("Error: Improper CPU id (< 0)\n");
      return -1;
    }
  // if this is greater than the maximum number of processors this system is CONFIGURED for,
  // (since there can be offline processors)
  // then we simply do a modulo operation instead of returning error, in effect
  // performing a round-robin assignment
  if(cpu_id >= sysconf(_SC_NPROCESSORS_CONF))
    {
      cpu_id = cpu_id % sysconf(_SC_NPROCESSORS_CONF); // ensures cpu_id in [0, max-1]
    }
  // index into our array
  cpu_id = cpuMap[cpu_id];

  pthread_t thread_to_set = thread_id;
  // If thread_id == 0, then simply set the affinity of the current thread to desired CPU
  if(thread_id == 0)
    {
      thread_to_set = pthread_self(); // this is the tid of current thread
    }

  printf("Setting Thread #%lu to CPU #%d\n", thread_to_set, cpu_id);

  // fetch our cpu_set_t var (for the given CPU)
  cpu_set_t cpu_set;
  CPU_ZERO(&cpu_set);
  CPU_SET(cpu_id, &cpu_set);

  // and finally set the thread affinity
  int return_value = pthread_setaffinity_np(thread_to_set, sizeof(cpu_set_t), &cpu_set);

  // make sure we didnt goof along the way
  if(return_value < 0)
    {
      printf("Error: Setting thread affinity failed\n");
      return -1;
    }
  return 0;
}


void worker ()
{
  // Assign this thread to a different CPU
    assignThreadToCPU(0, cpu_to_assign++);
  int i, j;
  Foo ** a;
  a = new Foo * [nobjects / nthreads];

  for (j = 0; j < niterations; j++) {

    for (i = 0; i < (nobjects / nthreads); i ++) {
      a[i] = (Foo *) Hoard::xxmalloc(sizeof(Foo) * size);
      new (a[i]) Foo[size];
      // a[inner] = new Foo[size];
      assert (a[i]);
    }
    
    for (i = 0; i < (nobjects / nthreads); i ++) {
      Hoard::xxfree(((void *) a[i]));
    }
  }

  delete [] a;
}

int main (int argc, char * argv[])
{
  thread ** threads;
  
  if (argc >= 2) {
    nthreads = atoi(argv[1]);
  }

  if (argc >= 3) {
    niterations = atoi(argv[2]);
  }

  if (argc >= 4) {
    nobjects = atoi(argv[3]);
  }

  if (argc >= 5) {
    work = atoi(argv[4]);
  }

  if (argc >= 6) {
    size = atoi(argv[5]);
  }

  printf ("Running threadtest for %d threads, %d iterations, %d objects, %d work and %d size...\n", nthreads, niterations, nobjects, work, size);

  threads = new thread*[nthreads];

  high_resolution_clock t;
  auto start = t.now();

  int i;
  for (i = 0; i < nthreads; i++) {
    threads[i] = new thread(worker);
  }

  for (i = 0; i < nthreads; i++) {
    threads[i]->join();
  }

  auto stop = t.now();
  auto elapsed = duration_cast<duration<double>>(stop - start);

  cout << "Time elapsed = " << elapsed.count() << endl;

  delete [] threads;

  return 0;
}

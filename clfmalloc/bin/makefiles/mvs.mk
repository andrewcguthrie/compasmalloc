# Make include file for Microsoft Visual Studio
#
# %I%

# Compiler options 
CC=cl
CCC=cl

CFLAGS=-nologo -c -W3 -DCRTAPI1=_cdecl -DCRTAPI2=_cdecl -D_WIN95 \
       -D_WIN32_WINDOWS=0x400 -D_WIN32_IE=0x300 -DWINVER=0x400 -D_WIN32 -DWIN32 \
       -D_DLL -D_MT \
	$($(GOAL)IFLAGS) $(INCLUDES) $($(GOAL)CFLAGS) $(USERCFLAGS)
CCFLAGS=$(CFLAGS)

prodCFLAGS=-Ox
symCFLAGS=-Ox -Zi
debugCFLAGS=-Zi -DDEBUG

# Assembler Options
AS=ml
ASFLAGS=-nologo -Zm -c -Cp -Gd -coff -DWIN32 $(INCLUDES) $($(GOAL)ASFLAGS) $(USERASFLAGS)

prodASFLAGS=
symASFLAGS=-Zi
debugASFLAGS=-Zdi -DDEBUG

# Archiver Options
AR=Lib
AROPTS=-subsystem:windows $(patsubst %,-def:$(AREXPORT).def,$(AREXPORT))
AOTAROPTS=-subsystem:windows 
RANLIB=

# Linker Options  
LINK=link   
LINKPATH=$(patsubst %,-libpath:%,$(LPATH))
LINKOPTS=-nologo -dll -subsystem:windows -nodefaultlib -incremental:no \
         -map:$(patsubst %$(sosuff),%.map,$@) $(LINKPATH) $(USERLDFLAGS) \
         -out:$@ $($(GOAL)LDFLAGS) $(patsubst %,%.exp,$(LINKEXP)) 
LINKEXTRA=$(patsubst %,%$(arsuff),$(SLINK)) $(patsubst %,%$(arsuff),$(DLINK)) \
	  $(patsubst %,-implib:%.lib,$(IMPORTLIB)) $(patsubst %,-def:%.def,$(IMPORTLIB)) \
          kernel32.lib oldnames.lib msvcrt.lib 


UTILLINKOPTS=-nologo -nodefaultlib -incremental:no \
             -map:$(patsubst %$(exesuff),%.map,$@) $(LINKPATH) $(USERLDFLAGS) \
             -out:$@ $($(GOAL)LDFLAGS)
UTILLINKEXTRA=kernel32.lib oldnames.lib msvcrt.lib

prodLDFLAGS=
symLDFLAGS=-debug
debugLDFLAGS=-debug

# Linking Executables
EXELINK=$(LINK)
EXELINKOPTS=-nologo -subsystem:console $(LINKPATH) -out:$@.exe $($(GOAL)LDFLAGS)
EXELINKEXTRA=$(patsubst %,%$(arsuff),$(SLINK)) $(patsubst %,%$(arsuff),$(DLINK)) \
	kernel32.lib msvcrt.lib


# Resource Compiler Options
RC=rc

# Default Pattern Rules
%.obj: %.cpp
	$(CCC) $(CCFLAGS) -Fo$@ $<
ifdef DEPFILES
	$(CCC) $(CCFLAGS) -E $< | grep -iv 'vc98' | $(PREPRO2DEPFILE)
endif

%.obj: %.c
	$(CC) $(CFLAGS) -Fo$@ $<
ifdef DEPFILES
	$(CC) $(CFLAGS) -E $< | grep -iv 'vc98' | $(PREPRO2DEPFILE)
endif

%.obj: %.pasm
	$(CC) -EP $(INCLUDES) $*.pasm >$*.asm
	$(AS) $(ASFLAGS) -Fo $@ $*.asm
	-rm -f $*.asm

%.obj: %.asm
	$(AS) $(ASFLAGS) -Fo $@ $<

NAMEOUTPUTOBJ=/Fo


# Make Include file for amd64-win64-mvs
#
# %W% %I%

include $(AMINO_ROOT)/bin/makefiles/amd64.mk
include $(AMINO_ROOT)/bin/makefiles/win.mk
include $(AMINO_ROOT)/bin/makefiles/mvs.mk

COMPILER=mvs # Microsoft Visual C++

# Compiler Options
CC=cl
CCC=cl

CFLAGS+=-D_WIN32_WINNT=0x0400 -D_WINSOCKAPI_ \
	-DWIN64 -MD -DX64

# Assembler Options
AS=ml64
ASFLAGS=-nologo -c -Cp -W3 -DWIN32 -DWIN64 -DAMINO_HOST_64BIT \
	-DAMINO_TARGET_64BIT $(INCLUDES) $($(GOAL)ASFLAGS) $(USERASFLAGS)

# Archiver options
AROPTS+=-machine:AMD64

# Linker Options  
LINKOPTS+=-machine:AMD64
LINKEXEOPTS+=-machine:AMD64
LINKEXTRA+=msvcrt.lib
LINKEXEEXTRA+=msvcrt.lib
UTILLINKOPTS+=-machine:AMD64

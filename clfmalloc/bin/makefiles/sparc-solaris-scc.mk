# Make Include file for ppc-aix-vacpp
#
# %W% %I%

include $(AMINO_ROOT)/bin/makefiles/sparc.mk

COMPILER=vacpp

# Compiler Options
CC=c99
CCC=CC

CFLAGS=-D_XOPEN_SOURCE_EXTENDED=1 -D_ALL_SOURCE \
       -Xc -mt -c \
       $($(GOAL)IFLAGS) $(INCLUDES) $($(GOAL)CFLAGS) $(USERCFLAGS)
CCFLAGS=$(CFLAGS)

prodCFLAGS=-O3
symCFLAGS=-g $(prodCFLAGS)
debugCFLAGS=-g -DDEBUG

# Assembler Options
AS=sunas
ASFLAGS= $(USERASFLAGS)
#ASVMXFLAGS=-xarch=v9d ??

prodASFLAGS=
symASFLAGS=
debugASFLAGS=

ASPP=
ASPPFLAGS=
SED=sed
M4=m4
GREP=grep

# Archiver Options
AR=ar
AROPTS=rcv $@ $^
RANLIB=ranlib $@


# Linker Options
SLIBCLEAN=slibclean
LINK=cc
LINKPATH=$(patsubst %,-L%,$(subst :, ,$(LPATH)))
LINKOPTS=$(LINKPATH) -G -dy  \
         -lm $($(GOAL)LDFLAGS) $(USERLDFLAGS) \
         -o $@

LINKEXTRA=$(patsubst %,-l%,$(SLINK))

# Linking executables
EXELINK=$(CCC)
EXELINKOPTS=$(LINKPATH) $($(GOAL)LDFLAGS) -o $@ $(USERLDFLAGS)
EXELINKEXTRA=$(LINKEXTRA)

ifeq ($(GOAL), debug)
USERCFLAGS +=
endif

NAMEOUTPUTOBJ=-o 
ifdef DEBUG_DLL
# if extra stuff is linked into debug version, add it here
#LINKEXTRA+=
#LINKEXTRAWDSTATIC=
else
endif


prodLDFLAGS=
symLDFLAGS=
debugLDFLAGS=


# Default Pattern Rules
%.o: %.cpp
	$(CCC) $(CCFLAGS) -o $@ -c $<
ifdef DEPFILES
	mv $*.u $*$(depsuff)
endif

%.o: %.c
	$(CC) $(CFLAGS) -o $@ -c $<
ifdef DEPFILES
	mv $*.u $*$(depsuff)
endif

%.o: %.dbg
	$(AS) $(ASFLAGS) -o $@ $<

%.dbg: %.i
	$(SED) 's/\!/\#/g' $< > $@

%.i: %.spp
	$(CC) -E $(CFLAGS) $< > $@

%.spp: %.s
	$(ASPP) $(ASPPFLAGS) $< $@

%$(wobjsuff): %.dbgw
	$(AS) $(ASFLAGS) -o $@ $<

%.dbgw: %.iw
	$(SED) 's/\!/\#/g' $< > $@

%.iw: %.sppw
	$(CC) -E $(CFLAGSW) $(CFLAGS) $< > $@

%.sppw: %.s
	$(ASPP) $(ASPPFLAGS) $< $@

%.d: %.cpp
	set -e; $(CC) -MM $(CFLAGS) $< | $(SED) 's/\($*\)\.o[ :]*/\1.o $@ : /g' > $@ ; \
	[ -s $@ ] || rm -f $@


# Cancel Default .s rules
%.o: %.s


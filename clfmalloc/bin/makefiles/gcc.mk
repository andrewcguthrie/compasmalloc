# Make include file for gcc
#
# sccsid 1.10.1.12

# Compiler
COMPILER=gcc

CC=gcc
CCC?=g++
CCVERSION=$(shell $(CC) -v 2>&1 | sed -e '1,3d; s/gcc version \([^ ]\+\).*/\1/')
CFLAGS=-Wall -D_REENTRANT -D_LONG_LONG $($(GOAL)IFLAGS) \
       $(INCLUDES) \
       $($(GOAL)CFLAGS) $(USERCFLAGS) -pthread
ifeq (1,$(shell expr "$(CCVERSION)" \>= "3.4.6"))
CCFLAGS=$(CFLAGS) 
#-fexceptions -fno-threadsafe-statics -Wno-deprecated
else
CCFLAGS=$(CFLAGS) 
#-fexceptions -Wno-deprecated
endif

DEBUGSYMS      ?=-g
OPTFLAG        ?=-O3 -DNDEBUG
prodCFLAGS      =$(OPTFLAG)
symCFLAGS       =$(OPTFLAG) $(DEBUGSYMS)
debugCFLAGS     =$(DEBUGSYMS) -DDEBUG

# Assembler
AS=as
ASFLAGS=$(INCLUDES) $($(GOAL)ASFLAGS) $(USERASFLAGS)


prodASFLAGS =
symASFLAGS  =--gstabs
debugASFLAGS=--gstabs  --defsym DEBUG=1

# M4
M4=m4
M4FLAGS=$(INCLUDES)

# Archiver
AR=ar
AROPTS=rcv $@ $?
AREXTRA=
RANLIB=
AOTAROPTS=$(AROPTS)

# Linker for shared libraries
LINK=$(CC)
LINKPATH=$(patsubst %,-L%,$(subst :, ,$(LPATH)))
LINKOPTS=-shared $(LINKPATH) $($(GOAL)LDFLAGS) $(USERLDFLAGS) -pthread
LINKEXTRA=-Wl,--start-group $(patsubst %,-l%,$(SLINK)) -Wl,--end-group \
	  $(patsubst %,-l%$(J9_VERSION),$(DLINK))

# I'd really like to have a -x to strip out local symbols from the prod builds; but I have to get the VM build
# on board with that idea first.
prodLDFLAGS =-Wl,-S
symLDFLAGS  =-Wl,-S
debugLDFLAGS=

# Linking executables
EXELINK=$(LINK)
EXELINKOPTS=$(LINKPATH) $($(GOAL)LDFLAGS)  -pthread
EXELINKEXTRA=$(LINKEXTRA)


# Default Pattern Rules
%.o: %.cpp
	$(CCC) $(CCFLAGS) -o $@ -c $<

%.o: %.c
	$(CC) $(CFLAGS) -o $@ -c $<

%.o: %.s
	$(AS) $(ASFLAGS) -o $@ $<


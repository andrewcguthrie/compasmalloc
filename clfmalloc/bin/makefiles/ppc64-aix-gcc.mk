# Make Include file for ppc64-aix-gcc
#
# %W% %I%


HOST=PPC64

include $(AMINO_ROOT)/bin/makefiles/nonwin.mk
include $(AMINO_ROOT)/bin/makefiles/gcc.mk



# Compiler Options
CFLAGS+=-D_REENTRANT -DUSING_ANSI -maix64
OPTFLAG=-O1 -mcpu=powerpc

# Assembler Options
AS=as
ASFLAGS=$(USERASFLAGS)

ASPP=aspp
ASPPFLAGS=
SED=sed
M4=m4


# Linker Options
LINKEXTRA+=-lstdc++ -Wl,-z,defs -lm -ldl
# FIXME!!! REMOVE STDC++!!!!

ifdef DEBUG_DLL
#LINKEXTRA+=
else
endif

# Default Pattern Rules
%.o: %.dbg
	$(AS) $(ASFLAGS) -o $@ $<
%.dbg: %.i
	$(SED) 's/\!/\#/g' $*.i > $*.dbg
%.i: %.s
	$(CPP) -x c $(CFLAGS) $*.s > $*.i

# Cancel Default .s rules
%.o: %.s


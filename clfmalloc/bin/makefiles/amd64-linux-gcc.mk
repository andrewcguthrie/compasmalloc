# Make Include file for amd64-linux64-gcc
#
# %W% %I%

include $(AMINO_ROOT)/bin/makefiles/nonwin.mk
include $(AMINO_ROOT)/bin/makefiles/gcc.mk

HOST=X86_64

COMPILER=gcc

# Compiler Options
CFLAGS+=-m64
OPTFLAG=-O3 -DNDEBUG

# Assembler Options
ASFLAGS += --64 --defsym AMINO_HOST_X86=1 --defsym AMINO_HOST_64BIT=1 --defsym AMINO_TARGET_X86=1 --defsym AMINO_TARGET_64BIT=1

# Linker Options
LINKOPTS += -m64
LINKEXTRA+= -ldl -lm

# Translate pasm file to gas AT&T format
%.s: %.pasm
	cpp -E -P $(INCLUDES) -o $*.asm $*.pasm
	perl $(AMINO_ROOT)/bin/makefiles/masm2gas.pl $(INCLUDES) $*.asm
	-rm $*.asm

# Translate MASM files to gas AT&T format
%.s:%.asm
	perl $(AMINO_ROOT)/bin/makefiles/masm2gas.pl --64 $(INCLUDES) $<

# Make Include file for ia32-win32-mvs
#
# %I%

include $(AMINO_ROOT)/bin/makefiles/ia32.mk
include $(AMINO_ROOT)/bin/makefiles/win.mk
include $(AMINO_ROOT)/bin/makefiles/mvs.mk


COMPILER=mvs # Microsoft Visual C++

CFLAGS+=-D_X86_=1
LINKOPTS+=-machine:i386
LINKEXTRA+=msvcirt.lib msvcirtd.lib
LINKOPTS+=-machine:i386


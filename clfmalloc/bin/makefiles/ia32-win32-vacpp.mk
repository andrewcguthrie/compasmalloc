# Make Include file for ia32-win32-vacpp
#
# %I%

include $(AMINO_ROOT)/bin/makefiles/ia32.mk
include $(AMINO_ROOT)/bin/makefiles/win.mk

COMPILER=vacpp

# Compiler Options
CC=icc
CCC=icc

INCLUDES=$(patsubst %,/I%,$(IPATH))
CFLAGS=-Wuse+ -Wuni+ -Wrea+ -Word+ -c -gx -gi -ft- -q -gm+ -qnortti -DWIN32 \
       -DWIN32_IBMC $($(GOAL)IFLAGS) $(INCLUDES) $($(GOAL)CFLAGS) $(USERCFLAGS)
CCFLAGS=$(CFLAGS)

prodCFLAGS=-o -oi -qalias=ansi -qtune=pentium2
symCFLAGS=-ti $(prodCFLAGS)
debugCFLAGS=-ti -DDEBUG

# Assembler Options
AS=ml
ASFLAGS=/nologo /Zm /c /Cp $(INCLUDES) $($(GOAL)ASFLAGS) $(USERASFLAGS)

prodASFLAGS=
symASFLAGS=/Zdi
debugASFLAGS=/Zdi /DDEBUG

# Archiver Options
AR=-ILib
ifdef AREXPORT 
AROPTS=/NOLOGO /NOBACKUP /NOEXTDICTIONARY /DEF:$(AREXPORT).def
else
AROPTS=/NOLOGO /NOBACKUP /NOEXTDICTIONARY /OUT:$@
endif
AREXTRA=$^
RANLIB=


# Linker Options  
LINK=ILink   
LINKOPTS=/DLL /MAP:$(patsubst %$(sosuff),%.map,$@) /SUBSYSTEM:CONSOLE /NOLOGO \
         /NOEXTDICTIONARY /NOE /OUT:$@ $($(GOAL)LDFLAGS) $(USERLDFLAGS) \
         $(LINKEXP).exp $^
ifdef DEBUG_DLL
#LINKEXTRA_A= stuff
#LINKEXTRA=$(patsubst %,../lib/%,$(LINKEXTRA_A))
else
endif

prodLDFLAGS=
symLDFLAGS=/DEBUG
debugLDFLAGS=/DEBUG

# Default Pattern Rules
.SUFFIXES: .obj .cpp .d .asm .c

%.obj: %.cpp
	$(CCC) $(CCFLAGS) -Fo$@ $<

%.obj: %.c
	$(CC) $(CFLAGS) -Fo$@ $<

%.obj: %.asm
	$(AS) $(ASFLAGS) /Fo $@ $<

%.d: %.cpp
	set -e; $(CC) -MM $(CFLAGS) $< | sed 's/\($*\)\.o[ :]*/\1.o $@ : /g' > $@ ;\
	[ -s $@ ] || rm -f $@


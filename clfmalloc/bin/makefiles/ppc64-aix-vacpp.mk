# Make Include file for ppc-aix-vacpp
#
# %W% %I%
#
HOST=PPC64

COMPILER=vacpp

# Compiler Options
CC=xlc_r
CCC=xlC_r

CFLAGS=-DAIXPPC -DRS6000 -D_XOPEN_SOURCE_EXTENDED=1 -D_ALL_SOURCE \
       -qenum=small -qmbcs -q64 -qlanglvl=extc99 -qtls -qrtti=all -c \
       $($(GOAL)IFLAGS) $(INCLUDES) $($(GOAL)CFLAGS) $(USERCFLAGS) -I/usr/vacpp/include
CCFLAGS=$(CFLAGS)

prodCFLAGS=-O5 -qarch=ppc64 -qinline
symCFLAGS=-g $(prodCFLAGS)
debugCFLAGS=-g -DDEBUG

# Assembler Options
AS=as
ASFLAGS=-mppc $(USERASFLAGS)
#for VMX only, disabled for now
#ASVMXFLAGS=-mPPC970 -poff
#ASVMXFLAGS=-mPWR6 -poff

prodASFLAGS=
symASFLAGS=
debugASFLAGS=

ASPP=aspp
ASPPFLAGS=
SED=sed
M4=m4
GREP=grep

# Archiver Options
AR=ar
AROPTS=-X64 rcv  $@ $^
RANLIB=ranlib $@


# Linker Options
SLIBCLEAN=slibclean
LINK=makeC++SharedLib_r
LINKPATH=$(patsubst %,-L%,$(subst :, ,$(LPATH)))
LINKOPTS=$(LINKPATH) -X64 -p 0 -bloadmap:lmap -brtl -bnoentry \
         -lm $($(GOAL)LDFLAGS) $(USERLDFLAGS) \
         -o $@

LINKEXTRA=$(patsubst %,-l%,$(SLINK))

# Linking executables
EXELINK=$(CCC)
EXELINKOPTS=$(LINKPATH) -q64 $($(GOAL)LDFLAGS) $(USERLDFLAGS)
EXELINKEXTRA=$(LINKEXTRA)

ifeq ($(GOAL), debug)
USERCFLAGS += -qfullpath
endif

NAMEOUTPUTOBJ=-o 
ifdef DEBUG_DLL
# if extra stuff is linked into debug version, add it here
#LINKEXTRA+=
#LINKEXTRAWDSTATIC=
else
endif


prodLDFLAGS= 
symLDFLAGS=
debugLDFLAGS=


# Default Pattern Rules
%.o: %.cpp
	$(CCC) $(CCFLAGS) -o $@ -c $<
ifdef DEPFILES
	mv $*.u $*$(depsuff)
endif

%.o: %.c
	$(CC) $(CFLAGS) -o $@ -c $<
ifdef DEPFILES
	mv $*.u $*$(depsuff)
endif

%.o: %.dbg
	$(AS) $(ASFLAGS) -o $@ $<

%.dbg: %.i
	$(SED) 's/\!/\#/g' $< > $@

%.i: %.spp
	$(CC) -E $(CFLAGS) $< > $@

%.spp: %.s
	$(ASPP) $(ASPPFLAGS) $< $@

%$(wobjsuff): %.dbgw
	$(AS) $(ASFLAGS) -o $@ $<

%.dbgw: %.iw
	$(SED) 's/\!/\#/g' $< > $@

%.iw: %.sppw
	$(CC) -E $(CFLAGSW) $(CFLAGS) $< > $@

%.sppw: %.s
	$(ASPP) $(ASPPFLAGS) $< $@

%.d: %.cpp
	set -e; $(CC) -MM $(CFLAGS) $< | $(SED) 's/\($*\)\.o[ :]*/\1.o $@ : /g' > $@ ; \
	[ -s $@ ] || rm -f $@


# Cancel Default .s rules
%.o: %.s


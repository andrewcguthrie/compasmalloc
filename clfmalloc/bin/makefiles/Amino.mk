##################
# Amino Makefile #
##################

# %W% %I%

ifdef AMINO_USEBINDIR
BASEBINDIR=$(AMINO_ROOT)/jitbin/$(GOAL)/
BINDIR=$(BASEBINDIR)$(MODULE)/
OBJECTS=$(patsubst %,$(BINDIR)%,$(OBJS))
else
OBJECTS=$(OBJS)
BINDIR=./
endif




INCLUDES=$(patsubst %,-I%,$(IPATH))

# Names of archive files
ATOMIC_LIB_AR           =atomic$(arsuff)

# Full paths to archive files
ATOMIC_LIB     =$(AMINO_ROOT)/lib/$(libprefix)$(ATOMIC_LIB_AR)

# set the default goal to be "debug" unless it has already been explicitly
# set to something else
ifeq ($(findstring prod,$(MAKECMDGOALS)),prod)
  GOAL=prod
endif
ifeq ($(findstring sym,$(MAKECMDGOALS)),sym)
  GOAL=sym
endif
ifndef GOAL
  GOAL=debug
endif



# Include the user-specific make include file. This file could define
# variables such as PLATFORM, USERCFLAGS, USERASFLAGS and
# USERLDFLAGS.
include $(AMINO_ROOT)/bin/makeOpts.mk

ifndef PLATFORM
$(warning You should run $(AMINO_ROOT)/select_platform.sh to select right platform.)
$(error   Aborting)
else
include $(AMINO_ROOT)/bin/makefiles/$(PLATFORM).mk
endif



	TARGETS	   =$(HOST)
	BUILDFILES =$($(HOST)FILES) $(depFILES)
	BUILDIFLAGS=$(HOSTFLAGS)

ifdef ASSUMES
	CFLAGS+=-DPROD_WITH_ASSUMES
endif




# what files to build for each build goal
#
prodIFLAGS=$(BUILDIFLAGS)
prodFILES=$(BUILDFILES)
symIFLAGS=$(prodIFLAGS)
symFILES=$(prodFILES)
debugIFLAGS=$(prodIFLAGS)
debugFILES=$(prodFILES)

# Make Include file for ia32-win32-mvs7
#
# %I%

include $(AMINO_ROOT)/bin/makefiles/ia32.mk
include $(AMINO_ROOT)/bin/makefiles/win.mk
include $(AMINO_ROOT)/bin/makefiles/mvs.mk


COMPILER=mvs7 # Microsoft Visual C++ 7.1

CFLAGS+=-wd4291 -Zm200 -D_X86_=1
LINKOPTS+=-machine:i386
LINKEXTRA+=msvcprt.lib
UTILLINKOPTS+=-machine:i386
UTILLINKEXTRA+=msvpcrt.lib

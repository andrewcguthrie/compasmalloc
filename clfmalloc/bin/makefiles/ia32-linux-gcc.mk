# Make Include file for ia32-linux-gcc
#
# %W% %I%

include $(AMINO_ROOT)/bin/makefiles/gcc.mk
include $(AMINO_ROOT)/bin/makefiles/nonwin.mk

HOST=X86_32

COMPILER=gcc

# Compiler Options
CFLAGS+= -D_REENTRANT -DUSING_ANSI
OPTFLAG=-march=pentium4 -O3 -DNDEBUG

# Assembler Options
AS=as
ASFLAGS=$(USERASFLAGS)

ASPP=aspp
ASPPFLAGS=
SED=sed
M4=m4


# Linker Options
LINKEXTRA+=-lstdc++ -Wl,-z,defs -lm -ldl -lpthread
# FIXME!!! REMOVE STDC++!!!!

ifdef DEBUG_DLL
#LINKEXTRA+=
else
endif

# Default Pattern Rules
%.o: %.dbg
	$(AS) $(ASFLAGS) -o $@ $<
%.dbg: %.i
	$(SED) 's/\!/\#/g' $*.i > $*.dbg
%.i: %.s
	$(CPP) -x assembler $(CFLAGS) $*.s > $*.i

# Cancel Default .s rules
%.o: %.s


/*
 * (c) Copyright 2008, IBM Corporation.
 *
 *	Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/*********************************************************************
 * Contact: Maged Michael <magedm@us.ibm.com>
 * Authors: Maged Michael, Michael Spear
 *********************************************************************/
#ifndef CSTM_C
#define CSTM_C
#include <unistd.h> /* eliminated getopt problem on aix */
#include <stdlib.h>
#include <stdio.h>
#include <assert.h>
#include <string.h>
#include <sys/mman.h>
#include <sys/time.h>
#include <setjmp.h>
#include <signal.h>

#include <amino/config.h>

#include "atomic.h"
#include "stm.h"
/*-------------------------------------------------------------------*/
#if defined(STATS)
#include "stats.h"
#endif /* STATS */
/*-------------------------------------------------------------------*/
#define false	0
#define true	1
/*-------------------------------------------------------------------*/
#define DEBUG_PRINT	if (debug) printf
#ifdef DEBUG
void print_debug() {}
#define ASSERT(x)	if (!(x)) { print_debug(); assert(x);}
#else
#define ASSERT(x)	assert(x)
#endif
/*--------------------------------------------------------------------*/
#define MMAP(size) \
	mmap(0,size,PROT_READ|PROT_WRITE,MAP_ANON|MAP_PRIVATE,-1,0)
/*--------------------------------------------------------------------*/
/*--------------------------------------------------------------------*/
#ifdef STATS
#define STATS_ADD(var,val)	my->stats->stats[my->txn_id][var] += val
#define STATS_MAX(var,val) \
    if (val > my->stats->stats[my->txn_id][var]) \
	my->stats->stats[my->txn_id][var] = val
#else /* !STATS */
#define STATS_ADD(var,val)
#define STATS_MAX(var,val)
#endif /* !STATS */
/*-------------------------------------------------------------------*/
#ifndef PROF_TIME
#define PROF_START_TIME()
#define PROF_END_TIME()
#else /* PROF_TIME */
#define PROF_START_TIME()	prof_start_time(my)
#define PROF_END_TIME()		prof_end_time(my)
#endif /* PROF_TIME */
/*-------------------------------------------------------------------*/
#define DEFAULT_LOG_2_BLOOM_FILTER_BITS	9
#define DEFAULT_LOG_2_NUM_ORECS		20
#define DEFAULT_LOG_2_BLOCK_SIZE	3
/*-------------------------------------------------------------------*/
#define MIN_LOG_2_BLOOM_FILTER_BITS	6
#define MIN_LOG_2_NUM_ORECS		10
#define MIN_LOG_2_BLOCK_SIZE		3
/*-------------------------------------------------------------------*/
#ifndef LOG_2_BLOOM_FILTER_BITS
#define LOG_2_BLOOM_FILTER_BITS	DEFAULT_LOG_2_BLOOM_FILTER_BITS
#elif   LOG_2_BLOOM_FILTER_BITS < MIN_LOG_2_BLOOM_FILTER_BITS
#undef  LOG_2_BLOOM_FILTER_BITS
#define LOG_2_BLOOM_FILTER_BITS	MIN_LOG_2_BLOOM_FILTER_BITS
#endif
/*-------------------------------------------------------------------*/
#ifndef LOG_2_NUM_ORECS
#define LOG_2_NUM_ORECS		DEFAULT_LOG_2_NUM_ORECS
#elif   LOG_2_NUM_ORECS 	< MIN_LOG_2_NUM_ORECS
#undef  LOG_2_NUM_ORECS
#define LOG_2_NUM_ORECS		MIN_LOG_2_NUM_ORECS
#endif
/*-------------------------------------------------------------------*/
#ifndef LOG_2_BLOCK_SIZE
#define LOG_2_BLOCK_SIZE	DEFAULT_LOG_2_BLOCK_SIZE
#elif   LOG_2_BLOCK_SIZE 	< MIN_LOG_2_BLOCK_SIZE
#undef  LOG_2_BLOCK_SIZE
#define LOG_2_BLOCK_SIZE	MIN_LOG_2_BLOCK_SIZE
#endif
/*-------------------------------------------------------------------*/
typedef union orec {
  struct {
#if defined(X86) || defined(X64)
    unsigned locked:1;
    unsigned long version:8*sizeof(void *)-1;
#else /* PPC */
    unsigned long version:8*sizeof(void *)-1;
    unsigned locked:1;
#endif
  } sep;
  void * all;
} orec_t;
/*-------------------------------------------------------------------*/
typedef struct meta_entry {
  volatile orec_t orec;
  orec_t latest;
#ifdef ENCOUNTER_ACQUIRE
  long write_list;
#endif
#if defined(META_ENTRY_SIZE)
  char pad[META_ENTRY_SIZE
	   -2*sizeof(orec_t)
#ifdef ENCOUNTER_ACQUIRE
	   -sizeof(long)
#endif
  ];
#endif /* META_ENTRY_SIZE */
} meta_entry_t;
/*--------------------------------------------------------------------*/
typedef struct read_entry {
  meta_entry_t * pmeta;
  orec_t orec;
#ifdef STATS
  void * addr;
#endif /* STATS */
} read_entry_t;
/*-------------------------------------------------------------------*/
typedef union value {
  char charval[sizeof(long long)/sizeof(char)];
  short shortval[sizeof(long long)/sizeof(short)];
  int intval[sizeof(long long)/sizeof(int)];
  long long llval[sizeof(long long)/sizeof(long long)];
} value_t;
/*--------------------------------------------------------------------*/
typedef struct write_entry {
  value_t val;
  void * addr;
  meta_entry_t * pmeta;
  int next;
  char size;
  char locked;
  char pad[8
	   -1*sizeof(int)
	   -2*sizeof(char)
  ];
} write_entry_t;
/*--------------------------------------------------------------------*/
typedef struct checkpoint_entry {
  value_t val;
  void * addr;
  char size;
  char pad[sizeof(long)-1];
} checkpoint_entry_t;
/*--------------------------------------------------------------------*/
/* To prevent livelock, writes are sorted after some # of retries */
#define DEFAULT_RETRY_THRESHOLD	10
#ifndef RETRY_THRESHOLD
#define RETRY_THRESHOLD DEFAULT_RETRY_THRESHOLD
#endif
/*--------------------------------------------------------------------*/
/* For stats: max number of distinct static transactions */
/* if too low, stats of excess txns are lumped together */
#define DEFAULT_MAX_TXNS	64
#ifndef MAX_TXNS
#define MAX_TXNS	DEFAULT_MAX_TXNS
#endif
/*--------------------------------------------------------------------*/
#ifdef STATS
/*--------------------------------------------------------------------*/
typedef struct thr_stats {
  unsigned int read_set;
  unsigned int read_list;
  unsigned int stats[MAX_TXNS][NUM_STATS];
} thr_stats_t;
/*--------------------------------------------------------------------*/
#define FNAME_LEN	59
typedef struct {
  char fname[FNAME_LEN+1];
  int line;
} txn_src_t;
/*--------------------------------------------------------------------*/
#endif /* STATS */
/*-------------------------------------------------------------------*/
#define TX_INACTIVE		0
#define TX_ACTIVE		1
#define TX_ABORTED		2
#define TX_DOOMED		3
#define TX_COMMITTED		4
/*-------------------------------------------------------------------*/
#define RETRY_EX		1
/*-------------------------------------------------------------------*/
#define INITIAL_READ_LIST	64
#define INITIAL_WRITE_LIST	16
#define INITIAL_MALLOC_LIST	16
#define INITIAL_FREE_LIST	16
#define INITIAL_CHECKPOINT_LIST	16
/*-------------------------------------------------------------------*/
#define BLOOM_FILTER_BITS	(1<<LOG_2_BLOOM_FILTER_BITS)
#define BLOOM_FILTER_MASK	(BLOOM_FILTER_BITS-1)
/*-------------------------------------------------------------------*/
#define WRITE_TO_NEW_OREC	0
#define WRITE_TO_OLD_OREC	1
#define WRITE_TO_OLD_BUFFER	2
#define WRITE_TO_MEMORY		3
#define WRITE_CONFLICT		4
/*-------------------------------------------------------------------*/
#define END_OF_LIST		-1
/*-------------------------------------------------------------------*/
typedef struct desc {
  unsigned volatile status; /* must be first word */
  unsigned nesting;
  unsigned num_reads;
  unsigned read_list_size;
  unsigned num_writes;
  unsigned write_list_size; 
  unsigned retried;
#ifdef GLOBAL_VERSION
  long last_valid_version;
#endif
  read_entry_t * reads;
  write_entry_t * writes;
  read_entry_t * next_read_entry;
  write_entry_t * next_write_entry;
  write_entry_t * we_match;

  unsigned num_mallocs;
  unsigned malloc_list_size;
  unsigned num_frees;
  unsigned free_list_size;
  unsigned num_checkpoints;
  unsigned checkpoint_list_size;
#ifndef INC_VALIDATION
  unsigned num_unvalidated; /* not used with INC_VALIDATION */
#endif
  unsigned upad;
  void * stackmin;
  void * stackmax;
  void ** mallocs;
  void ** frees;
  checkpoint_entry_t * checkpoints;
  void * buf; /* setjmp buffer */

#ifdef STATS
  unsigned int txn_id;
  thr_stats_t * stats;
#endif
#ifdef PROF_TIME
  unsigned int start_time;
  unsigned int time_in_transactions;
#endif
#ifndef ENCOUNTER_ACQUIRE
  char bloom_filter[BLOOM_FILTER_BITS/8];
#endif
  struct desc * next_desc;
  struct desc * next_free_desc;
  char pad[124];
} desc_t;
/*-------------------------------------------------------------------*/
#ifndef INLINED
#define extern 
#endif
/*-------------------------------------------------------------------*/
#define STM_NOT_INITIALIZED	0
#define STM_BEING_INITIALIZED	1
#define STM_INITIALIZED		2
/*-------------------------------------------------------------------*/
#ifdef FLEXIBLE
extern meta_entry_t * meta;
extern unsigned lognumorecs;
extern unsigned numorecs;
extern unsigned blockbits;
extern unsigned blockmask;
#else /* !FLEXIBLE */
#define lognumorecs	LOG_2_NUM_ORECS
#define numorecs	(1<<lognumorecs)
#define blockbits	LOG_2_BLOCK_SIZE
#define blockmask	((1<<(lognumorecs+blockbits))-1)
extern meta_entry_t meta[numorecs];
#endif /* !FLEXIBLE */
/*-------------------------------------------------------------------*/
static unsigned debug = 0;
static volatile long stm_initialized = STM_NOT_INITIALIZED;
static volatile long desc_free_list_lock = 0;
static desc_t * desc_free_list = NULL;
static volatile desc_t * desc_list = NULL;
#ifdef GLOBAL_VERSION
static volatile long global_version=0;
#endif
#ifdef STATS
static volatile long stats_version = 1;
static volatile long txn_list_lock = 0;
static int num_txns = 0;
static int more_txns = false;
static txn_src_t txn_src[MAX_TXNS];
#endif
/*-------------------------------------------------------------------*/
NOINLINE desc_t * desc_alloc();
NOINLINE void desc_free(desc_t * desc);
/*-------------------------------------------------------------------*/
/* TLS wrapper to support either gcc __thread or pthread_getspecific */
#ifdef DESC_TLS
#if !defined(AIX_PPC32) && !defined(AIX_PPC64) /* AIX can't use __thread */
#ifndef INLINED
__thread desc_t * tls_my = NULL;
#else /* INLINED */
extern __thread desc_t * tls_my;
#endif /* INLINED */
#define TLS_INIT() { tls_my = NULL; }
#define TLS_SET(x) tls_my = (x)
#define TLS_GET() tls_my
#else /* AIX_PPC32 or AIX_PPC64 is defined, so use pthread_getspecific etc */
#include <pthread.h>
#ifndef INLINED
pthread_key_t tls_key;
#else /* INLINED */
extern pthread_key_t tls_key;
#endif /* INLINED */
#define TLS_INIT() {int result __attribute__((unused)) = pthread_key_create(&tls_key, NULL); ASSERT(result == 0); }
#define TLS_SET(x) pthread_setspecific(tls_key, (void*)(x))
#define TLS_GET() (desc_t *)pthread_getspecific(tls_key)
#endif /* !AIX */
#endif /* DESC_TLS */
/*-------------------------------------------------------------------*/
/*-------------------------------------------------------------------*/
#ifdef PROF_TIME
void prof_start_time(desc_t * my) {
  struct timezone tz;
  struct timeval tv;
  gettimeofday(&tv,&tz);
  my->start_time = tv.tv_sec * 1000000 + tv.tv_usec;
}
void prof_end_time(desc_t * my) {
  struct timezone tz;
  struct timeval tv;
  gettimeofday(&tv,&tz);
  unsigned end_time = tv.tv_sec * 1000000 + tv.tv_usec;
  my->time_in_transactions += end_time - my->start_time;
}
#endif /* PROF_TIME */
/*-------------------------------------------------------------------*/
INLINE void common_cleanup(desc_t * my) {
  my->status = TX_INACTIVE;
  my->nesting = 0;
  my->num_reads = 0;
  my->next_read_entry = &my->reads[0];
  my->next_write_entry = &my->writes[0];

  my->num_checkpoints = 0;
  my->num_mallocs = 0;
  my->num_frees = 0;

#ifdef STATS
  my->stats->read_set = 0;
  my->stats->read_list = 0;
  my->txn_id = 0;
#endif /* STATS */
  if (my->num_writes != 0) {
    my->num_writes = 0;
#ifndef ENCOUNTER_ACQUIRE
    memset((void *)my->bloom_filter,0,BLOOM_FILTER_BITS/8);
#endif  /* !ENCOUNTER_ACQUIRE */
  }
  RESET_VALIDATION;
}
/*-------------------------------------------------------------------*/
NOINLINE void undo_write_locks(desc_t * my) {
  ASSERT(my);
  /* release orecs */
  unsigned numwrites = my->num_writes;
  int i;
  for (i=0;i<numwrites;i++) {
    if (my->writes[i].locked) {
      meta_entry_t * pmeta = my->writes[i].pmeta;
      orec_t orec;
      orec.all = pmeta->latest.all;
      /* no need for incrementing version number */
      pmeta->orec.all = orec.all;
      STATS_ADD(LOCK_UNDO,1);
      my->writes[i].locked = false;
    }
  }
}
/*-------------------------------------------------------------------*/
NOINLINE void wait_for_orec_change(desc_t * my,
				   meta_entry_t volatile * pmeta,
				   void * orecall) {
  undo_write_locks(my);
  while (pmeta->orec.all == orecall);
}
/*-------------------------------------------------------------------*/
static inline void do_write(void * to, void * from, int size);
/*-------------------------------------------------------------------*/
NOINLINE void cleanup_on_abort(desc_t * my) {
  /* undo write locks */
  undo_write_locks(my);
  /* undo mallocs */
  unsigned numallocs = my->num_mallocs;
  if (numallocs > 0) {
    int i;
    for (i=0;i<numallocs;i++) {
      free(my->mallocs[i]);
    }
  }
  /* undo private writes */
  unsigned numcheckpoints = my->num_checkpoints;
  if (numcheckpoints > 0) {
    int i;
    for (i=0;i<numcheckpoints;i++) {
      /* undo_private_write */
      checkpoint_entry_t * ce = &my->checkpoints[i];
      //memcpy(ce->addr,&ce->oldval[0],ce->size);
      do_write(ce->addr,&ce->val,ce->size);
    }
  }
  common_cleanup(my);
  my->retried++;
}
/*-------------------------------------------------------------------*/
#ifndef INLINED
void stm_retry(void * mydesc) {
  desc_t * my = mydesc;
  cleanup_on_abort(my);
  PROF_END_TIME();
  // retry
  LONGJMP(my->buf,RETRY_EX);
}
#endif /* !INLINED */
/*-------------------------------------------------------------------*/
static void stm_conflict(desc_t * my) {
  STM_CONFLICT(my);
}
/*-------------------------------------------------------------------*/
static int full_validation(desc_t * my) {
  EVENT_START(E_STM_VALIDATE);
  RESET_VALIDATION;
#ifdef GLOBAL_VERSION
  my->last_valid_version = global_version;
  LWSYNC; RBR;
#endif

  orec_t my1;
  my1.all = my;
  my1.sep.locked = true;
  /* Validate read set */
  int i;
  int valid = true;
  read_entry_t * re = my->reads;
  for (i=my->num_reads;
       i>0;
       i--,re++) {
    meta_entry_t * pmeta = re->pmeta;
    orec_t orec1,orec2;
    orec1.all = re->orec.all;
    VALIDATE_OREC(&pmeta->orec.all);
    orec2.all = pmeta->orec.all;
    if (orec1.all != orec2.all) {
      if (orec2.all != my1.all) {
	valid = false;
	break;
      } else {
	if (pmeta->latest.all != orec1.all) {
	  valid = false;
	  break;
	}
      }
    }
  }
  if (!valid) {
    STATS_ADD(READ_VALIDATION_RETRIES,1);
    stm_conflict(my);
  }
  EVENT_END(E_STM_VALIDATE);
  return valid;
}
/*-------------------------------------------------------------------*/
#define STM_VALIDATE_READ_SET(my)	stm_validate_read_set(my)
/*-------------------------------------------------------------------*/
static inline int stm_validate_read_set(desc_t * my) {
  STATS_ADD(READ_SET_VALIDATIONS,1);
  if (FAST_VALIDATION) {
    return true;
  }
#ifdef GLOBAL_VERSION
  if (my->last_valid_version == global_version) {
    return true;
  }
#ifdef FIRST_TRY
  if (!my->retried > 0) {
    return false;
  }
#endif
#endif
  int valid = full_validation(my);
  return valid;
}
/*-------------------------------------------------------------------*/
#if defined(SIGNAL)
/*-------------------------------------------------------------------*/
/* 
 * sig_check_consistency() is called whenever we receive a signal
 * during execution.  On a signal, we validate and if the validation
 * fails, then we retry.
*/
static void sig_check_consistency(int sig_num) {
  desc_t * my = TLS_GET();
  if (!STM_VALIDATE_READ_SET(my)) {
    /* printf("signal was due to inconsistency... calling stm_retry()\n") */
    /* clear the signal mask */
    sigset_t set;
    sigemptyset(&set);
    sigprocmask(SIG_SETMASK, &set, NULL);

    /* retry */
    STATS_ADD(SIGNAL_RETRIES,1);
    stm_retry(my);
  }
  else {
    psignal(sig_num,"Received signal");
    fprintf(stderr,
	    "  STM validation passed\n");
    fprintf(stderr,
	    "  Signal is not due to inconsistency in a doomed transaction\n");
    exit(-1);
  }
}
/*-------------------------------------------------------------------*/
/* Even with consistent reads we need to catch faulting loads */
static void initialize_sighandlers()
{
  struct sigaction sa;
  sa.sa_handler = sig_check_consistency;   /* set handler function */
  sigemptyset(&sa.sa_mask);                /* don't mask signals on receipt */
  sa.sa_flags = 0;                         /* no special flags */
  sigaction(SIGSEGV, &sa, NULL);
#ifndef INC_VALIDATION
  sigaction(SIGILL, &sa, NULL);
  sigaction(SIGFPE, &sa, NULL);
#endif /* INC_VALIDATION */
}
#endif /* SIGNAL */
/*-------------------------------------------------------------------*/
#ifndef INC_VALIDATION
#define INF_LOOP_LIMIT 256
#define INF_LOOP_MASK  0xff
NOINLINE void check_inf_loop(desc_t * my) { 
  if (!STM_VALIDATE_READ_SET(my))
    stm_conflict(my);
  my->num_unvalidated = 0;
}
#endif // INC_VALIDATION
/*-------------------------------------------------------------------*/
#ifndef INLINED
/*-------------------------------------------------------------------*/
/* only build stm_init in the library, not in inlined code */
//__attribute__((constructor)) void stm_init() {
void stm_init() {
  /* acquire lock for initialization */
  int ret =
    CAS(&stm_initialized,STM_NOT_INITIALIZED,STM_BEING_INITIALIZED);
  if (!ret) {
    while (stm_initialized != STM_INITIALIZED);
    return;
  }

  /* Initialize once only */
#ifdef PRINT_MESSAGE
  printf("Using STM\n");
#endif
  /*
   * Check that the orec structure is set properly such that the lock
   * bit is the lsb
   */
  orec_t orec;
  orec.all = 0;
  orec.sep.locked = 1;
  if (orec.all != (void *)1) {
    printf("Problem with orec structure. Reverse bit order in orec_t\n");
    assert(orec.all == (void *)1);
  }

  assert(sizeof(value_t) == sizeof(long long));

  ASSERT_META_SIZE;

#ifdef FLEXIBLE
  /* Set number of orecs */
  lognumorecs = LOG_2_NUM_ORECS;
  char * str = getenv("STM_LOG_2_NUM_ORECS");
  if (str) {
    int num = atoi(str);
    if (num < 10) num = 10;
    lognumorecs = num;
  }
  numorecs = (1 << lognumorecs);

  /* Set hashing masks */
  blockbits = LOG_2_BLOCK_SIZE;
  blockmask = (1<<(lognumorecs+blockbits))-1;
  
  /* allocate orecs */
  unsigned size = numorecs * sizeof(meta_entry_t);
  if ((meta = (meta_entry_t *) MMAP(size)) == MAP_FAILED) {
    perror("stm_init mmap failed\n");
  }
  
  /* clear meta */
  memset((void *)meta,0,size);
#endif /* FLEXIBLE */
  
#ifdef DESC_TLS
  /* initialize TLS */
  TLS_INIT();
#endif

#ifdef SIGNAL
  /* initialize signal handlers for detection consistency violations */
  initialize_sighandlers();
#endif /* SIGNAL */

  /* Publish updates before releasing lock */
  LWSYNC; WBW;
 /* announce completion of initialization */
  stm_initialized = STM_INITIALIZED;
  return;
}
#endif /* !INLINED */
/*-------------------------------------------------------------------*/
static void desc_list_insert(desc_t * desc) {
  while (true) {
    desc_t * head = (desc_t *) desc_list;
    desc->next_desc = head;
    LWSYNC; WBW;
    if (CAS(&desc_list,head,desc))
      break;
  }
}
/*-------------------------------------------------------------------*/
static void desc_free_list_insert(desc_t * desc) {
  tts_acq(&desc_free_list_lock);
  desc->next_free_desc = desc_free_list;
  desc_free_list = desc;
  tts_rel(&desc_free_list_lock);
}
/*-------------------------------------------------------------------*/
static desc_t * desc_alloc_from_free_list() {
  tts_acq(&desc_free_list_lock);
  desc_t * head = desc_free_list;
  if (head) {
    desc_free_list = head->next_free_desc;
  }
  tts_rel(&desc_free_list_lock);
  return head;
}
/*-------------------------------------------------------------------*/
NOINLINE desc_t * desc_alloc() {
  desc_t * desc = desc_alloc_from_free_list();
  if (!desc) {
    desc = (desc_t *) malloc(sizeof(desc_t));
    memset((void *)desc,0x4d,sizeof(desc_t));  
    desc->read_list_size = INITIAL_READ_LIST;
    desc->write_list_size = INITIAL_WRITE_LIST;
    desc->reads =
      (read_entry_t *) malloc(INITIAL_READ_LIST*sizeof(read_entry_t));
    desc->writes =
      (write_entry_t *) malloc(INITIAL_WRITE_LIST*sizeof(write_entry_t));

    desc->malloc_list_size = 0;
    desc->mallocs = NULL;
    desc->free_list_size = 0;
    desc->frees = NULL;
    desc->checkpoint_list_size = 0;
    desc->checkpoints = NULL;

#if defined(STATS)
    desc->stats = (thr_stats_t *) malloc(sizeof(thr_stats_t));
    memset((void *)desc->stats,0,sizeof(thr_stats_t));  
#endif
    common_cleanup(desc);
    desc_list_insert(desc);
  }
  desc->stackmin = (void *) ~0;
  desc->stackmax = NULL;
  desc->retried = 0;
  return desc;
}
/*-------------------------------------------------------------------*/
static void desc_free(desc_t * desc) {
  desc_free_list_insert(desc);
}
/*-------------------------------------------------------------------*/
#ifndef INLINED
/*-------------------------------------------------------------------*/
void * stm_thr_init() {
  if (!stm_initialized) {
    stm_init();
  }
#ifndef DESC_TLS
  desc_t * my = NULL;
#else /* DESC_TLS */
  desc_t * my = TLS_GET();
#endif /* DESC_TLS */
  if (my == NULL) {
    my = desc_alloc();
    common_cleanup(my);
#ifdef DESC_TLS
    TLS_SET(my);
#endif /* DESC_TLS */
    // printf("new desc 0x%p\n",my);
  }
  return my;
}
/*-------------------------------------------------------------------*/
#ifndef DESC_TLS
void stm_thr_retire(void * mydesc) {
  desc_t * my = (desc_t *)mydesc;
#else
void stm_thr_retire() {
  desc_t * my = TLS_GET();
#endif
  // printf("retiring desc 0x%p\n",my);
  if (!my) return;
  ASSERT(my->status == TX_INACTIVE);
  desc_free(my);
#ifdef DESC_TLS
  TLS_SET(0);
#endif
}
/*-------------------------------------------------------------------*/
#endif /* !INLINED */
/*-------------------------------------------------------------------*/
NOINLINE void expand_reads(desc_t * my) {
  assert(my->num_reads == my->read_list_size);
  unsigned size = my->read_list_size*sizeof(read_entry_t);
  read_entry_t * list = (read_entry_t *) malloc(size<<1);
  memcpy(list,my->reads,size);
  free(my->reads);
  my->read_list_size <<= 1;
  my->reads = list;
  my->next_read_entry = &list[my->num_reads];
}
/*-------------------------------------------------------------------*/
NOINLINE void expand_writes(desc_t * my) {
  assert(my->num_writes == my->write_list_size);
  unsigned size = my->write_list_size*sizeof(write_entry_t);
  write_entry_t * list = (write_entry_t *) malloc(size<<1);
  memcpy(list,my->writes,size);
  free(my->writes);
  my->write_list_size <<= 1;
  my->writes = list;
  my->next_write_entry = &list[my->num_writes];
}
/*-------------------------------------------------------------------*/
#ifndef FORJAVA
/*-------------------------------------------------------------------*/
INLINE int stm_in_transaction() {
  desc_t * my = TLS_GET();
  if (my && my->status != TX_INACTIVE) {
    STATS_ADD(STATUS_CHECK_ACTIVE,1);
    return true;
  } else {
    STATS_ADD(STATUS_CHECK_INACTIVE,1);
    return false;
  }
}
/*-------------------------------------------------------------------*/
INLINE void stm_stack_range(void * const addr, int size,
			    void * mydesc) {
  desc_t * my = (desc_t *) mydesc;
  if (!my)
    return;
  if (addr < my->stackmin) {
    my->stackmin = addr;
  }
  if (addr+size-1 > my->stackmax) {
    my->stackmax = addr+size-1;
  }
  STATS_ADD(ADDRESS_TAKEN_VARIABLES,1);
}
/*-------------------------------------------------------------------*/
INLINE void stm_check_forxlc() {
#ifndef STMXLC
  printf("ERROR: link compiler-instrumented codes with imcompatible STM lib!\n");
  printf(" HINT: recompile STM library with STMXLC=on\n");
  exit(-1);
#endif
}
/*-------------------------------------------------------------------*/
#endif
/*-------------------------------------------------------------------*/
#ifdef STATS 
static void set_txn_id(desc_t * my,char * fname,int line) {
  int found = false;
  tts_acq(&txn_list_lock);
  int i;
  for (i=0;i<num_txns;i++) {
    if ((strcmp(fname,&txn_src[i].fname[0]) == 0) &&
	(line == txn_src[i].line)) {
      my->txn_id = i;
      found = true;
      break;
    }
  }
  if (!found) {
    if (num_txns < MAX_TXNS) {
      strncpy(&txn_src[num_txns].fname[0],fname,FNAME_LEN);
      txn_src[num_txns].fname[FNAME_LEN] = 0;
      txn_src[num_txns].line = line;
      my->txn_id = num_txns;
      num_txns++;
    } else {
      more_txns = true;
      strcpy(&txn_src[MAX_TXNS-1].fname[0],"multiple");
      txn_src[MAX_TXNS-1].line = 0;
      my->txn_id = MAX_TXNS-1;
    }
  }  tts_rel(&txn_list_lock);
  return;
}
#endif /* STATS */
/*-------------------------------------------------------------------*/
#ifdef DESC_TLS
/*-------------------------------------------------------------------*/
INLINE void * stm_desc() {
  EVENT_START(E_STM_DESC);
  desc_t * my = TLS_GET();
  if (my == NULL) {
    stm_thr_init();
    my = TLS_GET();
    ASSERT(my);
  }
  STATS_ADD(NUM_STM_DESC_CALLS,1);
  EVENT_END(E_STM_DESC);
  return my;
}
/*-------------------------------------------------------------------*/
/***
 * stm_begin
 */
INLINE void stm_begin(jmp_buf buf, void * mydesc,char * fname,int line)
{
  desc_t * my = (desc_t *) mydesc;
#if !defined(STMXLC)
  if (my == NULL) {
    stm_thr_init();
    my = TLS_GET();
  }
#endif
  ASSERT(my);
  PROF_START_TIME();
  if (my->nesting == 0) {
    ASSERT(my->status == TX_INACTIVE);
    my->buf = (void *)buf;
    my->status = TX_ACTIVE;
#ifndef INC_VALIDATION
    /* clear infinite loop detection on new tx */
    my->num_unvalidated = 0;
#endif
#ifdef STATS
    set_txn_id(my,fname,line);
    // printf("%s %d %d\n",fname,line,my->txn_id);
#endif
#ifdef GLOBAL_VERSION
    my->last_valid_version = global_version;
    LWSYNC; RBR;
#endif
  } else {
    ASSERT(my->status == TX_ACTIVE);
  }
  my->nesting++;
  STATS_MAX(MAX_NESTING,my->nesting);
}
/*-------------------------------------------------------------------*/
#else /* !DESC_TLS */
/*-------------------------------------------------------------------*/
INLINE int stm_begin(void * mydesc) {
  desc_t * my = (desc_t *)mydesc;
  ASSERT(my);
  my->nesting++;
  if (my->nesting == 1) {
    ASSERT(my->status == TX_INACTIVE);
    my->status = TX_ACTIVE;
    return STM_INACTIVE;
  } else {
    ASSERT(my->status != TX_INACTIVE);
    ASSERT(my->status != TX_COMMITTED);
    if (my->status == TX_ACTIVE) {
      return STM_ACTIVE;
    } else {
      return STM_ABORTED;
    }
  }
}
/*-------------------------------------------------------------------*/
#endif /* !DESC_TLS */
/*-------------------------------------------------------------------*/
static inline unsigned key2ind(void * key) {
  unsigned ind = ((long)(key) & blockmask) >> blockbits;
  return ind;
}
/*-------------------------------------------------------------------*/
static inline meta_entry_t * locate_meta(void * key) {
  unsigned ind = key2ind(key);
  return &meta[ind];
}
/*-------------------------------------------------------------------*/
static inline long calc_bloom(void * const addr) {
  ASSERT(sizeof(long) == sizeof(void *));
  long bit = (long)addr;
#ifdef ALIASING
  bit >>= 3;
#endif
#if defined(PPC64) || defined(X64)
  ASSERT(sizeof(void *) == 8);
  bit ^= bit>>(LOG_2_BLOOM_FILTER_BITS<<2);
#else
  ASSERT(sizeof(void *) == 4);
#endif
  bit ^= bit>>(LOG_2_BLOOM_FILTER_BITS<<1);
  bit ^= bit>>LOG_2_BLOOM_FILTER_BITS;
  bit &= BLOOM_FILTER_MASK;
  ASSERT(bit >= 0);
  ASSERT(bit < BLOOM_FILTER_BITS);
  return bit;
}
/*-------------------------------------------------------------------*/
static inline int check_bloom(desc_t * my, long bloom) {
  ASSERT(bloom < BLOOM_FILTER_BITS);
  return ((my->bloom_filter[bloom>>3] >> (bloom & 7)) & 1);
}
/*-------------------------------------------------------------------*/
static inline void set_bloom(desc_t * my, long bloom) {
  ASSERT(bloom < BLOOM_FILTER_BITS);
  my->bloom_filter[bloom>>3] |= 1<<(bloom & 7);
}
/*-------------------------------------------------------------------*/
static void commit_frees(desc_t * my) {
  int i;
  for (i=0;i<my->num_frees;i++) {
    free(my->frees[i]);
  }
}
/*-------------------------------------------------------------------*/
static inline void cleanup_after_commit(desc_t * my) {

  /* do frees */
  if (my->num_frees > 0) {
    commit_frees(my);
  }

  my->retried = 0;
  common_cleanup(my);
}
/*-------------------------------------------------------------------*/
#ifndef INLINED
void stm_abort() {
  desc_t * my = TLS_GET();
  cleanup_on_abort(my);
}
#endif /* !INLINED */
/*-------------------------------------------------------------------*/
void stm_validate(void * mydesc) {
  desc_t * my = (desc_t *)mydesc;
  if (!my) 
    return;
  if (!STM_VALIDATE_READ_SET(my)) {
    stm_conflict(my);
  }
}
/*-------------------------------------------------------------------*/
NOINLINE void expand_malloc_list(desc_t * my) {
  if (my->malloc_list_size == 0) {
    my->mallocs = (void * *) malloc(INITIAL_MALLOC_LIST*sizeof(void *));
    my->malloc_list_size = INITIAL_MALLOC_LIST;
    return;
  }
  unsigned size = my->malloc_list_size * sizeof(void *);
  void * * list = (void * *) malloc(size<<1);
  memcpy(list,my->mallocs,size);
  free(my->mallocs);
  my->malloc_list_size <<= 1;
  my->mallocs = list;
}
/*-------------------------------------------------------------------*/
NOINLINE void expand_free_list(desc_t * my) {
  if (my->free_list_size == 0) {
    my->frees = (void * *) malloc(INITIAL_FREE_LIST*sizeof(void *));
    my->free_list_size = INITIAL_FREE_LIST;
    return;
  }
  unsigned size = my->free_list_size * sizeof(void *);
  void * * list = (void * *) malloc(size<<1);
  memcpy(list,my->frees,size);
  free(my->frees);
  my->free_list_size <<= 1;
  my->frees = list;
}
/*-------------------------------------------------------------------*/
NOINLINE void expand_checkpoint_list(desc_t * my) {
  if (my->checkpoint_list_size == 0) {
    my->checkpoints = (checkpoint_entry_t *)
      malloc(INITIAL_CHECKPOINT_LIST*sizeof(checkpoint_entry_t));
    my->checkpoint_list_size = INITIAL_CHECKPOINT_LIST;
    return;
  }
  unsigned size = my->checkpoint_list_size * sizeof(checkpoint_entry_t);
  checkpoint_entry_t * list = (checkpoint_entry_t *) malloc(size<<1);
  memcpy(list,my->checkpoints,size);
  free(my->checkpoints);
  my->checkpoint_list_size <<= 1;
  my->checkpoints = list;
}
/*-------------------------------------------------------------------*/
void * stm_malloc(size_t sz,void * mydesc) {
  void * ptr = malloc(sz);
  assert(ptr);
  EVENT_START(E_STM_MALLOC);
  desc_t * my = (desc_t *) mydesc;
#ifdef DEBUG
  memset(ptr,0x4d,sz);
#endif
  if (my->status == TX_INACTIVE) {
    //printf("calling stm_malloc_from_outside 0x%p 0x%p\n",my,ptr);
    return ptr;
  }
  STATS_ADD(NUM_MALLOCS,1);
  /* remember to free on abort */
  unsigned num = my->num_mallocs++;
  if (my->num_mallocs > my->malloc_list_size)
    expand_malloc_list(my);
  my->mallocs[num] = ptr;
#ifdef DEBUG
  DEBUG_PRINT("stm_malloc 0x%x\n",ptr);
#endif
  EVENT_END(E_STM_MALLOC);
  return ptr;
}
/*-------------------------------------------------------------------*/
void * stm_calloc(size_t ne,size_t sz,void * mydesc) {
  stm_malloc(ne*sz,mydesc);
}
/*-------------------------------------------------------------------*/
void stm_free(void * ptr,void * mydesc) {
  EVENT_START(E_STM_FREE);
  desc_t * my = (desc_t *) mydesc;
  if (my->status == TX_INACTIVE) {
    EVENT_END(E_STM_FREE);
    free(ptr);
    return;
  }
  STATS_ADD(NUM_FREES,1);
  /* check if allocated inside txn */
  if (my->num_mallocs) {
    int i;
    for (i=0;i<my->num_mallocs;i++) {
      if (ptr == my->mallocs[i]) {
	/* can free the block right away */
	STATS_ADD(NUM_FREE_PRIVATE,1);
	/* remember not to free the same block if aborted */
	my->mallocs[i] = NULL;
	/* mybe can avoid wasting a malloc entry */
	if (i+1 == my->num_mallocs) {
	  my->num_mallocs--;
	}
	EVENT_END(E_STM_FREE);
	/* free the block */
	free(ptr);
	return;
      }
    }
  }
  /* remember to free on commit */
  unsigned num = my->num_frees++;
  if (my->num_frees > my->free_list_size)
    expand_free_list(my);
  my->frees[num] = ptr;
  EVENT_END(E_STM_FREE);
}
/*-------------------------------------------------------------------*/
INLINE void stm_checkpoint(char * const addr,int size,void * mydesc) {
  desc_t * my = (desc_t *) mydesc;
  STATS_ADD(CHECKPOINTING_CALLS,1);
  ASSERT(my);
  if (my->status == TX_INACTIVE) return;
  ASSERT(my->status == TX_ACTIVE);
  ASSERT((size == 1) || (size == 2) || (size == 4) || (size == 8));
  ASSERT(((long)addr & (size-1)) == 0);
  /* Find the next log entry */
  unsigned num = my->num_checkpoints;
  my->num_checkpoints++;
  if (my->num_checkpoints > my->checkpoint_list_size)
    expand_checkpoint_list(my);
  checkpoint_entry_t * ce = &my->checkpoints[num];
  //memcpy(&ce->oldval[0],addr,size);
  do_write(&ce->val,addr,size);
  ce->addr = addr;
  ce->size = size;
}
/*-------------------------------------------------------------------*/
static inline write_entry_t * get_new_write_entry(desc_t * my) {
  /* Find the next log entry */
  if (my->num_writes == my->write_list_size)
    expand_writes(my);
  write_entry_t * we = my->next_write_entry;
  my->num_writes++;
  my->next_write_entry++;
  return we;
}
/*-------------------------------------------------------------------*/
NOINLINE write_entry_t * search_write_list(desc_t * my, void * const addr) {
  /* search write list */
  unsigned numwrites = my->num_writes;
  write_entry_t * found = NULL;
  int i;
  for (i=numwrites-1;i>=0;i--) {
    /* Checking address. Assuming no size-changing casts inside
     * transactions.*/
    if (addr == my->writes[i].addr) {
      found = &my->writes[i];
      break;
    }
  }
  return found;
}
/*-------------------------------------------------------------------*/
NOINLINE write_entry_t * get_write_entry(desc_t * my, void * const addr) {
  STATS_ADD(BLOOM_FILTER_CHECKS,1);
  /* first check write filter */
  long bloom = calc_bloom(addr);
  if (!check_bloom(my,bloom)) 
    return NULL;
  STATS_ADD(BLOOM_FILTER_MATCHES,1);
  /* search write list */
  write_entry_t * found = search_write_list(my,addr);
#ifdef STATS
  if (found) STATS_ADD(READ_AFTER_WRITE_MATCHES,1);
#endif /* STATS */
  return found;
}
/*-------------------------------------------------------------------*/
static inline write_entry_t * check_write_set(desc_t * my, void * const addr) {
  /* check write list */
  if (my->num_writes == 0)
    return NULL;
  return  get_write_entry(my,addr);
}
/*-------------------------------------------------------------------*/
static inline void set_read_entry(read_entry_t * re,
				  meta_entry_t * pmeta,
				  void * ver, void * const addr) {
  re->pmeta = pmeta;
  re->orec.all = ver;
#ifdef STATS /* to check for duplicates */
  re->addr = addr;
#endif /* STATS */
}
/*-------------------------------------------------------------------*/
static inline read_entry_t * get_new_read_entry(desc_t * my) {
  /* expand if last entry */
  if (my->num_reads == my->read_list_size) {
    expand_reads(my);
  }
  /* get next entry */
  read_entry_t * re = my->next_read_entry;
  /* Increment log counter */
  my->num_reads++;
  //  assert(my->num_reads < 100000);
  my->next_read_entry++;
  return re;
}
/*-------------------------------------------------------------------*/
static inline void add_to_read_list(desc_t * my, void * const addr,
				    meta_entry_t * pmeta,
				    void * orecall) {
#ifdef STATS
  my->stats->read_list++;
#endif /* STATS */
  read_entry_t * re = get_new_read_entry(my);
  set_read_entry(re,pmeta,orecall,addr);
}
/*-------------------------------------------------------------------*/
#ifdef STATS
static void read_barrier_stats(desc_t * my, void * key, void * const addr) {
  int i;
  int found_addr = false;
  int found_key = false;
  unsigned ind = key2ind(key);
  meta_entry_t * pmeta = (meta_entry_t *)&meta[ind];
  for (i=0;i<my->num_reads;i++) {
    if (pmeta == my->reads[i].pmeta) {
      found_key = true;
    }
    if (addr == my->reads[i].addr) {
      found_addr = true;
      break;
    }
  }
  if (found_addr) {
    STATS_ADD(DUPLICATE_READS,1);
  } else {
    my->stats->read_set++;
  }
  if (found_key) {
    STATS_ADD(DUPLICATE_READ_CONFLICT_SET,1);
  }
}
#endif /* STATS */
/*-------------------------------------------------------------------*/
#ifdef STATS
static void write_barrier_stats(desc_t * my, void * key, void * const addr) {
  int i;
  int found_addr = false;
  int found_key = false;
  unsigned ind = key2ind(key);
  meta_entry_t * pmeta = (meta_entry_t *)&meta[ind];
  for (i=0;i<my->num_writes;i++) {
    if (pmeta == my->writes[i].pmeta) {
      found_key = true;
    }
    if (addr == my->writes[i].addr) {
      found_addr = true;
      break;
    }
  }
  if (found_addr) {
    STATS_ADD(DUPLICATE_WRITES,1);
  }
  if (found_key) {
    STATS_ADD(DUPLICATE_WRITE_CONFLICT_SET,1);
  }
}
#endif /* STATS */
/*-------------------------------------------------------------------*/
static inline int read_barrier_checks(desc_t * my, void * const addr) {
#ifdef CHECK_CONTEXT
  if (my->status != TX_ACTIVE) {
    STATS_ADD(READ_BARRIERS_OUTSIDE_TXNS,1);
    return true;
  }
#endif /* CHECK_CONTEXT */
#ifdef ADDRESS_TAKEN
  if ((void *)addr >= my->stackmin) {
    if ((void *)addr <= my->stackmax) {
      STATS_ADD(READ_BARRIERS_FOR_STACK,1);
      return true;
    }
  }
#endif /* ADDRESS_TAKEN */
  return false;
}
/*-------------------------------------------------------------------*/
static inline int write_barrier_checks(desc_t * my, void * const addr) {
#ifdef CHECK_CONTEXT
  if (my->status != TX_ACTIVE) {
    STATS_ADD(WRITE_BARRIERS_OUTSIDE_TXNS,1);
    return true;
  }
#endif /* CHECK_CONTEXT */
#ifdef ADDRESS_TAKEN
  if ((void *)addr >= my->stackmin) {
    if ((void *)addr <= my->stackmax) {
      STATS_ADD(WRITE_BARRIERS_FOR_STACK,1);
      return true;
    }
  }
#endif /* ADDRESS_TAKEN */
  return false;
}
/*-------------------------------------------------------------------*/
static inline void do_read(void * const addr, void * pval, int size) {
  switch(size) {
  case 1: *(char *)pval = *(volatile char *)addr; return;
  case 2: *(short *)pval = *(volatile short *)addr; return;
  case 4: *(int *)pval = *(volatile int *)addr; return;
  case 8: *(long long *)pval = *(volatile long long *)addr; return;
  }
  ASSERT(0);
}
/*-------------------------------------------------------------------*/
static inline void do_write(void * const addr, void * pval, int size) {
  if (size == 8) {
    *(volatile long long *)addr = *(long long *)pval; return;
  } else if (size == 4) {
    *(volatile int *)addr = *(int *)pval; return;
  } else if (size == 2) {
    *(volatile short *)addr = *(short *)pval; return;
  } else if (size == 1) {
    *(volatile char *)addr = *(char *)pval; return;
  }
  ASSERT(0);
}
/*-------------------------------------------------------------------*/
static inline int are_equal(void * const addr, void * pval, int size) {
  switch(size) {
  case 1: return (*(volatile char *)addr == *(char *)pval);
  case 2: return (*(volatile short *)addr == *(short *)pval);
  case 4: return (*(volatile int *)addr == *(int *)pval);
  case 8: return (*(volatile long long *)addr == *(long long *)pval);
  }
  ASSERT(0);
}
/*-------------------------------------------------------------------*/
#ifdef ENCOUNTER_ACQUIRE
/*-------------------------------------------------------------------*/
static int read_meta_acquired(desc_t * my, void * const addr,
			      volatile meta_entry_t * pmeta,
			      void * pval, int size) {
  write_entry_t * we = &my->writes[pmeta->write_list];
  while (true) {
#ifndef ALIASING
    if (addr == we->addr) {
      STATS_ADD(READ_AFTER_WRITE_MATCHES,1);
      do_read(&we->val,pval,size);
      return true;
    } else {
      int next = we->next;
      if (next == END_OF_LIST) {
	return false;
      } else {
	we = &my->writes[next];
      }
    }
#else
    {
      fprintf(stderr,
	      "Aliasing not yet supported for encounter acquire\n");
      assert(0);
    }
#endif
  }
}
/*-------------------------------------------------------------------*/
static int check_write_after_write_acquire(desc_t * my,
					   void * const addr,
					   volatile meta_entry_t * pmeta,
					   void * orecptr) {
  if (orecptr == my) {
    write_entry_t * we = &my->writes[pmeta->write_list];
    while (true) {
      if (addr == we->addr) {
	my->we_match = we;
	return WRITE_TO_OLD_BUFFER;
      } else {
	int next = we->next;
	if (next == END_OF_LIST) {
	  my->we_match = we;
	  STATS_ADD(LOCK_SKIP,1);
	  return WRITE_TO_OLD_OREC;
	} else {
	  we = &my->writes[next];
	}
      }
    }
  } else {
    STATS_ADD(WRITE_ACQUIRE_RETRIES,1);
    return WRITE_CONFLICT;
  }
}
/*-------------------------------------------------------------------*/
#endif /* ENCOUNTER_ACQUIRE */
/*-------------------------------------------------------------------*/
static int log_write_bloom_match(desc_t * my, void * const addr,
				 void * pval, int size) {
  write_entry_t * we;
  int i;
  int numwrites = my->num_writes;
  for (i=numwrites, we = &my->writes[numwrites-1];
       i>0;
       i--, we--) {
    void * weaddr = (void *)we->addr;
#ifndef ALIASING
    if (addr == weaddr) {
      ASSERT(size == we->size);
      do_write(&we->val,pval,size);
      return true;
    }
#else /* ALIASINg */
    void * wealigned = (void *)((long)weaddr >> 3);
    void * aligned = (void *)((long)addr >> 3);
    if (aligned == wealigned) {
      int wesize = we->size;
      if (size == wesize) {
	if (addr == weaddr) {
	  do_write(&we->val,pval,size);
	  return true;
	}
      } else if (size < wesize) {
	if ((addr >= weaddr) && (addr+size <= weaddr+wesize)) {
	  STATS_ADD(ALIASING_INSTANCES,1);
	  int offset = weaddr - addr;
	  void * to = weaddr + offset;
	  do_write(to,pval,size);
	  assert(0);
	  return true;
	} 
      } else /* size > we->size */ {
	if ((addr <= weaddr) && (addr+size >= weaddr+wesize)) {
	  STATS_ADD(ALIASING_INSTANCES,1);
	  do_write(&we->val,pval,size);
	  we->addr = addr;
	  we->size = size;
	  return true;
	  // It is ok to stop here even if there is an earlier entry
	  // The aerlier entry is harmless
	}
      }
    }
#endif /* ALIASING */
  }
  return false;
}
/*-------------------------------------------------------------------*/
static int read_bloom_match(desc_t * my, void * const addr,
			    void * pval, int size) {
  int found = false;
  int numwrites = my->num_writes;
  write_entry_t * we = &my->writes[numwrites-1];
  unsigned int mask;
  int i;
  for (i=numwrites;
       i>0;
       i--, we--) {
    void * weaddr = (void *)we->addr;
#ifndef ALIASING
    if (addr == weaddr) {
      ASSERT(size == we->size);
      do_read(&we->val,pval,size);
      return true;
    }
#else /* ALIASING */
    void * wealigned = (void *)((long)weaddr >> 3);
    void * aligned = (void *)((long)addr >> 3);
    if (aligned == wealigned) {
      int wesize = we->size;
      if (size == wesize) {
	if (addr == weaddr) {
	  do_read(&we->val,pval,size);
	  return true;
	}
      } else if (size < wesize) {
	if ((addr >= weaddr) && (addr+size <= weaddr+wesize)) {
	  STATS_ADD(ALIASING_INSTANCES,1);
	  int offset = weaddr - addr;
	  void * from = weaddr + offset;
	  do_read(from,pval,size);
	  return true;
	} 
      } else /* size > we->size */ {
	if ((addr <= weaddr) && (addr+size >= weaddr+wesize)) {
	  STATS_ADD(ALIASING_INSTANCES,1);
	  if (!found) {
	    ISYNC; RBR;
	    do_read(addr,pval,size);
	    mask = (1<<size)-1;
	    found = true;
	  }
	  int offset = addr - weaddr;
	  void * from = weaddr;
	  void * to = addr + offset;
	  int bit = 1;
	  int i;
	  for (i=0;
	       i<wesize;
	       i++,from++,to++,bit<<=1) {
	    if (mask & bit) {
	      do_read(from,to,1);
	      mask &= ~(bit);
	    }
	  }
	  if (mask == 0) {
	    return true;
	  }
	}
      }
    }
#endif /* ALIASING */
  }
  return false;
}
/*-------------------------------------------------------------------*/
static int silent_write(desc_t * my, void * const addr,
			void * pval, volatile meta_entry_t * pmeta,
			int size, int action) {
  if (action == WRITE_TO_NEW_OREC) {
    orec_t orec;
    orec.all = pmeta->orec.all;
    if (!orec.sep.locked) {
      ISYNC; RBR;
      if (are_equal(addr,pval,size)) {
	STATS_ADD(SILENT_WRITES_BECAME_READS,1);
	add_to_read_list(my,addr,(meta_entry_t *)pmeta,orec.all);
#ifdef STATS
	my->stats->read_list--;
#endif
	return true;
      }
    }
  } else if (action == WRITE_TO_OLD_OREC) {
    return true;
  }
  return false;
}
/*-------------------------------------------------------------------*/
static inline void stm_read_any(desc_t * my, void * const addr, void * key,
				 void * pval, int size) {
  //  printf("reading ....  \n");
  EVENT_START(E_STM_READ);
  STATS_ADD(READ_BARRIERS,1);
  ASSERT((size == 1) || (size == 2) || (size == 4) || (size == 8));
  ASSERT(((long)addr & (size-1)) == 0);
#ifndef INC_VALIDATION
  /* if we've read a lot of objects without validating, then validate */
  if (((++my->num_unvalidated) & INF_LOOP_MASK) == 0) {
    check_inf_loop(my);
  }
#endif
  if (read_barrier_checks(my,(void *)addr)) {
    do_read(addr,pval,size);
    return;
  }
#ifdef STATS /* check for duplicates */
  read_barrier_stats(my,key,(void *)addr);
#endif /* STATS */
  
#ifndef ENCOUNTER_ACQUIRE /* COMMIT_TIME_ACQUIRE */
  if (my->num_writes) {
    STATS_ADD(BLOOM_FILTER_CHECKS,1);
    long bloom = calc_bloom(addr);
    if (check_bloom(my,bloom)) {
      STATS_ADD(BLOOM_FILTER_MATCHES,1);
      if (read_bloom_match(my,addr,pval,size)) {
	STATS_ADD(READ_AFTER_WRITE_MATCHES,1);
	EVENT_END(E_STM_READ);
	return;
      }
    }
  }
  volatile meta_entry_t * pmeta = locate_meta(key);
  if (!VALIDATE_OREC(&pmeta->orec.all))
  {
    orec_t orec;
    orec.all = pmeta->orec.all;
    if (orec.sep.locked) {
      STATS_ADD(READ_ENCOUNTER_RETRIES,1);
      wait_for_orec_change(my,pmeta,orec.all);
      stm_conflict(my);
      return;
    }
    // read orec before reading data
    ISYNC; RBR;
    // check if can skip read logging
#if defined(FIRST_TRY)
    if (my->retried > 0)
#endif
    {
      add_to_read_list(my,addr,(meta_entry_t *)pmeta,orec.all);
    }
  }
#else  /* ENCOUNTER_ACQUIRE */
  volatile meta_entry_t * pmeta = locate_meta(key);
  orec_t orec;
  orec.all = pmeta->orec.all;
  if (orec.sep.locked) {
    orec.sep.locked = false;
    if (orec.all != my) {
      STATS_ADD(READ_ENCOUNTER_RETRIES,1);
      wait_for_orec_change(my,pmeta,orec.all);
      stm_conflict(my);
      return;
    }
    if (read_meta_acquired(my,addr,(meta_entry_t *)pmeta,pval,size)) {
      STATS_ADD(READ_AFTER_WRITE_MATCHES,1);
      return;
    }
    return;
  }
  // read orec before reading data
  ISYNC; RBR;
  // check if can skip read logging
#if defined(FIRST_TRY)
  if (my->retried > 0)
#endif
  {
    add_to_read_list(my,addr,(meta_entry_t *)pmeta,orec.all);
  }
#endif /* ENCOUNTER_ACQUIRE */
  do_read(addr,pval,size);
#ifdef INC_VALIDATION
  // read data before validation
  LWSYNC; RBR;
  if (!STM_VALIDATE_READ_SET(my)) {
    stm_conflict(my);
    return;
  }
#endif // INC_VALIDATION
  EVENT_END(E_STM_READ);
  return;
}
/*-------------------------------------------------------------------*/
#ifdef ENCOUNTER_ACQUIRE
static inline int acquire_orec(desc_t * my,
			       volatile meta_entry_t * pmeta) {
  orec_t my1;
  my1.all = my;
  my1.sep.locked = 1;
  while (true) {
    orec_t orec;
    orec.all = pmeta->orec.all;
    if (orec.sep.locked) {
      wait_for_orec_change(my,pmeta,orec.all);
      return false;
    }
    int  ret = CAS(&(pmeta->orec),orec.all,my1.all);
    if (ret) {
      pmeta->latest.all = orec.all;
      pmeta->write_list = my->num_writes-1;
      STATS_ADD(LOCK_ACQUIRE,1);
      return true;
    }
  }
}
#endif /* ENCOUNTER_ACQUIRE */
/*-------------------------------------------------------------------*/
static inline void stm_write_any(desc_t * my, void * const addr, void * key,
				 void * pval, int size) {
  EVENT_START(E_STM_WRITE);
  STATS_ADD(WRITE_BARRIERS,1);
  ASSERT((size == 1) || (size == 2) || (size == 4) || (size == 8));
  ASSERT(((long)addr & (size-1)) == 0);
  void * to;
  int action;
  meta_entry_t * pmeta = locate_meta(key);
  while (1) {
    if (write_barrier_checks(my,addr)) {
      to = addr;
      break;
    }
#ifdef STATS /* check for duplicates */
  write_barrier_stats(my,key,(void *)addr);
#endif /* STATS */
#ifndef ENCOUNTER_ACQUIRE /* COMMIT_TIME_ACQUIRE */
    long bloom = calc_bloom((void *)addr);
    int match = check_bloom(my,bloom);
    if (match) {
      if (log_write_bloom_match(my,addr,pval,size)) {
	EVENT_END(E_STM_WRITE);
	return;
      }
    }
    action = WRITE_TO_NEW_OREC;
#else   /* ENCOUNTER_ACQUIRE */
    orec_t orec;
    orec.all = pmeta->orec.all;
    if (!orec.sep.locked) {
      action = WRITE_TO_NEW_OREC;
    } else /* orec is locked */ {
      orec.sep.locked = false;
      action = check_write_after_write_acquire(my,addr,pmeta,orec.all);
    }
#endif  /* ENCOUNTER_ACQUIRE */
    /* Silent Writes */
#ifdef STATS
    if (are_equal(addr,pval,size)) {
      if ((action == WRITE_TO_NEW_OREC) ||
	  (action == WRITE_TO_OLD_OREC)) {
	STATS_ADD(NUM_SILENT_WRITES,1);
      }
    }
#endif /* STATS */
#ifdef SILENT_WRITES
    if (are_equal(addr,pval,size)) {
      if (silent_write(my,addr,pval,pmeta,size,action)) {
	EVENT_END(E_STM_WRITE);
	return;
      }
    }
#endif /* SILENT_WRITES */
    if (action == WRITE_TO_NEW_OREC) {
      write_entry_t * we = get_new_write_entry(my);
      we->addr = addr;
      we->pmeta = pmeta;
      we->size = size;
      to = &we->val;
#ifndef ENCOUNTER_ACQUIRE
      we->locked = false;
      set_bloom(my,bloom);
#else   /* ENCOUNTER_ACQUIRE */
      we->next = END_OF_LIST;
      we->locked = acquire_orec(my,pmeta);
      if (!we->locked) {
	my->num_writes--;
	stm_conflict(my);
	return;
      }
    } else if (action == WRITE_TO_OLD_OREC) {
      write_entry_t * we = get_new_write_entry(my);
      we->addr = addr;
      we->pmeta = pmeta;
      we->locked = false;
      we->size = size;
      we->next = END_OF_LIST;
      my->we_match->next = my->num_writes-1;
      to = &we->val;
    } else if (action == WRITE_TO_OLD_BUFFER) {
      to = &my->we_match->val;
    } else if (action == WRITE_TO_MEMORY) {
      to = addr;
    } else /* conflict */ {
      stm_conflict(my);
      return;
#endif /* ENCOUNTER_ACQUIRE */
    }
    break;
  }
  do_write(to,pval,size);
  EVENT_END(E_STM_WRITE);
}
/*-------------------------------------------------------------------*/
#ifndef INLINED
/***
 * stm_{read|write|copy|set}_block
 */
void stm_read_block(char * to, char volatile * from,
		    int count, void * mydesc) {
  desc_t * my = mydesc;
  void * src = (void *)from;
  void * dst = (void *)to;
  void * end = src+count;
  while ((src < end) && ((long)src & 7)) {
    stm_read_any(my,src,src,dst,1);
    src += 1;
    dst += 1;
  }
  while (src+7 < end) {
    stm_read_any(my,src,src,dst,8);
    src += 8;
    dst += 8;
  }
  while (src < end) {
    stm_read_any(my,src,src,dst,1);
    src += 1;
    dst += 1;
  }
}
/*-------------------------------------------------------------------*/
void stm_write_block(char volatile * to, char * from,
		    int count, void * mydesc) {
  desc_t * my = mydesc;
  void * src = (void *)from;
  void * dst = (void *)to;
  void * end = dst+count;
  while ((dst < end) && ((long)dst & 7)) {
    stm_write_any(my,dst,dst,src,1);
    src += 1;
    dst += 1;
  }
  while (dst+7 < end) {
    stm_write_any(my,dst,dst,src,8);
    src += 8;
    dst += 8;
  }
  while (dst < end) {
    stm_write_any(my,dst,dst,src,1);
    src += 1;
    dst += 1;
  }
}
/*-------------------------------------------------------------------*/
void stm_set_block(char volatile * const addr, int c,
		   int count, void * mydesc) {
  desc_t * my = mydesc;
  char data[8];
  int i;
  for (i=0;i<8;i++)
    data[i] = c;
  void * src = &data[0];
  void * dst = (void *)addr;
  void * end = dst+count;
  while ((dst < end) && ((long)dst & 7)) {
    stm_write_any(my,dst,dst,src,1);
    dst += 1;
  }
  while (dst+7 < end) {
    stm_write_any(my,dst,dst,src,8);
    dst += 8;
  }
  while (dst < end) {
    stm_write_any(my,dst,dst,src,1);
    dst += 1;
  }
}
#endif /* !INLINED */
/*-------------------------------------------------------------------*/
#ifdef STMXLC
void stm_copy_block(char volatile * to, char volatile * from,
		    int count, void * mydesc) {
  desc_t * my = mydesc;
  void * data = stm_malloc(count,mydesc);
  stm_read_block(data,from,count,my);
  stm_write_block(to,data,count,my);
  stm_free(data,my);
}
#endif
/*-------------------------------------------------------------------*/
#define ITEM_TYPE	write_entry_t
#define SORT_FIELD	pmeta
static int partition(int first,int last,ITEM_TYPE * list) {
  ITEM_TYPE tmp;
  ITEM_TYPE item = list[first];
  int i = first;
  int j = last;
  while (1) {
    while (list[i].SORT_FIELD < item.SORT_FIELD) i++;
    while (list[j].SORT_FIELD > item.SORT_FIELD) j--;
    if (i >= j) return j;
    tmp = list[i]; list[i] = list[j]; list[j] = tmp; i++; j--;
  }
}
static void quicksort(int first,int last,ITEM_TYPE * list) {
  int mid;
  if (first < last) {
    mid = partition(first,last,list);
    quicksort(first,mid,list);
    quicksort(mid+1,last,list);
  }
}
/*-------------------------------------------------------------------*/
static void sort_ws(int num,write_entry_t * writes) {
  quicksort(0,num-1,writes);
}
/*-------------------------------------------------------------------*/
typedef void stm_end_ret_t;
INLINE stm_end_ret_t stm_end(void * mydesc) {
  EVENT_START(E_STM_END);
  desc_t * my = (desc_t *)mydesc;
  ASSERT(my->status != TX_INACTIVE);
  ASSERT(my->nesting > 0);
  my->nesting--;
  /*
   * Check if this is the end of a nested transaction
   */
  if (my->nesting > 0) {
    EVENT_END(E_STM_END);
    PROF_END_TIME();
    return;
  }
  
  // Outer transaction
  unsigned numwrites = my->num_writes;
  // check for read-only
  if (numwrites == 0) {
    // Read-only
    EVENT_START(E_STM_END_VALIDATE);
    if (!STM_VALIDATE_READ_SET(my)) {
      stm_conflict(my);
    }
    EVENT_END(E_STM_END_VALIDATE);
    STATS_ADD(READ_ONLY_COMMITS,1);
    STATS_ADD(READ_LIST_SIZES,my->stats->read_list);
    STATS_MAX(READ_LIST_MAX_SIZE,my->stats->read_list);
    STATS_ADD(READ_SET_SIZES,my->stats->read_set);
    STATS_MAX(READ_SET_MAX_SIZE,my->stats->read_set);
    /***
     * Success
     */
  } else {
    /***
     * Read-write case
     */
    int valid = true;
    orec_t my1;
    my1.all = my;
    my1.sep.locked = 1;
    EVENT_START(E_STM_ACQUIRE);
#ifndef ENCOUNTER_ACQUIRE /* COMMIT_TIME_ACQUIRE */
    /* Acquire write set orecs */
    if (my->retried >= RETRY_THRESHOLD) {
      // Retried so many times
      // Sort write list to prevent livelock
      sort_ws(my->num_writes,my->writes);
    }
    int i;
    write_entry_t * we;
    for (i=0,we=my->writes;
	 i<numwrites;
	 i++,we++) {
      meta_entry_t volatile * pmeta = we->pmeta;
      /* check if already locked by txn */ 
      orec_t orec;
      orec.all = pmeta->orec.all;
      if (orec.sep.locked) {
	we->locked = false;
	if (orec.all != my1.all) {
	  wait_for_orec_change(my,pmeta,orec.all);
	  valid = false;
	  break;
	}
	STATS_ADD(LOCK_SKIP,1);
      } else {
	int ret = CAS(&(pmeta->orec.all),orec.all,my1.all);
	if (!ret) {
	  valid = false;
	  we->locked = false;
	  break;
	}
	pmeta->latest.all = orec.all;
	we->locked = true;
	STATS_ADD(LOCK_ACQUIRE,1);
      }
    }
    if (!valid) {
      STATS_ADD(WRITE_ACQUIRE_RETRIES,1);
      stm_conflict(my);
    }
#endif /* !ENCOUNTER_ACQUIRE */
    // acquire ordered before validation and writing
    HWSYNC; RBR; WBW; /* WBR implicit in CAS */
    EVENT_END(E_STM_ACQUIRE);
    // Validate read set
    EVENT_START(E_STM_END_VALIDATE);
    valid = STM_VALIDATE_READ_SET(my);
    EVENT_END(E_STM_END_VALIDATE);
    if (!valid) {
      STATS_ADD(READ_VALIDATION_RETRIES,1);
      stm_conflict(my);
    }
    // Success guaranteed
#ifdef GLOBAL_VERSION
    while (true) {
      long ver = global_version;
      int ret = CAS(&global_version,ver,ver+1);
      if (ret) {
	break;
      }
    }
    // Update of version number before witing data
    LWSYNC; WBW;
#endif // GLOBAL_VERSION
    STATS_ADD(READ_WRITE_COMMITS,1);
    STATS_ADD(READ_LIST_SIZES,my->stats->read_list);
    STATS_MAX(READ_LIST_MAX_SIZE,my->stats->read_list);
    STATS_ADD(WRITE_LIST_SIZES,my->num_writes);
    STATS_MAX(WRITE_LIST_MAX_SIZE,my->num_writes);
    STATS_ADD(READ_SET_SIZES,my->stats->read_set);
    STATS_MAX(READ_SET_MAX_SIZE,my->stats->read_set);
    STATS_ADD(WRITE_SET_SIZES,my->num_writes);
    STATS_MAX(WRITE_SET_MAX_SIZE,my->num_writes);
    /* write to write set */
    // Perform deferred writes
    {
      int i;
      write_entry_t * we = my->writes;
      for (i=numwrites;
	   i>0;
	   i--,we++) {
	do_write((void *)we->addr,&we->val,we->size);
      }
    }
    // Releases ordered after writes
    LWSYNC; WBW;
    // Release orecs
    EVENT_START(E_STM_RELEASE);
    {
      int i;
      write_entry_t * we;
      for (i=0,we=my->writes;
	   i<numwrites;
	   i++,we++) {
	if (we->locked) {
	  meta_entry_t volatile * pmeta =  we->pmeta;
	  orec_t orec;
	  orec.all = pmeta->latest.all;
	  orec.sep.version++;
	  ASSERT(orec.sep.locked == 0);
	  pmeta->orec.all = orec.all;
	  STATS_ADD(LOCK_RELEASE,1);
	}
      }
    }
    EVENT_END(E_STM_RELEASE);
  }
  /* Cleanup after success */
  cleanup_after_commit(my);
  EVENT_END(E_STM_END);
  PROF_END_TIME();
}
/* ------------------------------------------------------------------ */
/**
 * for debugging
 */
NOINLINE void print_orec(void * key) {
  unsigned ind = key2ind(key);
  orec_t orec;
  orec.all = meta[ind].orec.all;
  long unsigned ver = orec.sep.version;
  printf("orec for 0x%p is [%lu,%d]\n",
	 key,
	 ver,
	 orec.sep.locked);
}
/* ------------------------------------------------------------------ */
NOINLINE void stm_debug_on() {
  debug = 1;
}
/* ------------------------------------------------------------------ */
#if !defined(INLINED)
/* ------------------------------------------------------------------ */
unsigned sum_orecs() {
  int i;
  unsigned sum=0;
  for (i=0;i<numorecs;i++) {
    sum += meta[i].orec.sep.version;
  }
  return sum;
}
/* ------------------------------------------------------------------ */
static void stm_time_prof_out(FILE * fd) {
#ifdef PROF_TIME
  unsigned int sum = 0;
  desc_t * desc;
  //printf("desc_list = %p\n",desc_list);
  for (desc = (desc_t *) desc_list;
       desc;
       desc = desc->next_desc) {
    sum =+ desc->time_in_transactions;
  }
  fprintf(fd,"%7d us time spent in transactions\n",sum);
#endif
}
void stm_print_time_prof() {
  stm_time_prof_out(stdout);
}
/* ------------------------------------------------------------------ */
#ifdef STATS
static void stm_print_derivative_stats(FILE * fd, unsigned * stats) {
  int ro = stats[READ_ONLY_COMMITS];
  int rw = stats[READ_WRITE_COMMITS];
  float total = ro + rw;
  fprintf(fd,"%8d\tTOTAL_COMMITS\n",
	  ro+rw);
  if (total == 0) total = 1;
  fprintf(fd,"%8.2f\tAVG_READ_SET_SIZE\n",
	  stats[READ_SET_SIZES]/total);
  fprintf(fd,"%8.2f\tAVG_WRITE_SET_SIZE\n",
	  stats[WRITE_SET_SIZES]/total);
  fprintf(fd,"%8.2f\tAVG_READ_LIST_SIZE\n",
	  stats[READ_LIST_SIZES]/total);
  fprintf(fd,"%8.2f\tAVG_WRITE_LIST_SIZE\n",
	  stats[WRITE_LIST_SIZES]/total);
  unsigned retries = 0;
  retries += stats[READ_ENCOUNTER_RETRIES];
  retries += stats[SIGNAL_RETRIES];
  retries += stats[WRITE_ACQUIRE_RETRIES];
  retries += stats[READ_VALIDATION_RETRIES];
  fprintf(fd,"%8d\tTOTAL_RETRIES\n",retries);
  fprintf(fd,"%8.2f\tAVG_RETRIES_PER_TXN\n",retries/total);
  fprintf(fd,"%8.2f\tAVG_CHECKPOINTING_CALLS_PER_TXN\n",
	  stats[CHECKPOINTING_CALLS]/total);
  float reads = stats[READ_BARRIERS];
  if (reads == 0) reads = 1;
  fprintf(fd,"%8.2f\tPCT_DUPLICATE_READS\n",
	  100*stats[DUPLICATE_READS]/reads);
  fprintf(fd,"%8.2f\tPCT_DUPLICATE_READ_CONFLICT_SET\n",
	  100*stats[DUPLICATE_READ_CONFLICT_SET]/reads);
  fprintf(fd,"%8.2f\tPCT_BLOOM_CHECK\n",
	  100*stats[BLOOM_FILTER_CHECKS]/reads);
  fprintf(fd,"%8.2f\tPCT_BLOOM_MATCH\n",
	  100*stats[BLOOM_FILTER_MATCHES]/reads);
  fprintf(fd,"%8.2f\tPCT_READ_AFTER_WRITE\n",
	  100*stats[READ_AFTER_WRITE_MATCHES]/reads);
  float writes = stats[WRITE_BARRIERS];
  if (writes == 0) writes = 1;
  fprintf(fd,"%8.2f\tPCT_DUPLICATE_WRITES\n",
	  100*stats[DUPLICATE_WRITES]/writes);
  fprintf(fd,"%8.2f\tPCT_SILENT_WRITES\n",
	  100*stats[NUM_SILENT_WRITES]/writes);
}
static void stm_print_aggregate_stats(FILE * fd) {
  fprintf(fd,"%8d\tTOTAL_METADATA_UPDATES\n",sum_orecs());
}
#endif /* STATS */
/* ------------------------------------------------------------------ */
void stm_print_stats() {
#if defined(STATS)
  long num;
  FAA(num,&stats_version,1);
  char outfile[100];
  unsigned stats[NUM_STATS];
  unsigned perstats[NUM_STATS];
  int i,j;
  for (j=0;j<NUM_STATS;j++)
    stats[j] = 0;
  for (i=0;i<num_txns;i++) {
    //    sprintf(outfile,"stm_stats_tag%02d_txn%02d.out",num,i+1);
    sprintf(outfile,"stm_stats_tag%02d_txn_%s_%d.out",
	    num,&txn_src[i].fname[0],txn_src[i].line);
    FILE * fd = fopen(outfile,"w");
    for (j=0;j<NUM_STATS;j++) {
      perstats[j] = 0;
    }
    desc_t * desc;
    for (desc = (desc_t *) desc_list;
	 desc;
	 desc = desc->next_desc) {
      for (j=0;j<NUM_STATS;j++) {
	stats[j] += desc->stats->stats[i][j];
	perstats[j] += desc->stats->stats[i][j];
      }
    }
    fprintf(fd,"Transaction at %s:%d *******\n",
	    &txn_src[i].fname[0],txn_src[i].line);
    stm_print_raw_stats(fd,perstats);
    stm_print_derivative_stats(fd,perstats);
    if ((more_txns) && (i+1 == MAX_TXNS)) {
      fprintf(fd,"WARNING: Represents multiple transactions\n");
      fprintf(fd,"\tIncrease MAX_TXNS for more accuracy\n");
    }
    fclose(fd);
  }
  // aggregate stats
  {
    sprintf(outfile,"stm_stats_tag%02d_all.out",num);
    FILE * fd = fopen(outfile,"w");
    stm_print_raw_stats(fd,stats);
    stm_print_derivative_stats(fd,stats);
    stm_print_aggregate_stats(fd);
    //stm_time_prof_out(fd);
    fclose(fd);
  }
#endif /* STATS */
}
/* ------------------------------------------------------------------ */
void stm_stats_out() {
  stm_print_stats();
}
/* ------------------------------------------------------------------ */
void stm_retire() {
  STM_PRINT_EVENT_COUNTS();
#ifdef STATS
  stm_print_stats();
#endif
}
/* ------------------------------------------------------------------ */
#endif /* !INLINED */
/* ------------------------------------------------------------------ */
#ifdef KEYBASED
#undef KEYBASED
#endif
#include "stm_rw.h"
#define KEYBASED
#include "stm_rw.h"
/* ------------------------------------------------------------------ */
#endif /* CSTM_C */

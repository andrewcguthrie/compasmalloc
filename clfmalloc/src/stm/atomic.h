/*
 * (c) Copyright 2008, IBM Corporation.
 *
 *	Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/*********************************************************************
 * Author: Maged Michael <magedm@us.ibm.com>
 *********************************************************************/
#ifndef ATOMIC_H
#define ATOMIC_H
/*--------------------------------------------------------------------*/
#if defined(X86) || defined(X64) || defined(SPARC32)
#define ret_t		char
#define ret32_t		char
#define ret64_t		char
#else
#define ret_t 		long
#define ret32_t 	int
#define ret64_t 	long long
#endif
#define rc_ret_t	ret32_t
/*--------------------------------------------------------------------*/
#if defined(PPC64) || defined(PPC32) /* PPC */

#define RBR
#define RBW
#define WBW
#define WBR

//#ifdef __GNUC__
#ifndef MC_FUNC

#define ACQFENCE(addr) \
	asm volatile ("junk %0,0;":"r"(addr))

#define ISYNC		asm volatile ("isync")
#define LWSYNC		asm volatile ("lwsync")
#define HWSYNC		asm volatile ("sync")

#define LL64(ret,addr) \
	asm volatile ("ldarx %0,0,%1;":"=r"(ret):"r"(addr))
#define LL32(ret,addr) \
	asm volatile ("lwarx %0,0,%1;":"=r"(ret):"r"(addr))
#define SC64(ret,addr,newval) \
 asm volatile ( \
	"  stdcx. %2,0,%1;\n" \
 	"  mfcr %0;\n" \
 	"  andis. %0,%0,0x2000;" \
	: "=r"(ret):"r"(addr),"r"(newval) \
        : "cr0","memory")
#define SC32(ret,addr,newval) \
 asm volatile ( \
	"  stwcx. %2,0,%1;\n" \
 	"  mfcr %0;\n" \
 	"  andis. %0,%0,0x2000;" \
	: "=r"(ret):"r"(addr),"r"(newval) \
        : "cr0","memory")

#else // MC_FUNC

#pragma mc_func ISYNC	{ "4c00012c" }
#pragma reg_killed_by ISYNC

#pragma mc_func LWSYNC	{ "7c2004ac" }
#pragma reg_killed_by LWSYNC

#pragma mc_func HWSYNC	{ "7c0004ac" }
#pragma reg_killed_by HWSYNC

long long ll64(void *);
#pragma mc_func ll64	{ "7c6018a8" } // ldarx r3,0,r3
#pragma reg_killed_by ll64 gr3
#define LL64(ret,addr)	(long long)ret = ll64((void *)addr)

int ll32(void *);
#pragma mc_func ll32	{ "7c601828" } // lwarx r3,0,r3
#pragma reg_killed_by ll32 gr3
#define LL32(ret,addr)	(int)ret = ll32((void *)addr)

long long sc64(long long , void *);
#pragma mc_func sc64	{ "7c6021ad" "7c600026" "74632000" }
// stdcx r3,0,r4; mfcr r3; andis r3,r3,0x2000
#pragma reg_killed_by sc64 gr3, cr0
#define SC64(ret,addr,newval) \
	(long long)ret = sc64((long long)newval,(void *)addr)

int sc32(int, void *);
#pragma mc_func sc32	{ "7c60212d" "7c600026" "74632000" }
// stwcx r3,0,r4; mfcr r3; andis r3,r3,0x2000
#pragma reg_killed_by sc32 gr3, cr0
#define SC32(ret,addr,newval) \
	(int)ret = sc32((int)newval,(void *)addr)

#endif // MC_FUNC

#ifdef GCC

#define GET_PC(pc) \
	asm ("mflr %0;":"=r"(pc))

#else /* !__GNUC__ */

long get_pc();
#pragma mc_func get_pc  { "7c6042a6" } // mfspr r3,8
#pragma reg_killed_by get_pc gr3
#define GET_PC(pc)	pc = get_pc()

#endif /* !__GNUC__ */

static inline int cas64(long long volatile * addr,
			long long expval,
			long long newval) {
  long long ret;
  do {
    long long oldval;
    LL64(oldval,addr);
    if (oldval != expval)
      return 0;
    SC64(ret,addr,newval);
  } while (!ret);
  return 1;
}

static inline int cas32(int volatile * addr,int expval,int newval) {
  int ret;
  do {
    unsigned cas_oldval;
    LL32(cas_oldval,addr);
    if (cas_oldval != (unsigned)expval)
      return 0;
    SC32(ret,addr,newval);
  } while (!ret);
  return 1;
}

#ifdef PPC64

#define LL(ret,addr)			LL64(ret,addr)
#define SC(ret,addr,newval)		SC64(ret,addr,newval)

#else /* PPC32 */

#define LL(ret,addr)			LL32(ret,addr)
#define SC(ret,addr,newval)		SC32(ret,addr,newval)

#endif

#elif defined(X86)

static inline int cas32(int volatile * addr,
			int expval,
			int newval) {
  char ret;
  __asm__ __volatile__(
    "lock; cmpxchg %2, %1; setz %0;\n" 
    : "=a"(ret), "=m"(*(addr)) 
    : "r" (newval), "a"(expval) 
    : "cc");
  return ret;
}

#define ISYNC
#define LWSYNC
#define HWSYNC
//		asm volatile ("mfence")
#define RBR
#define RBW
#define WBW
#define WBR     asm volatile ("mfence")

#define GET_PC(pc) \
	pc = 0xc0dec0de // fake pc - obsolete

#ifdef X86

static inline int cas64(long long volatile * addr,
			 long long expval,
			 long long newval) {
  long long oldval;
  __asm__ __volatile__( 
     "lock; cmpxchg8b %3"
     : "=A"(oldval)
     : "b"((int)newval),
     "c"((int)(newval >> 32)),
     "m"(*(volatile long long *)(addr)),
     "0"(expval)
     : "memory");
  return (oldval == expval);
}

#else /* X64 */

static inline int cas64(long long volatile * addr,
			 long long expval,
			 long long newval) {
  char ret;
  __asm__ __volatile__(
    "lock; cmpxchg %2, %1; setz %0;\n" 
    : "=a"(ret), "=m"(*(addr)) 
    : "r" (newval), "a"(expval) 
    : "cc");
  return ret;
}

#endif /* X86 v X64 */

#elif defined(SPARC32)

static inline int cas32(unsigned volatile * addr,
		 unsigned expval,
		 unsigned newval) {
  asm volatile (
	 "casa  %0 0x80, %2, %1\n"
       : "+m"(*(addr)), "+r"(newval)
       : "r" (expval)
       );
  return (newval == expval);
}

#define ISYNC
#define LWSYNC
#define RBR	asm volatile ("membar #LoadLoad")
#define RBW	asm volatile ("membar #LoadStore")
#define WBW	asm volatile ("membar #StoreStore")

#else
#error architecture not defined
#endif /* architecture */
/*--------------------------------------------------------------------*/
#define CAS32(addr,expval,newval) \
	cas32((int *)(addr),(int)(expval),(int)(newval))
#define CAS64(addr,expval,newval) \
	cas64((long long *)(addr),(long long)(expval),(long long)(newval))
/*--------------------------------------------------------------------*/
#if defined(BIT64)
#define CAS(addr,expval,newval)	CAS64(addr,expval,newval)
#else // 32-bit
#define CAS(addr,expval,newval)	CAS32(addr,expval,newval)
#endif // 64-bit v 32-bit
/*--------------------------------------------------------------------*/
#define FAA(oldval,addr,val) \
 do { \
    long newval; \
    oldval = *(addr); \
    newval = oldval + val; \
    int ret = CAS(addr,oldval,newval); \
    if (ret) break; \
 } while (1);
/*--------------------------------------------------------------------*/
static void tts_acq(volatile long * lock) {
  while (1) {
    while (*lock);
    int ret = CAS(lock,0,1);
    if (ret)
      break;
  }
  ISYNC;
}
/*--------------------------------------------------------------------*/
static void tts_rel(volatile long * lock) {
  LWSYNC;
  *lock = 0;
}
/* ------------------------------------------------------------------ */
#endif /* ATOMIC_H */

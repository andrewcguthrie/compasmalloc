/*
 * (c) Copyright 2008, IBM Corporation.
 *
 *	Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/*-------------------------------------------------------------------*/
/* Auto                                                              */
#define READ_ONLY_COMMITS        	0
#define READ_WRITE_COMMITS       	1
#define READ_LIST_SIZES          	2
#define WRITE_LIST_SIZES         	3
#define READ_SET_SIZES           	4
#define WRITE_SET_SIZES          	5
#define NUM_STM_DESC_CALLS       	6
#define READ_SET_VALIDATIONS     	7
#define READ_ENCOUNTER_RETRIES   	8
#define SIGNAL_RETRIES           	9
#define WRITE_ACQUIRE_RETRIES    	10
#define READ_VALIDATION_RETRIES  	11
#define WRITE_BARRIERS           	12
#define NUM_SILENT_WRITES        	13
#define SILENT_WRITES_BECAME_READS	14
#define WRITE_BARRIERS_OUTSIDE_TXNS	15
#define WRITE_BARRIERS_FOR_STACK 	16
#define DUPLICATE_WRITES         	17
#define DUPLICATE_WRITE_CONFLICT_SET	18
#define READ_BARRIERS            	19
#define READ_BARRIERS_OUTSIDE_TXNS	20
#define READ_BARRIERS_FOR_STACK  	21
#define DUPLICATE_READS          	22
#define DUPLICATE_READ_CONFLICT_SET	23
#define BLOOM_FILTER_CHECKS      	24
#define BLOOM_FILTER_MATCHES     	25
#define READ_AFTER_WRITE_MATCHES 	26
#define STATUS_CHECK_ACTIVE      	27
#define STATUS_CHECK_INACTIVE    	28
#define CHECKPOINTING_CALLS      	29
#define ALIASING_INSTANCES       	30
#define ADDRESS_TAKEN_VARIABLES  	31
#define LOCK_ACQUIRE             	32
#define LOCK_SKIP                	33
#define LOCK_RELEASE             	34
#define LOCK_UNDO                	35
#define NUM_MALLOCS              	36
#define NUM_FREES                	37
#define NUM_FREE_PRIVATE         	38
#define NUM_STAT_TOTALS          	39
#define READ_LIST_MAX_SIZE       	39
#define WRITE_LIST_MAX_SIZE      	40
#define READ_SET_MAX_SIZE        	41
#define WRITE_SET_MAX_SIZE       	42
#define MAX_NESTING              	43
#define NUM_STATS                	44
/*-------------------------------------------------------------------*/
#if defined(STATS)
static void stm_print_raw_stats(FILE * fd,unsigned * stats) {
  fprintf(fd,"%8d\t%s\n",stats[0],"READ_ONLY_COMMITS");
  fprintf(fd,"%8d\t%s\n",stats[1],"READ_WRITE_COMMITS");
  fprintf(fd,"%8d\t%s\n",stats[2],"READ_LIST_SIZES");
  fprintf(fd,"%8d\t%s\n",stats[3],"WRITE_LIST_SIZES");
  fprintf(fd,"%8d\t%s\n",stats[4],"READ_SET_SIZES");
  fprintf(fd,"%8d\t%s\n",stats[5],"WRITE_SET_SIZES");
  fprintf(fd,"%8d\t%s\n",stats[6],"NUM_STM_DESC_CALLS");
  fprintf(fd,"%8d\t%s\n",stats[7],"READ_SET_VALIDATIONS");
  fprintf(fd,"%8d\t%s\n",stats[8],"READ_ENCOUNTER_RETRIES");
  fprintf(fd,"%8d\t%s\n",stats[9],"SIGNAL_RETRIES");
  fprintf(fd,"%8d\t%s\n",stats[10],"WRITE_ACQUIRE_RETRIES");
  fprintf(fd,"%8d\t%s\n",stats[11],"READ_VALIDATION_RETRIES");
  fprintf(fd,"%8d\t%s\n",stats[12],"WRITE_BARRIERS");
  fprintf(fd,"%8d\t%s\n",stats[13],"NUM_SILENT_WRITES");
  fprintf(fd,"%8d\t%s\n",stats[14],"SILENT_WRITES_BECAME_READS");
  fprintf(fd,"%8d\t%s\n",stats[15],"WRITE_BARRIERS_OUTSIDE_TXNS");
  fprintf(fd,"%8d\t%s\n",stats[16],"WRITE_BARRIERS_FOR_STACK");
  fprintf(fd,"%8d\t%s\n",stats[17],"DUPLICATE_WRITES");
  fprintf(fd,"%8d\t%s\n",stats[18],"DUPLICATE_WRITE_CONFLICT_SET");
  fprintf(fd,"%8d\t%s\n",stats[19],"READ_BARRIERS");
  fprintf(fd,"%8d\t%s\n",stats[20],"READ_BARRIERS_OUTSIDE_TXNS");
  fprintf(fd,"%8d\t%s\n",stats[21],"READ_BARRIERS_FOR_STACK");
  fprintf(fd,"%8d\t%s\n",stats[22],"DUPLICATE_READS");
  fprintf(fd,"%8d\t%s\n",stats[23],"DUPLICATE_READ_CONFLICT_SET");
  fprintf(fd,"%8d\t%s\n",stats[24],"BLOOM_FILTER_CHECKS");
  fprintf(fd,"%8d\t%s\n",stats[25],"BLOOM_FILTER_MATCHES");
  fprintf(fd,"%8d\t%s\n",stats[26],"READ_AFTER_WRITE_MATCHES");
  fprintf(fd,"%8d\t%s\n",stats[27],"STATUS_CHECK_ACTIVE");
  fprintf(fd,"%8d\t%s\n",stats[28],"STATUS_CHECK_INACTIVE");
  fprintf(fd,"%8d\t%s\n",stats[29],"CHECKPOINTING_CALLS");
  fprintf(fd,"%8d\t%s\n",stats[30],"ALIASING_INSTANCES");
  fprintf(fd,"%8d\t%s\n",stats[31],"ADDRESS_TAKEN_VARIABLES");
  fprintf(fd,"%8d\t%s\n",stats[32],"LOCK_ACQUIRE");
  fprintf(fd,"%8d\t%s\n",stats[33],"LOCK_SKIP");
  fprintf(fd,"%8d\t%s\n",stats[34],"LOCK_RELEASE");
  fprintf(fd,"%8d\t%s\n",stats[35],"LOCK_UNDO");
  fprintf(fd,"%8d\t%s\n",stats[36],"NUM_MALLOCS");
  fprintf(fd,"%8d\t%s\n",stats[37],"NUM_FREES");
  fprintf(fd,"%8d\t%s\n",stats[38],"NUM_FREE_PRIVATE");
  fprintf(fd,"%8d\t%s\n",stats[39],"READ_LIST_MAX_SIZE");
  fprintf(fd,"%8d\t%s\n",stats[40],"WRITE_LIST_MAX_SIZE");
  fprintf(fd,"%8d\t%s\n",stats[41],"READ_SET_MAX_SIZE");
  fprintf(fd,"%8d\t%s\n",stats[42],"WRITE_SET_MAX_SIZE");
  fprintf(fd,"%8d\t%s\n",stats[43],"MAX_NESTING");
}
#endif /* STATS */
/*-------------------------------------------------------------------*/

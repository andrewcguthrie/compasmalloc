/*
 * (c) Copyright 2008, IBM Corporation.
 *
 *	Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/*--------------------------------------------------------------------*/
/* stm.h - author: Maged Michael                                      */
/*--------------------------------------------------------------------*/
#ifndef STM_H
#define STM_H
/*--------------------------------------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif
/*--------------------------------------------------------------------*/
#include <stdlib.h>
#include <setjmp.h>
/*--------------------------------------------------------------------*/
// Default options
#define GLOBAL_VERSION
#define INC_VALIDATION
#define SIGNAL
#define DESC_TLS
/*--------------------------------------------------------------------*/
#if defined(NOGLOBAL_VERSION)
#undef GLOBAL_VERSION
#endif
#if defined(NOINC_VALIDATION)
#undef INC_VALIDATION
#endif
#if defined(NODESC_TLS)
#undef DESC_TLS
#endif
#if defined(NOSIGNAL)
#undef SIGNAL
#endif
/*--------------------------------------------------------------------*/
#if defined(STATS) && !defined(DEBUG)
#define DEBUG
#endif
/*--------------------------------------------------------------------*/
#if defined(DEBUG) && !defined(INC_VALIDATION)
#define INC_VALIDATION
#endif
#if defined(DEBUG) && defined(SIGNAL)
#undef SIGNAL
#endif
/*--------------------------------------------------------------------*/
#if !defined(INC_VALIDATION) && !defined(SIGNAL)
#define SIGNAL
#endif
/*--------------------------------------------------------------------*/
#ifdef INLINED
#define INLINE static inline
#define NOINLINE __attribute__((noinline)) static
#else /* !INLINED */
#define INLINE
#define NOINLINE static
#endif /* !INLINED */
/*--------------------------------------------------------------------*/
#ifndef SAVESIGMASK
#define SAVEMASK	0
#else
#define SAVEMASK	1
#endif
/*--------------------------------------------------------------------*/
#ifndef NOSETJMP
#ifdef SIGNAL
#define SETJMP(buf)	sigsetjmp(buf,SAVEMASK)
#define LONGJMP		siglongjmp
#else /* SIGNAL */
#define SETJMP(buf)	setjmp(buf)
#define LONGJMP 	longjmp
#endif /* SIGNAL */
#else
#define SETJMP(buf)
#define LONGJMP		longjmp
#endif
/*--------------------------------------------------------------------*/
#define RESET_VALIDATION
#define VALIDATE_OREC(orec)	false
#define FAST_VALIDATION		false
#define EVENT_START(e)
#define EVENT_END(e)
#define STM_PRINT_EVENT_COUNTS()
#define ASSERT_META_SIZE
/*--------------------------------------------------------------------*/
#define STM_CONFLICT(desc)	stm_retry(desc)
/*--------------------------------------------------------------------*/
#define STM_VALIDATE()		stm_validate(__stm_desc)
/*--------------------------------------------------------------------*/
#define STM_ASSERT(x) \
	{ if (!(x)) { STM_SET_DESC(); STM_VALIDATE(); assert(x); }}
/*--------------------------------------------------------------------*/
void * stm_thr_init();
void stm_thr_retire();
/*--------------------------------------------------------------------*/
INLINE int stm_in_transaction();
INLINE void * stm_desc(); 
INLINE void stm_validate(void * mydesc);
/*--------------------------------------------------------------------*/
#define STM_SET_DESC()	void * __stm_desc = stm_desc()
/*--------------------------------------------------------------------*/
#define STM_BEGIN_CALL() \
	stm_begin(_jmpbuf,__stm_desc,__FILE__,__LINE__)
/*--------------------------------------------------------------------*/
#define STM_BEGIN \
  { \
    jmp_buf _jmpbuf; \
    SETJMP(_jmpbuf); \
    STM_BEGIN_CALL();
/*--------------------------------------------------------------------*/
#define STM_END \
    stm_end(__stm_desc); \
  }
/*--------------------------------------------------------------------*/
INLINE void stm_begin(jmp_buf buf,void * mydesc,char * fname,int line);
INLINE void stm_end(void * mydesc);
/*--------------------------------------------------------------------*/
#define STM_MALLOC(sz)		stm_malloc(sz,__stm_desc)
#define STM_FREE(ptr)		stm_free(ptr,__stm_desc)
#define STM_CALLOC(ne,sz)	stm_calloc(ne,sz,__stm_desc)
#define STM_CHECKPOINT(lval)	stm_checkpoint((char *)(&(lval)),sizeof(lval),__stm_desc)
/*--------------------------------------------------------------------*/
INLINE void * stm_malloc(size_t sz,void * mydesc);
INLINE void stm_free(void * ptr,void * mydesc);
INLINE void * stm_calloc(size_t ne,size_t sz,void * mydesc);
INLINE void stm_checkpoint(char * addr, int size,void * mydesc);
INLINE void stm_stack_range(void * addr, int size, void *mydesc);
/*--------------------------------------------------------------------*/
#define STM_READ_P(lval)	stm_read_ptr((void **)(void *)&(lval),__stm_desc)
#define STM_READ_F(lval)	stm_read_float((float *)&(lval),__stm_desc)
#define STM_READ_D(lval)	stm_read_double((double *)&(lval),__stm_desc)
#define STM_READ_C(lval)	stm_read_char((char *)&(lval),__stm_desc)
#define STM_READ_S(lval)	stm_read_short((short *)&(lval),__stm_desc)
#define STM_READ_I(lval)	stm_read_int((int *)&(lval),__stm_desc)
#define STM_READ_L(lval)	stm_read_long((long *)&(lval),__stm_desc)
#define STM_READ_LL(lval)	stm_read_ll((long long *)&(lval),__stm_desc)
#define STM_READ_UC(lval)	stm_read_uchar((unsigned char *)&(lval),__stm_desc)
#define STM_READ_US(lval)	stm_read_ushort((unsigned short *)&(lval),__stm_desc)
#define STM_READ_UI(lval)	stm_read_uint((unsigned int *)&(lval),__stm_desc)
#define STM_READ_UL(lval)	stm_read_ulong((unsigned long *)&(lval),__stm_desc)
#define STM_READ_ULL(lval)	stm_read_ull((unsigned long long *)&(lval),__stm_desc)
/*--------------------------------------------------------------------*/
#define STM_WRITE_P(lval,rval)	stm_write_ptr((void **)(void *)&(lval),(void *)(rval),__stm_desc)
#define STM_WRITE_F(lval,rval)	stm_write_float((float *)&(lval),rval,__stm_desc)
#define STM_WRITE_D(lval,rval)	stm_write_double((double *)&(lval),rval,__stm_desc)
#define STM_WRITE_C(lval,rval)	stm_write_char((char *)&(lval),rval,__stm_desc)
#define STM_WRITE_S(lval,rval)	stm_write_short((short *)&(lval),rval,__stm_desc)
#define STM_WRITE_I(lval,rval)	stm_write_int((int *)&(lval),rval,__stm_desc)
#define STM_WRITE_L(lval,rval)	stm_write_long((long *)&(lval),rval,__stm_desc)
#define STM_WRITE_LL(lval,rval)	stm_write_ll((long long *)&(lval),rval,__stm_desc)
#define STM_WRITE_UC(lval,rval)	stm_write_uchar((unsigned char *)&(lval),rval,__stm_desc)
#define STM_WRITE_US(lval,rval)	stm_write_ushort((unsigned short *)&(lval),rval,__stm_desc)
#define STM_WRITE_UI(lval,rval)	stm_write_uint((unsigned int *)&(lval),rval,__stm_desc)
#define STM_WRITE_UL(lval,rval)	stm_write_ulong((unsigned long *)&(lval),rval,__stm_desc)
#define STM_WRITE_ULL(lval,rval) stm_write_ull((unsigned long long *)&(lval),rval,__stm_desc)
/*--------------------------------------------------------------------*/
INLINE void * stm_read_ptr(void * volatile * addr,void * mydesc);
INLINE float stm_read_float(float volatile * addr,void * mydesc);
INLINE double stm_read_double(double volatile * addr,void * mydescy);
INLINE char stm_read_char(char volatile * addr,void * mydesc);
INLINE short stm_read_short(short volatile * addr,void * mydesc);
INLINE int stm_read_int(int volatile * addr,void * mydesc);
INLINE long stm_read_long(long volatile * addr,void * mydesc);
INLINE long long stm_read_ll(long long volatile * addr,void * mydesc);
INLINE unsigned char stm_read_uchar(unsigned char volatile * addr,void * mydesc);
INLINE unsigned short stm_read_ushort(unsigned short volatile * addr,void * mydesc);
INLINE unsigned int stm_read_uint(unsigned int volatile * addr,void * mydesc);
INLINE unsigned long stm_read_ulong(unsigned long volatile * addr,void * mydesc);
INLINE unsigned long long stm_read_ull(unsigned long long volatile * addr,void * mydesc);
/*--------------------------------------------------------------------*/
INLINE void stm_write_ptr(void * volatile * addr, void * val,void * mydesc);
INLINE void stm_write_float(float volatile * addr, float val,void * mydesc);
INLINE void stm_write_double(double volatile * addr, double val,void * mydesc);
INLINE void stm_write_char(char volatile * addr, char val,void * mydesc);
INLINE void stm_write_short(short volatile * addr, short val,void * mydesc);
INLINE void stm_write_int(int volatile * addr, int val,void * mydesc);
INLINE void stm_write_long(long volatile * addr, long val,void * mydesc);
INLINE void stm_write_ll(long long volatile * addr, long long val,void * mydesc);
INLINE void stm_write_uchar(unsigned char volatile * addr, unsigned char val,void * mydesc);
INLINE void stm_write_ushort(unsigned short volatile * addr, unsigned short val,void * mydesc);
INLINE void stm_write_uint(unsigned int volatile * addr, unsigned int val,void * mydesc);
INLINE void stm_write_ulong(unsigned long volatile * addr, unsigned long val,void * mydesc);
INLINE void stm_write_ull(unsigned long long volatile * addr, unsigned long long val,void * mydesc);
/*--------------------------------------------------------------------*/
INLINE void * stm_read_key_ptr(void * volatile * addr,void * key,void * mydesc);
INLINE float stm_read_key_float(float volatile * addr,void * key,void * mydesc);
INLINE double stm_read_key_double(double volatile * addr,void * key,void * mydescy);
INLINE char stm_read_key_char(char volatile * addr,void * key,void * mydesc);
INLINE short stm_read_key_short(short volatile * addr,void * key,void * mydesc);
INLINE int stm_read_key_int(int volatile * addr,void * key,void * mydesc);
INLINE long stm_read_key_long(long volatile * addr,void * key,void * mydesc);
INLINE long long stm_read_key_ll(long long volatile * addr,void * key,void * mydesc);
INLINE unsigned char stm_read_key_uchar(unsigned char volatile * addr,void * key,void * mydesc);
INLINE unsigned short stm_read_key_ushort(unsigned short volatile * addr,void * key,void * mydesc);
INLINE unsigned int stm_read_key_uint(unsigned int volatile * addr,void * key,void * mydesc);
INLINE unsigned long stm_read_key_ulong(unsigned long volatile * addr,void * key,void * mydesc);
INLINE unsigned long long stm_read_key_ull(unsigned long long volatile * addr,void * key,void * mydesc);
/*--------------------------------------------------------------------*/
INLINE void stm_write_key_ptr(void * volatile * addr,void * key, void * val,void * mydesc);
INLINE void stm_write_key_float(float volatile * addr,void * key, float val,void * mydesc);
INLINE void stm_write_key_double(double volatile * addr,void * key, double val,void * mydesc);
INLINE void stm_write_key_char(char volatile * addr,void * key, char val,void * mydesc);
INLINE void stm_write_key_short(short volatile * addr,void * key, short val,void * mydesc);
INLINE void stm_write_key_int(int volatile * addr,void * key, int val,void * mydesc);
INLINE void stm_write_key_long(long volatile * addr,void * key, long val,void * mydesc);
INLINE void stm_write_key_ll(long long volatile * addr,void * key, long long val,void * mydesc);
INLINE void stm_write_key_uchar(unsigned char volatile * addr,void * key, unsigned char val,void * mydesc);
INLINE void stm_write_key_ushort(unsigned short volatile * addr,void * key, unsigned short val,void * mydesc);
INLINE void stm_write_key_uint(unsigned int volatile * addr,void * key, unsigned int val,void * mydesc);
INLINE void stm_write_key_ulong(unsigned long volatile * addr,void * key, unsigned long val,void * mydesc);
INLINE void stm_write_key_ull(unsigned long long volatile * addr,void * key, unsigned long long val,void * mydesc);
/*--------------------------------------------------------------------*/
void stm_copy_block(char volatile * to, char volatile * from, int count, void * mydesc);
void stm_read_block(char * to, char volatile * from, int count, void * mydesc);
void stm_write_block(char volatile * to, char * from, int count, void * mydesc);
void stm_set_block(char volatile * addr, int c, int count, void * mydesc);
/*--------------------------------------------------------------------*/
void stm_retry(void * mydesc);
/* ------------------------------------------------------------------ */
void stm_retire();
void stm_print_time_prof();
void stm_stats_out();
void stm_print_stats();
/* ------------------------------------------------------------------ */
#ifdef INLINED
#include "stm.c"
#endif
/*--------------------------------------------------------------------*/
#ifdef __cplusplus
}
#endif
/*--------------------------------------------------------------------*/
#endif /* STM_H */
/*--------------------------------------------------------------------*/

/*
 * (c) Copyright 2008, IBM Corporation.
 *
 *	Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/*-------------------------------------------------------------------*/
/* Auto                                                              */
/*-------------------------------------------------------------------*/
#if defined(KEYBASED)
INLINE void * stm_read_key_ptr(void * volatile * addr, void * key, void * mydesc) {
  desc_t * my = (desc_t *)mydesc;
#else
INLINE void * stm_read_ptr(void * volatile * addr, void * mydesc) {
  desc_t * my = (desc_t *) mydesc;
  void * key = (void *)addr;
#endif
  void * val;
  void * a = (void *) addr;
  stm_read_any(my,a,key,&val,sizeof(val));
  return val;
}
/*-------------------------------------------------------------------*/
#ifdef KEYBASED
INLINE void stm_write_key_ptr(void * volatile * addr, void * key, void * val, void * mydesc) {
  desc_t * my = (desc_t *)mydesc;
#else
INLINE void stm_write_ptr(void * volatile * addr, void * val, void * mydesc) {
  desc_t * my = (desc_t *)mydesc;
  void * key = (void *)addr;
#endif
  void * a = (void *) addr;
  stm_write_any(my,a,key,(void *)&val,sizeof(val)); return;
}
/*-------------------------------------------------------------------*/
#if defined(KEYBASED)
INLINE float stm_read_key_float(float volatile * addr, void * key, void * mydesc) {
  desc_t * my = (desc_t *)mydesc;
#else
INLINE float stm_read_float(float volatile * addr, void * mydesc) {
  desc_t * my = (desc_t *) mydesc;
  void * key = (void *)addr;
#endif
  float val;
  void * a = (void *) addr;
  stm_read_any(my,a,key,&val,sizeof(val));
  return val;
}
/*-------------------------------------------------------------------*/
#ifdef KEYBASED
INLINE void stm_write_key_float(float volatile * addr, void * key, float val, void * mydesc) {
  desc_t * my = (desc_t *)mydesc;
#else
INLINE void stm_write_float(float volatile * addr, float val, void * mydesc) {
  desc_t * my = (desc_t *)mydesc;
  void * key = (void *)addr;
#endif
  void * a = (void *) addr;
  stm_write_any(my,a,key,(void *)&val,sizeof(val)); return;
}
/*-------------------------------------------------------------------*/
#if defined(KEYBASED)
INLINE double stm_read_key_double(double volatile * addr, void * key, void * mydesc) {
  desc_t * my = (desc_t *)mydesc;
#else
INLINE double stm_read_double(double volatile * addr, void * mydesc) {
  desc_t * my = (desc_t *) mydesc;
  void * key = (void *)addr;
#endif
  double val;
  void * a = (void *) addr;
  stm_read_any(my,a,key,&val,sizeof(val));
  return val;
}
/*-------------------------------------------------------------------*/
#ifdef KEYBASED
INLINE void stm_write_key_double(double volatile * addr, void * key, double val, void * mydesc) {
  desc_t * my = (desc_t *)mydesc;
#else
INLINE void stm_write_double(double volatile * addr, double val, void * mydesc) {
  desc_t * my = (desc_t *)mydesc;
  void * key = (void *)addr;
#endif
  void * a = (void *) addr;
  stm_write_any(my,a,key,(void *)&val,sizeof(val)); return;
}
/*-------------------------------------------------------------------*/
#if defined(KEYBASED)
INLINE char stm_read_key_char(char volatile * addr, void * key, void * mydesc) {
  desc_t * my = (desc_t *)mydesc;
#else
INLINE char stm_read_char(char volatile * addr, void * mydesc) {
  desc_t * my = (desc_t *) mydesc;
  void * key = (void *)addr;
#endif
  char val;
  void * a = (void *) addr;
  stm_read_any(my,a,key,&val,sizeof(val));
  return val;
}
/*-------------------------------------------------------------------*/
#ifdef KEYBASED
INLINE void stm_write_key_char(char volatile * addr, void * key, char val, void * mydesc) {
  desc_t * my = (desc_t *)mydesc;
#else
INLINE void stm_write_char(char volatile * addr, char val, void * mydesc) {
  desc_t * my = (desc_t *)mydesc;
  void * key = (void *)addr;
#endif
  void * a = (void *) addr;
  stm_write_any(my,a,key,(void *)&val,sizeof(val)); return;
}
/*-------------------------------------------------------------------*/
#if defined(KEYBASED)
INLINE short stm_read_key_short(short volatile * addr, void * key, void * mydesc) {
  desc_t * my = (desc_t *)mydesc;
#else
INLINE short stm_read_short(short volatile * addr, void * mydesc) {
  desc_t * my = (desc_t *) mydesc;
  void * key = (void *)addr;
#endif
  short val;
  void * a = (void *) addr;
  stm_read_any(my,a,key,&val,sizeof(val));
  return val;
}
/*-------------------------------------------------------------------*/
#ifdef KEYBASED
INLINE void stm_write_key_short(short volatile * addr, void * key, short val, void * mydesc) {
  desc_t * my = (desc_t *)mydesc;
#else
INLINE void stm_write_short(short volatile * addr, short val, void * mydesc) {
  desc_t * my = (desc_t *)mydesc;
  void * key = (void *)addr;
#endif
  void * a = (void *) addr;
  stm_write_any(my,a,key,(void *)&val,sizeof(val)); return;
}
/*-------------------------------------------------------------------*/
#if defined(KEYBASED)
INLINE int stm_read_key_int(int volatile * addr, void * key, void * mydesc) {
  desc_t * my = (desc_t *)mydesc;
#else
INLINE int stm_read_int(int volatile * addr, void * mydesc) {
  desc_t * my = (desc_t *) mydesc;
  void * key = (void *)addr;
#endif
  int val;
  void * a = (void *) addr;
  stm_read_any(my,a,key,&val,sizeof(val));
  return val;
}
/*-------------------------------------------------------------------*/
#ifdef KEYBASED
INLINE void stm_write_key_int(int volatile * addr, void * key, int val, void * mydesc) {
  desc_t * my = (desc_t *)mydesc;
#else
INLINE void stm_write_int(int volatile * addr, int val, void * mydesc) {
  desc_t * my = (desc_t *)mydesc;
  void * key = (void *)addr;
#endif
  void * a = (void *) addr;
  stm_write_any(my,a,key,(void *)&val,sizeof(val)); return;
}
/*-------------------------------------------------------------------*/
#if defined(KEYBASED)
INLINE long stm_read_key_long(long volatile * addr, void * key, void * mydesc) {
  desc_t * my = (desc_t *)mydesc;
#else
INLINE long stm_read_long(long volatile * addr, void * mydesc) {
  desc_t * my = (desc_t *) mydesc;
  void * key = (void *)addr;
#endif
  long val;
  void * a = (void *) addr;
  stm_read_any(my,a,key,&val,sizeof(val));
  return val;
}
/*-------------------------------------------------------------------*/
#ifdef KEYBASED
INLINE void stm_write_key_long(long volatile * addr, void * key, long val, void * mydesc) {
  desc_t * my = (desc_t *)mydesc;
#else
INLINE void stm_write_long(long volatile * addr, long val, void * mydesc) {
  desc_t * my = (desc_t *)mydesc;
  void * key = (void *)addr;
#endif
  void * a = (void *) addr;
  stm_write_any(my,a,key,(void *)&val,sizeof(val)); return;
}
/*-------------------------------------------------------------------*/
#if defined(KEYBASED)
INLINE long long stm_read_key_ll(long long volatile * addr, void * key, void * mydesc) {
  desc_t * my = (desc_t *)mydesc;
#else
INLINE long long stm_read_ll(long long volatile * addr, void * mydesc) {
  desc_t * my = (desc_t *) mydesc;
  void * key = (void *)addr;
#endif
  long long val;
  void * a = (void *) addr;
  stm_read_any(my,a,key,&val,sizeof(val));
  return val;
}
/*-------------------------------------------------------------------*/
#ifdef KEYBASED
INLINE void stm_write_key_ll(long long volatile * addr, void * key, long long val, void * mydesc) {
  desc_t * my = (desc_t *)mydesc;
#else
INLINE void stm_write_ll(long long volatile * addr, long long val, void * mydesc) {
  desc_t * my = (desc_t *)mydesc;
  void * key = (void *)addr;
#endif
  void * a = (void *) addr;
  stm_write_any(my,a,key,(void *)&val,sizeof(val)); return;
}
/*-------------------------------------------------------------------*/
#if defined(KEYBASED)
INLINE unsigned char stm_read_key_uchar(unsigned char volatile * addr, void * key, void * mydesc) {
  desc_t * my = (desc_t *)mydesc;
#else
INLINE unsigned char stm_read_uchar(unsigned char volatile * addr, void * mydesc) {
  desc_t * my = (desc_t *) mydesc;
  void * key = (void *)addr;
#endif
  unsigned char val;
  void * a = (void *) addr;
  stm_read_any(my,a,key,&val,sizeof(val));
  return val;
}
/*-------------------------------------------------------------------*/
#ifdef KEYBASED
INLINE void stm_write_key_uchar(unsigned char volatile * addr, void * key, unsigned char val, void * mydesc) {
  desc_t * my = (desc_t *)mydesc;
#else
INLINE void stm_write_uchar(unsigned char volatile * addr, unsigned char val, void * mydesc) {
  desc_t * my = (desc_t *)mydesc;
  void * key = (void *)addr;
#endif
  void * a = (void *) addr;
  stm_write_any(my,a,key,(void *)&val,sizeof(val)); return;
}
/*-------------------------------------------------------------------*/
#if defined(KEYBASED)
INLINE unsigned short stm_read_key_ushort(unsigned short volatile * addr, void * key, void * mydesc) {
  desc_t * my = (desc_t *)mydesc;
#else
INLINE unsigned short stm_read_ushort(unsigned short volatile * addr, void * mydesc) {
  desc_t * my = (desc_t *) mydesc;
  void * key = (void *)addr;
#endif
  unsigned short val;
  void * a = (void *) addr;
  stm_read_any(my,a,key,&val,sizeof(val));
  return val;
}
/*-------------------------------------------------------------------*/
#ifdef KEYBASED
INLINE void stm_write_key_ushort(unsigned short volatile * addr, void * key, unsigned short val, void * mydesc) {
  desc_t * my = (desc_t *)mydesc;
#else
INLINE void stm_write_ushort(unsigned short volatile * addr, unsigned short val, void * mydesc) {
  desc_t * my = (desc_t *)mydesc;
  void * key = (void *)addr;
#endif
  void * a = (void *) addr;
  stm_write_any(my,a,key,(void *)&val,sizeof(val)); return;
}
/*-------------------------------------------------------------------*/
#if defined(KEYBASED)
INLINE unsigned int stm_read_key_uint(unsigned int volatile * addr, void * key, void * mydesc) {
  desc_t * my = (desc_t *)mydesc;
#else
INLINE unsigned int stm_read_uint(unsigned int volatile * addr, void * mydesc) {
  desc_t * my = (desc_t *) mydesc;
  void * key = (void *)addr;
#endif
  unsigned int val;
  void * a = (void *) addr;
  stm_read_any(my,a,key,&val,sizeof(val));
  return val;
}
/*-------------------------------------------------------------------*/
#ifdef KEYBASED
INLINE void stm_write_key_uint(unsigned int volatile * addr, void * key, unsigned int val, void * mydesc) {
  desc_t * my = (desc_t *)mydesc;
#else
INLINE void stm_write_uint(unsigned int volatile * addr, unsigned int val, void * mydesc) {
  desc_t * my = (desc_t *)mydesc;
  void * key = (void *)addr;
#endif
  void * a = (void *) addr;
  stm_write_any(my,a,key,(void *)&val,sizeof(val)); return;
}
/*-------------------------------------------------------------------*/
#if defined(KEYBASED)
INLINE unsigned long stm_read_key_ulong(unsigned long volatile * addr, void * key, void * mydesc) {
  desc_t * my = (desc_t *)mydesc;
#else
INLINE unsigned long stm_read_ulong(unsigned long volatile * addr, void * mydesc) {
  desc_t * my = (desc_t *) mydesc;
  void * key = (void *)addr;
#endif
  unsigned long val;
  void * a = (void *) addr;
  stm_read_any(my,a,key,&val,sizeof(val));
  return val;
}
/*-------------------------------------------------------------------*/
#ifdef KEYBASED
INLINE void stm_write_key_ulong(unsigned long volatile * addr, void * key, unsigned long val, void * mydesc) {
  desc_t * my = (desc_t *)mydesc;
#else
INLINE void stm_write_ulong(unsigned long volatile * addr, unsigned long val, void * mydesc) {
  desc_t * my = (desc_t *)mydesc;
  void * key = (void *)addr;
#endif
  void * a = (void *) addr;
  stm_write_any(my,a,key,(void *)&val,sizeof(val)); return;
}
/*-------------------------------------------------------------------*/
#if defined(KEYBASED)
INLINE unsigned long long stm_read_key_ull(unsigned long long volatile * addr, void * key, void * mydesc) {
  desc_t * my = (desc_t *)mydesc;
#else
INLINE unsigned long long stm_read_ull(unsigned long long volatile * addr, void * mydesc) {
  desc_t * my = (desc_t *) mydesc;
  void * key = (void *)addr;
#endif
  unsigned long long val;
  void * a = (void *) addr;
  stm_read_any(my,a,key,&val,sizeof(val));
  return val;
}
/*-------------------------------------------------------------------*/
#ifdef KEYBASED
INLINE void stm_write_key_ull(unsigned long long volatile * addr, void * key, unsigned long long val, void * mydesc) {
  desc_t * my = (desc_t *)mydesc;
#else
INLINE void stm_write_ull(unsigned long long volatile * addr, unsigned long long val, void * mydesc) {
  desc_t * my = (desc_t *)mydesc;
  void * key = (void *)addr;
#endif
  void * a = (void *) addr;
  stm_write_any(my,a,key,(void *)&val,sizeof(val)); return;
}
/*-------------------------------------------------------------------*/

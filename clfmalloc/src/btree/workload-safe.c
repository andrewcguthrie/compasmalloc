#include <pthread.h>
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>

#include "txmanager.h"

#define FETCH 0
#define INSERT 1
#define DELETE 2

int id=0, THREADS, success=0, sum=0;

//extern void insertKeyValue(int, int);
//extern void pprint();

extern void initializeTManager(int, int);
extern int registerUserThread(int);

pthread_mutex_t mutex;

extern int smo_inserts;
extern int smo_deletes;
extern int smo_splits;

int completedProcs =0;

void * 
do_work(void *data){
  int rval = -1;
  int *values = NULL;

  int iam = *((int *) data);

  int rtID = registerUserThread(iam);

  printf("User thread %d has a registered thread ID %d\n", iam, rtID);

  int i=0, j=0;

  int count = 1024;

  for(i=0; i < count; i++){
    rval = insert(rtID, i, i);
  }

  /*
  rval = insert(rtID, 1, 1);
  rval = insert(rtID, 3, 3);
  rval = insert(rtID, 5, 5);
  rval = insert(rtID, 7, 7);
  rval = insert(rtID, 9, 9);
  rval = insert(rtID, 11,11);
  rval = insert(rtID, 13,13);
  rval = insert(rtID, 15,15);
  rval = insert(rtID, 17,17);

  rval = insert(rtID, 0, 0);
  rval = insert(rtID, 2, 2);
  rval = insert(rtID, 4, 4);
  rval = insert(rtID, 6, 6);
  rval = insert(rtID, 8, 8);
  rval = insert(rtID, 10, 10);
  rval = insert(rtID, 12, 12);
  rval = insert(rtID, 14, 14);
  rval = insert(rtID, 16, 16); 

  // llprint();
  */

  //pprint();

  for(i=0; i < count; i++){
    values = fetch(rtID, i);
    if (values == NULL){
        success = -1;
        return;
    }
    else{
      if(values[0] == i){
        success= 1;   
        sum += i;
      }
      else{
        success = -1;
	return;
      }
    } 
  }

  resetSMOCounters();

  //  pprint();

   for(i=0; i < count; i++){
    rval = deleteKV(rtID, i, i);
  }

  pprint();

  return;

  /*
  pthread_mutex_lock(&mutex);
  completedProcs++;
  pthread_mutex_unlock(&mutex);

  while (completedProcs < THREADS){}


  closeSMOThread();

  if (iam == 0){
    //llprint();

    for(i=0; i < count; i++){
      //      rval = deleteKV(rtID, i, i);
      values = fetch(rtID, i);
      printf("Value for key %d is %d\n", i, values[0]);
    }
  }

    */

  
  return 0;
}



int main (int argc, char **argv)
{
    pthread_t p[MAX_USER_THREADS];
    int i, ids[MAX_USER_THREADS], order;
   
    THREADS = 1;
    order = 2; 
   
    if (argc == 2){
      order = atoi(argv[1]);
    }
    else if (argc == 3){
      order = atoi(argv[1]);
      THREADS = atoi(argv[2]); 
    }

    printf("Initializing TManager\n");

    initializeTManager(order, THREADS);


    // start user threads
    
    for (i = 0; i < THREADS; i++){
      ids[i] = i;
      pthread_create (&p[i], NULL, *do_work, (void *)&(ids[i]));
    }
    
    // join
    for (i = 0; i < THREADS; i++){
	pthread_join (p[i], NULL);
    }

    printf("success is %d sum=%d\n", success, sum);

    terminateTManager();

#if 0    
    pprint();

    // Note key of value 0 is NOT inserted
    for(i=0; i <= counter; i++){
      int value = fetch(i);
      printf("Value of key value %d is %d\n",i, value);
    }

#endif

    printf("The number of SMO inserts=%d Deletes=%d splits=%d\n", smo_inserts, smo_deletes, smo_splits);
    
    return 0;
}


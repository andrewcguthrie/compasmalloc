#include <stdio.h>
#include "blink.h"



/* 
 * Request a read latch
 */

void acquire_shared_latch(node_t *node){

#if 0
  if (node->mode == SHARED){
    //   printf("acquire_shared_latch(): The access mode of node 0x%x is now SHARED\n", node);
    return;
  }
  else{
    while (node->mode != SHARED){
      //   printf("acquire_shared_latch(): Waiting for the mode to become shared again... Node 0x%x Current mode=%d\n", node, node->mode);
    }
  } 

#endif

  //   printf("acquire_shared_latch(): Waiting for node 0x%x to be shared again. current mode= %d\n", node, node->mode);

  while (node->mode == EXCLUSIVE){}

  return;
}


void release_shared_latch(node_t *node){
  // NO-OP
  //   printf("release_shared_latch(): The access mode of node 0x%x is now SHARED\n", node);
}

/*
 * Request a write (update) latch
 */

void acquire_update_latch(node_t *node){
 
  //  printf("acquire_update_latch(): Waiting for node 0x%x to become shared\n", node); 
  while (node->mode != SHARED){}

  node->mode = UPDATE;
  // printf("acquire_update_latch(): The access mode of node 0x%x is now UPDATE\n", node);
  return;
}

void release_update_latch(node_t *node){
  
  if (node->mode == UPDATE){
    node->mode = SHARED;
  }
  // printf("release_update_latch(): The access mode of node 0x%x is now SHARED\n", node);
  return;
}


/*
 * Heavy-weight exclusive node-level locks
 */

void acquire_exclusive_lock(node_t *node){

  node->mode = EXCLUSIVE;
  //  printf("acquire_exclusive_lock(): The access mode of node 0x%x is now EXCLUSIVE\n", node);
  return;
}

void release_exclusive_lock(node_t *node){
  while (node->mode != EXCLUSIVE){};

  node->mode = SHARED;
  //  printf("release_exclusive_lock(): The access mode of node 0x%x is now SHARED\n", node);

  return;
}

/*
 * Acquire and release mutexes
 */

void acquire_mutex(pthread_mutex_t *mutex){
   pthread_mutex_lock(mutex);
}


void release_mutex(pthread_mutex_t *mutex){
   pthread_mutex_unlock(mutex);
}

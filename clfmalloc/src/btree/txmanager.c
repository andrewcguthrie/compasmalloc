/* bplustree.c 
 * 
 * main program to initialize and build the asynchronous B+ Tree.
 * The values are stored only in the tree leaves.
 * The workload is concurrent (not transactional)
 *
 * @author= Rajesh Bordawekar 2007
 */

#include <stdlib.h>
#include <stdio.h>
#include <pthread.h>

#include "txmanager.h"
int user_thread_count=0;
pthread_mutex_t lock;

int threadID[MAX_USER_THREADS];
cursor_t *cursors[MAX_USER_THREADS][MAX_LEVELS];
int cindex[MAX_USER_THREADS];

extern node_t *ROOTNODE;
extern int tree_order;
extern int smoID;
int ids[2];
pthread_t SMOThread;
int userThreadCount=0;

volatile int status, pause2=0, resetCursor=0;
int smoWorkCount=0;
int phase2 = 0, finalSMOCount, phaseDone=0;

extern int waitingSMO, executedSMO, smoPhaseDone;

pthread_mutex_t* fifo_mutex=NULL;
pthread_mutex_t* tree_mutex=NULL;
pthread_mutex_t* leaf_mutex=NULL;

void *
do_SMOWork(void *data){
  int iam = *((int *) data);
  
  printf("SMOThread initialized with ID %d\n", iam);

  while(status){
    while(!pause2){
      // executeSMO();
      smoWorkCount++;

      phaseDone = 0;

      if (waitingSMO > finalSMOCount){
        finalSMOCount = waitingSMO;
      }

      if (phase2 == 1){
	if (executedSMO >= finalSMOCount){
	  phaseDone = 1;
	}
      }
    }
  }

  //  printf("SMOThread %d terminating. Total number of SMO operations %d\n", iam, smoWorkCount);
}

void 
markPhaseCompletion(){

  phase2 = 1;
  finalSMOCount = waitingSMO;
  printf("Initialized the markPhaseCompletion. FinalSMOCount=%d smoID=%d\n", finalSMOCount, smoID);
  while (phaseDone != 1){};
  phaseDone = 0;
  printf("Completed the markPhaseCompletion(). Pausing the SMO Thread\n");
  pauseSMOThread();
}

void resetSMOCounters(){
  // markPhaseCompletion();
  resetSMOCounts(ROOTNODE);
  waitingSMO = 0;
  executedSMO = 0;
  finalSMOCount = 0;
  smoQHead = NULL;
  smoQTop = NULL;
  lclQHead = NULL;
  lclQTop = NULL;
}

/*
 * The main transactional manager
 */

void initializeTManager(int order, int threads){

  int i=0, j=0;

  initializeBPTree(order);
  userThreadCount = threads;

  if (ROOTNODE == NULL){
    node_t *root = create_bptnode(tree_order, LEAF_NODE);
    //markRootNode(root);
    ROOTNODE= root;

    root_level = 0;
  }

  // For all participating threads, set the initial cursor to be the ROOT

  for(i=0; i < threads; i++){
     for(j=0; j < MAX_LEVELS; j++){
       cursors[i][j] = NULL;
     }
     threadQTop[i] = NULL;
     threadQHead[i] = NULL;
  }


  for(i=0; i< threads; i++){
    cursors[i][0] = create_cursor(ROOTNODE);
    cindex[i] = 0;
  }
   
  fifo_mutex = (pthread_mutex_t *) malloc(sizeof(pthread_mutex_t)); 
  tree_mutex = (pthread_mutex_t *) malloc(sizeof(pthread_mutex_t)); 
  leaf_mutex = (pthread_mutex_t *) malloc(sizeof(pthread_mutex_t)); 

  pthread_mutex_init(fifo_mutex, NULL);
  pthread_mutex_init(tree_mutex, NULL);
  pthread_mutex_init(leaf_mutex, NULL);

  //  spawnSMOThread(); 
}

void terminateTManager(){

  //  pauseSMOThread();

  int i=0, j=0;
  for(i=0; i < userThreadCount; i++){
    for(j=0; j < MAX_LEVELS; j++){
      free_cursor(cursors[i][j]);
    }
  }

  //  closeSMOThread();

  pthread_mutex_destroy(fifo_mutex);
  pthread_mutex_destroy(tree_mutex);
  pthread_mutex_destroy(leaf_mutex);
}

void markResetCursor(){
  resetCursor = 1;
}

void unmarkResetCursor(){
  resetCursor = 0;
}

void resetCursors(int rtID){
  int i=0;
  int count = cindex[rtID];
  for(i=0; i < count+1; i++){
    clean_cursor(cursors[rtID][i]);
  }

  // Assign the first cursor to be the ROOTNODE

  update_cursor(cursors[rtID][0], ROOTNODE);
  cindex[rtID] = 0;
}


/*
 * rtID would be used as an index into the cursor array
 */

int registerUserThread(int utID){

  int rtID = -1;

  //  printf("registerUserThread(): user thread id %d \n", utID);

  pthread_mutex_lock(&lock);
  rtID = user_thread_count;
  user_thread_count++;
  pthread_mutex_unlock(&lock);
  
  threadID[rtID] = utID;

  return rtID;
}

void spawnSMOThread(){

  pthread_mutex_lock(&lock);
  status = 1;
  pthread_mutex_unlock(&lock);
  
  ids[0] = 1;
  pthread_create(&SMOThread, NULL, *do_SMOWork, &ids[0]);
}

void pauseSMOThread(){
  pause2 = 1;
}

void restartSMOThread(){
  pause2 = 0;
  printf("Restarting the SMO Thread. waitingSMO=%d executedSMO=%d\n", waitingSMO, executedSMO);
}

void closeSMOThread(){


  pthread_mutex_lock(&lock);
  status = 0;
  phaseDone = 1;
  pthread_mutex_unlock(&lock);

  pthread_join(SMOThread, NULL);
}
                                     /* Main Concurrent User Calls */

/*
 * fetch() returns -1 on error; else returns the value 
 */

int* fetch(int rtID, int key){
  

  
  int *rval = NULL;
  
  if (resetCursor){
    resetCursors(rtID);
  }


  int current_index = cindex[rtID];

  rval = fetch_internal(rtID, key, &current_index);

  cindex[rtID] = current_index;

  return rval;
}

/*
 * insert() returns -1 on error;  0 otherwise
 */

int insert(int rtID, int key, int value){

  if (resetCursor){
    resetCursors(rtID);
  }

  int current_index = cindex[rtID];
  insertKeyValue(rtID, &current_index, key, value);

  cindex[rtID] = current_index;

  return 0;
}


/*
 * delete() returns -1 on error; 0 otherwise
 */

int delete(int rtID, int key){

  return -1;
}

int deleteKV(int rtID, int key, int value){
  
  if (resetCursor){
    resetCursors(rtID);
  }

  int current_index = cindex[rtID];
  int rval = deleteKeyValue(rtID, &current_index, key, value);

  cindex[rtID] = current_index;

  return 0;
}

                                                         /* Main Transactional User Calls */

/*
 * fetch() returns -1 on error; else returns the value 
 */

int tx_fetch(int rtID, int txID, int key){


  return -1;
}

/*
 * insert() returns -1 on error;  0 otherwise
 */

int tx_insert(int rtID, int txID, int key, int value){

  return -1;

}


/*
 * delete() returns -1 on error; 0 otherwise
 */

int tx_delete(int rtID, int txID, int key){

  return -1;
}

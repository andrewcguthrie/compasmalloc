
#include "blink.h"

#define MAX_USER_THREADS 128  // Can support upto 128 concurrent user threads

// Concurrent calls

int* fetch(int, int);
int insert(int, int, int);
int delete(int, int);
int deleteKV(int, int, int);

// Transacational calls

int tx_fetch(int, int, int);
int tx_insert(int, int, int, int);
int tx_delete(int, int, int);

void resetCursors(int);
void markResetCursor();
void unmarkResetCursor();

void spawnSMOThread();
void pauseSMOThread();
void restartSMOThread();
void closeSMOThread();
void markPhaseCompletion();
void resetSMOCounters();

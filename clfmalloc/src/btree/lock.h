#ifndef __BLINK_LOCK_H_
#define __BLINK_LOCK_H_
#endif


#ifdef __BLINK_LOCK_H_

#include <pthread.h>

// Access modes

#define UPDATE 0x4
#define EXCLUSIVE 0x8

#endif

/*********************************************************************
 * rwlock.c 
 * Implementation of CAS-based read-write locks
 * 
 * Author: Rajesh Bordawekar (bordaw@us.ibm.com)
 * 
 * CAS implementations taken from Maged Michael <magedm@us.ibm.com>
 *********************************************************************/


#ifndef RWLCK_C

#define RWLCK_C

#include <unistd.h> /* eliminated getopt problem on aix */
#include <stdlib.h>
#include <stdio.h>
#include <assert.h>
#include <string.h>
#include <sys/mman.h>
#include <sys/time.h>

#define X86

/*-------------------------------------------------------------------*/
#define false	0
#define true	1
/*-------------------------------------------------------------------*/

#define DEBUG_PRINT	if (debug) printf
#ifdef DEBUG
void print_debug() {}
#define ASSERT(x)	if (!(x)) { print_debug(); assert(x);}
#elif DOASSERT
#define ASSERT(x)	assert(x)
#else
#define ASSERT(x)
#endif

/*--------------------------------------------------------------------*/
#define MMAP(size) \
	mmap(0,size,PROT_READ|PROT_WRITE,MAP_ANON|MAP_PRIVATE,-1,0)
/*--------------------------------------------------------------------*/

#if defined(X86) || defined(X64) 
#define ret_t		char
#define ret32_t		char
#define ret64_t		char
#else
#define ret_t 		void *
#define ret32_t 	unsigned
#define ret64_t 	unsigned long long
#endif

#define rc_ret_t	ret32_t

/*--------------------------------------------------------------------*/

#if defined(PPC64) || defined(PPC32) /* PPC */

#define ISYNC		asm volatile ("isync")
#define LWSYNC		asm volatile ("lwsync")
#define HWSYNC		asm volatile ("sync")
#define RBR
#define RBW
#define WBW
#define WBR

#define LL64(ret,addr) \
	asm volatile ("ldarx %0,0,%1;":"=r"(ret):"r"(addr))

#define LL32(ret,addr) \
	asm volatile ("lwarx %0,0,%1;":"=r"(ret):"r"(addr))

#define SC64(ret,addr,newval) \
 asm volatile ( \
	"  stdcx. %2,0,%1;\n" \
 	"  mfcr %0;\n" \
 	"  andis. %0,%0,0x2000;" \
	: "=r"(ret):"r"(addr),"r"(newval) \
        : "cr0","memory")
#define SC32(ret,addr,newval) \
 asm volatile ( \
	"  stwcx. %2,0,%1;\n" \
 	"  mfcr %0;\n" \
 	"  andis. %0,%0,0x2000;" \
	: "=r"(ret):"r"(addr),"r"(newval) \
        : "cr0","memory")
#define CAS64(ret,addr,expval,newval) \
 do { \
    unsigned long long cas_oldval; \
    LL64(cas_oldval,addr); \
    if (cas_oldval != (unsigned long long)expval) { ret = 0; break; } \
    SC64(ret,addr,newval); \
 } while (!ret);
#define CAS32(ret,addr,expval,newval) \
 do { \
    unsigned cas_oldval; \
    LL32(cas_oldval,addr); \
    if (cas_oldval != (unsigned)expval) { ret = 0; break; } \
    SC32(ret,addr,newval); \
 } while (!ret);

#ifdef PPC64

#define LL(ret,addr)			LL64(ret,addr)
#define SC(ret,addr,newval)		SC64(ret,addr,newval)
#define CAS(ret,addr,expval,newval)	CAS64(ret,addr,expval,newval)

#else /* PPC32 */

#define LL(ret,addr)			LL32(ret,addr)
#define SC(ret,addr,newval)		SC32(ret,addr,newval)
#define CAS(ret,addr,expval,newval)	CAS32(ret,addr,expval,newval)

#endif

#elif defined(X86) || defined(X64)

#define CAS32(ret,addr,expval,newval) \
 __asm__ __volatile__( \
    "lock; cmpxchg %2, %1; setz %0;\n" \
    : "=a"(ret), "=m"(*(addr)) \
    : "r" (newval), "a"(expval) \
    : "cc")

char cas64(unsigned long long volatile * addr,
	   unsigned long long oval,
	   unsigned long long nval);

#define CAS64(ret,addr,expval,newval) \
	ret = cas64((unsigned long long volatile *)addr, \
		    (unsigned long long)expval, \
		    (unsigned long long)newval)

#define ISYNC
#define LWSYNC
#define HWSYNC
//		asm volatile ("mfence")
#define RBR
#define RBW
#define WBW
#define WBR     asm volatile ("mfence")

#ifdef X86

#define CAS(ret,addr,expval,newval)	CAS32(ret,addr,expval,newval)

#else /* X64 */

#define CAS(ret,addr,expval,newval)	CAS64(ret,addr,expval,newval)

#endif /* X86 v X64 */

#elif defined(SPARC32)

#define CAS32(ret,addr,expval,newval) \
	ret = cas32((unsigned volatile *)addr, \
		    (unsigned)expval, \
		    (unsigned)newval \
		    )
static char cas32(unsigned volatile * addr,
		  unsigned expval,
		  unsigned newval) {
  asm volatile ( \
	 "casa  %0 0x80, %2, %1\n" \
       : "+m"(*(addr)), "+r"(newval) \
       : "r" (expval) \
       );
  return (newval == expval);
}

#define ISYNC
#define LWSYNC
#define RBR	asm volatile ("membar #LoadLoad")
#define RBW	asm volatile ("membar #LoadStore")
#define WBW	asm volatile ("membar #StoreStore")

#define CAS(ret,addr,expval,newval)	CAS32(ret,addr,expval,newval)

#else
#error architecture not defined
#endif /* architecture */

/*--------------------------------------------------------------------*/

#define FAA(oldval,addr,val) \
 do { \
    ret_t ret; \
    long newval; \
    oldval = *(addr); \
    newval = oldval + val; \
    CAS(ret,addr,oldval,newval); \
    if (ret) break; \
 } while (1);

/*--------------------------------------------------------------------*/

#define FAS(oldval,addr,val) \
 do { \
    ret_t ret; \
    long newval; \
    oldval = *(addr); \
    newval = oldval - val; \
    CAS(ret,addr,oldval,newval); \
    if (ret) break; \
 } while (1);


/*------------- Traditional exclusive locks. Borrowed as is from Maged's implementation ---------*/

void lock_acquire(volatile unsigned long * lock) {
  ret_t ret;
  while (1) {
    while (*lock);
    CAS(ret,lock,0,1);
    if (ret)
      break;
  }
  ISYNC;
}
/*--------------------------------------------------------------------*/
void lock_release(volatile unsigned long * lock) {
  *lock = 0;
  LWSYNC;
}


/*-------------- Read-write Locks. ----------------------------------
 * 
 * The read-write lock uses a 32-bit integer to manager accesses from multiple readers and writers.
 * The LSB is used to determine exclusive access by writers. Writers wait for all readers to exit 
 * (lock value to become 0) and then, a single writer sets the LSB to 1. All remaining writers and 
 * readers spin-wait for the LSB to become 0.
 * 
 * The reader wait for the LSB to become 0, and then update the lock value by 2. As long as the LSB
 * is 0, multiple readers can access the shared data. While releasing the lock, each reader 
 * decrements the value by 2.
 *
 */

void wlck_acquire(volatile unsigned long *lock){
  ret_t ret;
  while (1) {
    while (*lock);
    CAS(ret,lock,0,1);
    if (ret)
      break;
  }
  ISYNC;
}

/*--------------------------------------------------------------------*/
void wlck_release(volatile unsigned long *lock){
  *lock = 0;
  LWSYNC;
}

/*--------------------------------------------------------------------*/
void rlck_acquire(volatile unsigned long *lock){
  ret_t ret;
  unsigned long oldval, newval;
  while (1) {
    while ((oldval = *lock) == 1); // spin until lower-bit is 0
    newval = oldval + 2;
    CAS(ret,lock,oldval, newval);
    if (ret)
      break;
  }
  ISYNC;
}
/*--------------------------------------------------------------------*/
void rlck_release(volatile unsigned long *lock){
  ret_t ret;
  unsigned long oldval, newval;
  while (1) {
    oldval = *lock;
    newval = oldval - 2;
    CAS(ret,lock,oldval, newval);
    if (ret)
      break;
  }
  LWSYNC; 
}

#endif /* RWLCK_C */

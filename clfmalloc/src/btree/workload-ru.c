#include <pthread.h>
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>

#include "txmanager.h"

#include <sys/time.h>
#include <sys/utsname.h>

#define FETCH 0
#define INSERT 1
#define DELETE 2

int id=0, THREADS=0, success=0, sum=0, inserts=0, percent=0, updates=0, totalUpdates=0;
int deleteOffset=0;

//extern void insertKeyValue(int, int);
//extern void pprint();

extern void initializeTManager(int, int);
extern int registerUserThread(int);

pthread_mutex_t mutex;

extern int smo_inserts;
extern int smo_deletes;
extern int smo_splits;

volatile int completedProcs =0;
volatile int restart = 0;
int successes[128];
int rtIDs[128];
int totalInserts=0;

void * 
do_init(void *data){
  int rval = -1;
  int *values = NULL;

  int iam = *((int *) data);

  int rtID = registerUserThread(iam);

  rtIDs[iam] = rtID;

 //printf("User thread %d has a registered thread ID %d\n", iam, rtID);

  int i=iam, j=0;

  if (iam ==0){
     while (i < totalInserts){
        rval = insert(rtID, i, i);
        i++;
     }
  }

  pthread_mutex_lock(&mutex);
// printf("User thread %d has completed work. CompletedProcs=%d(%d)\n", rtID, completedProcs,THREADS);
  completedProcs++;
  pthread_mutex_unlock(&mutex);
  
  while (completedProcs < THREADS){}
  

  if (iam == 0){
 //      llprint();
 //      pprint();
       resetSMOCounters();
       restart =1;
       completedProcs = 0;
  }
  /*else{
    while (restart != 1){}
  }*/

  return 0;
}


void *
do_work(void *data){
  int rval = -1;
  int *values = NULL;

  int iam = *((int *) data);
  int rtID = rtIDs[iam];

//  printf("User thread %d has a registered thread ID %d\n", iam, rtID);

 int i=iam, j=0, k=0,l=0;

#if 0
  while (j < inserts){
    values = fetch(rtID, i);

    if (values == NULL){
      printf("ERROR!. key %d not found\n",i);
      //     pprint();
      exit(0);
    }
    else{
      if (values[0] != i){
        printf("ERROR! Value of key %d is %d\n", i, values[0]);
        exit(0);
      }
      /*  else{
        printf("Value of key %d is %d\n", i, values[0]);
        } */
    }
   }
#endif

    j=0;
    while (j < updates){
      k =  totalInserts+i;
      rval = insert(rtID, k,k); 
      i = i+THREADS;
      j++;
    }

  pthread_mutex_lock(&mutex);
  completedProcs++;
  pthread_mutex_unlock(&mutex);

  while (completedProcs < THREADS){}
  
  //pprint();
}

void *
do_test(void *data){
  int rval = -1;
  int *values = NULL;

  int iam = *((int *) data);
  int rtID = rtIDs[iam];

//  printf("User thread %d has a registered thread ID %d\n", iam, rtID);

 int i=iam, j=0, k=0,l=0;

  while (j < updates){
    k = totalInserts+i;
    values = fetch(rtID, k);
    i = i+THREADS;
    j++;

    if (values == NULL){
      printf("ERROR!. key %d not found\n",i);
      //     pprint();
      exit(0);
    }
    else{
      if (values[0] != k){
        printf("ERROR! Value of key %d is %d\n", k, values[0]);
        exit(0);
      }
      /*  else{
        printf("Value of key %d is %d\n", i, values[0]);
        } */
    }
   }

  successes[iam]=1;

  pthread_mutex_lock(&mutex);
  completedProcs++;
  pthread_mutex_unlock(&mutex);

  while (completedProcs < THREADS){}
 
  //pprint();
}

int main (int argc, char **argv)
{
    pthread_t p[MAX_USER_THREADS];
    int i, ids[MAX_USER_THREADS], order;
   
    THREADS = 1;
    order = 2; 
    inserts = 128;

    if (argc == 2){
      order = atoi(argv[1]);
    }
    else if (argc == 3){
      order = atoi(argv[1]);
      THREADS = atoi(argv[2]); 
    }
    else if (argc == 4){
      order = atoi(argv[1]);
      THREADS = atoi(argv[2]); 
      inserts = atoi(argv[3]);
    }
    else if (argc == 5){
      order = atoi(argv[1]);
      THREADS = atoi(argv[2]);
      inserts = atoi(argv[3]);
      percent = atoi(argv[4]);
    }

    totalInserts = inserts*THREADS;

    totalUpdates = (int) (totalInserts*percent)/100;
    updates = (int) totalUpdates/THREADS;

    printf("Initializing TManager. Order=%d THREADS=%d inserts=%d totalInserts=%d updates=%d totalUpdates=%d\n", order, THREADS, inserts, totalInserts, updates, totalUpdates);
    
    initializeTManager(order, THREADS);

    markResetCursor();

    // start user threads

   for (i = 0; i < THREADS; i++){
      ids[i] = i;
      pthread_create (&p[i], NULL, *do_init, (void *)&(ids[i]));
    }

    // join
    for (i = 0; i < THREADS; i++){
        pthread_join (p[i], NULL);
    }

   //    closeSMOThread();

    // get start timing
    struct timezone tz;
    struct timeval tv;
    gettimeofday(&tv,&tz);
    
    unsigned t1 = tv.tv_sec * 1000000 + tv.tv_usec;
    
    for (i = 0; i < THREADS; i++){
      successes[i] =0;
      pthread_create (&p[i], NULL, *do_work, (void *)&(ids[i]));
    }
    
    // join
    for (i = 0; i < THREADS; i++){
	pthread_join (p[i], NULL);
    }

    // get end timing
    gettimeofday(&tv,&tz);
    unsigned t2 = tv.tv_sec * 1000000 + tv.tv_usec;

   for (i = 0; i < THREADS; i++){
      successes[i] =0;
      pthread_create (&p[i], NULL, *do_test, (void *)&(ids[i]));
    }

    // join
    for (i = 0; i < THREADS; i++){
        pthread_join (p[i], NULL);
    }


    for (i = 0; i < THREADS; i++){
      success += successes[i];
    }

    if (success){
      printf("The test is successful.\n");
      /* print timing info */
      struct utsname utsname;
      char *machine_name;
      if (uname (&utsname) < 0)
        machine_name = "(unknown)";
      else
        machine_name = utsname.nodename;
      printf("Timing: %7d usec ",t2-t1);
      printf("  Machine: %s", machine_name);
      printf("  Procs: %d ", THREADS);
      printf("  Inserts per proc: %d\n",inserts);
    }
    else{
      printf("The test has failed.\n");
    }

    terminateTManager();

    return 0;
}


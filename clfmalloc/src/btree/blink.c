/* bplustree.c
 *
 * @author= Rajesh Bordawekar 2007
 *
 */

#include <stdlib.h>
#include <stdio.h>

#include "blink.h"

int tree_order = 0;
int order_upper_limit = 0;
int order_lower_limit = 0;

int total_node_count = 0;
int node_count = 0;
int leaf_count = 0;

int smo_inserts = 0;
int smo_deletes = 0;
int smo_splits = 0;

node_t *ROOTNODE = NULL;
node_t *LMLEAF = NULL; //last modified leaf

volatile int waitingSMO = 0;
volatile int executedSMO = 0;
volatile int smoPhaseDone = 0;
volatile int smoID = 0;
volatile int insertSMOCount = 0, removeSMOCount = 0, exeSMOCount = 0;

volatile int phase, finalSMOCount;

extern cursor_t *cursors[MAX_USER_THREADS][MAX_LEVELS];

extern pthread_mutex_t *fifo_mutex;
extern pthread_mutex_t *tree_mutex;

extern int accesses[65536], count;

typedef unsigned long mylock_t;

volatile mylock_t tree_rwlck = 0;

void searchSubtree(int rtID, node_t *base, int key, int mode, int *depth,
		int *index, node_t **parentnode, node_t **leaf, int *offset) {

	int i = 0, j = 0;
	int current_index = *index;
	node_t *newbase = NULL, *rightSibling = NULL;

	if (isLeafNode(base)) {

		rlck_release(&tree_rwlck);

		*leaf = base;

		wlck_acquire(&(base->rwlck));

		switch (mode) {

		case FETCH_EQ:
			for (i = 0; i < base->item_count; i++) {
				if (key == base->items[i]->key) {
					*offset = i;

					wlck_release(&(base->rwlck));
					rlck_acquire(&tree_rwlck);

					return;
				}
			}
			*offset = -1;

			wlck_release(&(base->rwlck));
			rlck_acquire(&tree_rwlck);

			return;

		case FETCH_GE:
			for (i = 0; i < base->item_count; i++) {
				if (key >= base->items[i]->key) {
					*offset = i;
					wlck_release(&(base->rwlck));
					rlck_acquire(&tree_rwlck);

					return;
				}
			}
			*offset = -1;
			wlck_release(&(base->rwlck));
			rlck_acquire(&tree_rwlck);

			return;

		case FETCH_GT:
			for (i = 0; i < base->item_count; i++) {
				if (key > base->items[i]->key) {
					*offset = i;
					wlck_release(&(base->rwlck));
					rlck_acquire(&tree_rwlck);

					return;
				}
			}
			*offset = -1;
			wlck_release(&(base->rwlck));
			rlck_acquire(&tree_rwlck);

			return;

		case FETCH_LE:
			for (i = 0; i < base->item_count; i++) {
				if (key <= base->items[i]->key) {
					*offset = i;
					wlck_release(&(base->rwlck));
					rlck_acquire(&tree_rwlck);

					return;
				}
			}
			*offset = -1;
			wlck_release(&(base->rwlck));
			rlck_acquire(&tree_rwlck);

			return;

		case FETCH_LT:
			for (i = 0; i < base->item_count; i++) {
				if (key < base->items[i]->key) {
					*offset = i;
					wlck_release(&(base->rwlck));
					rlck_acquire(&tree_rwlck);

					return;
				}
			}
			*offset = -1;
			wlck_release(&(base->rwlck));
			rlck_acquire(&tree_rwlck);

			return;

		default:
			*offset = -1;
			wlck_release(&(base->rwlck));
			rlck_acquire(&tree_rwlck);

			return;

		}
	}

	(*depth)++;

	int nextSibling = 0;
	while (base != NULL) {

		nextSibling = 0;

		// First search the sub-tree routed at the current node (base)

		int cursor_offset = current_index + (*depth);

		if (cursors[rtID][cursor_offset] == NULL) {
			cursor_t *newcursor = create_cursor(base);
			cursors[rtID][cursor_offset] = newcursor;
		} else { // Nodes get over-written
			if (cursors[rtID][cursor_offset]->node != base) {
				update_cursor(cursors[rtID][cursor_offset], base);
			}
		}

		if (key <= base->maxKey) {

			int child_offset = 0;
			i = 0;
			while (i < base->item_count) {
				if (base->items[i]->key > key) {
					child_offset = i - 1;
					break;
				} else if (base->items[i]->key == key) {
					child_offset = i;
					break;
				}
				i++;
			}

			if (child_offset < 0) {
				child_offset = 0;
			}

			newbase = base->children[child_offset];

			while (key > newbase->maxKey) {
				if ((newbase->right == NULL) || (newbase->right->VN == -1)) {
					break;
				}
				if (key < (newbase->right)->minKey) {
					break;
				}
				newbase = newbase->right;
			}

			//   while (newbase != base->children[base->item_count]){
			while (newbase != NULL) {

				nextSibling = 0;

				if (newbase->VN != -1) {

					if (key <= newbase->maxKey) {
						*index = current_index;
						searchSubtree(rtID, newbase, key, mode, depth, index,
								parentnode, leaf, offset);

						if (!(isLeafNode(newbase))) { // newbase was an internal node
							*depth--;
						}

						if ((*offset != -1) || (*leaf != NULL)) {
							if (!(isLeafNode(newbase))) {
								(*index)++;
							} else {
								*parentnode = base;
							}
							return;
						}
					} else {

						int traverseChild = 0;

						if (newbase->right != NULL) {
							if ((key > newbase->maxKey) && (key
									< (newbase->right)->minKey)) {
								traverseChild = 1;
							}
						} else {
							traverseChild = 1;
						}

						// Now check the last child of the current node

						int addlastNode = 0;
						if ((newbase->children != NULL) && (traverseChild == 1)) {

							(*depth)++;
							cursor_offset = current_index + (*depth);

							if (cursors[rtID][cursor_offset] == NULL) {
								cursor_t *newcursor = create_cursor(newbase);
								cursors[rtID][cursor_offset] = newcursor;
							} else { // Nodes get over-written
								if (cursors[rtID][cursor_offset]->node
										!= newbase) {
									update_cursor(cursors[rtID][cursor_offset],
											newbase);
								}
							}

							node_t *newbase2 =
									newbase->children[newbase->item_count];

							while (key > newbase2->maxKey) {
								if ((newbase2->right == NULL)
										|| (newbase2->right->VN == -1)) {
									break;
								}
								if (key < (newbase2->right)->minKey) {
									break;
								}
								newbase2 = newbase2->right;
							}

							if (newbase2->VN != -1) {
								*index = current_index;
								searchSubtree(rtID, newbase2, key, mode, depth,
										index, parentnode, leaf, offset);

								if ((*offset != -1) || (*leaf != NULL)) {
									if (key > (*leaf)->maxKey) {
										if (!(isLeafNode(newbase2))) { // newbase2 was an internal node  // REVIEW
											// (*depth)--;
											(*depth) = (*depth)
													- (newbase->level); // leaf level is 0
										}
										nextSibling = 1;
									}

									if (!nextSibling) {
										if (!(isLeafNode(newbase2))) {
											// One for newbase2 and one for newbase
											(*depth) -= 2;
											(*index) += 2;
										} else {

											cursor_offset = current_index
													+ (*depth) + 1;

											if (key > newbase->maxKey) {
												if (newbase->right != NULL) {
													if ((newbase->right)->children[0]
															== *leaf) {
														newbase
																= newbase->right;
													}
												}
											}
											if (cursors[rtID][cursor_offset]
													== NULL) {
												cursor_t *newcursor =
														create_cursor(newbase);
												cursors[rtID][cursor_offset]
														= newcursor;
											} else { // Nodes get over-written
												if (cursors[rtID][cursor_offset]->node
														!= newbase) {
													update_cursor(
															cursors[rtID][cursor_offset],
															newbase);
												}
											}
											(*index)++;

											*parentnode = newbase;
										}
										return;
									}
								} else {
									if (!(isLeafNode(newbase2))) { // newbase2 was an internal node
										(*depth)--;
									}
								}
							} else { // VN == -1
								(*depth)--;
							}
						}

						if (newbase->right != NULL) {
							if (key < (newbase->right)->minKey) {
								*index = current_index;
								searchSubtree(rtID, newbase->right, key, mode,
										depth, index, parentnode, leaf, offset);

								if (!(isLeafNode(newbase->right))) { // newbase was an internal node // CHANGE RRB
									*depth--;
								}

								if ((*offset != -1) || (*leaf != NULL)) {
									if (!(isLeafNode(newbase->right))) {
										if (newbase->right->level == 1) {
											if (cursors[rtID][cursor_offset]
													== NULL) {
												cursor_t *newcursor =
														create_cursor(
																newbase->right);
												cursors[rtID][cursor_offset]
														= newcursor;
											} else { // Nodes get over-written
												if (cursors[rtID][cursor_offset]->node
														!= newbase->right) {
													update_cursor(
															cursors[rtID][cursor_offset],
															newbase->right);
												}
											}

											*parentnode = newbase->right;
										}
										(*index)++;
									} else {
										*parentnode = base;
									}
									return;
								}
							}
						}

					}
				}
				newbase = newbase->right;
			}
		}

		node_t *oldbase = NULL;

		// search the last child of the base

		if (base->children[base->item_count] != NULL) {
			newbase = base->children[base->item_count];

			if (newbase == NULL) {
				*leaf = NULL;
				*offset = -1;

				return;
			}

			rightSibling = newbase->right;
			oldbase = newbase;

			if ((key <= newbase->maxKey) || (rightSibling == NULL)) {
				if (newbase->VN != -1) {

					*index = current_index;
					if (key == 6) {
						nodePrint(ROOTNODE);
						llprint();
					}
					searchSubtree(rtID, newbase, key, mode, depth, index,
							parentnode, leaf, offset);

					if (!(isLeafNode(newbase))) {
						(*depth)--;
					}

					if ((*offset != -1) || (*leaf != NULL)) {
						if (!(isLeafNode(newbase))) {
							(*index)++;
						} else {
							*parentnode = base;
						}

						return;
					}
				}
			}

			while (rightSibling != NULL) {

				if (rightSibling->VN == -1) {
					while (rightSibling->right != NULL) {
						if (rightSibling->VN == -1) {
							rightSibling = rightSibling->right;
						} else {
							break;
						}
					}

				}
				if ((key < rightSibling->minKey) || (rightSibling->VN == -1)) {
					// key could be in the last child of newbase
					if (newbase->VN != -1) {
						*index = current_index;
						searchSubtree(rtID, newbase, key, mode, depth, index,
								parentnode, leaf, offset);

						if (!(isLeafNode(newbase))) {
							*depth--;
						}

						if ((*offset != -1) || (*leaf != NULL)) {
							if (!(isLeafNode(newbase))) {
								(*index)++;
							} else {
								*parentnode = base;
							}

							return;
						}
					}
				}

				newbase = rightSibling;
				if (key <= newbase->maxKey) {

					*index = current_index;
					searchSubtree(rtID, newbase, key, mode, depth, index,
							parentnode, leaf, offset);

					if (!(isLeafNode(newbase))) {
						*depth--;
					}

					if ((*offset != -1) || (*leaf != NULL)) {
						if (!(isLeafNode(newbase))) {
							(*index)++;
						} else {
							node_t *newnode, *leftnode = NULL;

							if (newbase->maxKey > base->maxKey) {

								newnode = base;
								leftnode = base;
								while ((newnode != NULL) && (newbase->maxKey
										> newnode->maxKey)) {
									leftnode = newnode;
									newnode = newnode->right;
								}

								// now newbase can be the last child of the leftnode or the first child of newnode
								if (newnode != NULL) {

									if (newnode->VN == -1) {
										*parentnode = leftnode;
									} else {
										if (newnode->children[0]->VN == -1) {

											if (newnode->children[1] != NULL) {
												if (newnode->children[1]
														== newbase) {
													*parentnode = newnode;
												} else {
													*parentnode = leftnode;
												}
											} else {
												*parentnode = leftnode;
											}
										} else {
											if ((newnode)->children[0]
													== newbase) {
												*parentnode = newnode;
											} else {
												*parentnode = leftnode;
											}
										}
									}
								} else {
									*parentnode = leftnode;
								}

							} else {
								*parentnode = base;
							}
							/*
							 if (base->right != NULL){
							 *parentnode = base->right;
							 }
							 else{
							 *parentnode = base;
							 }*/
						}

						return;
					}
				}

				newbase = rightSibling;
				rightSibling = rightSibling->right;

				/*	if (rightSibling != NULL){
				 if (rightSibling->maxKey > base->maxKey){
				 // Either the rightSibling is the last child of the base or the first child of base->right

				 if (base->right != NULL){
				 if ((base->right)->children[0] == rightSibling){
				 base = base->right;
				 }
				 }
				 }
				 } */

			}

		}

		// Now search the sub-tree routed at the right sibling...
		base = base->right;

	}

	// The item is the largest item to be inserted yet
	// Consider the right-most node to be the node to updated

	if (isLeafNode(newbase)) {
		*parentnode = base;
		*leaf = newbase;
		*offset = -1;
	}

	return;
}

// The main navigation routine.. Can traverse down, left, and up
void fetchLeafNodePredicate(int rtID, int key, int mode, int *index,
		node_t **parentnode, node_t **leaf, int *offset) {

	int i = 0, j = 0, current_index = -1;
	int depth = -1;

	current_index = *index;

	cursor_t *cursor = cursors[rtID][current_index];
	node_t *base = cursor->node;

	// First check if the key lies in the node's range else traverse up the tree

	node_t *current = base;
	int cursor_index = current_index, child_offset = -1;

	if (key <= current->minKey) {
		// Go upto the parent and come down.. Safe but slightly slow strategy.
		node_t *parent = NULL;

		if (cursor_index == 0) {

			/*  while ((!(key >= current->minKey)) && (current != ROOTNODE)){
			 //parent = findParentNode(current);
			 if (parent == NULL){
			 printf("Parent node NULL. Error in navigation. Exiting..\n");
			 exit(0);
			 }
			 current = parent;
			 } */
			current = ROOTNODE;

			update_cursor(cursors[rtID][0], current);

			// start traversing from the parent; keeping the current_index constant
			cursor = cursors[rtID][0];
			base = cursor->node;
			*parentnode = base;
		} else {

			cursor_index--;
			parent = cursors[rtID][cursor_index]->node;
			*parentnode = parent;

			while ((cursor_index > 0) && (!(key > parent->minKey))) {
				cursor_index--;
				parent = cursors[rtID][cursor_index]->node;
			}

			if (parent->VN == -1) {
				while ((((cursors[rtID][cursor_index])->node)->VN != -1)
						&& (cursor_index > 0)) {
					cursor_index--;
				}
			}

			if ((cursor_index == 0) && (key <= parent->minKey)) {
				update_cursor(cursors[rtID][0], ROOTNODE);
			}

			// update the index
			current_index = cursor_index;
			cursor = cursors[rtID][current_index];
			base = cursor->node;
			// start traversal from the selected parent node

		}
	}

	if ((base == ROOTNODE) && (ROOTNODE->children == NULL)) {
		i = 0;
		while (i < ROOTNODE->item_count) {
			if (ROOTNODE->items[i]->key == key) {
				*leaf = ROOTNODE;
				*offset = i;
				break;
			} else {
				i++;
			}
		}
		/* *leaf = NULL;
		 *offset = -1;
		 *index = current_index;  */

		return;
	}

	// Do the consistency checks and traverse up the tree, if needed

	if (current_index > 0) {
		if (base->VN == -1) {
			while (current_index > 0) {

				clean_cursor(cursors[rtID][current_index]);

				current_index--;
				cursor = cursors[rtID][current_index];
				base = cursors[rtID][current_index]->node;

				if (base->VN != -1) {
					break;
				}
			}
		}

		// Found an index with a live node

		if (cursor->VN < base->VN) {
			// Update the cursor VN
			//      printf("1 Updating VN of the cursor. Old VN=%d new VN=%d\n", cursor->VN, base->VN);
			cursor->VN = base->VN;
		} else if (cursor->VN != base->VN) {
			// Report Error and exit
			//      printf("1 Error: Cached-node VN is higher than the tree node VN. Exiting\n");
			exit(0);
		}
	}

	if (current_index == 0) {
		// First check if the base VN is -1
		if (base->VN == -1) {
			// the ROOTNODE has been updated in the background..
			clean_cursor(cursor);

			// Replace the node with the correct rootnode
			base = ROOTNODE;
		}

		if (cursor->VN < base->VN) {
			// Update the cursor VN
			cursor->VN = base->VN;
		} else if (cursor->VN > base->VN) {
			// Report Error and exit
			printf(
					"Error: Cached-node VN is higher than the tree node VN. Exiting\n");
			exit(0);
		}
	}

	// For the internal nodes and ROOTNODE with children, do a recursive descent

	*index = current_index;
	searchSubtree(rtID, base, key, mode, &depth, index, parentnode, leaf,
			offset);
}

int *fetch_internal(int rtID, int key, int *index) {

	int i = 0, offset = -1;
	node_t *leaf = NULL, *parentnode = NULL;
	int rsize = tree_order;
	int *rval = (int *) malloc(sizeof(int) * rsize); // Assume initial size to be tree_order


	rlck_acquire(&tree_rwlck);

	fetchLeafNodePredicate(rtID, key, FETCH_EQ, index, &parentnode, &leaf,
			&offset);

	if ((leaf == NULL) || (offset == -1)) {

		rlck_release(&tree_rwlck);
		return NULL;
	} else {
		int count = 0;

		rlck_release(&tree_rwlck);

		rlck_acquire(&(leaf->rwlck));

		while ((offset + count) < (leaf->item_count)) {
			if ((leaf->items[offset + count]->key == key)) {
				if (count < rsize) {
					rval[count] = leaf->items[offset + count]->value;
					count++;
				} else {
					rsize = 2 * rsize;
					int *rval2 = (int *) malloc(sizeof(int) * rsize);

					for (i = 0; i < rsize / 2; i++) {
						rval2[i] = rval[i];
					}
					free(rval);
					rval = rval2;
					rval[count] = leaf->items[offset + count]->value;
					count++;
				}
			} else {
				count++;
			}
		}

		rlck_release(&(leaf->rwlck));

		return rval;
	}

}

int deleteKeyValue(int rtID, int *index, int key, int value) {

	int i = 0, offset = 0, offset2 = 0;

	cursor_t *cursor = cursors[rtID][(*index)];
	node_t *base = cursor->node;
	smonode_t *smonode = NULL;

#if 0
	if ((waitingSMO - executedSMO) >=2) {
		while((waitingSMO-executedSMO) != 0) {}
	}
#endif

	//acquire_mutex(tree_mutex);

	rlck_acquire(&tree_rwlck);

	//  printf("deleteKeyValue(): thread %d deleting key %d\n", rtID, key);

	//  pprint();

	while (base->VN == -1) {

		if ((*index) == 0) {
			update_cursor(cursors[rtID][0], ROOTNODE);
		} else {
			(*index)--;
		}

		cursor = cursors[rtID][(*index)];

		base = cursor->node;
	}

	int cached_VN = cursor->VN;

	if (base == ROOTNODE) {

		if (isLeafNode(ROOTNODE)) {

			i = 0;

			while (i < ROOTNODE->item_count) {
				if ((ROOTNODE->items[i]->key == key)
						&& (ROOTNODE->items[i]->value == value)) {
					offset = i;
					break;
				} else {
					i++;
				}
			}

			for (i = offset + 1; i < ROOTNODE->item_count; i++) {
				ROOTNODE->items[i - 1] = ROOTNODE->items[i];
			}

			ROOTNODE->item_count--;
			ROOTNODE->VN++;

			if (ROOTNODE->item_count == 0) {
				ROOTNODE->VN = -1;
				ROOTNODE->refCount--;

				if (ROOTNODE->refCount == 0) {
					free_bptnode(ROOTNODE);
				}

				ROOTNODE = NULL;
				return 0;
			}

			if (ROOTNODE->items[0]->key < ROOTNODE->minKey) {
				ROOTNODE->minKey = ROOTNODE->items[0]->key;
			}

			if (ROOTNODE->items[ROOTNODE->item_count - 1]->key
					< ROOTNODE->maxKey) {
				ROOTNODE->maxKey
						= ROOTNODE->items[ROOTNODE->item_count - 1]->key;
			}

			cursor->VN = base->VN; // ????

			wlck_release(&tree_rwlck);

			return 0;
		}

	}

	offset = -1;
	node_t *leaf = NULL, *parentnode = NULL;

	fetchLeafNodePredicate(rtID, key, FETCH_EQ, index, &parentnode, &leaf,
			&offset);

	if ((leaf == NULL) || (offset == -1)) {

		//  printf("Error. FetchLeafNodePredicate return either NULL leaf or illegal offset\n");
		//    exit(0);
		// release_mutex(tree_mutex);

		wlck_release(&tree_rwlck);
		return -1;
	}

	if ((cursors[rtID][*index]->node != parentnode)) {
		update_cursor(cursors[rtID][*index], parentnode);
	}

	fixParentCursor(rtID, (*index));

	wlck_release(&tree_rwlck);

	wlck_acquire(&tree_rwlck);

	int repeatedKeys = 0;
	for (i = offset; i < leaf->item_count; i++) {
		if (leaf->items[i]->key == key) {
			if (leaf->items[i]->value == value) {
				break;
			} else {
				repeatedKeys++;
			}
		}
	}

	offset = offset + repeatedKeys;

	kvnode_t *freedItem = leaf->items[offset];
	leaf->items[offset] = NULL;

	for (i = offset + 1; i < leaf->item_count; i++) {
		leaf->items[i - 1] = leaf->items[i];
	}

	free_kvnode(freedItem);

	leaf->items[(leaf->item_count - 1)] = NULL;
	leaf->item_count--;
	leaf->VN++;

	if (leaf->item_count > 0) {

		if (leaf->item_count == 1) {
			leaf->minKey = leaf->items[0]->key;
			leaf->maxKey = leaf->items[0]->key;
		} else {
			if (leaf->items[0]->key < leaf->minKey) {
				leaf->minKey = leaf->items[0]->key;
			}

			if (leaf->items[leaf->item_count - 1]->key < leaf->maxKey) {
				leaf->maxKey = leaf->items[leaf->item_count - 1]->key;
			}
		}

	} else {

		leaf->VN = -1;
		leaf->refCount--;
		leaf->status = EMPTY;
		leaf->minKey = -1;
		leaf->maxKey = -1;

		// Invoke the SMO Operation for deletion and updating the parent node

		smonode = safeLeafDelete(rtID, leaf, key, index);

	}

	if (smonode != NULL) {
		int oVal = enqSMONodeLocal(rtID, smonode);
	}

	smonode = deqSMONodeLocal(rtID);
	while (smonode != NULL) {

		executeSMO(rtID, smonode);
		smonode = deqSMONodeLocal(rtID);
	}

	//  release_mutex(tree_mutex);

	wlck_release(&tree_rwlck);

	//   pprint();
	//  threadCursorPrint(rtID, *index);


	return;
}

// Returns the index into the cursor array
void insertKeyValue(int rtID, int *index, int key, int value) {

	int i = 0, j = 0;

	kvnode_t *newitem = create_kvnode(key, value);
	cursor_t *cursor = cursors[rtID][(*index)];

	node_t *base = cursor->node;
	int cached_VN = cursor->VN;

#if 0
	if ((waitingSMO - executedSMO) >=2) {
		while((waitingSMO-executedSMO) != 0) {}
	}
#endif

	rlck_acquire(&tree_rwlck);

	accesses[count] = key;
	count++;

	// Case 1. base is the root node

	//printf("Thread %d inserting key %d value %d\n", rtID, key, value);
	// printf("WaitingSMO=%d executedSMO=%d\n", waitingSMO, executedSMO);

	if (base == ROOTNODE) {

		rlck_release(&tree_rwlck);
		wlck_acquire(&(base->rwlck));

		if (isLeafNode(ROOTNODE)) { // Does not have children

			if (LMLEAF == NULL) {
				LMLEAF = ROOTNODE;
			}

			//      nodeKeyPrint(base, 0, 0);

			i = 0;
			while (i < ROOTNODE->item_count) {
				if (ROOTNODE->items[i]->key > key) {
					//insert key into item
					for (j = ROOTNODE->item_count; j >= i; j--) {
						ROOTNODE->items[j + 1] = ROOTNODE->items[j];
					}
					ROOTNODE->items[i] = newitem;
					ROOTNODE->item_count++;
					ROOTNODE->VN++;

					break;
				} else if (ROOTNODE->items[i]->key == key) {
					//insert key into item if value is bigger than current value
					if (ROOTNODE->items[i]->value > value) {
						for (j = ROOTNODE->item_count; j >= i; j--) {
							ROOTNODE->items[j + 1] = ROOTNODE->items[j];
						}
						ROOTNODE->items[i] = newitem;
						ROOTNODE->item_count++;
						ROOTNODE->VN++;

						break;
					}
					i++;
				} else {
					i++;
				}
			}

			// Insert the new item at the end of the list

			if (i == base->item_count) {
				base->items[base->item_count] = newitem;
				base->item_count++;
				base->VN++;
			}

			if (key >= base->maxKey) {
				base->maxKey = key;
			}
			if (key <= base->minKey) {
				base->minKey = key;
			}

			if (base->item_count > order_upper_limit) {
				firstRootSplit();
			}

			// what is this doing here???

			if (cursor->VN < base->VN) {
				cursor->VN = base->VN;
			}

			//	nodeKeyPrint(base, 1, 0);

			wlck_release(&(base->rwlck));

			return;
		}

		wlck_release(&(base->rwlck));
		rlck_acquire(&tree_rwlck);
	}

	// General case: ROOT has children and also, for internal nodes
	int offset = -1;
	node_t *leaf = NULL, *parentnode;

	//nodePrint(ROOTNODE);

	fetchLeafNodePredicate(rtID, key, FETCH_LE, index, &parentnode, &leaf,
			&offset);

	if (leaf == NULL) {
		printf(
				"Error. FetchLeafNodePredicate returned a Null node. Exiting...\n");

		rlck_release(&tree_rwlck);
		exit(0);
	}

	if ((cursors[rtID][*index]->node != parentnode) && (parentnode != NULL)) {
		update_cursor(cursors[rtID][*index], parentnode);
	}

	fixParentCursor(rtID, (*index));

	rlck_release(&tree_rwlck);

	// Fix the parent..

	//  nodePrint(parentnode);

	wlck_acquire(&(leaf->rwlck));

	for (i = 0; i < leaf->item_count; i++) {
		if (key <= leaf->items[i]->key) {
			offset = i;
			break;
		}
	}

	if ((newitem->key > leaf->maxKey) && (offset != -1)) {
		node_t *leaf2 = NULL;
		// move to the right of node
		while ((leaf->right != NULL) && (newitem->key > leaf->maxKey)) {
			leaf2 = leaf->right;
			wlck_release(&(leaf->rwlck));
			leaf = leaf2;
			wlck_acquire(&(leaf->rwlck));
		}
	} else if ((newitem->key > leaf->maxKey) && (offset == -1)) {
		node_t *leaf2 = NULL;
		if (leaf->right != NULL) {
			leaf2 = leaf->right;
			wlck_release(&(leaf->rwlck));
			leaf = leaf2;
			wlck_acquire(&(leaf->rwlck));
		}
	}

	// key in range of current leaf
	for (i = 0; i < leaf->item_count; i++) {
		if (key <= leaf->items[i]->key) {
			offset = i;
			break;
		}
	}

	//  node_t *parent = findParentNode(leaf);


	// The inserted key is the largest so far and also unique.


	//  nodeKeyPrint(leaf, 0, 0);

	if ((leaf != NULL) && (offset == -1)) {
		leaf->items[leaf->item_count] = newitem;
		leaf->item_count++;
		leaf->VN++;

		if (key > leaf->maxKey) {
			leaf->maxKey = key;
		}

		//    nodeKeyPrint(leaf, 1, 0);
	} else if ((leaf != NULL) && (offset != -1)) {

		// Warning to myself: item_count is 1-based and other indexes are 0-based
		// TODO: Fix offset to support non-unique items

		// First make sure, if there are more keys that equal the key to be inserted

		int repeatedKeys = 0;
		for (i = offset; i < leaf->item_count; i++) {
			if (leaf->items[i]->key == key) {
				if (leaf->items[i]->value >= value) {
					break;
				} else {
					repeatedKeys++;
				}
			}
		}

		offset = offset + repeatedKeys;

		for (i = leaf->item_count; i > offset; i--) {
			leaf->items[i] = leaf->items[i - 1];
		}
		leaf->items[offset] = newitem;

		leaf->item_count++;
		leaf->VN++;

		if (key < leaf->minKey) {
			leaf->minKey = key;
		}

		//    nodeKeyPrint(leaf, 1, 0);
	}

	// create a SMO node/request
	smonode_t *smonode = safeLeafInsert(rtID, leaf, index);

	wlck_release(&(leaf->rwlck));

	//execute insert SMO
	//REVIEW SMO cannot be done concurrently
	wlck_acquire(&tree_rwlck);

	if (smonode != NULL) {
		int oVal = enqSMONodeLocal(rtID, smonode);
	}

	smonode = deqSMONodeLocal(rtID);

	while (smonode != NULL) {

		executeSMO(rtID, smonode);
		smonode = deqSMONodeLocal(rtID);
	}

	wlck_release(&tree_rwlck);

	return;
}

smonode_t *safeLeafDelete(int rtID, node_t *leaf, int key, int *index) {

	int i = 0, offset = 0;
	node_t *parent = NULL, *leftSibling = NULL, *leftSibling2 = NULL,
			*rightSibling = NULL;
	kvnode_t *deletedItem = NULL;
	cursor_t **smo_cursors = NULL;
	smonode_t *smonode = NULL;

	parent = cursors[rtID][*index]->node;

	if (parent == NULL) {
		return NULL;
	}

	leftSibling = findLeftSibling(parent, leaf, &offset);
	rightSibling = leaf->right;

	while (rightSibling != NULL) {
		if (rightSibling->VN != -1) {
			break;
		}
		rightSibling = rightSibling->right;
	}

	offset = 0;
	while ((offset < parent->item_count) && (parent->items[offset]->key < key)) {
		offset++;
	}

	if (offset == parent->item_count) {
		offset--;
	}

	deletedItem = copy_kvnode(parent->items[offset]);

	if (leftSibling != NULL) {
		leftSibling->right = rightSibling;
	} else {
		LMLEAF = rightSibling;
	}

	// Now the leaf is disconnected from the linked list
	// It is still linked from the parent
	// BUT: it still points to the rightSibling. RightSibling has two pointers.
	// Now, update the parent

	smo_cursors = (cursor_t **) malloc(MAX_LEVELS*sizeof(cursor_t *));

	for (i = 0; i < MAX_LEVELS; i++) {
		smo_cursors[i] = NULL;
	}

	for (i = 0; i < (*index) + 1; i++) {
		smo_cursors[i] = copy_cursor(cursors[rtID][i]);
	}

	// index points to the parent

	waitingSMO++;
	smonode = create_smonode(parent, deletedItem, SMO_DELETE, (*index),
			&smo_cursors[0]);
	//    printf("Created a DELETE SMO smoID=%d while removing key %d deleteditem key=%d value=%d offset=%d item_count=%d\n", smonode->ID, key, deletedItem->key, deletedItem->value, offset, parent->item_count);
	//    nodePrint(parent);
	return smonode;
}

// NOTE: All updates to the internal (non-leaf) nodes are triggered by leaf modifications.

smonode_t *safeLeafInsert(int rtID, node_t *leaf, int *index) {

	int i = 0, maxRange = 0;
	node_t *newLeaf = NULL, *rightSibling = NULL;
	kvnode_t *promotedItem = NULL;

	// Insertion proceeds in two steps:
	// Immediate creation of a new leaf and delayed update of the parent node (by the SMO thread)

	// need to split node
	if (leaf->item_count > order_upper_limit) {

		// Get the pointer to the possible parent
		node_t *parent = cursors[rtID][*index]->node;

		//    printf("safeLeafInsert(): Splitting leaf node: ");
		//    nodePrint(leaf);

		//    printf("safeLeafInsert(): Parent node: ");
		//    nodePrint(parent);

		//    nodeKeyPrint(leaf, 0, 0);

		// new right leaf
		newLeaf = create_bptnode(tree_order, LEAF_NODE);
		newLeaf->item_count = tree_order;

		for (i = 0; i < tree_order; i++) {
			newLeaf->items[i] = leaf->items[i + tree_order];
		}

		newLeaf->VN++;
		newLeaf->minKey = newLeaf->items[0]->key;
		newLeaf->maxKey = newLeaf->items[newLeaf->item_count - 1]->key;

		maxRange = newLeaf->maxKey;

		// newleaf gets the same level as its left sibling (old leaf)
		// leaf levels are always 0
		newLeaf->level = 0;

		// update the leaf being split

		leaf->item_count = (leaf->item_count) - (tree_order);

		// minKey remains constant

		if (leaf->items[leaf->item_count - 1]->key <= leaf->maxKey) {
			leaf->maxKey = leaf->items[leaf->item_count - 1]->key;
		}
		// Increment the leaf VN

		leaf->VN++;

		promotedItem = copy_kvnode(leaf->items[tree_order - 1]);

		rightSibling = leaf->right;

		while (rightSibling != NULL) {
			if (rightSibling->VN != -1) {
				break;
			}
			rightSibling = rightSibling->right;
		}

		// Set up the links

		newLeaf->right = rightSibling;
		leaf->right = newLeaf;

		//    nodeKeyPrint(leaf, 1, 0);
		//    nodeKeyPrint(newLeaf, 2, 0);

		// The ROOTNODE case has been handled separately
		if (parent == NULL) {
			parent = create_bptnode(tree_order, NONLEAF_NODE);
			parent->level = 1;
		}

		cursor_t **smo_cursors = NULL;

		smo_cursors = (cursor_t **) malloc(MAX_LEVELS*sizeof(cursor_t *));
		for (i = 0; i < MAX_LEVELS; i++) {
			smo_cursors[i] = NULL;
		}

		int smo_index = 0;

		// index points to the parent
		if ((*index) > 1) {
			for (i = 0; i < (*index); i++) {
				smo_cursors[i] = copy_cursor(cursors[rtID][i]);
			}
			smo_index = (*index) - 1;
		}

		waitingSMO++;
		//    printf(" new=%d \n", waitingSMO);

		if (parent->level != (leaf->level + 1)) {
			printf(
					"RRB Error! Parent 0x%x of leaf 0x%x must have level %d has level %d\n",
					parent, leaf, (leaf->level) + 1, parent->level);
			nodePrint(parent);
			nodePrint(leaf);
			exit(0);
		}

		smonode_t *smonode = create_smonode(parent, promotedItem, SMO_INSERT,
				smo_index, &smo_cursors[0]);
		setInsertSibling(smonode, newLeaf);
		updateSMORange(smonode, maxRange);

		//   printf("safeLeafInsert(): Created a INSERT SMO smoID=%d to insert  insertedItem key=%d value=%d\n", smonode->ID, promotedItem->key, promotedItem->value);
		//    printf("waiting SMO=%d executedSMO=%d\n", waitingSMO, executedSMO);
		//    nodePrint(parent);
		//    smonodePrint(smonode);

		return smonode;
	}

	return NULL;
}

void executeSMO(int rtID, smonode_t *smonode) {

#if 0
	if (smonode == NULL) {
		smonode = deqLclQ();
		if (smonode == NULL) {
			return;
		}
	}

#endif

	node_t *current = smonode->node;

	int action = smonode->op;

	//  acquire_mutex(tree_mutex);

	current->status = SMO_EXEC;

	//  printf("executeSMO() SMONode Information: smonode=0x%x smonode ID=%d action=%d cstamp=%d node=0x%x exeSMOCount=%d\n", smonode, smonode->ID, smonode->op, smonode->cstamp, current, current->exeSMOCount);

	//  smonodePrint(smonode);
	//  nodePrint(current);

	if (current->VN != -1) {
		exeSMOCount++;
		if (action == SMO_INSERT) {
			executeInsert(rtID, smonode);
			smo_inserts++;
		} else if (action == SMO_DELETE) {
			executeDelete(rtID, smonode);
			smo_deletes++;
		} else if (action == SMO_SPLIT) {
			splitInternalNode(rtID, smonode);
			smo_splits++;
		}
	} else if (current->VN == -1) {
		printf("Executed an SMO on a condemned node\n");
	}

	current = smonode->node;

	if (current->status == SMO_EXEC) {
		current->status = NORMAL;
	}
	current->exeSMOCount++;
	//  printf("Updating SMONode Information: smonode ID=%d action=%d cstamp=%d node=0x%x exeSMOCount=%d\n", smonode->ID, smonode->op, smonode->cstamp, current, current->exeSMOCount);

	executedSMO++;

	//  printf("executeSMOThread waitingSMO=%d executedSMO %d insertSMOCount=%d removeSMOCount=%d exeSMOCount=%d\n", waitingSMO, executedSMO, insertSMOCount, removeSMOCount, exeSMOCount);

	smonode->refCount--;

	if (smonode->refCount == 0) {
		free_smonode(smonode);
	}

	//  release_mutex(tree_mutex);

	/*
	 if (phase == 1){
	 if ((executedSMO >= finalSMOCount) && (smoQHead == NULL)){
	 smoPhaseDone = 1;
	 }
	 } */

	return;

}

void executeDelete(int rtID, smonode_t *smonode) {

	node_t *base = NULL, *parent = NULL, *deletedNode = NULL;
	node_t *leftSibling = NULL, *rightSibling = NULL;
	kvnode_t *item = NULL, *deletedItem = NULL;

	int i = 0, offset = 0;

	base = smonode->node;
	item = smonode->item;

	//  printf("executeDelete(): waitingSMO=%d executedSMO=%d smo with ID=%d Removing item key=%d value=%d\n", waitingSMO, executedSMO, smonode->ID, item->key, item->value);
	//  nodePrint(base);

	// Two main cases: (1) item_count is 1 and (2) item_count > 2

	node_t *newparent = findParentNode(base); // do we need it

	if (base->item_count == 1) {

		if (!(base->hasSingleChild)) {
			base->hasSingleChild = 1;
			return;
		}

		base->hasSingleChild = 0;

		// The node has a single item and two children that need to be deleted

		// Fix the parent

		free_kvnode(base->items[0]);
		base->items[0] = NULL;

		smonode->item = NULL;
		base->item_count = 0;

		base->children[0]->refCount--;
		if (base->children[0]->refCount <= 0) {
			free_bptnode(base->children[0]);
			base->children[0] = NULL;
		}

		base->children[1]->refCount--;
		if (base->children[1]->refCount <= 0) {
			free_bptnode(base->children[1]);
			base->children[1] = NULL;
		}

		base->VN = -1;

		//   base->minKey = -1;
		//    base->maxKey = -1;
		base->refCount--;
		base->status = EMPTY;

		// Now fix the links


		if (smonode->index > 0) {
			if (smonode->cursors[(smonode->index) - 1] != NULL) {
				parent = smonode->cursors[(smonode->index) - 1]->node;
			}

			/*    if (parent != NULL){
			 if (newparent != parent){
			 update_cursor(smonode->cursors[(smonode->index)-1], newparent);
			 parent = newparent;
			 }
			 }
			 */
		} else {
			parent = ROOTNODE;
		}
		/* else{
		 if (newparent != NULL){
		 parent = newparent;
		 }
		 } */

		if (parent == NULL) {
			printf("Error. Parent of an internal node is NULL. Exiting...\n");
			exit(0);
		}

		leftSibling = findLeftSibling(parent, base, &offset);

		rightSibling = base->right;

		while (rightSibling != NULL) {
			if (rightSibling->VN != -1) {
				break;
			}
			rightSibling = rightSibling->right;
		}

		if (leftSibling != NULL) {
			leftSibling->right = rightSibling;
		}

		if (parent == ROOTNODE) {

			if (parent->item_count == 0) {
				ROOTNODE = NULL;

				parent->refCount--;
				if (parent->refCount <= 0) {
					free_bptnode(parent);
				}

				return;
			} else if (parent->item_count == 1) {
				if (!(parent->hasSingleChild)) {
					parent->hasSingleChild = 1;

					return;
				} else {

					// No copying as the action is immediate

					deletedItem = parent->items[0];
					// ROOTNODE will  become empty.
					// EXTREMELY RARE CASE: Just delete the nodes and return...

					free_kvnode(deletedItem);
					smonode->item = NULL;
					parent->item_count = 0;

					parent->children[0]->refCount--;
					if (parent->children[0]->refCount <= 0) {
						parent->children[0] = NULL;
						free_bptnode(parent->children[0]);
					}

					parent->children[1]->refCount--;
					if (parent->children[1]->refCount <= 0) {
						parent->children[1] = NULL;
						free_bptnode(parent->children[1]);
					}

					ROOTNODE = NULL;

					parent->refCount--;
					if (parent->refCount <= 0) {
						free_bptnode(parent);
					}

					return;
				}
			} else {// Rootnode has more than one items
				// Get the child offset

				for (i = 0; i <= parent->item_count; i++) {
					if (parent->children[i] == base) {
						offset = i;
						break;
					}
				}

				if (offset == 0) {
					deletedItem = copy_kvnode(parent->items[0]);
				} else {
					deletedItem = copy_kvnode(parent->items[offset - 1]);
				}

				smonode->node = parent;
				smonode->item = deletedItem;
				smonode->op = SMO_DELETE;
				smonode->index = (smonode->index) - 1;
				smonode->cstamp = parent->smoCount;
				parent->smoCount++;

				waitingSMO++;
				int oVal = enqSMONodeLocal(rtID, smonode);

				smonode = deqSMONodeLocal(rtID);
				while (smonode != NULL) {

					executeSMO(rtID, smonode);
					smonode = deqSMONodeLocal(rtID);
				}

				return;
			}
		} else {
			if (parent->item_count == 1) {
				if (!(parent->hasSingleChild)) {
					parent->hasSingleChild = 1;

					return;
				} else {
					deletedItem = copy_kvnode(parent->items[0]);

					smonode->node = parent;
					smonode->item = deletedItem;
					smonode->op = SMO_DELETE;
					smonode->index = (smonode->index) - 1;

					smonode->cstamp = parent->smoCount;
					parent->smoCount++;

					waitingSMO++;
					int val = enqSMONodeLocal(rtID, smonode);

					smonode = deqSMONodeLocal(rtID);
					while (smonode != NULL) {

						executeSMO(rtID, smonode);
						smonode = deqSMONodeLocal(rtID);
					}

					return;
				}
			} else { // Parent has more than one items


				// Get the child offset
				offset = 0;
				for (i = 0; i <= parent->item_count; i++) {
					if (parent->children[i] == base) {
						offset = i;
						break;
					}
				}

				if (offset == 0) {
					deletedItem = copy_kvnode(parent->items[0]);
				} else {
					deletedItem = copy_kvnode(parent->items[offset - 1]);
				}

				smonode->node = parent;
				smonode->item = deletedItem;
				smonode->op = SMO_DELETE;
				smonode->index = (smonode->index) - 1;

				smonode->cstamp = parent->smoCount;
				parent->smoCount++;

				clean_cursor(smonode->cursors[(smonode->index)]);

				waitingSMO++;
				int oVal = enqSMONodeLocal(rtID, smonode);

				smonode = deqSMONodeLocal(rtID);
				while (smonode != NULL) {

					executeSMO(rtID, smonode);
					smonode = deqSMONodeLocal(rtID);
				}

				return;
			}
		}
	}

	// Repeated search for correctness

	for (i = 0; i < base->item_count; i++) {
		if (base->items[i]->key == item->key) {
			offset = i;
			break;
		}
	}

	// compute the child node to be removed from the list and freed

	node_t *childToBeFreed = NULL;
	int childOffset = -1;

	if (offset == 0) {
		// Either children[0] or children[1] is to be removed
		if (base->children[0]->VN == -1) {
			childOffset = 0;
		} else {
			childOffset = 1;
		}
	} else {
		childOffset = offset + 1;
	}

	// Actual item to be removed

	item = base->items[offset];

	for (i = offset; i < base->item_count - 1; i++) {
		base->items[i] = base->items[i + 1];
	}

	childToBeFreed = base->children[childOffset];

	if (childToBeFreed->VN != -1) {
		// got the next child by mistake..
		childOffset = childOffset - 1;
		childToBeFreed = base->children[childOffset];
	}

	if (smonode->index > 0) {
		parent = smonode->cursors[(smonode->index) - 1]->node;
	} else {
		parent = ROOTNODE;
	}
	// Set up the links

	leftSibling = findLeftSibling(parent, base->children[childOffset], &offset);

	rightSibling = base->children[childOffset]->right;
	while (rightSibling != NULL) {
		if (rightSibling->VN != -1) {
			break;
		}
		rightSibling = rightSibling->right;
	}

	base->children[childOffset] = NULL;

	for (i = childOffset; i < base->item_count; i++) {
		base->children[i] = base->children[i + 1];
	}

	if (leftSibling != NULL) {
		leftSibling->right = rightSibling;
	}

	childToBeFreed->refCount--;

	if (childToBeFreed->refCount <= 0) {
		free_bptnode(childToBeFreed);
	}

	base->items[(base->item_count - 1)] = NULL;
	base->children[base->item_count] = NULL;
	base->item_count--;

	base->minKey = base->items[0]->key;
	base->maxKey = base->items[base->item_count - 1]->key;

	free_kvnode(item);

	return;
}

void executeInsert(int rtID, smonode_t *smonode) {

	node_t *base = smonode->node;
	kvnode_t *item = smonode->item;

	int i = 0, j = 0, offset;

	// First get the latest version of the node

	node_t* latest = NULL;

	node_t *current = base;
	node_t *oldBase = base;

	//  printf("executeInsert() smonode ID=%d inserting key=%d value=%d into node 0x%x\n", smonode->ID, item->key, item->value, current);
	//  printf("WaitingSMO=%d executedSMO=%d\n", waitingSMO, executedSMO);
	//  nodePrint(current);
	//  smonodePrint(smonode);

	//  THIS IS WRONG!!

#if 0
	while (((item->key> getMaxRange(current)) || (current->VN == -1)) && (current->right != NULL)) {
		if ((current->right)->VN != -1) {
			if (item->key < (current->right)->maxKey) {
				current = current->right;
				break;
			}
		}
		else {
			current = current->right;
		}
		current = current->right;
	}
#endif

	//  printf(" # ");
	//  nodePrint(current);

	base = current;

	nodeKeyPrint(base, 0, 1);

	if (oldBase != base) {
		smonode->node = base;
	}

	base->status = SMO_EXEC;

	j = 0;
	for (i = 0; i < base->item_count; i++) {
		if (item->key <= base->items[i]->key) {
			if (item->key == base->items[i]->key) {
				while (base->items[i + j]->value > item->value) {
					j++;
				}
				break;
			}
			break;
		}
	}

	offset = i + j;

	// Actual Insertion

	for (i = base->item_count; i > offset; i--) {
		base->items[i] = base->items[i - 1];
	}
	base->items[offset] = copy_kvnode(item);

	// Fix the children...

	//  nodePrint(base);

	if (base->children == 0) {
		printf("ERROR!! Exiting..\n");
		//  exit(0);
	}

	for (i = base->item_count; i > offset; i--) {
		base->children[i + 1] = base->children[i];
	}

	//   printf("RRB executeInsert(). base=0x%x node=0x%x rightSibling=0x%x\n", base, base->children[offset], smonode->rightSibling);
	//   base->children[offset+1] = base->children[offset]->right;
	base->children[offset + 1] = smonode->rightSibling;

	base->item_count++;
	base->VN++;

	if (item->key < base->minKey) {
		base->minKey = item->key;
	}

	if (item->key > base->maxKey) {
		base->maxKey = item->key;
	}

	nodeKeyPrint(base, 1, 1);

	if (base->item_count > order_upper_limit) {

		//      printf("executeInsert invoking splitInternalNode(): SMO smoID=%d Node to be split: ",smonode->ID);
		//      nodePrint(base);
		//      smonodePrint(smonode);

		splitInternalNode(rtID, smonode);

	}
	// else{
	//    printf("executeInsert(): Update node: ");
	//    nodePrint(base);
	//}

}

void splitInternalNode(int rtID, smonode_t *smonode) {

	int i = 0, j = 0, offset = 0, maxRange = 0;
	node_t *base = NULL, *newnode = NULL, *rightSibling = NULL;
	kvnode_t *promotedItem = NULL;

	base = smonode->node;

	waitingSMO++;

	// Insertion proceeds in two steps:
	// Immediate creation of a new node and delayed update of the parent node (by the SMO thread)


	printf("Executing splitInternalNode() 0: smonode ");
	//     smonodePrint(smonode);
	printf("Node to be split: \n");
	nodePrint(base);

	nodeKeyPrint(base, 0, 1);

	if (base->item_count == (2 * tree_order)) {

		//  printf("****************************************************\n");

		newnode = create_bptnode(tree_order, NONLEAF_NODE);
		newnode->item_count = tree_order - 1;

		//    printf("splitInternalNode() 1: creating a new node: 0x%x\n", newnode);

		for (i = 0; i < tree_order - 1; i++) {
			newnode->items[i] = base->items[i + tree_order + 1];
			base->items[i + tree_order + 1] = NULL;
			newnode->children[i] = base->children[i + tree_order + 1];
			base->children[i + tree_order + 1] = NULL;
		}

		// Update the last child
		newnode->children[tree_order - 1] = base->children[base->item_count];
		base->children[base->item_count] = NULL;
		newnode->VN++;

		newnode->minKey = newnode->items[0]->key;
		newnode->maxKey = newnode->items[newnode->item_count - 1]->key;

		maxRange = newnode->maxKey;

		// newleaf gets the safe level as its left sibling (old leaf)
		// leaf levels are always 0
		newnode->level = base->level;

		// update the internal node being split

		promotedItem = base->items[tree_order];
		base->items[tree_order] = NULL;

		base->item_count = (base->item_count) - (tree_order);

		// minKey remains constant

		if (base->items[base->item_count - 1]->key <= base->maxKey) {
			base->maxKey = base->items[base->item_count - 1]->key;
		}
		// Increment the leaf VN

		base->VN++;

		rightSibling = base->right;

		while (rightSibling != NULL) {
			if (rightSibling->VN != -1) {
				break;
			}
			rightSibling = rightSibling->right;
		}

		// Set up the links

		newnode->right = rightSibling;
		base->right = newnode;

		//   printf("executeSMONode(): 1 Newly created nodes\n");
		//   nodePrint(base);
		//   nodePrint(newnode);

		//   nodeKeyPrint(base, 1, 1);
		//   nodeKeyPrint(newnode, 2, 1);
	} else if (base->item_count == tree_order) {

		node_t *current = base->right;

		while ((current != NULL)
				&& ((current->item_count) <= order_upper_limit)) {
			current = current->right;
		}

		if (current != NULL) {
			base = current;
			smonode->node = base;
		} else {
			waitingSMO--;
			return;
		}

		//   waitingSMO--;
		//  return;

	}

	if ((base->item_count) >= (2 * tree_order)) {

		newnode = create_bptnode(tree_order, NONLEAF_NODE);
		newnode->item_count = base->item_count - (tree_order + 1);

		//    printf("splitInternalNode() 2: creating a new node 0x%x\n", newnode);

		for (i = 0; i < newnode->item_count; i++) {
			newnode->items[i] = base->items[i + tree_order + 1];
			base->items[i + tree_order + 1] = NULL;
			newnode->children[i] = base->children[i + tree_order + 1];
			base->children[i + tree_order + 1] = NULL;
		}

		// Update the last child
		newnode->children[newnode->item_count]
				= base->children[base->item_count];
		newnode->VN++;

		newnode->minKey = newnode->items[0]->key;
		newnode->maxKey = newnode->items[newnode->item_count - 1]->key;

		maxRange = newnode->maxKey;

		// newleaf gets the same level as its left sibling (old leaf)
		// leaf levels are always 0
		newnode->level = base->level;

		// update the leaf being split

		promotedItem = copy_kvnode(base->items[tree_order]);
		free_kvnode(base->items[tree_order]);
		base->items[tree_order] = NULL;

		base->item_count = base->item_count - (tree_order + 1);

		if (base->items[base->item_count - 1]->key <= base->maxKey) {
			base->maxKey = base->items[base->item_count - 1]->key;
		}
		// Increment the leaf VN

		base->VN++;

		rightSibling = base->right;

		while (rightSibling != NULL) {
			if (rightSibling->VN != -1) {
				break;
			}
			rightSibling = rightSibling->right;
		}

		// Set up the links

		newnode->right = rightSibling;
		base->right = newnode;

		//    printf("splitInternalNode() 2: splitted nodes\n");
		//    nodePrint(base);
		//    nodePrint(newnode);

		//    nodeKeyPrint(base, 1, 1);
		//    nodeKeyPrint(newnode, 2, 1);
	}

	node_t *parent = NULL;

	// Get the pointer to the possible parent
	if (smonode->index == 0) {

		// Two cases: ROOTNODE is being split
		if (base == ROOTNODE) {
			parent = create_bptnode(tree_order, NONLEAF_NODE);
			//      printf("splitInternalNode(): created a new parent node 0x%x\n", parent);
			parent->level = base->level + 1;
			nodeKeyPrint(parent, 2, 1);
		} else {
			parent = findParentNode(base);

			if (parent->level != (base->level) + 1) {
				printf(
						"2 RRB Error! Parent 0x%x of node 0x%x must have level %d has level %d\n",
						parent, base, (base->level) + 1, parent->level);
				nodePrint(parent);
				nodePrint(base);
				exit(0);
				exit(0);

			}

			nodeKeyPrint(parent, 2, 1);
		}

		// Actual Insertion
		if (parent->item_count == 0) {
			// NEW ROOTNODE

			parent->items[0] = promotedItem;
			parent->children[0] = base;
			parent->children[1] = base->right;
			parent->item_count++;
			parent->VN++;
			parent->minKey = promotedItem->key;
			parent->maxKey = promotedItem->key;

			markRootNode(parent);
		} else {

			j = 0;
			for (i = 0; i < parent->item_count; i++) {
				if (promotedItem->key <= parent->items[i]->key) {
					if (promotedItem->key == parent->items[i]->key) {
						while (parent->items[i + j]->value
								> promotedItem->value) {
							j++;
						}
						break;
					}
					break;
				}
			}

			offset = i + j;

			for (i = parent->item_count; i > offset; i--) {
				parent->items[i] = parent->items[i - 1];
			}
			parent->items[offset] = promotedItem;

			if (offset != parent->item_count) {
				for (i = parent->item_count; i > offset; i--) {
					parent->children[i + 1] = parent->children[i];
				}

				parent->children[offset + 1] = parent->children[offset]->right;
			} else {
				parent->children[offset + 1] = parent->children[offset]->right;
			}

			parent->item_count++;
			parent->VN++;

			if (promotedItem->key < parent->minKey) {
				parent->minKey = promotedItem->key;
			}

			if (promotedItem->key > parent->maxKey) {
				parent->maxKey = promotedItem->key;
			}

			//        printf("splitInternalNode() 3: splitted parents\n");
			//        nodePrint(parent);

			//	nodeKeyPrint(parent, 1, 1);

		}

		if (parent->item_count > order_upper_limit) {

			cursor_t **smo_cursors = NULL;

			node_t *gp = findParentNode(parent);

			if (gp == NULL) {
				gp = ROOTNODE;
			}

			smo_cursors = (cursor_t **) malloc(MAX_LEVELS*sizeof(cursor_t *));

			for (i = 0; i < MAX_LEVELS; i++) {
				smo_cursors[i] = NULL;
			}

			for (i = 0; i < smonode->index; i++) {
				smo_cursors[i] = copy_cursor(smonode->cursors[i]);
			}

			if (smo_cursors[smonode->index] == NULL) {
				smo_cursors[smonode->index] = create_cursor(gp);
			} else {
				update_cursor(smo_cursors[smonode->index], gp);
			}

			// index points to the parent


			smonode_t *smonode2 = create_smonode(parent, NULL, SMO_SPLIT,
					smonode->index, &smo_cursors[0]);
			updateSMORange(smonode2, maxRange);

			int val = enqSMONodeLocal(rtID, smonode2);

			smonode2 = deqSMONodeLocal(rtID);
			while (smonode2 != NULL) {

				executeSMO(rtID, smonode2);
				smonode2 = deqSMONodeLocal(rtID);
			}
		} else {
			waitingSMO--;
		}

		return;
	} else {

		// RRB CHANGE 1/20/08
		parent = smonode->cursors[(smonode->index)]->node;

		if (parent->level > base->level + 1) {
			parent = findParentNode(base);
		}

#if 0
		clean_cursor(smonode->cursors[(smonode->index)]);
		free_cursor(smonode->cursors[(smonode->index)]);
#endif

		cursor_t **smo_cursors = (cursor_t **) malloc(MAX_LEVELS
		* sizeof(cursor_t *));

		for (i = 0; i < MAX_LEVELS; i++) {
			smo_cursors[i] = NULL;
		}

		for (i = 0; i < smonode->index; i++) {
			smo_cursors[i] = copy_cursor(smonode->cursors[i]);
		}

		smonode_t *smonode2 = create_smonode(parent, promotedItem, SMO_INSERT,
				(smonode->index) - 1, &smo_cursors[0]);
		setInsertSibling(smonode2, newnode);
		updateSMORange(smonode2, maxRange);

		if (parent->level > base->level + 1) {
			node_t *newparent = findParentNode(base);
			printf("RRB. Found the level mismatch\n");
			printf("********************************\n");
			nodePrint(base);
			nodePrint(newnode);
			nodePrint(parent);
			nodePrint(newparent);
			printf("********************************\n");

		}
		// smonodePrint(smonode);
		//  printf("splitInternalNode(): Created a INSERT SMO smoID=%d to insert  insertedItem key=%d value=%d\n", smonode2->ID, promotedItem->key, promotedItem->value);
		// nodePrint(parent);
		// smonodePrint(smonode2);

#if 0
		// update the smonode
		smonode->node = parent;
		smonode->item = promotedItem;
		smonode->op = SMO_INSERT;
		smonode->index = (smonode->index)-1;

		smonode->cstamp = parent->smoCount;
		parent->smoCount++;
#endif

		int val = enqSMONodeLocal(rtID, smonode2);
		smonode2 = deqSMONodeLocal(rtID);
		while (smonode2 != NULL) {

			executeSMO(rtID, smonode2);
			smonode2 = deqSMONodeLocal(rtID);
		}
	}

}

void firstRootSplit() {

	int i = 0;
	node_t *rightChild = NULL, *leftChild = NULL;

	rightChild = create_bptnode(tree_order, LEAF_NODE);
	leftChild = create_bptnode(tree_order, LEAF_NODE);

	// First instantiate the leftChild

	nodeKeyPrint(ROOTNODE, 0, 0);

	for (i = 0; i < tree_order; i++) {
		leftChild->items[i] = copy_kvnode(ROOTNODE->items[i]);
	}

	leftChild->VN++;
	leftChild->item_count = tree_order;

	leftChild->level = 0;
	leftChild->refCount = 0;

	leftChild->maxKey = leftChild->items[tree_order - 1]->key;
	leftChild->minKey = leftChild->items[0]->key;

	LMLEAF = leftChild;

	// Now instantiate the rightChild

	for (i = 0; i < tree_order; i++) {
		rightChild->items[i] = copy_kvnode(ROOTNODE->items[tree_order + i]);
	}

	rightChild->VN++;
	rightChild->item_count = tree_order;

	rightChild->level = 0;
	rightChild->refCount = 0;

	rightChild->maxKey = rightChild->items[tree_order - 1]->key;
	rightChild->minKey = rightChild->items[0]->key;

	// Setup the links
	leftChild->right = rightChild;

	// Update the rootnode

	markInternalNode(ROOTNODE);

	if (ROOTNODE->children == NULL) {
		ROOTNODE->children = (node_t **) malloc((2 * tree_order + 2)
				* sizeof(node_t *));
		for (i = 0; i < (2 * tree_order + 2); i++) {
			ROOTNODE->children[i] = NULL;
		}
	}

	ROOTNODE->item_count = 1;
	kvnode_t *newitem = copy_kvnode(ROOTNODE->items[tree_order - 1]);

	for (i = 0; i < 2 * tree_order; i++) {
		free_kvnode(ROOTNODE->items[i]);
		ROOTNODE->items[i] = NULL;
	}

	ROOTNODE->items[0] = newitem;

	ROOTNODE->children[0] = leftChild;
	ROOTNODE->children[1] = rightChild;

	ROOTNODE->VN++;
	ROOTNODE->level++;

	root_level = ROOTNODE->level;

	ROOTNODE->maxKey = ROOTNODE->items[0]->key;
	ROOTNODE->minKey = ROOTNODE->items[0]->key;

	nodeKeyPrint(ROOTNODE, 2, 1);
	nodeKeyPrint(leftChild, 2, 0);
	nodeKeyPrint(rightChild, 2, 0);

}

node_t *findParentNode(node_t *child) {

	node_t *parent = NULL, *current = ROOTNODE, *node = NULL;
	int level = child->level;
	int current_level = current->level;

	int i = 0;

	if (child == ROOTNODE) {
		return ROOTNODE;
	}

	while (current_level > level) {

		for (i = 0; i < current->item_count; i++) {
			if (child->maxKey <= current->items[i]->key) {
				node = current->children[i];
				if (node == child) {
					return current;
				}
				if (node->level == (child->level) + 1) {
					return node;
				}
				break;
			}
		}

		if (child->maxKey > current->items[current->item_count - 1]->key) {
			node = current->children[current->item_count];
			if (node == child) {
				return current;
			} else {
				if ((node->right) == child) {
					return current;
				}
			}
		}

		if ((node != NULL) && (node->level != 0)) {
			parent = current;
			current = node;
		}

		current_level = current->level;
	}

	if (current->level == child->level) {
		/*
		 printf("3 RRB Error in findParentNode(). Mismatch in the level\n");
		 nodePrint(parent);
		 llprint();
		 exit(0);
		 */
		return parent;
	}

	return current;

}

#if 0
node_t *findParentNode(node_t *child) {

	node_t *current = ROOTNODE, *node=NULL;
	int child_level = child->level;
	int current_level = current->level;

	int i=0, done=0, proceed=0;

	if (child == ROOTNODE) {
		return ROOTNODE;
	}

	while (current_level> child_level) {
		proceed =0, done=0;

		for(i=0; i < current->item_count; i++) {
			if ((child->maxKey <= current->items[i]->key) && (!proceed)) {
				node = current->children[i];
				proceed =1;
			}
		}

		if (proceed == 0) {
			if (child->maxKey> current->items[current->item_count-1]->key) {
				node = current->children[current->item_count];
				if (node == child) {
					done = 1;
				}
				else if (node->right != NULL) {
					node = node->right;
					if (node == child) {
						done = 1;
					}
				}
			}
		}

		if (done) {
			return current;
		}

		if ((proceed) && (node != NULL) && (node->level != 0)) {
			current = node;
		}
		else {
			return current;
		}

		current_level = current->level;
	}

	return current;

}
#endif

node_t *findParentNodeOffset(node_t *child, int *offset) {

	node_t *current = ROOTNODE, *node;
	int level = child->level;
	int current_level = current->level;

	int i = 0;

	if (child == ROOTNODE) {
		*offset = -1;
		return NULL;
	}

	while (current_level > level) {

		for (i = 0; i < current->item_count; i++) {
			if (child->maxKey <= current->items[i]->key) {
				node = current->children[i];
				*offset = i;
				break;
			}
		}

		if (child->maxKey > current->items[current->item_count - 1]->key) {
			node = current->children[current->item_count];
			*offset = current->item_count;
		}

		if (node == child) {
			return current;
		}

		current = node;

		current_level = current->level;
	}

	return NULL;
}

node_t *findLeftSibling(node_t *parent, node_t *child, int *child_offset) {

	int i = 0, j = 0, offset = -1, offset2 = -1, done = 0;
	node_t *lsib = NULL, *lsib2 = NULL, *currentp = NULL, *currentc = NULL,
			*pastp = NULL;

	for (i = 0; i <= parent->item_count; i++) {
		if (parent->children[i] == child) {
			offset = i;
			*child_offset = i;
			break;
		}
	}

	lsib = NULL;
	if (parent != NULL) {

		if (offset > 0) {
			offset--;

			lsib = parent->children[offset];

			while ((lsib->VN == -1) && (offset > 0)) {
				offset--;

				lsib = parent->children[offset];
			}

			if (lsib != NULL) {
				if (lsib->VN == -1) {
					return NULL;
				}
			}

			return lsib;
		} else if (offset <= 0) {

			if ((parent == ROOTNODE) && (parent->hasSingleChild)) {
				return NULL;
			}

			lsib = NULL;

			lsib_t **sibArray = (lsib_t **) malloc(((ROOTNODE->level) + 1)
					* sizeof(lsib_t *));

			int index = 0;

			node_t *gp = findParentNodeOffset(parent, &offset2);

			if (gp == NULL) {
				return NULL;
			}

			while (!done) {
				if (offset2 <= 0) {

					int done = 0;
					int firstChild = 0;

					node_t *gp2 = findParentNodeOffsetCursors(parent, &offset2,
							&index, &sibArray);

					while (!done) {
						int index2 = 0; // Upward pass
						for (i = index; i >= 0; i--) {

							if (sibArray[i] == NULL)
								return NULL;

							firstChild += sibArray[i]->offset;

							if (sibArray[i]->offset > 0) {
								index2 = i;
								break;
							}
						}

						if (index2 > 0) {
							currentp = sibArray[index2 - 1]->node;
						} else {
							currentp = ROOTNODE;
							if (firstChild == 0) {
								return NULL;
							}
						}

						// Downward pass

						for (i = index2; i <= index; i++) {
							if (sibArray[i]->offset > 0) {
								offset2 = sibArray[i]->offset - 1;
							} else {
								offset2 = currentp->item_count;
							}
							pastp = currentp;
							currentp = currentp->children[offset2];
							if (currentp == NULL) {
								sibArray[i]->offset = 0;
								done = 0;
								break;
							}
							while ((currentp->VN == -1) && (offset2 > 0)) {
								offset2--;
								currentp = pastp->children[offset2];
							}
							if ((currentp->VN == -1) && (offset2 == 0)) {
								sibArray[i]->offset = 0;
								done = 0;
								break;
							}
						}
						done = 1;
					}

					for (i = currentp->item_count; i >= 0; i--) {
						if (currentp->children[i]->VN != -1) {
							lsib = currentp->children[i];
							break;
						}
					}

					return lsib;
				} else {
					while (offset2 > 0) {
						lsib2 = gp->children[offset2 - 1];

						while ((lsib2->VN == -1) && (offset2 > 0)) {
							offset2--;
							lsib2 = gp->children[offset2];

						}

						if (lsib2->VN == -1) {
							return NULL;
						}

						int count = lsib2->item_count;
						lsib = lsib2->children[count];

						if (lsib->VN != -1) {
							done = 1;

							break;
						}

						lsib = NULL;

						while ((count >= 0) && (lsib2->children[count] != NULL)) {
							if (lsib2->children[count]->VN == -1) {
								count--;
							} else {
								break;
							}
						}
						if (count >= 0) {
							lsib = lsib2->children[count];
							done = 1;
							break;
						}

						offset2--;
					}
				}
			}
		}

		if (lsib != NULL) {
			if (lsib->VN == -1) {
				return NULL;
			}
		}

		return lsib;
	}

	if (lsib != NULL) {
		if (lsib->VN == -1) {
			return NULL;
		}
	}

	return lsib;
}

node_t *findParentNodeOffsetCursors(node_t *child, int *offset, int *index,
		lsib_t ***lsib) {

	node_t *current = ROOTNODE, *node;
	int level = child->level, index2;
	int current_level = current->level; // ROOTNODE->level
	lsib_t *lsibitem = NULL;

	int i = 0, j = 0;

	if (child == ROOTNODE) {
		*offset = -1;
		return ROOTNODE;
	}

	*index = 0;
	while (current_level > level) {

		for (i = 0; i < current->item_count; i++) {
			if (child->maxKey <= current->items[i]->key) {
				node = current->children[i];
				j = i;
				while ((current->children[j]->VN == -1) && (j > 0)) {
					j--;
				}
				*offset = j;
				lsibitem = (lsib_t *) malloc(sizeof(lsib_t));
				lsibitem->level = current_level;
				lsibitem->offset = i;
				lsibitem->node = node;

				(*lsib)[*index] = lsibitem;
				break;
			}
		}

		if (child->maxKey > current->items[current->item_count - 1]->key) {
			node = current->children[current->item_count];
			j = current->item_count;
			while ((current->children[j]->VN == -1) && (j > 0)) {
				j--;
			}
			*offset = j;

			lsibitem = (lsib_t *) malloc(sizeof(lsib_t));
			lsibitem->level = current_level;
			lsibitem->offset = *offset;
			lsibitem->node = node;
			(*lsib)[*index] = lsibitem;
		}

		if (node == child) {
			return current;
		}

		current = node;

		current_level = current->level;
		(*index)++;
	}

	return NULL;
}

node_t *findLeftSiblingCursors(node_t *parent, node_t *child, int *child_offset) {

	int i = 0, offset = -1, offset2 = -1, done = 0;
	node_t *lsib = NULL, *lsib2 = NULL;

	for (i = 0; i <= parent->item_count; i++) {
		if (parent->children[i] == child) {
			offset = i;
			*child_offset = i;
			break;
		}
	}

	lsib = NULL;
	if (parent != NULL) {

		if (offset > 0) {
			offset--;

			lsib = parent->children[offset];

			while ((lsib->VN == -1) && (offset > 0)) {
				offset--;

				lsib = parent->children[offset];
			}

			if (lsib != NULL) {
				if (lsib->VN == -1) {
					return NULL;
				}
			}

			return lsib;
		} else if (offset <= 0) {
			lsib = NULL;
			node_t *cls = NULL;
			lsib_t **sibArray = (lsib_t **) malloc((parent->level)
					* sizeof(lsib_t *));

			int index = 0;
			node_t *gp = findParentNodeOffsetCursors(parent, &index, &offset2,
					&sibArray);

			if (gp == NULL) {
				return NULL;
			}

			while (!done) {
				if (offset2 <= 0) {

					done = 1;
					break;
				} else {
					while (offset2 > 0) {
						lsib2 = gp->children[offset2 - 1];

						while ((lsib2->VN == -1) && (offset2 > 0)) {
							offset2--;
							lsib2 = gp->children[offset2];

						}

						if (lsib2->VN == -1) {
							return NULL;
						}

						int count = lsib2->item_count;
						lsib = lsib2->children[count];

						if (lsib->VN != -1) {
							done = 1;

							break;
						}

						lsib = NULL;

						while ((count >= 0) && (lsib2->children[count] != NULL)) {
							if (lsib2->children[count]->VN == -1) {
								count--;
							} else {
								break;
							}
						}
						if (count >= 0) {
							lsib = lsib2->children[count];
							done = 1;
							break;
						}

						offset2--;
					}
				}
			}
		}

		if (lsib != NULL) {
			if (lsib->VN == -1) {
				return NULL;
			}
		}

		return lsib;
	}

	if (lsib != NULL) {
		if (lsib->VN == -1) {
			return NULL;
		}
	}

	return lsib;
}

void fixParentCursor(int rtID, int offset) {

	int i = 0, j = 0;
	node_t *child = NULL, *parent = NULL;
	if (offset <= 0)
		return;

	if (cursors[rtID][offset] == NULL)
		return;

	if (cursors[rtID][offset - 1] == NULL)
		return;

	for (i = offset; i > 0; i--) {
		child = cursors[rtID][i]->node;
		parent = cursors[rtID][i - 1]->node;

		//   printf("Fixing parentCursor child=0x%x parent=0x%x\n", child, parent);

		//  nodePrint(child);
		//  nodePrint(parent);

		if (child->minKey > parent->maxKey) {
			if (child != parent->children[parent->item_count]) {

				node_t *rs = parent->right;

				while (rs != NULL) {
					if (rs->VN != -1) {
						rs = rs->right;
					} else {
						break;
					}
				}
				if (rs != NULL) {

					for (j = 0; j <= rs->item_count; j++) {
						if (child == rs->children[j]) {
							update_cursor(cursors[rtID][i - 1], rs);
							break;

							//   printf("Updating parentCursor parent=0x%x newParent=0x%x\n", parent, rs);

							//    nodePrint(parent);
							//  nodePrint(rs);
						}
					}
				}
			}
		}
	}

}

/* Supporting Functions */

/*
 * last checked 4/16/07 RRB
 */

kvnode_t *create_kvnode(int key, int value) {
	kvnode_t *item = (kvnode_t *) malloc(sizeof(kvnode_t));
	item->key = key;
	item->value = value;

	return item;
}

kvnode_t *copy_kvnode(kvnode_t *node) {
	kvnode_t *item = (kvnode_t *) malloc(sizeof(kvnode_t));
	item->key = node->key;
	item->value = node->value;

	return item;
}

void free_kvnode(kvnode_t *node) {
	if (node) {
		free(node);
	}
}

int getKey(kvnode_t *item) {
	return item->key;
}

int getValue(kvnode_t *item) {
	return item->value;
}

node_t *create_bptnode(int order, int type) {

	node_t *node = NULL;
	int i = 0;

	node = (node_t *) malloc(sizeof(node_t));

	node->item_count = 0;
	node->items = (kvnode_t **) malloc((2 * order + 2) * sizeof(kvnode_t *));
	node->maxKey = MIN_KEY_LIMIT;
	node->minKey = MAX_KEY_LIMIT;
	// #children is always equals item_count+1
	if (type == NONLEAF_NODE) {
		node->children = (node_t **) malloc(((2 * order + 2) + 1)
				* sizeof(node_t *));
		for (i = 0; i < (2 * order + 3); i++) {
			node->children[i] = NULL;
		}
	} else {
		node->children = NULL;
	}

	node->type = type;
	node->right = NULL;
	node->VN = 0;
	node->refCount = 0;
	node->level = 0;
	node->status = NORMAL;
	node->smoCount = 0;
	node->exeSMOCount = 0;
	node->mode = 0;
	node->hasSingleChild = 0;
	node->rwlck = 0;

	return node;
}

/*
 * last checked 4/16/07 RRB
 */

void free_bptnode(node_t *node) {

	int i = 0;
	//  nodePrint(node);
	if (node != NULL) {
		if (node->item_count > 0) {
			for (i = 0; i < node->item_count; i++) {
				if (node->items[i] != NULL) {
					free_kvnode(node->items[i]);
				}
			}
		}

		if (node->items != NULL) {
			free(node->items);
		}

		if (node->children != NULL) {
			for (i = 0; i < node->item_count; i++) {
				if (node->children[i] != NULL) {
					free_bptnode(node->children[i]);
				}
			}
			if (node->children[node->item_count] != NULL) {
				free_bptnode(node->children[node->item_count]);
			}
		}
		free(node->children);
		free(node);
	}

}

/*
 * last checked 4/16/07 RRB
 */

void setTreeOrder(int order) {
	tree_order = order;
}

/*
 * last checked 4/16/07 RRB
 */

int getTreeOrder() {
	return tree_order;
}

/*
 * last checked 4/16/07 RRB
 */

// This is only invoked in the splitInternalNode()
void markRootNode(node_t *node) {
	ROOTNODE = node;
}

/*
 * last checked 4/16/07 RRB
 */

void markInternalNode(node_t *node) {
	node->type = NONLEAF_NODE;
}

/*
 * last checked 4/16/07 RRB
 */

void markLeafNode(node_t *node) {
	node->type = LEAF_NODE;
}

/*
 * last checked 4/16/07 RRB
 */

short isLeafNode(node_t *node) {
	if (node->type == LEAF_NODE) {
		return 1;
	}
	return 0;
}

/*
 * last checked 4/16/07 RRB
 */

short isRootNode(node_t *node) {
	if (ROOTNODE == node) {
		return 1;
	}
	return 0;
}

/*
 * last checked 4/16/07 RRB
 */

short isInternalNode(node_t *node) {
	if (node->type == NONLEAF_NODE) {
		return 1;
	}
	return 0;
}

/*
 * last checked 4/16/07 RRB
 */

int getItemCount(node_t *node) {
	return node->item_count;
}

void initializeBPTree(int order) {
	tree_order = order; // t
	order_lower_limit = tree_order - 1; // t-1
	order_upper_limit = 2 * tree_order - 1; // 2t-1

	//  printf("order_lower_limit=%d order_upper_limit=%d\n", order_lower_limit, order_upper_limit);
}

/*
 * Free the entire tree
 * Traverses the tree in the depth-first manner and cleans it in pre-order
 * (children before parent, left child before right child)
 * RRB
 */

void freeBPTree() {

	if (ROOTNODE != NULL) {
		if (isLeafNode(ROOTNODE)) {
			free_bptnode(ROOTNODE);
		} else {
			traverse_FreeTree(ROOTNODE);
			free_bptnode(ROOTNODE);
		}
	}
}

cursor_t *create_cursor(node_t *node) {

	cursor_t *cursor = (cursor_t *) malloc(sizeof(cursor_t));
	cursor->node = node;
	cursor->VN = node->VN;
	cursor->refCount = 0;

	node->refCount++;

	return cursor;
}

cursor_t *copy_cursor(cursor_t *cursor) {

	cursor_t *newcursor = (cursor_t *) malloc(sizeof(cursor_t));

	newcursor->node = cursor->node;
	newcursor->VN = cursor->VN;

	(cursor->node)->refCount++;

	return newcursor;
}

void clean_cursor(cursor_t *cursor) {
	if (cursor->node != NULL) {
		cursor->node->refCount--;
	}
	cursor->node = NULL;
	cursor->VN = 0;
}

void update_cursor(cursor_t *cursor, node_t *node) {
	if (cursor->node != NULL) {
		cursor->node->refCount--;
	}
	cursor->node = node;
	cursor->VN = node->VN;
	node->refCount++;
}

void free_cursor(cursor_t *cursor) {

	if (cursor == NULL)
		return;
	/*
	 if (cursor->node != NULL){
	 (cursor->node)->refCount--;
	 }*/

	free(cursor);

}

smonode_t *create_smonode(node_t *node, kvnode_t *item, int op, int index,
		cursor_t **cursors) {

	smonode_t *smonode = NULL;
	smonode = (smonode_t *) malloc(sizeof(smonode_t));

	smonode->node = node;
	smonode->item = item;
	smonode->op = op;
	smonode->index = index;
	smonode->next = NULL;
	smonode->cursors = cursors;
	smonode->ID = smoID;
	smonode->refCount = 0;
	smonode->cstamp = 0;
	smonode->maxRange = 0;
	smoID++;

	smonode->cstamp = node->smoCount;
	node->smoCount++;

	//  printf("smonode 0x%x with ID %d has cstamp=%d with node 0x%x smoCount=%d\n", smonode, smonode->ID, smonode->cstamp, node, node->smoCount);


	return smonode;
}

void updateSMORange(smonode_t *smonode, int range) {
	smonode->maxRange = range;
}

int getMaxRange(node_t *node) {

	int items = node->item_count, j = 0;

	j = items;

	while (j >= 0) {

		if (node->children[j]->VN == -1) {
			j--;
		} else {
			return node->children[j]->maxKey;
		}

	}

	return node->maxKey;
}

void setInsertSibling(smonode_t *smonode, node_t *node) {
	//  printf("RRB Inserting rightSibling 0x%x\n", node);
	// nodePrint(node);
	smonode->rightSibling = node;
}

void free_smonode(smonode_t *smonode) {
	int i = 0;

	if (smonode != NULL) {
		if (smonode->cursors != NULL) {
			for (i = 0; i < smonode->index; i++) {
				free_cursor(smonode->cursors[i]);
			}
			free(smonode->cursors);
		}
		smonode->cursors = NULL;
		smonode->node = NULL;
		smonode->item = NULL;

		free(smonode);
	}
}

/*
 * Enqueue the SMOQueue
 */

int enqSMONodeLocal(int rtID, smonode_t * smonode) {

	smonode_t *qHead = threadQHead[rtID];
	smonode_t *qTop = threadQTop[rtID];

	node_t *current = NULL;
	int oldValue = -1;

	insertSMOCount++;

	//  current = smonode->node;
	//  current->status = IN_SMO_Q;

	smonode->refCount++;

	if (qHead == NULL) {
		qHead = smonode;
		threadQHead[rtID] = qHead;
		threadQTop[rtID] = qHead;
		qHead->next = NULL;
	} else {
		qTop->next = smonode;
		threadQTop[rtID] = smonode;
		threadQTop[rtID]->next = NULL;
	}

	return oldValue;
}

smonode_t *deqSMONodeLocal(int rtID) {

	smonode_t *qHead = threadQHead[rtID];
	smonode_t *currentSMO = NULL, *rvalSMO = NULL, *pastSMO = NULL;
	node_t *current = NULL;
	int min_stamp = -1, smocount = 0;

	//  current = smonode->node;
	//  current->status = IN_SMO_Q;

	if (qHead != NULL) {

		currentSMO = qHead;
		current = currentSMO->node;
		qHead = currentSMO->next;
		threadQHead[rtID] = qHead;
		if (threadQHead[rtID] == NULL) {
			threadQTop[rtID] = NULL;
		}
		removeSMOCount++;
		currentSMO->next = NULL;

#if 0
		currentSMO = qHead;
		pastSMO = qHead;

		while (currentSMO != NULL) {
			current = currentSMO->node;

			if (currentSMO->cstamp == current->exeSMOCount) {

				if (qHead == currentSMO) {
					qHead = currentSMO->next;
					threadQHead[rtID] = qHead;
					if (threadQHead[rtID] == NULL) {
						threadQTop[rtID] = NULL;
					}
				}
				else {
					if (pastSMO != NULL) {
						pastSMO->next = currentSMO->next;
					}
				}

				removeSMOCount++;
				currentSMO->next = NULL;

				return currentSMO;
			}
			pastSMO = currentSMO;
			currentSMO = currentSMO->next;
		}

#endif

	}

	return currentSMO;

}

/*
 * Enqueue the SMOQueue
 */

int enqSMONode(smonode_t * smonode) {

	node_t *current = NULL;
	int oldValue = -1;

	//  acquire_mutex(fifo_mutex);
	-insertSMOCount++;

	//  current = smonode->node;
	//  current->status = IN_SMO_Q;

	smonode->refCount++;

	if (smoQHead == NULL) {
		smoQHead = smonode;
		smoQTop = smoQHead;
		smonode->next = NULL;
	} else {
		smoQTop->next = smonode;
		smoQTop = smonode;
		smoQTop->next = NULL;
	}

	//  printf("inserting smonode 0x%x with ID %d has cstamp=%d with node 0x%x smoCount=%d exeSMOCount=%d smoQHead=0x%x\n", smonode, smonode->ID, smonode->cstamp, smonode->node, smonode->node->smoCount, smonode->node->exeSMOCount, smoQHead);
	//  release_mutex(fifo_mutex);

	oldValue = waitingSMO;

	return oldValue;
}

/*
 * Dequeue the SMOQueue
 */

smonode_t *deqSMONode() {

	smonode_t *currentSMO = NULL, *rvalSMO = NULL, *pastSMO = NULL;
	node_t *current = NULL;
	int min_stamp = -1, smocount = 0;

	if (smoQHead != NULL) {
		//    acquire_mutex(fifo_mutex);
		currentSMO = smoQHead;
		current = currentSMO->node;
		smoQHead = currentSMO->next;
		currentSMO->next = NULL;
		removeSMOCount++;
		//    release_mutex(fifo_mutex);
	} // removed from the list


	if (currentSMO != NULL) {

		/*    if (currentSMO->cstamp > current->exeSMOCount){
		 printf("adding smonode=0x%x node=0x%x cstamp=%d exeSMOCount=%d\n",currentSMO, currentSMO->node, currentSMO->cstamp, (currentSMO->node)->exeSMOCount);
		 enqLclQ(currentSMO);
		 return NULL;
		 }
		 */
		//    printf("deqSMONode returning smonode=0x%x\n", currentSMO);
		return currentSMO;
	}

	return NULL;
}

/*
 * enqueue into the local FIFO
 */

void enqLclQ(smonode_t * smonode) {

	node_t *current = NULL, *input = NULL;
	smonode_t *currentSMO = NULL, *pastSMO = NULL;

	input = smonode->node;

	if (lclQHead == NULL) {
		lclQHead = smonode;
		lclQTop = lclQHead;
		smonode->next = NULL;

		return;
	} else {
		currentSMO = lclQHead;
		pastSMO = currentSMO;
		current = currentSMO->node;
		while (currentSMO != NULL) {
			if (!((current == input) && (currentSMO->cstamp < smonode->cstamp))) {
				pastSMO = currentSMO;
				currentSMO = currentSMO->next;
				current = currentSMO->node;
			} else {
				break;
			}
		}

		// insert smonode between pastSMO and currentSMO

		if (pastSMO == lclQHead) {
			smonode->next = currentSMO;
			lclQHead = smonode;
		} else {
			pastSMO->next = smonode;
			smonode->next = currentSMO;
		}
	}

	return;
}

smonode_t *deqLclQ() {

	smonode_t *currentSMO = NULL, *pastSMO = NULL;
	node_t *current = NULL;

	currentSMO = lclQHead;
	pastSMO = lclQHead;

	while (currentSMO != NULL) {
		current = currentSMO->node;

		if (currentSMO->cstamp == current->exeSMOCount + 1) {
			if (lclQHead == currentSMO) {
				lclQHead = currentSMO->next;
			} else {
				if (pastSMO != NULL) {
					pastSMO->next = currentSMO->next;
				}
			}
			return currentSMO;
		}
		pastSMO = currentSMO;
		currentSMO = currentSMO->next;
	}

	return NULL;
}

void resetSMOCounts(node_t *current) {

	int i = 0, j = 0;

	if (!(isLeafNode(current))) {
		current->smoCount = 0;
		current->exeSMOCount = 0;
		for (i = 0; i < current->item_count; i++) {
			resetSMOCounts(current->children[i]);
		}
		resetSMOCounts(current->children[current->item_count]);
	}
}

void traverse_FreeTree(node_t *base) {

	int i = 0;
	node_t *current;

	for (i = 0; i <= base->item_count; i++) {
		current = base->children[i];
		if (isLeafNode(current)) {
			free_bptnode(current);
		} else {
			traverse_FreeTree(current);
			free_bptnode(current);
		}
	}

}

void acquireRootLock(int mode) {

	node_t *oldRootNode = ROOTNODE;

	// First Try to acquire the lock in the specified mode

	if (mode == 0) {
		rlck_acquire(&(ROOTNODE->rwlck));
	} else {
		wlck_acquire(&(ROOTNODE->rwlck));
	}

	if (oldRootNode != ROOTNODE) {
		// Rootnode has changed
		releaseRootLock(mode);

		if (mode == 0) {
			rlck_acquire(&(ROOTNODE->rwlck));
		} else {
			wlck_acquire(&(ROOTNODE->rwlck));
		}

	}
}

void releaseRootLock(int mode) {

	if (mode == 0) {
		rlck_release(&(ROOTNODE->rwlck));
	} else {
		wlck_release(&(ROOTNODE->rwlck));
	}

}

/*
 * pretty print routine RRB 6/13/07
 * Prints a depth-first pre-order traversal of the B+Tree. Useful for debugging purposes.
 */
int depth = 0;

void pprint() {
	int i = 0;

	//  markPhaseCompletion();

	if (waitingSMO > finalSMOCount) {
		finalSMOCount = waitingSMO;
	}

	while ((finalSMOCount - executedSMO) > 0) {
	}

	if (ROOTNODE != NULL) {
		printf("Beginning the traversal of the B-Link Tree.\n");

		if (isLeafNode(ROOTNODE)) {
			printf("\t");
			printf("The B-Link Tree contains only one node\n");
			for (i = 0; i < ROOTNODE->item_count; i++) {
				printf("\t");
				printf("The %d key is %d with value %d\n", i,
						ROOTNODE->items[i]->key, ROOTNODE->items[i]->value);
			}
		} else {
			traverseTreeNode(ROOTNODE, &depth);
		}
		printf("The traversal of the B-Link Tree completed.\n");
	} else {
		printf("The root node is null. Tree is empty. Exiting.\n");
	}

}

void traverseTreeNode(node_t *current, int *depth) {

	int i = 0, j = 0;

	if (isLeafNode(current)) {

		for (i = 0; i < *depth; i++) {
			printf("\t");
		}

		if (current->status == EMPTY) {
			printf(
					"EMPTY LEAFNODE 0x%x has level %d with minKey=%d and maxKey=%d #keys=%d #children=%d\n",
					current, current->level, current->minKey, current->maxKey,
					current->item_count, current->item_count);
		} else {
			printf(
					"LEAFNODE 0x%x has level %d with minKey=%d and maxKey=%d #keys=%d #children=%d\n",
					current, current->level, current->minKey, current->maxKey,
					current->item_count, current->item_count);
		}
		for (i = 0; i < current->item_count; i++) {
			for (j = 0; j < *depth + 1; j++) {
				printf("\t");
			}
			printf("The %d key has value %d and RID %d\n", i,
					current->items[i]->key, current->items[i]->value);
		}
		for (j = 0; j < *depth; j++) {
			printf("\t");
		}
		printf("Leaf traversal completed.\n");

	} else {
		if (current == ROOTNODE) {
			printf(
					"ROOTNODE 0x%x has level %d with minKey=%d and maxKey=%d #keys=%d #children=%d\n",
					current, current->level, current->minKey, current->maxKey,
					current->item_count, current->item_count + 1);
		} else {
			for (i = 0; i < *depth; i++) {
				printf("\t");
			}
			if (current->status == EMPTY) {
				printf(
						"EMPTY INDEXNODE 0x%x has level %d with minKey=%d and maxKey=%d\n",
						current, current->level, current->minKey,
						current->maxKey);
			} else {
				printf(
						"INDEXNODE 0x%x has status %d level %d with minKey=%d and maxKey=%d #keys=%d #children=%d\n",
						current, current->status, current->level,
						current->minKey, current->maxKey, current->item_count,
						current->item_count + 1);

			}
		}
		for (i = 0; i < current->item_count; i++) {
			for (j = 0; j < *depth; j++) {
				printf("\t");
			}
			printf("The node 0x%x BEFORE key[%d] with value %d\n",
					current->children[i], i, current->items[i]->key);
			(*depth)++;
			traverseTreeNode(current->children[i], depth);
			(*depth)--;
		}

		for (i = 0; i < *depth; i++) {
			printf("\t");
		}
		if (current->item_count > 0) {
			printf("The node 0x%x AFTER key[%d] with value %d\n",
					current->children[current->item_count], current->item_count
							- 1, current->items[current->item_count - 1]->key);
			(*depth)++;
			traverseTreeNode(current->children[current->item_count], depth);
			(*depth)--;
		}
	}

}

void llprint() {

	node_t *current = LMLEAF;
	int i, j;

	printf("\n\n\n\n");
	rootPrint();

	while (current != NULL) {
		printf(
				"LEAFNODE has level %d with minKey=%d and maxKey=%d #keys=%d #children=%d\n",
				current->level, current->minKey, current->maxKey,
				current->item_count, current->item_count);

		for (i = 0; i < current->item_count; i++) {
			printf("\t");
			printf("The %d key has value %d and RID %d\n", i,
					current->items[i]->key, current->items[i]->value);
		}
		current = current->right;
	}
	printf("\n\n\n\n");
}

void rootPrint() {
	node_t *current = ROOTNODE;
	printf(
			"ROOTNODE has level %d with minKey=%d and maxKey=%d #keys=%d #children=%d\n",
			current->level, current->minKey, current->maxKey,
			current->item_count, current->item_count + 1);
}

void nodePrint(node_t *node) {
	node_t *current = node;
	int i = 0;
	if (current != NULL) {
		printf(
				"Current node 0x%x has level %d with minKey=%d and maxKey=%d #keys=%d #children=%d mode=%d hasSingeClhild=%d\n",
				current, current->level, current->minKey, current->maxKey,
				current->item_count, current->item_count + 1, current->mode,
				current->hasSingleChild);
		printf("Keys:");
		for (i = 0; i < node->item_count; i++) {
			printf(" %d ", node->items[i]->key);
		}
		printf("\n");

		if ((node->children != NULL) && (node->VN != -1)) {

			for (i = 0; i <= node->item_count; i++) {
				current = node->children[i];
				printf(
						" Child node 0x%x has level %d with minKey=%d and maxKey=%d #keys=%d #children=%d mode=%d\n",
						current, current->level, current->minKey,
						current->maxKey, current->item_count,
						current->item_count + 1, current->mode);
			}
		}
	}
}

void leafPrint(node_t *current) {
	int i = 0;

	printf(
			"LEAFNODE 0x%x has level %d with minKey=%d and maxKey=%d #keys=%d #children=%d\n",
			current, current->level, current->minKey, current->maxKey,
			current->item_count, current->item_count);

	for (i = 0; i < current->item_count; i++) {
		printf("\t");
		printf("The %d key has value %d and RID %d\n", i,
				current->items[i]->key, current->items[i]->value);
	}

}

void smonodePrint(smonode_t *currentSMO) {

	printf(
			"Current smonode=0x%x ID=%d node=0x%x cstamp=%d index=%d exeSMOCount=%d rightSibling=0x%x\n",
			currentSMO, currentSMO->ID, currentSMO->node, currentSMO->cstamp,
			currentSMO->index, (currentSMO->node)->exeSMOCount,
			currentSMO->rightSibling);

	int i = 0;

	for (i = 0; i <= currentSMO->index; i++) {
		printf(" Index=%d ", i);
		cursorPrint(currentSMO->cursors[i]);
	}
}

void cursorPrint(cursor_t *cursor) {

	if (cursor == NULL) {
		printf("The cursor is null\n");
	} else {
		printf("Cursor node is ");
		nodePrint(cursor->node);
	}

}

void threadCursorPrint(int rtID, int index) {

	int i = 0;

	printf("Thread %d cursor state: \n", rtID);
	for (i = 0; i < index; i++) {
		cursorPrint(cursors[rtID][i]);
	}
}

void nodeKeyPrint(node_t *current, int mode, int node) {

	int i = 0;

#if 0
	if (mode == 0) {
		printf(" BEFORE ");
	}
	else if (mode == 1) {
		printf(" AFTER ");
	}
	else if (mode == 2) {
		printf(" NEW ");
	}
	if (node == 0) {
		printf(" LEAF ");
	}
	else if (node == 1) {
		printf(" INTERNAL ");
	}
	printf("Node 0x%x has level %d with minKey=%d and maxKey=%d #keys=%d: ", current, current->level, current->minKey, current->maxKey, current->item_count);

	for(i=0; i < current->item_count; i++) {
		printf(" ");
		printf("[%d]=%d ", i, current->items[i]->key);
	}

	printf("\n");
#endif
}

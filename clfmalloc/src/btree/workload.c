#include <pthread.h>
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>

#include "txmanager.h"

#include <sys/time.h>
#include <sys/utsname.h>

#define FETCH 0
#define INSERT 1
#define DELETE 2

int id = 0, THREADS = 0, success = 0, sum = 0, inserts = 0;

//extern void insertKeyValue(int, int);
//extern void pprint();

extern void initializeTManager(int, int);
extern int registerUserThread(int);

pthread_mutex_t mutex;

extern int smo_inserts;
extern int smo_deletes;
extern int smo_splits;

volatile int completedProcs = 0;
volatile int restart = 0;
int successes[128];
int accesses[65536], count = 0;
int rtIDs[128];

void *
do_work(void *data) {
	int rval = -1;
	int *values = NULL;

	int iam = *((int *) data);

	int rtID = registerUserThread(iam);
	rtIDs[iam] = rtID;

	int i = ((iam + 1) * inserts) - 1, j = 0;

	while (j < inserts) {
		//     rval = insert(rtID, i, i);
		rval = insert(rtID, accesses[j], accesses[j]);
		i--;
		j++;
	}

	pthread_mutex_lock(&mutex);
	completedProcs++;
	pthread_mutex_unlock(&mutex);

	while (completedProcs < THREADS) {
	}
	//  printf("User thread %d has completed work. CompletedProcs=%d(%d)\n", rtID, completedProcs,THREADS);

	if (iam == 0) {
		//   llprint();
		//    pprint();
		resetSMOCounters();
		restart = 1;
	} else {
		while (restart != 1) {
		}
	}
}

void *
do_test(void *data) {
	int i = 0, j = 0, rval = -1;
	int *values = NULL;
	int error = 0;

	int iam = *((int *) data);
	int rtID = rtIDs[iam];

	i = iam * inserts;
	j = 0;

	while (j < inserts) {

		//     printf("\n ** Thread %d fetching key %d\n", iam, i);

		values = fetch(rtID, accesses[i]);

		//  values = fetch(rtID, i);

		if (values == NULL) {
			printf("ERROR!. Thread %d(%d) key %d not found\n", iam, i, i);
			error = 1;
			break;
			// pprint();
			//  exit(0);
		} else {
			if (values[0] != accesses[i]) {
				printf("ERROR! Thread %d Value of key %d is %d\n", iam, i,
						values[0]);
				error = 1;
				break;
				//	exit(0);
			} else {
				//		printf("thread %d Value of key %d is %d\n", iam, i, values[0]);
			}
		}

		//  pprint();

		//printf("\n ** Thread %d deleting key %d\n", iam, i);

		//rval = deleteKV(rtID, accesses[j], accesses[j]);
		// rval = deleteKV(rtID, i, i);

		i++;
		j++;
	}

	if (error) {
		successes[iam] = 0;
	} else {
		successes[iam] = 1;
	}

	pthread_mutex_lock(&mutex);
	completedProcs++;
	pthread_mutex_unlock(&mutex);

	while (completedProcs < THREADS) {
	}

	return 0;
}

int main(int argc, char **argv) {
	pthread_t p[MAX_USER_THREADS];
	int i, ids[MAX_USER_THREADS], order, len = 0, read = 0;
	FILE *fp;
	char *line = NULL;

	THREADS = 1;
	order = 2;
	inserts = 128;

	if (argc == 2) {
		order = atoi(argv[1]);
	} else if (argc == 3) {
		order = atoi(argv[1]);
		THREADS = atoi(argv[2]);
	} else if (argc == 4) {
		order = atoi(argv[1]);
		THREADS = atoi(argv[2]);
		inserts = atoi(argv[3]);
	}

	printf("Initializing TManager. Order=%d THREADS=%d inserts=%d\n", order,
			THREADS, inserts);

	initializeTManager(order, THREADS);

	markResetCursor();

	fp = fopen("log", "r");

	i = 0;
	while ((read = getline(&line, &len, fp)) != -1) {
		accesses[i] = atoi(line);
		printf("%d\n", accesses[i]);
		i++;
	}

	// start user threads

	// get start timing
	struct timezone tz;
	struct timeval tv;
	gettimeofday(&tv, &tz);

	unsigned t1 = tv.tv_sec * 1000000 + tv.tv_usec;

	for (i = 0; i < THREADS; i++) {
		ids[i] = i;
		successes[i] = 0;
		pthread_create(&p[i], NULL, *do_work, (void *) &(ids[i]));
	}

	// join
	for (i = 0; i < THREADS; i++) {
		pthread_join(p[i], NULL);
	}

	// get end timing
	gettimeofday(&tv, &tz);
	unsigned t2 = tv.tv_sec * 1000000 + tv.tv_usec;

	completedProcs = 0;

	for (i = 0; i < THREADS; i++) {
		successes[i] = 0;
		pthread_create(&p[i], NULL, *do_test, (void *) &(ids[i]));
	}

	// join
	for (i = 0; i < THREADS; i++) {
		pthread_join(p[i], NULL);
	}

	success = 1;

	for (i = 0; i < THREADS; i++) {
		success *= successes[i];
	}

	if (success) {
		printf("The test is successful.\n");
		/* print timing info */
		struct utsname utsname;
		char *machine_name;
		if (uname(&utsname) < 0)
			machine_name = "(unknown)";
		else
			machine_name = utsname.nodename;
		printf("Timing: %7d usec ", t2 - t1);
		printf("  Machine: %s", machine_name);
		printf("  Procs: %d ", THREADS);
		printf("  Inserts per proc: %d\n", inserts);
	} else {
		printf("The test has failed.\n");
		pprint();
	}

	terminateTManager();

	fp = fopen("log", "w");

	for (i = 0; i < count; i++) {
		fprintf(fp, "%d\n", accesses[i]);
	}

	/*
	 // Note key of value 0 is NOT inserted
	 for(i=0; i <= count; i++){
	 int value = fetch(i);
	 printf("Value of key value %d is %d\n",i, value);
	 }
	 */

	//  printf("The number of SMO inserts=%d Deletes=%d splits=%d\n", smo_inserts, smo_deletes, smo_splits);

	return 0;
}


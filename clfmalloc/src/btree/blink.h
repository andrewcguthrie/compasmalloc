/* blink.h
 *
 * Header file for the B-Link Implementation of a B+ Tree of order t
 * @author=Rajesh Bordawekar March 2007
 *
 * The inner non-root nodes have at least t-1 and at-most 2*t-1 entries. Each inner node can have at least t to at most children.
 * The keys in the inner nodes act as placeholders.
 * All offsets are 0-based.
 *
 */

#ifndef __BLINK_H_
#define __BLINK_H_
#endif

#ifdef __BLINK_H_

#include "lock.h"

#define ROOT_NODE 0
#define LEAF_NODE 1
#define NONLEAF_NODE 2

#define NORMAL 0
#define IN_SMO_Q 1
#define SMO_EXEC 2
#define EMPTY 3

#define SHARED 0
#define EXCL 1 // exclusive

// Modes of predicated fetch

#define FETCH_EQ 0 //equals
#define FETCH_LE 1 // less or equals
#define FETCH_LT 2 // less than
#define FETCH_GT 3 // greater than
#define FETCH_GE 4 // greater or equals


/* Thread states */

#define WAIT -1
#define EXECUTE 0
#define RETURN 1

/* The Item type */

typedef struct kvnode{ // key-value pair
  int key;
  int value;
} kvnode_t;

#define MAX_KEY_LIMIT 0xFFFFFFF
#define MIN_KEY_LIMIT 0

/*
 * This giant data structure should be ideally split into two classes,
 * where the leaf class extends the node class
 */
typedef struct btnode *nodeptr;

typedef struct btnode{
  short type;
  int item_count; // number of pair on node
  int maxKey; // Used by internal nodes
  int minKey;
  kvnode_t **items;
  /* Root level is 0, increments as the tree grows "downwards" */
  int level;
  nodeptr *children;
  nodeptr right;
  int VN;
  int refCount;
  int status;
  short hasSingleChild;
  int smoCount;
  int exeSMOCount;
  int mode;
  int rwlck;
} node_t;

typedef struct cursornode{
  node_t *node;
  int VN; // Note: this is the cached copy of the VN. A node's VN can change during execution.
  int refCount;
} cursor_t;

typedef struct siblingnode{
  int level;
  int offset;
  node_t *node;
} lsib_t;

#define MAX_LEVELS 32
#define MAX_USER_THREADS 128

int root_level; /* Root level+1 is the total number of levels */

/* SMO Operations */

#define SMO_INSERT 1
#define SMO_DELETE 2
#define SMO_SPLIT 3

/* SMO Entry */
typedef struct smonode{
  node_t *node;
  kvnode_t *item; /* Item to be inserted or deleted */
  int op;
  int refCount;
  int index;
  int cstamp;
  int ID;
  int maxRange;
  node_t *rightSibling;
  cursor_t **cursors;
  struct smonode *next;
} smonode_t;

smonode_t *smoQTop;
smonode_t *smoQHead;

smonode_t *lclQTop;
smonode_t *lclQHead;

smonode_t *threadQTop[MAX_USER_THREADS];
smonode_t *threadQHead[MAX_USER_THREADS];

/* Function prototypes */

void initializeTManager(int, int);
void terminateTManager();

node_t *create_bptnode(int, int);
void free_bptnode(node_t *);
kvnode_t *create_kvnode(int, int);
kvnode_t *copy_kvnode(kvnode_t *);
void free_kvnode(kvnode_t *);
int getKey(kvnode_t *);
int getValue(kvnode_t *);
int getMaxRange(node_t *);
void fixParentCursor(int, int);

node_t *splitChild(node_t *, int, node_t *);
void firstRootSplit();
int deleteKeyValue(int, int *, int, int);
smonode_t *safeLeafInsert(int, node_t *, int *);
smonode_t *safeLeafDelete(int, node_t *, int, int *);
void safeDelete(node_t *, node_t *);
void insertKeyValue(int, int *, int, int);
void updateSMORange(smonode_t *, int);
void setInsertSibling(smonode_t *, node_t *);
void nodeKeyPrint(node_t *, int, int);

smonode_t *create_smonode(node_t *, kvnode_t *, int, int, cursor_t **);
int enqSMONode(smonode_t *);
void enqLclQ(smonode_t *);
void free_smonode(smonode_t *);
smonode_t *deqSMONode();
smonode_t *deqLclQ();
int enqSMONodeLocal(int, smonode_t *);
smonode_t *deqSMONodeLocal(int);
int *fetch_internal(int, int, int *);
void fetchLeafNodePredicate(int, int, int, int *, node_t **, node_t **, int *);
void searchSubtree(int, node_t *, int, int, int *, int *, node_t **, node_t **, int *);
void searchNodePredicate(node_t *, int, node_t **, int *);
void pprint();
void llprint();
void nodePrint();
void cursorPrint();
void smonodePrint();
void rootPrint();
void leafPrint();
void threadCursorPrint(int, int);
void executeSMO(int, smonode_t *);
void executeInsert(int, smonode_t *);
void executeDelete(int, smonode_t *);
void splitInternalNode(int, smonode_t *);

cursor_t *create_cursor();
cursor_t *copy_cursor(cursor_t *);
void free_cursor(cursor_t *);
void update_cursor(cursor_t *, node_t *);
void clean_cursor(cursor_t *);

void markRootNode(node_t *);
void markLeafNode(node_t *);
void markInternalNode(node_t *);
int getKeyCount(node_t *);
short isRootNode(node_t *);
short isLeafNode(node_t *);
void traverseTreeNode(node_t *, int *);
void initializeBPTree(int);
void freeBPTree();
void traverse_FreeTree(node_t *);
node_t *findParentNode(node_t *);
node_t *findLeftSibling(node_t *, node_t *, int *);
node_t *findParentNodeOffset(node_t *, int *);
node_t *findLeftSiblingCursors(node_t *, node_t *, int *);
node_t *findParentNodeOffsetCursors(node_t *, int *, int *, lsib_t ***);
void resetSMOCounts(node_t *);

void acquire_shared_latch(node_t *);
void release_shared_latch(node_t *);
void acquire_update_latch(node_t *);
void release_update_latch(node_t *);
void acquire_exclusive_latch(node_t *);
void release_exclusive_latch(node_t *);
void acquire_mutex(pthread_mutex_t *);
void release_mutex(pthread_mutex_t *);

void acquireRootLock(int);
void releaseRootLock(int);

#endif /* _BLINK_H_  */

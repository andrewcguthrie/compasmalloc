#include <pthread.h>
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>

#include "txmanager.h"

#define FETCH 0
#define INSERT 1
#define DELETE 2

int id=0, THREADS=0, success=0, sum=0, inserts=0;

//extern void insertKeyValue(int, int);
//extern void pprint();

extern void initializeTManager(int, int);
extern int registerUserThread(int);

pthread_mutex_t mutex;

extern int smo_inserts;
extern int smo_deletes;
extern int smo_splits;

int completedProcs =0;
volatile int restart = 0;
int successes[32];

void * 
do_work(void *data){
  int rval = -1;
  int *values = NULL;

  int iam = *((int *) data);

  int rtID = registerUserThread(iam);

  printf("User thread %d has a registered thread ID %d\n", iam, rtID);

  int i=iam, j=0;

  while (j < inserts){
    rval = insert(rtID, i, i);
    i = i+THREADS;
    j++;
  }


  pthread_mutex_lock(&mutex);
  completedProcs++;
  pthread_mutex_unlock(&mutex);
  
  printf("User thread %d has completed work. CompletedProcs=%d(%d)\n", rtID, completedProcs,THREADS);

  while (completedProcs < THREADS){}
  
  if (iam == 0){
    //   llprint();
    //   pprint();
    resetSMOCounters();
    restart =1;
  }
  else{
    while (restart != 1){}
  }


  i=iam;
  j=0;

  while (j < inserts){
    values = fetch(rtID, i);

    if (values == NULL){
      printf("ERROR!. key %d not found\n",i);
      pprint();
      exit(0);
    }
    else{
      if (values[0] != i){
	printf("ERROR! Value of key %d is %d\n", i, values[0]);
	exit(0);
      }
      /*  else{
	printf("Value of key %d is %d\n", i, values[0]);
	} */
    }

    rval = deleteKV(rtID, i, i);

    i = i+THREADS;
    j++;
  }


  successes[iam]=1;

  pthread_mutex_lock(&mutex);
  completedProcs++;
  pthread_mutex_unlock(&mutex);

  while (completedProcs < 2*THREADS){}

  if (iam == 0){
    pprint();
  }
  
  /*
  rval = insert(rtID, 0, 0);
  rval = insert(rtID, 4, 4);
  rval = insert(rtID, 8, 8);
  rval = insert(rtID, 12, 12);
  rval = insert(rtID, 16, 16);
  rval = insert(rtID, 20, 20);
  rval = insert(rtID, 24, 24);
  rval = insert(rtID, 1, 1);
  rval = insert(rtID, 5, 5);
  rval = insert(rtID, 9, 9);
  rval = insert(rtID, 9, 9);
  rval = insert(rtID, 2, 2);
 
  rval = insert(rtID, 13,13);
  rval = insert(rtID, 17,17);
  rval = insert(rtID, 21,21);
  rval = insert(rtID, 25,25);
  rval = insert(rtID, 6, 6);
  rval = insert(rtID, 10, 10);
  rval = insert(rtID, 14, 14);
  rval = insert(rtID, 18, 18);
  rval = insert(rtID, 22, 22);
  rval = insert(rtID, 26, 26);
  */


#if 0
  rval = deleteKV(rtID, 1,1);
  rval = deleteKV(rtID, 5,5);
  rval = deleteKV(rtID, 9, 9);
  rval = deleteKV(rtID, 13, 13);
  rval = deleteKV(rtID, 2, 2);
  rval = deleteKV(rtID, 6, 6);
  rval = deleteKV(rtID, 10, 10);
  rval = deleteKV(rtID, 3, 3);

#endif

  return 0;
}



int main (int argc, char **argv)
{
    pthread_t p[MAX_USER_THREADS];
    int i, ids[MAX_USER_THREADS], order;
   
    THREADS = 1;
    order = 2; 
    inserts = 128;

   
    if (argc == 2){
      order = atoi(argv[1]);
    }
    else if (argc == 3){
      order = atoi(argv[1]);
      THREADS = atoi(argv[2]); 
    }
    else if (argc == 4){
      order = atoi(argv[1]);
      THREADS = atoi(argv[2]); 
      inserts = atoi(argv[3]);
    }

    printf("Initializing TManager. Order=%d THREADS=%d inserts=%d\n", order, THREADS, inserts);
    
    initializeTManager(order, THREADS);


    // start user threads
    
    for (i = 0; i < THREADS; i++){
      ids[i] = i;
      successes[i] =0;
      pthread_create (&p[i], NULL, *do_work, (void *)&(ids[i]));
    }
    
    // join
    for (i = 0; i < THREADS; i++){
	pthread_join (p[i], NULL);
    }

    for (i = 0; i < THREADS; i++){
      success += successes[i];
    }

    if (success){
      printf("The test is successful.\n");
    }
    else{
      printf("The test has failed.\n");
    }

    terminateTManager();

#if 0    
    // Note key of value 0 is NOT inserted
    for(i=0; i <= counter; i++){
      int value = fetch(i);
      printf("Value of key value %d is %d\n",i, value);
    }

#endif

    //  printf("The number of SMO inserts=%d Deletes=%d splits=%d\n", smo_inserts, smo_deletes, smo_splits);
    
    return 0;
}


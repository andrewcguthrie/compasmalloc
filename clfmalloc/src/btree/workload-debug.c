#include <pthread.h>
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <fcntl.h>

#include "txmanager.h"

#define FETCH 0
#define INSERT 1
#define DELETE 2

int id=0, THREADS=0, success=0, sum=0, inserts=0;

//extern void insertKeyValue(int, int);
//extern void pprint();

extern void initializeTManager(int, int);
extern int registerUserThread(int);

pthread_mutex_t mutex;

extern int smo_inserts;
extern int smo_deletes;
extern int smo_splits;

int completedProcs =0;
volatile int restart = 0;
int successes[32];
int accesses[1024], count=0;

void * 
do_work(void *data){
  int rval = -1;
  int *values = NULL;

  int iam = *((int *) data);

  int rtID = registerUserThread(iam);

  //  printf("User thread %d has a registered thread ID %d\n", iam, rtID);

#if 0

  if (iam == 0){
    rval = insert(rtID, 13, 13);
    rval = insert(rtID, 27, 27);
    rval = insert(rtID, 26, 26);
    rval = insert(rtID, 25, 25);
    rval = insert(rtID, 24, 24);
    rval = insert(rtID, 23, 23);
    rval = insert(rtID, 22, 22);
    rval = insert(rtID, 21, 21);
    rval = insert(rtID, 20, 20);
    rval = insert(rtID, 19, 19);
    rval = insert(rtID, 12, 12);
    rval = insert(rtID, 11, 11);
    rval = insert(rtID, 10, 10);
    rval = insert(rtID, 9, 9);
    rval = insert(rtID, 8, 8);
    rval = insert(rtID, 7, 7);
    rval = insert(rtID, 6, 6);
    rval = insert(rtID, 5, 5);
    rval = insert(rtID, 4, 4);
    rval = insert(rtID, 3, 3);
    rval = insert(rtID, 2, 2);
    rval = insert(rtID, 1, 1);
    rval = insert(rtID, 0, 0);
    rval = insert(rtID, 18, 18);
    rval = insert(rtID, 17, 17);
    rval = insert(rtID, 16, 16);
    rval = insert(rtID, 15, 15);
    rval = insert(rtID, 14, 14);
  }

  pprint();

#endif


  int i=((iam+1)*inserts)-1, j=0;

   while (j < inserts){
  //  while (j < 39){
     //   rval = insert(rtID, accesses[j], accesses[j]);
    rval = insert(rtID, i, i);
    //   printf("User thread %d inserted item %d\n", rtID, i);
    i--;
    j++;
  }
  

   //  rval = insert(rtID, accesses[(inserts-1)], accesses[(inserts-1)]);


  pthread_mutex_lock(&mutex);
  completedProcs++;
  pthread_mutex_unlock(&mutex);
  
  // printf("User thread %d has completed work. CompletedProcs=%d(%d)\n", rtID, completedProcs,THREADS);

  while (completedProcs < THREADS){}
  
  if (iam == 0){
    // llprint();
    // pprint();
    resetSMOCounters();
    restart =1;
 #if 0   
   rval = deleteKV(rtID, 18, 18);
   rval = deleteKV(rtID, 19, 19);
   rval = deleteKV(rtID, 20, 20);
   rval = deleteKV(rtID, 21, 21);
   rval = deleteKV(rtID, 22, 22);
   rval = deleteKV(rtID, 23, 23);
   rval = deleteKV(rtID, 24, 24);
   rval = deleteKV(rtID, 25, 25);
   rval = deleteKV(rtID, 26, 26);
#endif
  }
  else{
    while (restart != 1){}
  }

  i=iam*inserts;
  j=0;

  while (j < inserts){

    //  printf("\n ** Thread %d fetching key %d\n", iam, i);

    values = fetch(rtID, i);

    if (values == NULL){
      printf("ERROR!. Thread %d key %d not found\n",iam, i);
      pprint();
      //      llprint();
      exit(0);
    }
    else{
      if (values[0] != i){
	printf("ERROR! Thread %d Value of key %d is %d\n", iam, i, values[0]);
	exit(0);
      }
      else{
	//	printf("thread %d Value of key %d is %d\n", iam, i, values[0]);
      } 
    }

    //  pprint();

    //   printf("\n ** Thread %d deleting key %d\n", iam, i);
    
    //rval = deleteKV(rtID, i, i);
    

    i++;
    j++;
  }

  successes[iam]=1;

  pthread_mutex_lock(&mutex);
  completedProcs++;
  pthread_mutex_unlock(&mutex);

  while (completedProcs < 2*THREADS){}

  if (iam == 0){
   // pprint();
  }
  


#if 0
  rval = insert(rtID, 19, 19);
  rval = insert(rtID, 0, 0);
  rval = insert(rtID, 4, 4);
  rval = insert(rtID, 8, 8);
  rval = insert(rtID, 12, 12);
  rval = insert(rtID, 16, 16);
  rval = insert(rtID, 20, 20);
  rval = insert(rtID, 24, 24);
  rval = insert(rtID, 1, 1);
  rval = insert(rtID, 5, 5);
  rval = insert(rtID, 9, 9);
  rval = insert(rtID, 15, 15);
  rval = insert(rtID, 2, 2);
 
  rval = insert(rtID, 13,13);
  rval = insert(rtID, 17,17);
  rval = insert(rtID, 21,21);
  rval = insert(rtID, 25,25);
  rval = insert(rtID, 6, 6);
  rval = insert(rtID, 10, 10);
  rval = insert(rtID, 14, 14);
  rval = insert(rtID, 18, 18);
  rval = insert(rtID, 22, 22);
  rval = insert(rtID, 26, 26);
  rval = insert(rtID, 23, 23);


  rval = deleteKV(rtID, 1,1);
  rval = deleteKV(rtID, 5,5);
  rval = deleteKV(rtID, 9, 9);
  rval = deleteKV(rtID, 13, 13);
  rval = deleteKV(rtID, 2, 2);
  rval = deleteKV(rtID, 6, 6);
  rval = deleteKV(rtID, 10, 10);
  rval = deleteKV(rtID, 3, 3);

#endif

  return 0;
}



int main (int argc, char **argv)
{
    pthread_t p[MAX_USER_THREADS];
    int i=0, len=0, read=0, ids[MAX_USER_THREADS], order;
    FILE *fp;
    char *line = NULL;
    
    THREADS = 1;
    order = 2; 
    inserts = 128;

   
    if (argc == 2){
      order = atoi(argv[1]);
    }
    else if (argc == 3){
      order = atoi(argv[1]);
      THREADS = atoi(argv[2]); 
    }
    else if (argc == 4){
      order = atoi(argv[1]);
      THREADS = atoi(argv[2]); 
      inserts = atoi(argv[3]);
    }

    printf("Initializing TManager. Order=%d THREADS=%d inserts=%d\n", order, THREADS, inserts);
    
    initializeTManager(order, THREADS);

#if 0
    fp = fopen("log", "r");

    while ((read = getline(&line, &len, fp)) != -1){
      accesses[i] = atoi(line);
      printf("%d\n", accesses[i]);
      i++;
    }
  
#endif

    markResetCursor();


    // start user threads
    
    for (i = 0; i < THREADS; i++){
      ids[i] = i;
      successes[i] =0;
      pthread_create (&p[i], NULL, *do_work, (void *)&(ids[i]));
    }
    
    // join
    for (i = 0; i < THREADS; i++){
	pthread_join (p[i], NULL);
    }

    for (i = 0; i < THREADS; i++){
      success += successes[i];
    }

    if (success){
      printf("The test is successful.\n");
    }
    else{
      printf("The test has failed.\n");
    }

    terminateTManager();

#if 0    
    // Note key of value 0 is NOT inserted
    for(i=0; i <= counter; i++){
      int value = fetch(i);
      printf("Value of key value %d is %d\n",i, value);
    }

#endif

    return 0;
}


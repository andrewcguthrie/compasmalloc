Notes on use of lock-free allocator (CLFMalloc):
- Environment variables
- Supported functions
- Linking


Environment variables:

The allocator checks one environment variable CLFMALLOC_NUMHEAPS. If
the value is valid (i.e., an integer between 1 and 512), the number of
heaps is set to the smallest power of 2 greater than or equal to the
value. If the variable is undefined or its value is non-integer or out
of the range 1..512, a default value of 64 is used.  Unpopulated heaps
occupy only 1 KB. The maximum size of a populated heap is in the order
of 1 MB. Typically, the average size of a populated heap is less than
100 KB. In general it is good for latency and scalability to have the
number of heaps about 4 times the number of hardware threads.


Supported functions:

CLFMalloc supports the standard allocation functions malloc, free, 
calloc, and realloc.


Linking:

For a program to use CLFMalloc, it can be linked explicitly with the
clfmalloc library. On some systems calls to malloc functions can be
interposed by setting the variable LD_PRELOAD to the clfmalloc library
without need for replacing the default malloc library or relinking.


The lock-free malloc algorithms are described in the paper:

    Maged M. Michael, Scalable Lock-Free Dynamic Memory Allocation,
    The 2004 ACM SIGPLAN Conference on Programming Language Design and
    Implementation (PLDI), pages 35-46, June 2004.

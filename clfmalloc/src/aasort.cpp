/*
 * (c) Copyright 2007, IBM Corporation.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  Change History:
 *
 *  yy-mm-dd  Developer  Defect     Description
 *  --------  ---------  ------     -----------
 *  08-07-28  ganzhi     N/A        Initial version
 */

#include <cstdlib>
#include <iostream>
#include <stdexcept>
#include <assert.h>

#include <vector>
#include <algorithm>

#include <amino/aasort.h>
#include <amino/vector_inc.h>

using namespace std;
namespace amino{

    const int SHRINK_FACTOR_UP=13;
    const int SHRINK_FACTOR_DOWN=10;

    void printVector(int * start){
        for(int i=0;i<4;i++){
            cout<<start[i]<<"\t";
        }
        cout<<endl;
    }

    void printAll(int * start, int length){
        cout<<"Print All: \n";
        length = length/4;
        for(int i=0;i<length;i++)
            printVector(start+i*4);
        cout<<"End All.\n";
    }

    /**
     * @brief sort the array with in-core algorithm.
     * @param array start address of array, which must be 16-byte aligned
     * @param end end of the array, which satisfies <code>length%16==0</code>
     */
    void incore_sort(int * array, int * end){
        int length = end-array;
#ifdef DEBUG
        //start address of array should be 16-byte aligned
        assert(((unsigned long) array)%16 == 0);
        assert(length%16==0);
#endif

        // length of vector
        int vec_len = length/4;
        sortInVec(array,vec_len);
#if defined(DEBUG) && defined (VERBOSE)
        cout<<"After sort in vector:\n";
        printAll(array, length);
#endif

        int distance = vec_len*SHRINK_FACTOR_DOWN/SHRINK_FACTOR_UP;
        while(distance!=1){
            int expand_dis = distance*4;
#if defined(DEBUG) && defined (VERBOSE)
            cout<<"Distance: "<<distance<<endl;
#endif
            for(int i=0;i<vec_len-distance;i++){
                int * arg1 = array+i*4;
                int * arg2 = array+i*4+expand_dis;
#if defined(DEBUG) && defined (VERBOSE)
                printAll(array, length);
                cout<<"cmp: "<<i<<endl;
#endif
                vector_cmpswap_noret(arg1, arg2);
            }
            for(int i=vec_len-distance;i<vec_len;i++){
                int * arg1 = array+i*4;
                int * arg2 =array+(i+distance-vec_len)*4;
#if defined(DEBUG) && defined (VERBOSE)
                printAll(array, length);
                cout<<"cmp_skew: "<<i<<endl;
#endif
                vector_cmpswap_skew_noret(arg1, arg2); 
            }
            distance = distance*SHRINK_FACTOR_DOWN/SHRINK_FACTOR_UP;
        }

        bool exchange;
#ifdef DEBUG
        int rep = 0;
#endif
        do{
#if defined(DEBUG)
            rep++;
#endif
            //#endif
            exchange=false;
            for(int i=0;i<vec_len-1;i++){
#if defined(DEBUG) && defined (VERBOSE)
                printAll(array, length);
                cout<<"cmp: "<<i<<endl;
#endif
                if(vector_cmpswap(array+i*4, array+i*4+4))
                    exchange=true;
            }
#if defined(DEBUG) && defined (VERBOSE)
            printAll(array, length);
            cout<<"cmp_skew: "<<endl;
#endif
            if(vector_cmpswap_skew(array+4*vec_len-4, array))
                exchange=true;
        }while(exchange);
#if defined(DEBUG)
        cout <<"Number of 1 Dis: "<<rep<<endl;
#endif
        /*Matrix should be transposed*/
        int matrix_len = vec_len/4;
        for(int i=0;i<matrix_len;i++){
            transpose4_4(array+i*16);
        } 

        /*This flag is used to mark if corresponding vector 
         * has been put to the right position*/
        vector<bool> flags(vec_len);
        fill(flags.begin(), flags.end(), false);
        int tmpVec[4]__attribute__((aligned(16)));
        //cout<<"Copying\n";
        for(int i=0;i<vec_len;i++){
            if(flags[i])
                continue;
            int j=i;
            copyVector(array+j*4, tmpVec);
            do{
                flags[j]=true;
                int k=j/4+matrix_len*(j%4);
                if(k==j)
                    break;
                if(flags[k]){
                    //cout<<"Copy from "<<j<<" To "<<k<<endl;
                    copyVector(tmpVec, array+k*4);
                    break;
                } else{
                    //cout<<"Swap from "<<j<<" To "<<k<<endl;
                    swapVector(array+k*4, tmpVec);
                    j=k;
                }
            }while(true);
        }
        return;

    }

    /**
     * @brief This method merges two consecutive arrays which are both already softed in
     * ascending order.
     *
     * Since this method uses SIMD instruction to merge, it requires that the 
     * two arrays are 4-byte aligned. And the array length is multiple of 4.
     *
     * @param start The 1st array to merge
     * @param middle The start of 2nd array to merge
     * @param end The 2nd array to merge
     */
    void aa_merge(int * start, int * middle, int * end){
        int length = end-start;
        int * buffer;
        posix_memalign((void **)(&buffer), 16, sizeof(int)*length);

        int length1= middle-start;
        int length2=length-length1;

        int pos1=0;
        int pos2=0;
        int targetPos=0;

        int tmpMin[4]__attribute__((aligned(16)));
        int tmpMax[4]__attribute__((aligned(16)));

        copyVector(start+pos1, tmpMin);
        copyVector(middle+pos2, tmpMax);
        do{
            vector_merge(tmpMin, tmpMax);
            copyVector(tmpMin, buffer+targetPos);
            targetPos+=4;
            if(start[pos1+4]<=middle[pos2+4]){
                pos1+=4;
                if(pos1>=length1)
                    break;
                copyVector(start+pos1, tmpMin);
            }else{
                pos2+=4;
                if(pos2>=length2)
                    break;
                copyVector(middle+pos2, tmpMin);
            }
        }while(true);

        copyVector(tmpMax,buffer+targetPos);
        targetPos+=4;
        if(pos1<length1){
            pos1+=4;
            copy(start+pos1, middle, buffer+targetPos);
        }
        else if(pos2<length1){
            pos2+=4;
            copy(middle+pos2, end, buffer+targetPos);
        }
        copy(buffer, buffer+length, start);

        free(buffer);
    }

    void aa_sort(int * start, int *end){
        int length=end-start;
        incore_sort(start, start+length/2);
        incore_sort(start+length/2, end);

        aa_merge(start, start+length/2, end);
    }
}

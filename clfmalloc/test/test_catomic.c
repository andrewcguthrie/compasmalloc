/*
 * (c) Copyright 2008, IBM Corporation.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
#include <stdio.h>
#include <pthread.h>

#include <amino/cstdatomic>

#define OP_COUNT 100000
#define TH_COUNT 8

/**
 * @brief this function will be executed in a separated thread to operate on
 * shared atomic variable
 */
void * test_atomic_int(void * arg){
    atomic_int * pInt = arg;
    int i;

    for(i=0;i<OP_COUNT;i++){
        int tmp = pInt->__f__;
        int newV = tmp+1;

        while(!atomic_compare_swap(pInt, &tmp, newV))
        {newV = tmp+1;}
    }

    return NULL;
}

/**
 * @brief this program aims to test atomic package under a C program
 */
int main() {
    atomic_int ai;
    int i;
    ai.__f__ = 0;

    pthread_t tids[TH_COUNT];

    for(i=0;i<TH_COUNT;i++){
        pthread_create(&(tids[i]), NULL, test_atomic_int, &ai);
    }

    
    for(i=0;i<TH_COUNT;i++){
        pthread_join(tids[i], NULL);
    }

    printf("Result %d\n", atomic_load(&ai));
    if(atomic_load(&ai)!=OP_COUNT*TH_COUNT){
        fprintf(stderr, "Atomic Error!\n");
        return 1;
    } 
    else
        printf("Atomic Int Verified!\n");

    return 0;
}

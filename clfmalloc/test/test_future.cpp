/*
 * (c) Copyright 2008, IBM Corporation.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *
 *  Change History:
 *
 *  yy-mm-dd  Developer  Defect     Description
 *  --------  ---------  ------     -----------
 *  08-08-18  ganzhi     N/A        Initial implementation
 */

#include <sys/time.h>
#include <iostream>
#include <algorithm>
#include <amino/aasort.h>

#include <test_future.h>
#include <amino/executor.h>

#include <cppunit/ui/text/TestRunner.h>
#include <cppunit/extensions/HelperMacros.h>
#include <cppunit/extensions/TestFactoryRegistry.h>

using namespace std;

using namespace test;

using namespace amino;

typedef FutureTest<SingleExecutor> SgFutureTest;
typedef FutureTest<ThreadPoolExecutor> TPFutureTest;
//CPPUNIT_TEST_SUITE_REGISTRATION(SgFutureTest) ;
CPPUNIT_TEST_SUITE_REGISTRATION(TPFutureTest) ;

int main() {
	  CppUnit::TextUi::TestRunner runner;
	  CppUnit::TestFactoryRegistry &registry = CppUnit::TestFactoryRegistry::getRegistry();
	  runner.addTest( registry.makeTest() );
	  bool wasSuccessful = runner.run( "", false );
	  return !wasSuccessful;
}

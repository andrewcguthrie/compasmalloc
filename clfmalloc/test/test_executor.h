/*
 * (c) Copyright 2008, IBM Corporation.
 *
 *      Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  Change History:
 *
 *  yy-mm-dd  Developer  Defect     Description
 *  --------  ---------  ------     -----------
 *  08-08-03  ganzhi     N/A        Initial implementation
 */

#ifndef TEST_EXECUTOR_H_
#define TEST_EXECUTOR_H_
#include <cppunit/extensions/HelperMacros.h>
#include <cppunit/TestAssert.h>
#include "baseTest.h"

#include <amino/cstdatomic>
#include <amino/thread.h>

#include <iostream>
#include <assert.h>
#include <unistd.h>

namespace test{
    using namespace amino;

    class DoNothing:public Runnable{
        public:
            void* run(){
                cout<<"Executed in an executor!"<<endl;
                return NULL;
            }
    };

    const int ACCU_COUNT=1000;

    class Accumulate:public Runnable{
        private:
            atomic<int> * count;
        public:
            Accumulate(atomic<int> * arg){
                count = arg;
            }

            void* run(){
                for(int i=0;i<ACCU_COUNT;i++)
                    (*count)++;
                return NULL;
            }
    };

    class Wait6S:public Runnable{
        public:
            void* run(){
                cout<<"Begin wait 6S\n";
                sleep(6);
                cout<<"End wait 6S\n";
                cout<<flush;
                return NULL;
            }
    };

    template<typename Executor>
        class CallExecutor{
            private:
                Executor* exec;
                Wait6S * waiter;
            public:
                CallExecutor(Executor * executor, Wait6S * w6s){
                    exec = executor;
                    waiter = w6s;
                }

                void operator()(){
                    exec->execute(waiter);
                }
        };

    template<typename Executor>
        class ExecutorTest :
            public CppUnit::TestFixture, public BaseTest<int> {
                CPPUNIT_TEST_SUITE(ExecutorTest);
//                CPPUNIT_TEST(testExecuteOne); 
//                CPPUNIT_TEST(testShutdown);
                CPPUNIT_TEST(testTimedWait); 
//                CPPUNIT_TEST(testAccumulate); 
//                CPPUNIT_TEST(testAccumulateMassive); 
                CPPUNIT_TEST_SUITE_END();

                public:
                ExecutorTest() {
                }

                void setUp() {
                }

                void reset() {
                }

                void tearDown() {
                }

                void testExecuteOne(){
                    Executor executor;
                    DoNothing done;
                    executor.execute(&done);
                    executor.shutdown();
                    executor.waitTermination();
                }

                void testAccumulate(){
                    Executor executor;
                    atomic<int> sum;
                    sum = 0;
                    Accumulate do1(&sum), do2(&sum);
                    executor.execute(&do1);
                    executor.execute(&do2);
                    executor.shutdown();
                    executor.waitTermination();

                    CPPUNIT_ASSERT(sum.load()==2*ACCU_COUNT);
                }

                void testAccumulateMassive(){
                    const int TASK_COUNT=200;

                    Executor executor;
                    atomic<int> sum;
                    sum = 0;

                    Accumulate * ppAcc[TASK_COUNT];
                    for(int i=0;i<TASK_COUNT;i++){
                        ppAcc[i] = new Accumulate(&sum);
                    }

                    for(int i=0;i<TASK_COUNT;i++){
                        executor.execute(ppAcc[i]);
                    }
                    executor.shutdown();
                    executor.waitTermination();

                    CPPUNIT_ASSERT(sum.load()==ACCU_COUNT*TASK_COUNT);
                }

                void testShutdown(){
                    Executor executor;
                    executor.shutdown();
                    DoNothing done;
                    try{
                        executor.execute(&done);
                    }catch(std::logic_error e){
                        executor.waitTermination();
                        return;
                    }
                    CPPUNIT_ASSERT(false); 
                }

                void testTimedWait(){
                    Executor executor;
                    Wait6S waiter;
                    CallExecutor<Executor> *ce = new CallExecutor<Executor>(
                            &executor, &waiter);
                    Thread tmpThr(*ce);
                    sleep(2);
                    executor.shutdown();
                    bool res;
                    res = executor.waitTermination(2000);
                    if(res){
                        executor.waitTermination();
                        CPPUNIT_ASSERT_MESSAGE("Executor finished before 6S!\n", res);
                    }

                    res = executor.waitTermination(18000);
                    if(!res){
                        executor.waitTermination();
                        CPPUNIT_ASSERT_MESSAGE("Executor is not finished after 10S\n", res);
                    }

                    delete ce;
                }
            };
}
#endif /*TEST_EXECUTOR_H_*/

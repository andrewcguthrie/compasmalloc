/*
 * (c) Copyright 2008, IBM Corporation.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
#ifndef BDEQUETEST_H_
#define BDEQUETEST_H_

#include <cppunit/extensions/HelperMacros.h>
#include <cppunit/TestAssert.h>
#include "baseTest.h"

#include <algorithm>

namespace test {
using namespace amino;

template<typename DequeType, typename ParaType, char const* CLASS_NAME>
class BDequeTest: public CppUnit::TestFixture, public BaseTest<ParaType> {
    CPPUNIT_TEST_SUITE(BDequeTest);	
    CPPUNIT_TEST(testPushRightMT);
	CPPUNIT_TEST(test4DirectionMT);
	CPPUNIT_TEST(testProducerConsumer);
	CPPUNIT_TEST_SUITE_END();

private:
	DequeType *deque;
public:
	BDequeTest() {
	}

	DequeType * getDeque() {
		return deque;
	}

	void setUp() {
		deque = new DequeType();
	}

	void reset() {
		delete deque;
		deque = new DequeType();
	}

	void tearDown() {
		delete deque;
	}

	/**
	 * This function tests push&take in both ends of deque with multiple-threads.
	 */
	void testPushRightMT();
	void test4DirectionMT();

	void testProducerConsumer();
};

template<typename DequeType, typename ParaType, char const* CLASS_NAME>
class Thread4Dir: public TestThread<ParaType> {
private:
	DequeType *deque;
	BDequeTest<DequeType, ParaType, CLASS_NAME> *testcase;
	int operationNum;

public:
	Thread4Dir(DequeType * q, BDequeTest<DequeType, ParaType, CLASS_NAME> * qt, int nOperation) :
	deque(q), testcase(qt), operationNum(nOperation) {
	}

	static atomic<ParaType> checksum;

	static ParaType getChecksum() {
		return checksum.load(memory_order_relaxed);
	}

	void* run() {
#ifdef DEBUG
		int repeat =0;
		ParaType prev;
#endif
		for (int i = 0; i < operationNum; ++i) {
			if (i % 2 == 0) {
				deque->pushRight(i);
				this->inVec.push_back(i);
				ParaType tmp;
				if(deque->takeLeft(tmp)) {
					this->outVec.push_back(tmp);
				}
			} else {
				deque->pushLeft(i);
				this->inVec.push_back(i);
				ParaType tmp;
				if(deque->takeRight(tmp)) {
					this->outVec.push_back(tmp);
				}
#ifdef DEBUG
				// For debugging 89276
				if (tmp == prev)
				repeat++;
				else {
					prev = tmp;
					repeat=0;
				}

				if (repeat>10)
				cout<< "I got too many equal element" << endl;
#endif
			}
		}
        return NULL;
	}
};

template<typename DequeType, typename ParaType, const char * CLASS_NAME>
atomic<ParaType> Thread4Dir<DequeType, ParaType, CLASS_NAME>::checksum;

template<typename DequeType, typename ParaType, char const* CLASS_NAME>
class ThreadPushRight: public TestThread<ParaType> {
private:
	DequeType *deque;
	BDequeTest<DequeType, ParaType, CLASS_NAME> *testcase;
	int operationNum;

public:
	ThreadPushRight(DequeType * q,
			BDequeTest<DequeType, ParaType, CLASS_NAME> * qt, int nOperation) :
	deque(q), testcase(qt), operationNum(nOperation) {
	}

	static atomic<ParaType> checksum;

	static ParaType getChecksum() {
		return checksum.load(memory_order_relaxed);
	}

	void* run() {
		for (int i = 0; i < operationNum; ++i) {
			if (i % 2 == 0) {
				deque->pushRight(i);
				this->inVec.push_back(i);
			} else {
				deque->pushLeft(i);
				this->inVec.push_back(i);
			}
		}
        return NULL;
	}
};

template<typename DequeType, typename ParaType, const char * CLASS_NAME>
atomic<ParaType> ThreadPushRight<DequeType, ParaType, CLASS_NAME>::checksum;

template<typename DequeType, typename ParaType, const char * CLASS_NAME>
class ThreadPushRightFactory: public ThreadFactory<ParaType> {
private:
	DequeType *deque;
	BDequeTest<DequeType, ParaType, CLASS_NAME> *testcase;
public:
	ThreadPushRightFactory(DequeType * q, BDequeTest<DequeType, ParaType,
			CLASS_NAME> * qt) :
	deque(q), testcase(qt) {
	}

	virtual Runnable ** createThreads(int threadNum, int elementNum,
			int operationNum) {
		testcase->reset();
		deque = testcase->getDeque();

		this->inVec.clear();
		this->outVec.clear();

		ThreadPushRight<DequeType, ParaType, CLASS_NAME>::checksum = 0;

		Runnable ** threads;
		threads = new Runnable*[threadNum];
		for (int i = 0; i < threadNum; ++i)
		threads[i]
		= new ThreadPushRight<DequeType, ParaType, CLASS_NAME> (deque, testcase, operationNum);

		return threads;
	}

	typedef ThreadPushRight<DequeType, ParaType, CLASS_NAME> ThreadPushRight_T;

	virtual void verifyResult(int threadNum, int elementNum) {
		while (!deque->empty()) {
			ParaType tmp;
			if(deque->takeRight(tmp)) {
				this->outVec.push_back(tmp);
			}
		}
		cout << this->outVec.size() << endl;
		ThreadFactory<ParaType>::verifyResult(threadNum, elementNum);
	}

};

template<typename DequeType, typename ParaType, char const* CLASS_NAME>
class Thread4DirFactory: public ThreadFactory<ParaType> {
private:
	DequeType *deque;
	BDequeTest<DequeType, ParaType, CLASS_NAME> *testcase;

public:
	Thread4DirFactory(DequeType * q,
			BDequeTest<DequeType, ParaType, CLASS_NAME> * qt) :
	deque(q), testcase(qt) {
	}

	virtual Runnable ** createThreads(int threadNum, int elementNum,
			int operationNum) {
		testcase->reset();
		deque = testcase->getDeque();

		this->inVec.clear();
		this->outVec.clear();

		Thread4Dir<DequeType, ParaType, CLASS_NAME>::checksum = 0;

		Runnable ** threads;
		threads = new Runnable*[threadNum];
		for (int i = 0; i < threadNum; ++i)
		threads[i]
		= new Thread4Dir<DequeType, ParaType, CLASS_NAME> (deque, testcase, operationNum);

		return threads;
	}

	typedef Thread4Dir<DequeType, ParaType, CLASS_NAME> Thread4Dir_T;
};

template<typename DequeType, typename ParaType, const char * CLASS_NAME>
void BDequeTest<DequeType, ParaType, CLASS_NAME>::testPushRightMT() {
	ThreadPushRightFactory<DequeType, ParaType, CLASS_NAME> dequeueFactory(
			deque, this);
	ThreadRunner::runThreads(&dequeueFactory, CLASS_NAME, "testPushRightMT");
}

template<typename DequeType, typename ParaType, const char * CLASS_NAME>
void BDequeTest<DequeType, ParaType, CLASS_NAME>::test4DirectionMT() {
	Thread4DirFactory<DequeType, ParaType, CLASS_NAME> dequeueFactory(deque,
			this);
	ThreadRunner::runThreads(&dequeueFactory, CLASS_NAME, "testDequeMT");
}

template<typename DequeType, typename ParaType, char const* CLASS_NAME>
class TakeThread: public TestThread<ParaType> {
private:
	DequeType *deque;
	BDequeTest<DequeType, ParaType, CLASS_NAME> *testcase;
	int elementNum;
public:
	TakeThread(DequeType * q, BDequeTest<DequeType, ParaType, CLASS_NAME> * qt,
			int nElement) :
	deque(q), testcase(qt), elementNum(nElement) {
	}

	void* run() {
		for (int i = 0; i < elementNum; ++i) {
			if (i % 2 == 0) {
				ParaType tmp;
				if(deque->takeLeft(tmp)) {
					this->outVec.push_back(tmp);
				}
			} else {
				ParaType tmp;
				if(deque->takeRight(tmp)) {
					this->outVec.push_back(tmp);
				}
			}
		}
        return NULL;
	}
};

template<typename DequeType, typename ParaType, char const* CLASS_NAME>
class PushThread: public TestThread<ParaType> {
private:
	DequeType *deque;
	BDequeTest<DequeType, ParaType, CLASS_NAME> *testcase;
	int elementNum;
public:
	PushThread(DequeType * q, BDequeTest<DequeType, ParaType, CLASS_NAME> * qt,
			int nElement) :
	deque(q), testcase(qt), elementNum(nElement) {
	}

	void* run() {
		for (int i = 0; i < elementNum; ++i) {
			if (i % 2 == 0) {
				deque->pushRight(i);
			} else {
				deque->pushLeft(i);
			}
			this->inVec.push_back(i);
		}
        return NULL;
	}
};

template<typename DequeType, typename ParaType, const char * CLASS_NAME>
void BDequeTest<DequeType, ParaType, CLASS_NAME>::testProducerConsumer() {
	const TestConfig * tc = TestConfig::getInstance();
    const vector<int> threadV = tc->getThreadNum();
	int elementNum = tc->getElementNum();

    typedef TakeThread<DequeType, ParaType, CLASS_NAME> TakeType;
    typedef TakeType * pTakeType;
    typedef PushThread<DequeType, ParaType, CLASS_NAME> PushType;
    typedef PushType * pPushType;

    for(unsigned int i=0;i<threadV.size();i++){
        int threadN = threadV[i];
        pTakeType * takes = new pTakeType[threadN];
        pPushType * pushs = new pPushType[threadN];
        for(int j=0;j<threadN;j++){
            takes[j] = new TakeType(deque, this, elementNum);
            pushs[j] = new PushType(deque, this, elementNum);
        }

        for(int j=0;j<threadN;j++){
            takes[j]->start();
            pushs[j]->start();
        }

        for(int j=0;j<threadN;j++){
            takes[j]->join();
            pushs[j]->join();
        }

        int sum1 = 0;
        int sum2 = 0;
        for(int j=0;j<threadN;j++){
            sum1+=takes[j]->outVec.size();
            sum2+=pushs[j]->inVec.size();
        }
        CPPUNIT_ASSERT(sum1==sum2);
        // TODO: verity equality of elements inside blockingdeque

        for(int j=0;j<threadN;j++){
            delete takes[j];
            delete pushs[j];
        }
        delete [] takes;
        delete [] pushs;
    }
}
}
#endif /*BDEQUETEST_H_*/

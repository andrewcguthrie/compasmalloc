/*
 * (c) Copyright 2008, IBM Corporation.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
#include "randArrayGenerator.h"

namespace test {

template<> int* RandArrayGenerator<int>::getRandArray(int* a, int n) {
	srand(time(NULL));
	for (int i = 0; i < n; ++i) {
		a[i] = rand() % n;
	}
	return a;
}

template<> long* RandArrayGenerator<long>::getRandArray(long* a, int n) {
	srand(time(NULL));
	for (int i = 0; i < n; ++i) {
		a[i] = rand() % n;
	}
	return a;
}

template<> char* RandArrayGenerator<char>::getRandArray(char* a, int n) {
	srand(time(NULL));
	for (int i = 0; i < n; ++i) {
		a[i] = 'a' + rand() % 26;
	}
	return a;
}

template<> double* RandArrayGenerator<double>::getRandArray(double* a, int n) {
	srand(time(NULL));
	for (int i = 0; i < n; ++i) {
		a[i] = static_cast<double>(rand()) / RAND_MAX * 100;
	}
	return a;
}

template<> std::string* RandArrayGenerator<std::string>::getRandArray(
		std::string* a, int n) {
	srand(time(NULL));

	for (int i = 0; i < n; ++i) {
		int stringLen = rand()%9+1;
		for (int j = 0; j < stringLen; ++j)
			a[i].push_back('a' + rand()%26);
	}
	return a;
}

template<> int* RandArrayGenerator<int>::getRandUniqueArray(int* a, int n) {
	for (int i = 0; i < n; ++i) {
		a[i] = i;
	}
	std::random_shuffle(a, a+n);
	return a;
}

template<> long* RandArrayGenerator<long>::getRandUniqueArray(long* a, int n) {
	for (int i = 0; i < n; ++i) {
		a[i] = i;
	}
	std::random_shuffle(a, a+n);
	return a;
}

template<> double* RandArrayGenerator<double>::getRandUniqueArray(double* a, int n) {
	double RAND_DOUBLE_NUM = 0.7;// just a number between 0 and 1.
	for (int i = 0; i < n; ++i) {
		a[i] = static_cast<double>(i) / RAND_DOUBLE_NUM;
	}
	std::random_shuffle(a, a+n);
	return a;
}

template<> std::string* RandArrayGenerator<std::string>::getRandUniqueArray(
		std::string* a, int n) {
	srand(time(NULL));

	for (int i = 0; i < n; ++i) {
		int stringLen = rand()%9+10;
		for (int j = 0; j < stringLen; ++j)
			a[i].push_back('a' + rand()%26);
	}
	return a;
}
}

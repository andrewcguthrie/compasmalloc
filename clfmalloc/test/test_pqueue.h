/*
 * (c) Copyright 2008, IBM Corporation.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
#ifndef PQUEUETEST_H_
#define PQUEUETEST_H_
#include <cppunit/extensions/HelperMacros.h>
#include <cppunit/TestAssert.h>
#include <stdexcept>
#include "baseTest.h"

namespace test {

template<typename QueueType, typename ParaType, char const* CLASS_NAME>
class PQueueTest: public CppUnit::TestFixture, public BaseTest<ParaType> {
    CPPUNIT_TEST_SUITE(PQueueTest);
    CPPUNIT_TEST(testEmptyST);
	CPPUNIT_TEST(testEnqueueST);
	CPPUNIT_TEST(testDequeueST);
	CPPUNIT_TEST(testEnqueueMT);
	CPPUNIT_TEST(testDequeueMT);
	CPPUNIT_TEST(testBound);
//	CPPUNIT_TEST(testEnDequeuePairMT);
	CPPUNIT_TEST_SUITE_END ();

private:
	QueueType *queue;
public:
	PQueueTest() {
	}

	QueueType * getQueue() {
		return queue;
	}

	void setUp() {
		queue = new QueueType();
	}

	void reset() {
		delete queue;
		queue = new QueueType();
	}

	void tearDown() {
		delete queue;
	}

	void testEmptyST();
	void testEnqueueST();
	void testDequeueST();
	void testEnqueueMT();
	void testDequeueMT();
	void testEnDequeuePairMT();
	void testBound();
};

#define ThreadDequeue_T ThreadDequeue<QueueType, ParaType, CLASS_NAME>
#define ThreadEnqueue_T ThreadEnqueue<QueueType, ParaType, CLASS_NAME>

template<typename QueueType, typename ParaType, char const* CLASS_NAME> class ThreadEnqueue: public TestThread<
ParaType> {
private:
	QueueType *queue;
	PQueueTest<QueueType, ParaType, CLASS_NAME> *testcase;
	int elementNum;
	int operationNum;

public:
	ThreadEnqueue(QueueType * q,
			PQueueTest<QueueType, ParaType, CLASS_NAME>* qt, int nElement,
			int nOperation, int threadId = 0) : TestThread<ParaType>(threadId),
    queue(q), testcase(qt), elementNum(nElement), operationNum(nOperation)
	{

		//TODO initialize once for all object using static block
		RandArrayGenerator<ParaType>::getRandUniqueArray(testcase->data,
				testcase->BaseTest<ParaType>::NELEMENT * testcase->BaseTest<
				ParaType>::MAXTHREADN);
	}

	void* run() {
		for (int i = 0; i < operationNum; ++i) {
			queue->enqueue(testcase->data[i + ((this->threadId) * elementNum)]);
            //queue->dumpQueue();
			this->inVec.push_back(testcase->data[i + ((this->threadId)
							* elementNum)]);
		}
        return NULL;
	}
};

template<typename QueueType, typename ParaType, char const* CLASS_NAME> class ThreadDequeue: public TestThread<
ParaType> {
private:
	QueueType *queue;
	PQueueTest<QueueType, ParaType, CLASS_NAME> *testcase;
	int elementNum;
	int operationNum;

public:

	ThreadDequeue(QueueType * q,
			PQueueTest<QueueType, ParaType, CLASS_NAME> * qt, int nElement,
			int nOperation) :
	queue(q), testcase(qt), elementNum(nElement), operationNum(nOperation) {
	}

	void* run() {
		for (int i = 0; i < operationNum; ++i) {
			ParaType tmp;
			if (queue->dequeue(tmp)) {
				this->outVec.push_back(tmp);
			}
		}
        return NULL;
	}
};
template<typename QueueType, typename ParaType, char const* CLASS_NAME> class ThreadDequeueFactory: public ThreadFactory<
ParaType> {
private:
	QueueType *queue;
	PQueueTest<QueueType, ParaType, CLASS_NAME> *testcase;
public:
	ThreadDequeueFactory(QueueType * q, PQueueTest<QueueType, ParaType,
			CLASS_NAME> * qt) :
	queue(q), testcase(qt) {
	}

	virtual Runnable ** createThreads(int threadNum, int elementNum,
			int operationNum) {
		testcase->reset();
		queue = testcase->getQueue();
		this->inVec.clear();
		this->outVec.clear();

		for (int i = 0; i < threadNum; ++i) {
			ThreadEnqueue_T threadEnqueue(queue, testcase, elementNum, i);
			threadEnqueue.run();

			copy(threadEnqueue.inVec.begin(), threadEnqueue.inVec.end(),
					back_inserter(this->inVec));
		}
		Runnable ** threads;
		threads = new Runnable*[threadNum];
		for (int i = 0; i < threadNum; ++i)
		threads[i]
		= new ThreadDequeue<QueueType, ParaType, CLASS_NAME> (queue, testcase, elementNum, operationNum);

		return threads;
	}

	virtual void verifyResult(int threadNum, int elementNum) {
		CPPUNIT_ASSERT(queue->size() == 0);

		//remove duplicated element in inVec
		sort(this->inVec.begin(), this->inVec.end());
		this->inVec.erase(unique(this->inVec.begin(),this->inVec.end()),this->inVec.end());

		ThreadFactory<ParaType>::verifyResult(threadNum, elementNum);
	}
};

template<typename QueueType, typename ParaType, char const* CLASS_NAME> class ThreadEnqueueFactory: public ThreadFactory<
ParaType> {
private:
	QueueType *queue;
	PQueueTest<QueueType, ParaType, CLASS_NAME> *testcase;
public:
	ThreadEnqueueFactory(QueueType * q, PQueueTest<QueueType, ParaType,
			CLASS_NAME> * qt) :
	queue(q), testcase(qt) {
	}

	virtual Runnable ** createThreads(int threadNum, int elementNum,
			int operationNum) {
		testcase->reset();
		queue = testcase->getQueue();
		this->inVec.clear();
		this->outVec.clear();

		Runnable ** threads;
		threads = new Runnable*[threadNum];
		for (int i = 0; i < threadNum; ++i)
		threads[i]
		= new ThreadEnqueue<QueueType, ParaType, CLASS_NAME> (queue, testcase, elementNum, operationNum, i);

		return threads;
	}

	virtual void verifyResult(int threadNum, int elementNum) {
		//		CPPUNIT_ASSERT(queue->size()==threadNum*elementNum);
		while (!queue->empty()) {
			ParaType tmp;
			if (queue->dequeue(tmp)) {
				this->outVec.push_back(tmp);
			}
		}

		//remove duplicated element in inVec
		sort(this->inVec.begin(), this->inVec.end());
		this->inVec.erase(unique(this->inVec.begin(),this->inVec.end()),this->inVec.end());

		ThreadFactory<ParaType>::verifyResult(threadNum, elementNum);
	}
};

template<typename QueueType, typename ParaType, char const* CLASS_NAME> class ThreadEnDequeueFactory: public ThreadFactory<
ParaType> {
private:
	QueueType *queue;
	PQueueTest<QueueType, ParaType, CLASS_NAME> *testcase;
public:
	ThreadEnDequeueFactory(QueueType * q, PQueueTest<QueueType, ParaType,
			CLASS_NAME> * qt) :
	queue(q), testcase(qt) {
	}

	virtual Runnable ** createThreads(int threadNum, int elementNum,
			int operationNum) {
		testcase->reset();
		queue = testcase->getQueue();
		this->inVec.clear();
		this->outVec.clear();

		for (int i = 0; i < threadNum; ++i) {
			ThreadEnqueue_T threadEnqueue(queue, testcase, elementNum, i);
			threadEnqueue.run();

			copy(threadEnqueue.inVec.begin(), threadEnqueue.inVec.end(),
					back_inserter(this->inVec));
		}
		Runnable ** threads;
		threads = new Runnable*[threadNum];

		for (int i = 0; i < threadNum; i += 2) {
			threads[i]
			= new ThreadEnqueue<QueueType, ParaType, CLASS_NAME> (queue, testcase, elementNum, operationNum, i
					/ 2);
			if (i + 1 < threadNum) {
				threads[i + 1] = new ThreadDequeue<QueueType, ParaType,
				CLASS_NAME> (queue, testcase, elementNum, operationNum);
			}
		}
		return threads;
	}

	virtual void verifyResult(int threadNum, int elementNum) {
		while (!queue->empty()) {
			ParaType tmp;
			if (queue->dequeue(tmp)) {
				this->outVec.push_back(tmp);
			}
		}

		//remove duplicated element in inVec
		sort(this->inVec.begin(), this->inVec.end());
//		cout << "inVec before :" << this->inVec.size();
		//		cout << "inVec .... size: " << this->inVec.size() << endl;
		//		for (typename vector<ParaType>::iterator ite = this->inVec.begin(); ite
		//				!= this->inVec.end(); ++ite) {
		//			cout << *ite << " : ";
		//		}
		//		cout << endl;
		this->inVec.erase(unique(this->inVec.begin(),this->inVec.end()),this->inVec.end());
//		cout << ", after :" << this->inVec.size() << endl;

		sort(this->outVec.begin(), this->outVec.end());
//		cout << "outVec before :" << this->outVec.size();
		//		cout << "inVec .... size: " << this->inVec.size() << endl;
		//		for (typename vector<ParaType>::iterator ite = this->inVec.begin(); ite
		//				!= this->inVec.end(); ++ite) {
		//			cout << *ite << " : ";
		//		}
		//		cout << endl;
		this->outVec.erase(unique(this->outVec.begin(),this->outVec.end()),this->outVec.end());
//		cout << ", after :" << this->outVec.size() << endl;

		//		cout << "inVec .... size: " << this->inVec.size() << endl;
		//		for (typename vector<ParaType>::iterator ite = this->inVec.begin(); ite
		//				!= this->inVec.end(); ++ite) {
		//			cout << *ite << " : ";
		//		}
		//		cout << endl;

		//		cout << "outVec ...." << this->outVec.size() << endl;
		//		for (typename vector<ParaType>::iterator ite = this->outVec.begin(); ite
		//				!= this->outVec.end(); ++ite) {
		//			cout << *ite << " : ";
		//		}
		//		cout << endl;

		ThreadFactory<ParaType>::verifyResult(threadNum, elementNum);

	}
};

template<typename QueueType, typename ParaType, char const* CLASS_NAME> void PQueueTest<
QueueType, ParaType, CLASS_NAME>::testEmptyST() {
	CPPUNIT_ASSERT(true == queue->empty());
	CPPUNIT_ASSERT(0 == queue->size());
}

template<typename QueueType, typename ParaType, char const* CLASS_NAME>
void PQueueTest<QueueType, ParaType, CLASS_NAME>::testEnqueueST() {
	ThreadEnqueue_T threadEnqueue(queue, this, BaseTest<ParaType>::NELEMENT,
			BaseTest<ParaType>::NOPERATION);
	threadEnqueue.run();
	//	cout << BaseTest<ParaType>::NOPERATION << " : " << queue->size() << endl;
	CPPUNIT_ASSERT(BaseTest<ParaType>::NOPERATION == queue->size());

	/*verify enqueued data*/
	vector<ParaType> outVec;
	while (!queue->empty()) {
		ParaType tmp;
		if (queue->dequeue(tmp)) {
			outVec.push_back(tmp);
		}
	}

	sort(threadEnqueue.inVec.begin(), threadEnqueue.inVec.end());
	sort(outVec.begin(), outVec.end());

//	cout << "inVec .... size: " << threadEnqueue.inVec.size() << endl;
//	for (typename vector<ParaType>::iterator ite = threadEnqueue.inVec.begin(); ite
//		!= threadEnqueue.inVec.end(); ++ite) {
//		cout << *ite << " : ";
//	}
//	cout << endl;

//	cout << "outVec ...." << outVec.size() << endl;
//	for (typename vector<ParaType>::iterator ite = outVec.begin(); ite
//		!= outVec.end(); ++ite) {
//		cout << *ite << " : ";
//	}
//	cout << endl;


	CPPUNIT_ASSERT(threadEnqueue.inVec == outVec);
}

template<typename QueueType, typename ParaType, char const* CLASS_NAME> void PQueueTest<
QueueType, ParaType, CLASS_NAME>::testDequeueST() {
	ThreadEnqueue_T threadEnqueue(queue, this, BaseTest<ParaType>::NELEMENT,
			BaseTest<ParaType>::NOPERATION);
	ThreadDequeue_T threadDequeue(queue, this, BaseTest<ParaType>::NELEMENT,
			BaseTest<ParaType>::NOPERATION);
	threadEnqueue.run();
	//	queue->dumpQueue();
	//	CPPUNIT_ASSERT(BaseTest<ParaType>::NOPERATION == queue->size());
	threadDequeue.run();
	//	cout << "starting dequeue ...." << endl;
	//	queue->dumpQueue();
	//	cout << queue->size() << endl;

	CPPUNIT_ASSERT(0 == queue->size());

	sort(threadEnqueue.inVec.begin(), threadEnqueue.inVec.end());
	sort(threadDequeue.outVec.begin(), threadDequeue.outVec.end());

	CPPUNIT_ASSERT(threadEnqueue.inVec == threadDequeue.outVec);
}

template<typename QueueType, typename ParaType, char const* CLASS_NAME> void PQueueTest<
QueueType, ParaType, CLASS_NAME>::testEnqueueMT() {
	ThreadEnqueueFactory<QueueType, ParaType, CLASS_NAME> factory(queue, this);
	ThreadRunner::runThreads(&factory, CLASS_NAME, "testEnqueueMT");
}

template<typename QueueType, typename ParaType, char const* CLASS_NAME> void PQueueTest<
QueueType, ParaType, CLASS_NAME>::testDequeueMT() {
	ThreadDequeueFactory<QueueType, ParaType, CLASS_NAME> dequeueFactory(queue,
			this);
	ThreadRunner::runThreads(&dequeueFactory, CLASS_NAME, "testDequeueMT");
}

template<typename QueueType, typename ParaType, char const* CLASS_NAME> void PQueueTest<
QueueType, ParaType, CLASS_NAME>::testEnDequeuePairMT() {
	ThreadEnDequeueFactory<QueueType, ParaType, CLASS_NAME>
	factory(queue, this);
	ThreadRunner::runThreads(&factory, CLASS_NAME, "testEnDequeuePairMT");
}

template<typename QueueType, typename ParaType, char const* CLASS_NAME> void PQueueTest<
QueueType, ParaType, CLASS_NAME>::testBound() {
	ParaType tmp;
	bool res = queue->dequeue(tmp);
	CPPUNIT_ASSERT(!res);
}
}
#endif /*PQueueTest_H_*/

/*
 * (c) Copyright 2008, IBM Corporation.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
#include <cppunit/ui/text/TestRunner.h>
#include <cppunit/extensions/HelperMacros.h>
#include <cppunit/extensions/TestFactoryRegistry.h>

//*************** include head files of test case here ***********************
#include <amino/queue.h>
#include <amino/deque.h>
#include <amino/bdeque.h>

#include "test_queue.h"
//************************ end of include head files ************************

using namespace test;

REGISTRATION_TEST(QueueTest, LockFreeQueue, int);
REGISTRATION_TEST(QueueTest, LockFreeQueue, char);
REGISTRATION_TEST(QueueTest, LockFreeQueue, long);
REGISTRATION_TEST(QueueTest, LockFreeQueue, double);
REGISTRATION_TEST(QueueTest, LockFreeQueue, string);

//REGISTRATION_TEST(QueueTest, ShavitQueue, int);
//REGISTRATION_TEST(QueueTest, ShavitQueue, char);
//REGISTRATION_TEST(QueueTest, ShavitQueue, long);
//REGISTRATION_TEST(QueueTest, ShavitQueue, double);
//REGISTRATION_TEST(QueueTest, ShavitQueue, string);
//
//REGISTRATION_TEST(QueueTest, LockFreeDeque, int);
//REGISTRATION_TEST(QueueTest, LockFreeDeque, char);
//REGISTRATION_TEST(QueueTest, LockFreeDeque, long);
//REGISTRATION_TEST(QueueTest, LockFreeDeque, double);
//REGISTRATION_TEST(QueueTest, LockFreeDeque, string);
//
//REGISTRATION_TEST(QueueTest, BlockingDeque, int);
//REGISTRATION_TEST(QueueTest, BlockingDeque, char);
//REGISTRATION_TEST(QueueTest, BlockingDeque, long);
//REGISTRATION_TEST(QueueTest, BlockingDeque, double);
//REGISTRATION_TEST(QueueTest, BlockingDeque, string);
//
//REGISTRATION_TEST(QueueTest, ShavitQueueAlloc, int);
//REGISTRATION_TEST(QueueTest, ShavitQueueAlloc, char);
//REGISTRATION_TEST(QueueTest, ShavitQueueAlloc, long);
//REGISTRATION_TEST(QueueTest, ShavitQueueAlloc, double);
//REGISTRATION_TEST(QueueTest, ShavitQueueAlloc, string);

int main() {
	CppUnit::TextUi::TestRunner runner;
	CppUnit::TestFactoryRegistry &registry =
			CppUnit::TestFactoryRegistry::getRegistry();
	runner.addTest(registry.makeTest() );
	bool wasSuccessful = runner.run("", false);
	return !wasSuccessful;
}

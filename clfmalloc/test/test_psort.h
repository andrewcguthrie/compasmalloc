/*
 * (c) Copyright 2008, IBM Corporation.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  Change History:
 *
 *  yy-mm-dd  Developer  Defect     Description
 *  --------  ---------  ------     -----------
 *  08-08-14  ganzhi     N/A        Initial version
 */

#ifndef TEST_PSORT_H_
#define TEST_PSORT_H_
#include <cppunit/extensions/HelperMacros.h>
#include <cppunit/TestAssert.h>
#include "baseTest.h"

#include <amino/cstdatomic>
#include <amino/thread.h>
#include <amino/parallel_sort.h>
#include <amino/tp_exec.h>

#include <iostream>
#include <unistd.h>
#include <algorithm>
#include <sstream>

namespace test{
    using namespace amino;

    template<typename ParaType, char const* CLASS_NAME> 
        class ParallelSortTest :
            public CppUnit::TestFixture, public BaseTest<ParaType> {
                CPPUNIT_TEST_SUITE(ParallelSortTest);
                CPPUNIT_TEST(testSort); 
                CPPUNIT_TEST_SUITE_END();

                public:
                ParallelSortTest() {
                }

                void setUp() {
                }

                void reset() {
                }

                void tearDown() {
                }

                void testSort(){
                    Logger log(stderr);
                    vector<int> thread_v = TestConfig::getInstance()->getThreadNum();
                    ParaType* backup =  new ParaType[this->NELEMENT * this->MAXTHREADN];
                    std::copy(this->data, this->data + this->NELEMENT * this->MAXTHREADN, backup);
                    ParaType* verify =  new ParaType[this->NELEMENT * this->MAXTHREADN];
                    for(unsigned int i=0;i<thread_v.size();i++){
                        ThreadPoolExecutor exec(thread_v[i]);
                        std::copy(this->data, this->data + this->NELEMENT * thread_v[i], verify);

                        struct timeval t1, t2, t3;
                        gettimeofday(&t1, NULL);

                        parallel_sort(this->data, this->data + this->NELEMENT * thread_v[i], thread_v[i], &exec);
                        gettimeofday(&t2, NULL);

                        std::sort(verify, verify + this->NELEMENT * thread_v[i]);

                        gettimeofday(&t3, NULL);


                        int takes1 = (t2.tv_sec - t1.tv_sec)* 1000000 + t2.tv_usec - t1.tv_usec; 
                        log.log(
                                "INFO: class:	%s\tnThread:\t%d\tnElement:\t%d\ttestName:\t%s\tTakes:\t%d\tmicroseconds\n",
                                CLASS_NAME, thread_v[i], this->NELEMENT, "testParallelSort", takes1);

                        int takes2 = (t3.tv_sec - t2.tv_sec)* 1000000 + t3.tv_usec - t2.tv_usec; 
                        log.log(
                                "INFO: class:	%s\tnThread:\t%d\tnElement:\t%d\ttestName:\t%s\tTakes:\t%d\tmicroseconds\n",
                                CLASS_NAME, 1, this->NELEMENT*thread_v[i], "testSerialSort", takes2);

                        for(int k=0;k<this->NELEMENT * thread_v[i];k++){
                            ostringstream output;
                            output << k <<"th element is " << this->data[k]<< " while " << "verify is "<< verify[k]<<endl;

                            CPPUNIT_ASSERT_MESSAGE(output.str(), this->data[k]==verify[k]);
                        }

                        std::copy(backup, backup + this->NELEMENT * this->MAXTHREADN, this->data);
                        exec.shutdown();
                        exec.waitTermination();
                    }

                    delete [] backup;
                    delete [] verify;
                }
            };
}
#endif /*TEST_PSORT_H_*/

/*
 * (c) Copyright 2008, IBM Corporation.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
#ifndef TEST_LIST_H_
#define TEST_LIST_H_
#include <cppunit/extensions/HelperMacros.h>
#include <cppunit/TestAssert.h>
#include "baseTest.h"

namespace test {

using namespace amino;
template<typename SetType, typename ParaType, char const* CLASS_NAME>
class SetTest: public CppUnit::TestFixture, public BaseTest<ParaType> {

CPPUNIT_TEST_SUITE(SetTest)
;	CPPUNIT_TEST(testEmptyST);
	CPPUNIT_TEST(testInsertST);
	CPPUNIT_TEST(testRemoveST);
	CPPUNIT_TEST(testInsertMT);
	CPPUNIT_TEST(testRemoveMT);
	CPPUNIT_TEST(testInsertRemoveMT);
	CPPUNIT_TEST_SUITE_END();
private:
	SetType *set;

public:
	SetTest() {
	}

	SetType * getSet() {
		return set;
	}

	void setUp() {
		set = new SetType();
	}

	void reset() {
		delete set;
		set = new SetType();
	}

	void tearDown() {
		delete set;
	}

	void testEmptyST();
	void testInsertST();
	void testRemoveST();
	void testInsertMT();
	void testRemoveMT();
	void testInsertRemoveMT();
};

#define ThreadInsert_T ThreadInsert<SetType, ParaType, CLASS_NAME>
#define ThreadRemove_T ThreadRemove<SetType, ParaType, CLASS_NAME>

template<typename SetType, typename ParaType, char const* CLASS_NAME> class ThreadInsert :
public TestThread<ParaType> {
private:
	SetType *set;
	SetTest<SetType, ParaType, CLASS_NAME> *testcase;
	int elementNum;
	int operationNum;

public:

	ThreadInsert(SetType *s, SetTest<SetType, ParaType, CLASS_NAME>* st,
			int nElement, int nOperation, int threadId = 0) :
	TestThread<ParaType>(threadId), set(s), testcase(st), elementNum(nElement),operationNum(nOperation) {
	}

	void* run() {
		for (int i = 0; i< operationNum; ++i) {
			if (set->insert(testcase->data[i%((this->threadId+1)*elementNum)]))
			this->inVec.push_back(testcase->data[i%((this->threadId+1)*elementNum)]);
		}
		return NULL;
	}
};

template<typename SetType, typename ParaType, char const* CLASS_NAME> class ThreadRemove :
public TestThread<ParaType> {
private:
	SetType *set;
	SetTest<SetType, ParaType, CLASS_NAME> *testcase;
	int elementNum;
	int operationNum;
public:
	ThreadRemove(SetType *s, SetTest<SetType, ParaType, CLASS_NAME>* st,
			int nElement, int nOperation, int nThread = 1) :TestThread<ParaType>(nThread),
        set(s), testcase(st), elementNum(nElement), operationNum(nOperation) {
	}

	void *run() {
		for (int i = 0; i< operationNum; ++i) {
			ParaType tmp = testcase->data[i%((this->threadId+1)*elementNum)];
			if (set->remove(tmp)) {
				this->outVec.push_back(tmp);
			}
		}

		return NULL;
	}
};

template<typename SetType, typename ParaType, char const* CLASS_NAME> class ThreadInsertFactory :
public ThreadFactory<ParaType> {
private:
	SetType *set;
	SetTest<SetType, ParaType, CLASS_NAME> *testcase;
public:
	ThreadInsertFactory(SetType * s,
			SetTest<SetType, ParaType, CLASS_NAME> * st) :
	set(s), testcase(st) {
	}

	virtual Runnable ** createThreads(int threadNum, int elementNum, int operationNum) {
		testcase->reset();
		set = testcase->getSet();
		this->inVec.clear();
		this->outVec.clear();

		Runnable ** threads;
		threads = new Runnable*[threadNum];
		for (int i = 0; i < threadNum; ++i)
		threads[i] = new ThreadInsert<SetType, ParaType, CLASS_NAME>(set, testcase, elementNum,operationNum, i);

		return threads;
	}

	virtual void verifyResult(int threadNum, int elementNum) {
		CPPUNIT_ASSERT(this->inVec.size() == set->size());

		vector<ParaType> vec(this->inVec);

		bool result = true;
		while(!(this->inVec.empty())) {
			if(set->search(this->inVec.back())) {
				this->inVec.pop_back();
			}
			else {
				result = false;
				break;
			}
		}
		CPPUNIT_ASSERT(result);
	}
};

template<typename SetType, typename ParaType, char const* CLASS_NAME> class ThreadRemoveFactory :
public ThreadFactory<ParaType> {
private:
	SetType *set;
	SetTest<SetType, ParaType, CLASS_NAME> *testcase;
public:
	ThreadRemoveFactory(SetType * s,
			SetTest<SetType, ParaType, CLASS_NAME> * st) :
	set(s), testcase(st) {
	}

	virtual Runnable ** createThreads(int threadNum, int elementNum, int operationNum) {
		testcase->reset();
		set = testcase->getSet();
		this->inVec.clear();
		this->outVec.clear();

		for (int i = 0; i < threadNum; ++i) {
			ThreadInsert_T threadInsert(set, testcase, elementNum, i);
			threadInsert.run();

			copy(threadInsert.inVec.begin(), threadInsert.inVec.end(),
					back_inserter(this->inVec));
		}

		Runnable ** threads;
		threads = new Runnable*[threadNum];
		for (int i = 0; i < threadNum; ++i)
		threads[i] = new ThreadRemove<SetType, ParaType, CLASS_NAME>(set, testcase, elementNum,operationNum, i);

		return threads;
	}

	virtual void verifyResult(int threadNum, int elementNum) {
		CPPUNIT_ASSERT(this->inVec.size() == this->outVec.size() + set->size());

		bool result = true;

		typename vector<ParaType>::iterator iter = this->outVec.begin();
		for(; iter != this->outVec.end(); ++iter) {
			typename vector<ParaType>::iterator itFound = find(this->inVec.begin(),this->inVec.end(),*iter);
			if(itFound != this->inVec.end())
			this->inVec.erase(itFound);
		}

		while(!this->outVec.empty()) {
			if(set->search(this->outVec.back())) {
				result = false;
				break;
			}
			this->outVec.pop_back();
		}

		CPPUNIT_ASSERT(result);
		while(!this->inVec.empty()) {
			if(set->search(this->inVec.back())) {
				this->inVec.pop_back();
			}
			else {
				result = false;
				break;
			}
		}
		CPPUNIT_ASSERT(result);
	}
};
template<typename SetType, typename ParaType, char const* CLASS_NAME> class ThreadInsertRemoveFactory :
public ThreadFactory<ParaType> {
private:
	SetType *set;
	SetTest<SetType, ParaType, CLASS_NAME> *testcase;
public:
	ThreadInsertRemoveFactory(SetType * s,
			SetTest<SetType, ParaType, CLASS_NAME> * st) :
	set(s), testcase(st) {
	}

	virtual Runnable ** createThreads(int threadNum, int elementNum, int operationNum) {
		testcase->reset();
		set = testcase->getSet();
		this->inVec.clear();
		this->outVec.clear();

		Runnable ** threads;
		threads = new Runnable*[threadNum];

		for (int i = 0; i < threadNum; ++i) {
			ThreadInsert_T threadInsert(set, testcase, elementNum, i);
			threadInsert.run();

			copy(threadInsert.inVec.begin(), threadInsert.inVec.end(),
					back_inserter(this->inVec));
		}

		for (int i = 0; i < threadNum; i += 2) {
			threads[i] = new ThreadInsert<SetType, ParaType, CLASS_NAME>(set, testcase, elementNum,operationNum, i);
			if (i+1 < threadNum)
			threads[i+1] = new ThreadRemove<SetType, ParaType, CLASS_NAME>(set, testcase, elementNum,operationNum,i);
		}
		return threads;
	}

	virtual void verifyResult(int threadNum, int elementNum) {
		CPPUNIT_ASSERT(this->inVec.size() == this->outVec.size() + set->size());

		bool result = true;

		typename vector<ParaType>::iterator iter = this->outVec.begin();

		for(iter = this->outVec.begin(); iter != this->outVec.end(); ++iter) {
			typename vector<ParaType>::iterator itFound = find(this->inVec.begin(),this->inVec.end(),*iter);

			if(itFound != this->inVec.end()) {
				this->inVec.erase(itFound);
			}
		}

		while(!this->inVec.empty()) {
			if(set->search(this->inVec.back())) {
				this->inVec.pop_back();
			}
			else {
				result = false;
				break;
			}
		}
		CPPUNIT_ASSERT(result);
	}
};

template<typename SetType, typename ParaType, char const* CLASS_NAME> void SetTest<
SetType, ParaType, CLASS_NAME>::testEmptyST() {
	CPPUNIT_ASSERT( true == set->empty());
}

template<typename SetType, typename ParaType, char const* CLASS_NAME> void SetTest<
SetType, ParaType, CLASS_NAME>::testInsertST() {
	ThreadInsert_T threadInsert(set, this, BaseTest<ParaType>::NELEMENT, BaseTest<ParaType>::NOPERATION);
	threadInsert.run();

	CPPUNIT_ASSERT(threadInsert.inVec.size() == set->size());

	bool result = true;
	while(!(threadInsert.inVec.empty())) {
		if(set->search(threadInsert.inVec.back())) {
			threadInsert.inVec.pop_back();
		}
		else {
			result = false;
			break;
		}
	}
	CPPUNIT_ASSERT(result);
}

template<typename SetType, typename ParaType, char const* CLASS_NAME> void SetTest<
SetType, ParaType, CLASS_NAME>::testRemoveST() {
	ThreadInsert_T threadInsert(set, this, BaseTest<ParaType>::NELEMENT, BaseTest<ParaType>::NOPERATION);
	ThreadRemove_T threadRemove(set, this, BaseTest<ParaType>::NELEMENT, BaseTest<ParaType>::NOPERATION);

	threadInsert.run();
	threadRemove.run();

	CPPUNIT_ASSERT(threadInsert.inVec.size() == threadRemove.outVec.size() + set->size());

	bool result = true;
	while(!threadRemove.outVec.empty()) {
		if(set->search(threadRemove.outVec.back())) {
			result = false;
			break;
		}
		threadRemove.outVec.pop_back();
	}

	CPPUNIT_ASSERT(result);
}

template<typename SetType, typename ParaType, char const* CLASS_NAME> void SetTest<
SetType, ParaType, CLASS_NAME>::testInsertMT() {
	ThreadInsertFactory<SetType, ParaType, CLASS_NAME> factory(set, this);
	ThreadRunner::runThreads(&factory, CLASS_NAME, "testInsertMT");
}
template<typename SetType, typename ParaType, char const* CLASS_NAME> void SetTest<
SetType, ParaType, CLASS_NAME>::testRemoveMT() {
	ThreadRemoveFactory<SetType, ParaType, CLASS_NAME> factory(set, this);
	ThreadRunner::runThreads(&factory, CLASS_NAME, "testRemoveMT");
}

template<typename SetType, typename ParaType, char const* CLASS_NAME> void SetTest<
SetType, ParaType, CLASS_NAME>::testInsertRemoveMT() {
	ThreadInsertRemoveFactory<SetType, ParaType, CLASS_NAME>
	factory(set, this);
	ThreadRunner::runThreads(&factory, CLASS_NAME, "testInsertRemoveMT");
}
}
#endif /*TEST_LIST_H_*/

/*
 * (c) Copyright 2008, IBM Corporation.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
#ifndef DEQUETEST_H_
#define DEQUETEST_H_
#include <cppunit/extensions/HelperMacros.h>
#include <cppunit/TestAssert.h>
#include <stdexcept>

#include "baseTest.h"

namespace test {
using namespace amino;

template<typename DequeType, typename ParaType, char const* CLASS_NAME>
class DequeTest: public CppUnit::TestFixture, public BaseTest<ParaType> {
    CPPUNIT_TEST_SUITE(DequeTest);
    CPPUNIT_TEST(testPushRightMT);
	CPPUNIT_TEST(test4DirectionMT);
	CPPUNIT_TEST(testBoundLeft);
	CPPUNIT_TEST(testBoundRight);
	CPPUNIT_TEST_SUITE_END();

private:
	DequeType *deque;
public:
	DequeTest() {
	}

	DequeType * getDeque() {
		return deque;
	}

	void setUp() {
		deque = new DequeType();
	}

	void reset() {
		delete deque;
		deque = new DequeType();
	}

	void tearDown() {
		delete deque;
	}

	/**
	 * This function tests push&pop in both ends of deque with multiple-threads.
	 */
	void testPushRightMT();
	void test4DirectionMT();

	void testBoundLeft();
	void testBoundRight();
};

template<typename DequeType, typename ParaType, char const* CLASS_NAME>
class Thread4Dir: public TestThread<ParaType> {
private:
	DequeType *deque;
	DequeTest<DequeType, ParaType, CLASS_NAME> *testcase;
	int elementNum;
	int operationNum;

public:
	Thread4Dir(DequeType * q, DequeTest<DequeType, ParaType, CLASS_NAME> * qt,
			int nElement, int nOperation) :
	deque(q), testcase(qt), elementNum(nElement), operationNum(nOperation) {
	}

	static atomic<ParaType> checksum;

	static ParaType getChecksum() {
		return checksum.load(memory_order_relaxed);
	}

	void* run() {
		for (int i = 0; i < operationNum; ++i) {
			if (i % 2 == 0) {
				deque->pushRight(testcase->data[i%((this->threadId+1)*elementNum)]);
				this->inVec.push_back(testcase->data[i%((this->threadId+1)*elementNum)]);
				try {
					ParaType tmp;
					if(deque->popLeft(tmp)) {
						this->outVec.push_back(tmp);
					}
					// Intentionally generate exception
					if (i % 7 == 0) {
						if(deque->popLeft(tmp)) {
							this->outVec.push_back(tmp);
						}
					}
				} catch (std::range_error e) {
				}
			} else {
				deque->pushLeft(testcase->data[i%((this->threadId+1)*elementNum)]);
				this->inVec.push_back(testcase->data[i%((this->threadId+1)*elementNum)]);
				ParaType tmp;
				try {
					if(deque->popRight(tmp)) {
						this->outVec.push_back(tmp);
					}
					// Intentionally generate exception
					if (i % 7 == 0) {
						if(deque->popLeft(tmp)) {
							this->outVec.push_back(tmp);
						}
					}
				} catch (std::range_error e) {
				}
			}
		}
		return NULL;
	}
};

template<typename DequeType, typename ParaType, const char * CLASS_NAME>
atomic<ParaType> Thread4Dir<DequeType, ParaType, CLASS_NAME>::checksum;

template<typename DequeType, typename ParaType, char const* CLASS_NAME>
class ThreadPushRight: public TestThread<ParaType> {
private:
	DequeType *deque;
	DequeTest<DequeType, ParaType, CLASS_NAME> *testcase;
	int elementNum;
	int operationNum;

public:
	ThreadPushRight(DequeType * q,
			DequeTest<DequeType, ParaType, CLASS_NAME> * qt, int nElement, int nOperation) :
	deque(q), testcase(qt), elementNum(nElement),operationNum(nOperation) {
	}

	static atomic<ParaType> checksum;

	static ParaType getChecksum() {
		return checksum.load(memory_order_relaxed);
	}

	void* run() {
		for (int i = 0; i < operationNum; ++i) {
			if (i % 2 == 0) {
				deque->pushRight(testcase->data[i%((this->threadId+1)*elementNum)]);
				this->inVec.push_back(testcase->data[i%((this->threadId+1)*elementNum)]);
			} else {
				deque->pushLeft(testcase->data[i%((this->threadId+1)*elementNum)]);
				this->inVec.push_back(testcase->data[i%((this->threadId+1)*elementNum)]);
			}
		}
		return NULL;
	}
};

template<typename DequeType, typename ParaType, const char * CLASS_NAME>
atomic<ParaType> ThreadPushRight<DequeType, ParaType, CLASS_NAME>::checksum;

template<typename DequeType, typename ParaType, const char * CLASS_NAME>
class ThreadPushRightFactory: public ThreadFactory<ParaType> {
private:
	DequeType *deque;
	DequeTest<DequeType, ParaType, CLASS_NAME> *testcase;
public:
	ThreadPushRightFactory(DequeType * q, DequeTest<DequeType, ParaType,
			CLASS_NAME> * qt) :
	deque(q), testcase(qt) {
	}

	virtual Runnable ** createThreads(int threadNum, int elementNum,
			int operationNum) {
		testcase->reset();
		deque = testcase->getDeque();

		this->inVec.clear();
		this->outVec.clear();

		ThreadPushRight<DequeType, ParaType, CLASS_NAME>::checksum = 0;

		Runnable ** threads;
		threads = new Runnable*[threadNum];
		for (int i = 0; i < threadNum; ++i)
            threads[i] = new ThreadPushRight<DequeType, ParaType, CLASS_NAME> (
                    deque, testcase, elementNum, operationNum);

		return threads;
	}

	typedef ThreadPushRight<DequeType, ParaType, CLASS_NAME> ThreadPushRight_T;

	virtual void verifyResult(int threadNum, int elementNum) {
		while (!deque->empty()) {
			ParaType tmp;
			if(deque->popRight(tmp)) {
				this->outVec.push_back(tmp);
			}
		}
		ThreadFactory<ParaType>::verifyResult(threadNum, elementNum);
	}

};

template<typename DequeType, typename ParaType, char const* CLASS_NAME>
class Thread4DirFactory: public ThreadFactory<ParaType> {
private:
	DequeType *deque;
	DequeTest<DequeType, ParaType, CLASS_NAME> *testcase;

public:
	Thread4DirFactory(DequeType * q,
			DequeTest<DequeType, ParaType, CLASS_NAME> * qt) :
	deque(q), testcase(qt) {
	}
	virtual Runnable ** createThreads(int threadNum, int elementNum,
			int operationNum) {
		testcase->reset();
		deque = testcase->getDeque();

		this->inVec.clear();
		this->outVec.clear();

		Thread4Dir<DequeType, ParaType, CLASS_NAME>::checksum = 0;

		Runnable ** threads;
		threads = new Runnable*[threadNum];
		for (int i = 0; i < threadNum; ++i)
            threads[i]
                = new Thread4Dir<DequeType, ParaType, CLASS_NAME> (deque, testcase, elementNum, operationNum);

		return threads;
	}

	typedef Thread4Dir<DequeType, ParaType, CLASS_NAME> Thread4Dir_T;
};

template<typename DequeType, typename ParaType, const char * CLASS_NAME>
void DequeTest<DequeType, ParaType, CLASS_NAME>::testPushRightMT() {
	ThreadPushRightFactory<DequeType, ParaType, CLASS_NAME> dequeueFactory(
			deque, this);
	ThreadRunner::runThreads(&dequeueFactory, CLASS_NAME, "testPushRightMT");
}

template<typename DequeType, typename ParaType, const char * CLASS_NAME>
void DequeTest<DequeType, ParaType, CLASS_NAME>::test4DirectionMT() {
	Thread4DirFactory<DequeType, ParaType, CLASS_NAME> dequeueFactory(deque,
			this);
	ThreadRunner::runThreads(&dequeueFactory, CLASS_NAME, "testDequeMT");
}

template<typename DequeType, typename ParaType, const char * CLASS_NAME>
void DequeTest<DequeType, ParaType, CLASS_NAME>::testBoundLeft() {
	ParaType tmp;
	bool res = deque->popLeft(tmp);

	CPPUNIT_ASSERT(!res);
}

template<typename DequeType, typename ParaType, const char * CLASS_NAME>
void DequeTest<DequeType, ParaType, CLASS_NAME>::testBoundRight() {
	ParaType tmp;
	bool res = deque->popRight(tmp);

	CPPUNIT_ASSERT(!res);
}
}
#endif /*DEQUETEST_H_*/

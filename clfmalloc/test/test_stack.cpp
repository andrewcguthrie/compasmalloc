/*
 * (c) Copyright 2008, IBM Corporation.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
#include <cppunit/ui/text/TestRunner.h>
#include <cppunit/extensions/HelperMacros.h>
#include <cppunit/extensions/TestFactoryRegistry.h>

//*************** include head files of test case here ***********************
#include <amino/stack.h>
#include <amino/ebstack.h>
#include <test_stack.h>
//************************ end of include head files ************************

using namespace test;

class myobj
{
public:
   int a;
public:
   myobj()
   {}

   ~myobj()
   {}
};

bool operator < (const myobj& obj1, const myobj& obj2)
   {
       return obj1.a<obj2.a; 
   }

bool operator == (const myobj& obj1, const myobj& obj2)
   {
       return obj1.a == obj2.a ;
   }

ostream& operator << (ostream& out, const myobj& s)
   {
       out << s.a << "\n";
       return out;
   }


REGISTRATION_TEST(StackTest, LockFreeStack, int);
REGISTRATION_TEST(StackTest, LockFreeStack, char);
REGISTRATION_TEST(StackTest, LockFreeStack, long);
REGISTRATION_TEST(StackTest, LockFreeStack, double);
REGISTRATION_TEST(StackTest, LockFreeStack, string);
REGISTRATION_TEST(StackTest, LockFreeStack, myobj);

REGISTRATION_TEST(StackTest, EBStack, int);
REGISTRATION_TEST(StackTest, EBStack, char);
REGISTRATION_TEST(StackTest, EBStack, long);
REGISTRATION_TEST(StackTest, EBStack, double);
REGISTRATION_TEST(StackTest, EBStack, string);

int main() {
	CppUnit::TextUi::TestRunner runner;
	CppUnit::TestFactoryRegistry &registry =
			CppUnit::TestFactoryRegistry::getRegistry();
	runner.addTest(registry.makeTest() );
	bool wasSuccessful = runner.run("", false);
	return !wasSuccessful;
}

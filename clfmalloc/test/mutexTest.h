/*
 * (c) Copyright 2008, IBM Corporation.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
#ifndef MUTEXTEST_H_
#define MUTEXTEST_H_
#include <cppunit/extensions/HelperMacros.h>
#include <cppunit/TestAssert.h>
#include "baseTest.h"
#include "threadFactory.h"
#include <amino/mutex.h>
#include <amino/thread.h>

using namespace amino;
namespace test{

    /**
     * Test case for mutex class
     * @author Zhi Gan (ganzhi@gmail.com)
     */
    class MutexTest : public CppUnit::TestFixture {
        CPPUNIT_TEST_SUITE(MutexTest);
        CPPUNIT_TEST(testMutex);
        CPPUNIT_TEST(testRecursiveMutex);
        CPPUNIT_TEST_SUITE_END();

        private:
        mutex * mut;

        public:
        void setUp() {
            mut = new mutex();
        }

        void reset(){
            delete mut;
            mut = new mutex();
        }

        /**
         * This test case will start 2 threads. Each thread will
         * try to acquire a lock and then sleep three seconds. So
         * if the mutex work correctly, the whole test case will
         * need longer than 6 seconds.
         */
        void testMutex();

        void testRecursiveMutex();

        void tearDown() {
            delete mut;
        }
    };

    class MutexThread : public Thread {
        private:
            mutex * fMutex;
        public:
            MutexThread(mutex * mut) {
                fMutex = mut;
            }

            void* run() {
                fMutex->lock();
                sleep(3);
                fMutex->unlock();
                return NULL;
            }
    };

    class RecursiveMutexThread : public Thread {
        private:
            mutex * fMutex;
        public:
            RecursiveMutexThread(mutex * mut) {
                fMutex = mut;
            }

            void* run() {
                fMutex->lock();
                fMutex->lock();
                sleep(3);
                fMutex->unlock();
                fMutex->unlock();

                return NULL;
            }
    };
}
#endif /*MUTEXTEST_H_*/

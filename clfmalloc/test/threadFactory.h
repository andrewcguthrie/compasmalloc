/*
 * (c) Copyright 2008, IBM Corporation.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
#ifndef THREAD_FACTORY_H
#define THREAD_FACTORY_H

#include <vector>
#include <iostream>
#include "testThread.h"

using namespace amino;

namespace test {

template<typename ParaType> class ThreadFactory {
public:
        virtual ~ThreadFactory() {}

	vector<ParaType> inVec;
	vector<ParaType> outVec;
	/**
	 * Create multiple threads which can be used to run simultaneously.
	 * This method should also clean and setup internal state for next
	 * round of execution.
	 */
	virtual Runnable ** createThreads(int threadNum, int elementNum,
			int operationNum)=0;
	virtual void deleteThreads(Runnable ** threads, int threadNum) {
		for (int i = 0; i < threadNum; ++i) {
			if (TestThread<ParaType> * p
					= dynamic_cast<TestThread<ParaType> *> (threads[i])) {
				delete p;
			}
		}
		delete[] threads;
	}
	/**
	 * Verify result when created threads finished.
	 */
	virtual void verifyResult(int threadNum, int elementNum) {
		sort(inVec.begin(), inVec.end());
		sort(outVec.begin(), outVec.end());

		if (inVec != outVec) {
			cout << "inVec .... size: " << inVec.size() << endl;
						for (typename vector<ParaType>::iterator ite = inVec.begin(); ite
					!= inVec.end(); ++ite) {
				cout << *ite << " : ";
			}
			cout << endl;

			cout << "outVec .... size: " << outVec.size() << endl;
						for (typename vector<ParaType>::iterator ite = outVec.begin(); ite
								!= outVec.end(); ++ite) {
							cout << *ite << " : ";
						}
						cout << endl;

			cout << "difference of inVec and outVec" << endl;
			ParaType result[100];
			ParaType* resultEnd = set_symmetric_difference(
					inVec.begin(), inVec.end(), outVec.begin(), outVec.end(),
					result);

			for (ParaType* ite = result; ite
					!= resultEnd; ++ite) {
				cout << *ite << " : ";
			}
			cout << endl;

		}

		CPPUNIT_ASSERT(inVec == outVec);
	}

	/**
	 * collect data from vector in every thread and add them to one vector in factory.
	 */
	virtual void collection(Runnable ** threads, int threadNum) {
		for (int i = 0; i < threadNum; ++i) {
			if (TestThread<ParaType> * p
					= dynamic_cast<TestThread<ParaType> *> (threads[i])) {
				if (!p->inVec.empty()) {
					copy(p->inVec.begin(), p->inVec.end(), back_inserter(inVec));
				}
				if (!p->outVec.empty()) {
					copy(p->outVec.begin(), p->outVec.end(), back_inserter(
							outVec));
				}
			}
		}
	}
};
}

#endif

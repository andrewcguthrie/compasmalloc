/*
 * (c) Copyright 2008, IBM Corporation.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
#include <sys/time.h>
#include <iostream>
#include <algorithm>
#include <amino/aasort.h>

#include <test_aasort.h>

#include <cppunit/ui/text/TestRunner.h>
#include <cppunit/extensions/HelperMacros.h>
#include <cppunit/extensions/TestFactoryRegistry.h>

using namespace std;

namespace test{

    const int LENGTH = 8192;

    int test_sort(int & takes1, int & takes2){
        int buffer[LENGTH] __attribute__((aligned(16)));
        int verify[LENGTH] __attribute__((aligned(16)));
        for(int i=0;i<LENGTH;i++){
            buffer[i] = rand() % 32767;
            verify[i] = buffer[i];
        }
#if defined(DEBUG) && defined (VERBOSE)
        printAll(buffer, LENGTH);
        cout<<endl;
#endif

        struct timeval t1, t2, t3;
        gettimeofday(&t1, NULL);

        aa_sort(buffer, buffer+LENGTH);

        gettimeofday(&t2, NULL);

        sort(verify, verify+LENGTH);

        gettimeofday(&t3, NULL);
        takes1 = (t2.tv_sec - t1.tv_sec)* 1000000 + t2.tv_usec - t1.tv_usec;
        takes2 = (t3.tv_sec - t2.tv_sec)* 1000000 + t3.tv_usec - t2.tv_usec;

#if defined(DEBUG)
        cout<< "AAsort takes "<<takes1<<" microseconds"<<endl;
        cout<< "STL sort takes "<<takes2<<" microseconds"<<endl;
#endif

        /**
         * We only checked the 1/4 of the array
         */
        for(int i=0;i<LENGTH/4;i++){
            if(buffer[i]!=verify[i]){
                cout<<"Error! \n"<<LENGTH<<" "<<i<<endl;
#ifdef DEBUG
                printAll(buffer, LENGTH);
                cout<<endl;
                printAll(verify, LENGTH);
#endif
                CPPUNIT_ASSERT(buffer[i]!=verify[i]);
            }
        }
        return 0;
    }

    void AASortTest::testAASort(){
        int sum1, sum2, time1, time2;
        sum1=0;
        sum2=0;
        for(int i=0;i<10;i++){
#if defined(DEBUG)
            cout<<"Begin Testing "<<i<<endl;
#endif
            test_sort(time1, time2);
            sum1+=time1;
            sum2+=time2;
        }
        sum1/=10;
        sum2/=10;
#if defined(DEBUG)
        cout<<"Average time of AASort: "<<sum1<<endl;
        cout<<"Average time of STL Sort: "<<sum2<<endl;
#endif
    }
}

using namespace test;

CPPUNIT_TEST_SUITE_REGISTRATION(AASortTest) ;

int main() {
    CppUnit::TextUi::TestRunner runner;
    CppUnit::TestFactoryRegistry &registry = CppUnit::TestFactoryRegistry::getRegistry();
    runner.addTest( registry.makeTest() );
    bool wasSuccessful = runner.run( "", false );
    return !wasSuccessful;
}

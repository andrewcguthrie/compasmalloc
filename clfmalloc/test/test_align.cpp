/*
 * (c) Copyright 2008, IBM Corporation.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
#include <iostream>

using namespace std;

typedef struct dw{
    int f;
}__attribute__((aligned(64))) hello;


int main(int argc, char **argv[]){

    cout<< sizeof(hello)<<endl;
    hello h[10];
    cout<< sizeof(h)<< endl;

    for(int i=0;i<10;i++){
        h[i].f=i;
    }

    hello * p = h;
    cout<<p<<endl;
    cout<<++p<<endl;


    cout<<__alignof__(hello)<<endl;

    int * buffer;
    posix_memalign((void **)(&buffer), 64, 64);
    cout<<buffer <<endl;

    return 0;
}

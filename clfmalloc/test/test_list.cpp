/*
 * (c) Copyright 2008, IBM Corporation.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
#include <cppunit/ui/text/TestRunner.h>
#include <cppunit/extensions/HelperMacros.h>
#include <cppunit/extensions/TestFactoryRegistry.h>

//*************** include head files of test case here ***********************
#include "test_list.h"
#include <amino/list.h>
#include <amino/ordered_list.h>
#include <amino/sync_list.h>
//************************ end of include head files ************************

using namespace test;

REGISTRATION_TEST(ListTest,List,int);
REGISTRATION_TEST(ListTest,List,char);
REGISTRATION_TEST(ListTest,List,long);
REGISTRATION_TEST(ListTest,List,double);
REGISTRATION_TEST(ListTest,List,string);

REGISTRATION_TEST(ListTest,OrderedList,int);
REGISTRATION_TEST(ListTest,OrderedList,char);
REGISTRATION_TEST(ListTest,OrderedList,long);
REGISTRATION_TEST(ListTest,OrderedList,double);
REGISTRATION_TEST(ListTest,OrderedList,string);

REGISTRATION_TEST(ListTest,SyncList,int);
REGISTRATION_TEST(ListTest,SyncList,char);
REGISTRATION_TEST(ListTest,SyncList,long);
REGISTRATION_TEST(ListTest,SyncList,double);
REGISTRATION_TEST(ListTest,SyncList,string);

int main() {
	CppUnit::TextUi::TestRunner runner;
	CppUnit::TestFactoryRegistry &registry =
			CppUnit::TestFactoryRegistry::getRegistry();
	runner.addTest(registry.makeTest() );
	bool wasSuccessful = runner.run("", false);
	return !wasSuccessful;
}

/*
 * (c) Copyright 2008, IBM Corporation.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
#ifndef CONDVARTEST_H_
#define CONDVARTEST_H_
#include <cppunit/extensions/HelperMacros.h>
#include <cppunit/TestAssert.h>
#include "baseTest.h"
#include "threadFactory.h"
#include <amino/condition.h>
#include <amino/mutex.h>
#include <amino/thread.h>

using namespace amino;

namespace test{

    /**
     * Test case for Condition class
     * @author Zhi Gan (ganzhi@gmail.com)
     */
    class ConditionTest : public CppUnit::TestFixture {
        CPPUNIT_TEST_SUITE(ConditionTest)
            ;
        CPPUNIT_TEST(testCondition);
        CPPUNIT_TEST_SUITE_END()
            ;

        private:
        condition_variable * condVar;
        mutex fMutex;
        public:
        void setUp() {
            condVar = new condition_variable ();
        }

        void reset() {
            delete condVar;
            condVar = new condition_variable();
        }

        void testCondition();

        void tearDown() {
            delete condVar;
        }
    };

    class ConditionThread : public Thread {
        private:
            condition_variable* fCondition;
            mutex * fMutex;
        public:
            ConditionThread(condition_variable * condVar, mutex * m) {
                fCondition = condVar;
                fMutex = m;
            }

            void* run() {
                unique_lock<mutex> llock(*fMutex);
                fCondition->wait(llock);
                return NULL;
            }
    };
}
#endif /*CONDVARTEST_H_*/

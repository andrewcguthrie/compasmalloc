/*
 * (c) Copyright 2008, IBM Corporation.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
#ifndef TEST_STACK_H_
#define TEST_STACK_H_

#include <cppunit/extensions/HelperMacros.h>
#include <cppunit/TestAssert.h>
#include "baseTest.h"

namespace test {
template<class TestType, class ParaType, char const* CLASS_NAME> class StackTest: public CppUnit::TestFixture,
		public BaseTest<ParaType> {
    CPPUNIT_TEST_SUITE(StackTest);
    CPPUNIT_TEST(testEmptyST);
	CPPUNIT_TEST(testPushST);
	CPPUNIT_TEST(testPopST);
	CPPUNIT_TEST(testPushMT);
	CPPUNIT_TEST(testPopMT);
	CPPUNIT_TEST(testPushPopPairMT);
	CPPUNIT_TEST_SUITE_END();

private:
	TestType *stack;

public:
	StackTest() {
	}

	TestType * getStack() {
		return stack;
	}

	void setUp() {
		stack = new TestType();
	}

	void reset() {
		delete stack;
		stack = new TestType();
	}

	void tearDown() {
		delete stack;
	}

	void testEmptyST();
	void testPushST();
	void testPopST();
	void testPushMT();
	void testPopMT();
	void testPushPopPairMT();
};

#define ThreadPop_T ThreadPop<TestType,ParaType, CLASS_NAME>
#define ThreadPush_T ThreadPush<TestType,ParaType, CLASS_NAME>

template<class TestType, class ParaType, char const* CLASS_NAME> class ThreadPush :
public TestThread<ParaType> {
private:
	TestType *stack;
	StackTest<TestType,ParaType, CLASS_NAME>* testcase;
	int elementNum;
	int operationNum;
public:

	ThreadPush(TestType * s, StackTest<TestType,ParaType, CLASS_NAME>* tc,
			int nElement, int nOperation, int threadId = 0) :
	TestThread<ParaType>(threadId), stack(s), testcase(tc), elementNum(nElement),operationNum(nOperation)
    {
	}

	void* run() {
		for (int i = 0; i< operationNum; ++i) {
			stack->push(testcase->data[i%((this->threadId+1)*elementNum)]);
			this->inVec.push_back(testcase->data[i%((this->threadId+1)*elementNum)]);
		}
		return NULL;
	}
};

template<class TestType, class ParaType, char const* CLASS_NAME> class ThreadPop :
public TestThread<ParaType> {
private:
	TestType *stack;
	StackTest<TestType, ParaType, CLASS_NAME> * test;
	int elementNum;
	int operationNum;
public:

	ThreadPop(TestType * s, StackTest<TestType,ParaType, CLASS_NAME> * st,
			int nElement, int nOperation) :
	stack(s), test(st), elementNum(nElement),operationNum(nOperation) {
	}

	void* run() {
		for (int i = 0; i< operationNum; ++i) {
			ParaType tmp;
			if(stack->pop(tmp)) {
				this->outVec.push_back(tmp);
			}
		}
		return NULL;
	}
};

template<class TestType, class ParaType, char const* CLASS_NAME> class ThreadPopFactory :
public ThreadFactory<ParaType> {
private:
	TestType* stack;
	StackTest<TestType,ParaType, CLASS_NAME> * test;
public:

	ThreadPopFactory(TestType * s, StackTest<TestType,ParaType, CLASS_NAME> * t) :
	stack(s), test(t) {
	}

	virtual Runnable** createThreads(int threadNum, int elementNum, int operationNum) {
		test->reset();
		stack = test->getStack();
		this->inVec.clear();
		this->outVec.clear();

		for (int i = 0; i < threadNum; ++i) {
			ThreadPush_T threadPush(stack, test, elementNum, operationNum, i);
			threadPush.run();

			copy(threadPush.inVec.begin(), threadPush.inVec.end(),
					back_inserter(this->inVec));
		}

		Runnable** threads;
		typedef Runnable* pRunnable;
		threads = new pRunnable[threadNum];
		for (int i=0; i<threadNum; i++) {
			threads[i] = new ThreadPop<TestType, ParaType, CLASS_NAME>(stack, test, elementNum,operationNum);
		}
		return threads;
	}

	virtual void verifyResult(int threadNum, int elementNum) {
		CPPUNIT_ASSERT(stack->size() == 0);

		ThreadFactory<ParaType>::verifyResult(threadNum, elementNum);
	}
};

template<class TestType, class ParaType, char const* CLASS_NAME> class ThreadPushFactory :
public ThreadFactory<ParaType> {
private:
	TestType * stack;
	StackTest<TestType,ParaType, CLASS_NAME>* test;
public:

	ThreadPushFactory(TestType* s, StackTest<TestType,ParaType, CLASS_NAME>* t) :
	stack(s), test(t) {
	}

	virtual Runnable ** createThreads(int threadNum, int elementNum, int operationNum) {
		test->reset();
		stack = test->getStack();
		this->inVec.clear();
		this->outVec.clear();

		Runnable ** threads;
		typedef Runnable * pRunnable;
		threads = new pRunnable[threadNum];
		for (int i=0; i<threadNum; i++) {
			threads[i] = new ThreadPush_T(stack, test, elementNum,operationNum, i);
		}
		return threads;
	}

	virtual void verifyResult(int threadNum, int elementNum) {
		//		CPPUNIT_ASSERT( stack->size()==threadNum*elementNum);

		while (!stack->empty()) {
			ParaType tmp;
			if(stack->pop(tmp)) {
				this->outVec.push_back(tmp);
			}
		}

		ThreadFactory<ParaType>::verifyResult(threadNum, elementNum);
	}
};

template<class TestType, class ParaType, char const* CLASS_NAME> class ThreadPushPopPairFactory :
public ThreadFactory<ParaType> {
private:
	TestType * stack;
	StackTest<TestType,ParaType, CLASS_NAME>* test;
public:

	ThreadPushPopPairFactory(TestType* s,
			StackTest<TestType,ParaType, CLASS_NAME>* t) :
	stack(s), test(t) {
	}

	virtual Runnable ** createThreads(int threadNum, int elementNum, int operationNum) {
		test->reset();
		stack = test->getStack();
		this->inVec.clear();
		this->outVec.clear();

		for (int i = 0; i < threadNum; ++i) {
			ThreadPush_T threadPush(stack, test, elementNum, operationNum, i);
			threadPush.run();

			copy(threadPush.inVec.begin(), threadPush.inVec.end(),
					back_inserter(this->inVec));
		}

		Runnable ** threads;

		threads = new Runnable *[threadNum];
        int i = 0;
		while (i < threadNum) {
			threads[i] = new ThreadPush_T(stack, test, elementNum,operationNum, i/2);
            i++;
			if (i < threadNum){
                threads[i] = new ThreadPop_T(stack, test, elementNum,operationNum);
                i++;
            }
		}

		return threads;
	}

	virtual void verifyResult(int threadNum, int elementNum) {
		while (!stack->empty()) {
			ParaType tmp;
			if(stack->pop(tmp)) {
				this->outVec.push_back(tmp);
			}
		}

		ThreadFactory<ParaType>::verifyResult(threadNum, elementNum);
	}
};

template<class TestType, class ParaType, char const* CLASS_NAME> void StackTest<
TestType, ParaType, CLASS_NAME>::testEmptyST() {
	CPPUNIT_ASSERT( true == stack->empty());
	CPPUNIT_ASSERT( 0 == stack->size());
}

template<class TestType, class ParaType, char const* CLASS_NAME> void StackTest<
TestType, ParaType, CLASS_NAME>::testPushST() {

	ThreadPush_T threadPush(stack, this, BaseTest<ParaType>::NELEMENT, BaseTest<ParaType>::NOPERATION);
	threadPush.run();
	CPPUNIT_ASSERT(BaseTest<ParaType>::NOPERATION == stack->size());

	vector<ParaType> outVec;
	while (!stack->empty()) {
		ParaType tmp;
		if(stack->pop(tmp)) {
			outVec.push_back(tmp);
		}
	}

	sort(threadPush.inVec.begin(), threadPush.inVec.end());
	sort(outVec.begin(), outVec.end());

	CPPUNIT_ASSERT(threadPush.inVec == outVec);
}

template<class TestType, class ParaType, char const* CLASS_NAME> void StackTest<
TestType, ParaType, CLASS_NAME>::testPopST() {
	ThreadPush_T threadPush(stack, this, BaseTest<ParaType>::NELEMENT, BaseTest<ParaType>::NOPERATION);
	ThreadPop_T threadPop(stack, this, BaseTest<ParaType>::NELEMENT, BaseTest<ParaType>::NOPERATION);
	threadPush.run();
	CPPUNIT_ASSERT(BaseTest<ParaType>::NOPERATION == stack->size());
	threadPop.run();

	CPPUNIT_ASSERT( 0 == stack->size());

	sort(threadPush.inVec.begin(), threadPush.inVec.end());
	sort(threadPop.outVec.begin(), threadPop.outVec.end());

	CPPUNIT_ASSERT(threadPush.inVec == threadPop.outVec);
}

template<class TestType, class ParaType, char const* CLASS_NAME> void StackTest<
TestType, ParaType, CLASS_NAME>::testPushMT() {
	ThreadPushFactory<TestType,ParaType, CLASS_NAME> factory(stack, this);
	ThreadRunner::runThreads(&factory, CLASS_NAME, "testPushMT");
}

template<class TestType, class ParaType, char const* CLASS_NAME> void StackTest<
TestType, ParaType, CLASS_NAME>::testPushPopPairMT() {
	ThreadPushPopPairFactory<TestType,ParaType, CLASS_NAME> factory(stack, this);
	ThreadRunner::runThreads(&factory, CLASS_NAME, "testPushPopPairMT");
}

template<class TestType, class ParaType, char const* CLASS_NAME> void StackTest<
TestType, ParaType, CLASS_NAME>::testPopMT() {
	ThreadPopFactory<TestType, ParaType, CLASS_NAME> factory(stack, this);
	ThreadRunner::runThreads(&factory, CLASS_NAME, "testPopMT");
}
}
#endif /*TEST_STACK_H_*/

/*
 * (c) Copyright 2008, IBM Corporation.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  yy-mm-dd  Developer  Defect     Description
 *  --------  ---------  ------     -----------
 *  08-11-01  ganzhi    N/A        Initial implementation
 */
#ifndef TEST_ATOMIC_H_
#define TEST_ATOMIC_H_

#include <cstdlib>
#include <iostream>
#include <cppunit/extensions/HelperMacros.h>
#include <cppunit/TestAssert.h>
#include "baseTest.h"
#include <amino/cstdatomic>
#include <ctime>

using namespace std;

/**
 * @brief this test case tests general functionality for all atomic types.
 */
template<class ParaType> 
class AtomicTest : public CppUnit::TestFixture{
    CPPUNIT_TEST_SUITE(AtomicTest);
    CPPUNIT_TEST(testAcqRel);
    CPPUNIT_TEST(testStore);
    CPPUNIT_TEST_SUITE_END();

public:
    AtomicTest() {
    }

    void setUp() {
    }

    void reset() {
    }

    void tearDown() {
    }

    /**
     * @brief A producer/consumer sample is used here to check if acquire and
     * release barrier are correctly implemented
     *
     * Create a thread to load with acquire barrier, and create another
     * thread to store with release, and check if there is incorrect reordering
     * occured.
     *
     * */
    void testAcqRel();
    void testStore();
};

class IntProducer{
    private:
        atomic<bool> * _flag;
        volatile int * _product;
    public:
        IntProducer(atomic<bool> & flag, volatile int & product){
            _flag = &flag;
            _product = &product;
        }

        void operator()(){
            int opCount = TestConfig::getInstance()->getOperationNum();
            for(int i=0;i<opCount;i++){
                // wait unti _flag becomes FALSE
                while(_flag->load(memory_order_acquire))
                {
                    //cout<<"Producer: _flag is still true "<< i<< "\n";
                }

                (*_product) = i;

                //cout<<"Producer: changing flag to true\n";
                _flag->store(true, memory_order_release);
            }
        }
};

class IntConsumer{
    private:
        atomic<bool> * _flag;
        volatile int * _product;
    public:
        volatile bool error;

        IntConsumer(atomic<bool> & flag, volatile int & product){
            _flag = &flag;
            _product = &product;

            error = false;
        }

        void operator()(){
            int opCount = TestConfig::getInstance()->getOperationNum();

            for(int i=0;i<opCount;i++){
                // wait until product is ready
                while(!_flag->load(memory_order_acquire))
                {
                    //cout<<"Consumer: _flag is still false "<< i<< "\n";
                }

                if(*_product != i)
                    error = true;

                //cout<<"Consumer: changing flag to false\n";
                _flag->store(false, memory_order_release);
            } 
        }
};

template<typename ParaType> 
void AtomicTest<ParaType>::testAcqRel() {
    /* 
     * false: slot is empty, so producer can create product
     * true: slot is full, so consumer can consume product
     */
    atomic<bool> flag;
    flag = false;
    /* the product */
    volatile int product;
    product = 0;

    IntProducer producer(flag, product);
    IntConsumer consumer(flag, product);

    Thread thread1(producer);
    Thread thread2(consumer);

    thread1.join();
    thread2.join();

    CPPUNIT_ASSERT(!consumer.error);
}

template<typename ParaType> 
void AtomicTest<ParaType>::testStore() {
    atomic<int> sink;
    int tmp;
    int opCount = TestConfig::getInstance()->getOperationNum();
    for(int i=0;i<opCount;i++){
        memory_order mo;
        switch(i%5){
            case 0:
                mo = memory_order_relaxed;
                break;
            case 1:
                mo = memory_order_acquire;
                break;
            case 2:
                mo = memory_order_release;
                break;
            case 3:
                mo = memory_order_acq_rel;
                break;
            case 4:
                mo = memory_order_seq_cst;
                break;
        }
        tmp=rand();
        int tmp1 = tmp;
        sink.store(tmp, mo);
        CPPUNIT_ASSERT(tmp == tmp1);
        CPPUNIT_ASSERT(tmp == sink.load(mo));
    }
}

/**
 * @brief this test case covers mathematic operation of atomic numbers types
 */
template<class ParaType, char const* CLASS_NAME> 
class AtomicNumTest:public CppUnit::TestFixture{
    CPPUNIT_TEST_SUITE(AtomicNumTest);
    CPPUNIT_TEST(testPlusSub);
    CPPUNIT_TEST_SUITE_END();

    private:
        atomic<ParaType> fVar;
    public:
    AtomicNumTest() {
    }

    atomic<ParaType> * getAtomic(){
        return &fVar;
    }

    void setUp() {
        fVar = 0;
    }

    void reset() {
    }

    void tearDown() {
    }

    void testPlusSub();
};

template<class ParaType, char const* CLASS_NAME> 
class ThreadMath : public TestThread<ParaType> {
private:
	atomic<ParaType> * atomic_var;
	AtomicNumTest<ParaType, CLASS_NAME> * test;
	int operationNum;
public:

	ThreadMath(atomic<ParaType> * var, AtomicNumTest<ParaType, CLASS_NAME> * test_case,
			int nOperation) :
	atomic_var(var), test(test_case), operationNum(nOperation) {
	}

    /**
     * @brief this method will try to add and sub the same number from the
     * atomic variable.  so the final result will be no change at all.
     */
	void* run() {
		for (int i = 0; i< operationNum; ++i) {
            if(i%2 == 0){
                (*atomic_var)++;
                ParaType oldV = atomic_var->load();
                do{
                    //cout<<"CMP_SWP "<<i<<endl;
                }while(!atomic_var->compare_swap(oldV, oldV+1)); 
            }
            else{
                (*atomic_var)--;
                ParaType oldV = atomic_var->load();
                do{
                    //cout<<"CMP_SWP "<<i<<endl;
                }while(!atomic_var->compare_swap(oldV, oldV-1)); 
            }
		}
		return NULL;
	}
};

template<class ParaType, char const* CLASS_NAME> class MathThreadFactory :
public ThreadFactory<ParaType> {
private:
	atomic<ParaType> * atomic_var;
	AtomicNumTest<ParaType, CLASS_NAME> * test;
public:

	MathThreadFactory(atomic<ParaType> * s, AtomicNumTest<ParaType, CLASS_NAME> * test_case) :
        atomic_var(s), test(test_case) 
    {
    }

	virtual Runnable** createThreads(int threadNum, int elementNum, int operationNum) {
		test->reset();
		atomic_var = test->getAtomic();
		this->inVec.clear();
		this->outVec.clear();

		typedef Runnable* pRunnable;
		pRunnable* threads;
		threads = new pRunnable[threadNum];
		for (int i=0; i<threadNum; i++) {
			threads[i] = new ThreadMath<ParaType, CLASS_NAME>(atomic_var, test, operationNum);
		}
		return threads;
	}

	virtual void verifyResult(int threadNum, int elementNum) {
		CPPUNIT_ASSERT(atomic_var->load() == 0);
	}
};

template<class ParaType, char const* CLASS_NAME> 
void AtomicNumTest<ParaType, CLASS_NAME>::testPlusSub() {
	MathThreadFactory<ParaType, CLASS_NAME> factory(&fVar, this);
	ThreadRunner::runThreads(&factory, CLASS_NAME, "testPlusSub");
}

/**
 * @brief this test case covers mathematic operation of atomic numbers types
 */
template<char const* CLASS_NAME>
class AtomicBoolTest : public CppUnit::TestFixture{
    CPPUNIT_TEST_SUITE(AtomicBoolTest);
    CPPUNIT_TEST(testBoolOp);
    CPPUNIT_TEST(testBoolOpMt);
    CPPUNIT_TEST_SUITE_END();

    private:
        atomic<bool> fVar;
    public:
    AtomicBoolTest() {
    }

    atomic<bool> * getAtomic(){
        return &fVar;
    }

    void setUp() {
        fVar = false;
    }

    void reset() {
        fVar = false; 
    }

    void tearDown() {
    }

    void testBoolOp();

    void testBoolOpMt();
};

template<char const* CLASS_NAME>
void AtomicBoolTest<CLASS_NAME>::testBoolOp()
{
    atomic<bool>* pb = getAtomic();
    bool tmp = pb->load();
    CPPUNIT_ASSERT(!tmp);
    pb->store(true);
    tmp = pb->load();
    CPPUNIT_ASSERT(tmp);
   
    bool tmp1 = false;
    while(!(pb->compare_swap(tmp, tmp1)));
    tmp = pb->load();
    CPPUNIT_ASSERT(!tmp);
}

template<char const* CLASS_NAME> 
class ThreadBool : public TestThread<bool> {
private:
	atomic<bool> * atomic_var;
	AtomicBoolTest<CLASS_NAME> * test;
	int operationNum;
	int randNum;
public:

	ThreadBool(atomic<bool> * var, AtomicBoolTest<CLASS_NAME> * test_case,
			int nOperation, int randN) :
	atomic_var(var), test(test_case), operationNum(nOperation), randNum(randN)
	{
	}
        
       /**
       *@brief this method will test the compare_swap function in multi thread way.
       */
	void* run() {
	    int opNum = operationNum + randNum;
	    
	    for(int i=0;i<opNum;i++){
                while( true)
		{
	            bool tmp = atomic_var->load();
	            if (atomic_var->compare_swap(tmp,!tmp))
			break;
		}
            } 

	    return NULL;
	}
};

template<char const* CLASS_NAME> class BoolThreadFactory :
public ThreadFactory<bool>{
private:
	atomic<bool> * atomic_var;
	AtomicBoolTest<CLASS_NAME> * test;
	int total;
public:

	BoolThreadFactory(atomic<bool> * s, AtomicBoolTest<CLASS_NAME> * test_case) :
        atomic_var(s), test(test_case), total(0)
    {
    }

	virtual Runnable** createThreads(int threadNum, int elementNum, int operationNum) {
		test->reset();
		atomic_var = test->getAtomic();
		this->inVec.clear();
		this->outVec.clear();

		typedef Runnable* pRunnable;
		pRunnable* threads;
		threads = new pRunnable[threadNum];
                srand(time(NULL));
		for (int i=0; i<threadNum; i++) {
	            int  randNum = rand()%10;
		    threads[i] = new ThreadBool<CLASS_NAME>(atomic_var, test, operationNum, randNum);
		    total += randNum;	
		}
		
		return threads;
	}
	
	virtual void verifyResult(int threadNum, int elementNum) {
		if( (total%2 == 0) )
                {
		    CPPUNIT_ASSERT(!atomic_var->load());
		}
		else 
		{
		    CPPUNIT_ASSERT(atomic_var->load());
		}
	       
		// reset the atomi_var's value
	        total = 0;
	}
};

template<char const* CLASS_NAME>
void AtomicBoolTest<CLASS_NAME>::testBoolOpMt() {
	BoolThreadFactory<CLASS_NAME > factory(&fVar, this);
	ThreadRunner::runThreads(&factory, CLASS_NAME, "testBoolOpMt");
}

#endif /*TEST_ATOMIC_H_*/

/*
 * (c) Copyright 2008, IBM Corporation.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
#ifndef TEST_SMR_H_
#define TEST_SMR_H_

#include <cppunit/extensions/HelperMacros.h>
#include <cppunit/TestAssert.h>
#include <amino/smr.h>
#include "baseTest.h"
#include <iostream>

namespace test {
    using namespace internal;
template<typename T, int K> class ThreadEmploy;
template<typename T, int K> class ThreadDelete;

template<typename T, int K>
class SMRTest: public CppUnit::TestFixture, public BaseTest<T> {
    CPPUNIT_TEST_SUITE(SMRTest);
    CPPUNIT_TEST(testMT);
	CPPUNIT_TEST_SUITE_END();

friend class ThreadEmploy<T,K>;
friend class ThreadDelete<T,K>;

private:
    SMR<T, K> * mm;

public:
    int* data[K];   

public:
	SMRTest() {
        mm = getSMR<T, K>();
        int i = 0;

        for (; i<K; i++)
        {
            data[i] = new int(i);
        }
	}

	void setUp() {    

	}

	void reset() {
	}

	void tearDown() {
	}

	void testMT();
};

//This thread will employ the data itmes in data array.
template<typename T, int K> class ThreadEmploy :
    public TestThread<T> {
private:
    int elementNum;
	int operationNum;
    SMRTest<T,K>* pst;
public:

    ThreadEmploy(int nElement, int nOperation, int threadId = 0, SMRTest<T,K>* pmst = NULL) :
	TestThread<T>(threadId), elementNum(nElement),operationNum(nOperation),pst(pmst) 
    {
	}

	void* run() {
        typename SMR<T, K>::HP_Rec * hp = pst->mm->getHPRec();

        Logger log(stderr);
        log.log(	"INFO: class:	%s\tThread: 1 \t nElement:\t%d\ttestName:\t%s\t\n",
					"SMRTest", this->elementNum,
					"test SMR");
        int i = 0;

        for (; i<3; i++)
        {
            pst->mm->employ(hp, i, pst->data[i]);

            //print the data pointed by the HP
            std::cout<<*(hp->hp[i])<<std::endl;
        }

        //sleep 5 seconds
        sleep(5);

        return NULL;
	}
};

//These threads will employ the data itmes in data array.
template<typename T, int K> class ThreadDelete :
public TestThread<T> {
private:
	int elementNum;
	int operationNum;
    SMRTest<T,K>* pst;
public:

	ThreadDelete(int nElement, int nOperation, int threadId = 0, SMRTest<T,K>* pmst = NULL) :
	TestThread<T>(threadId), elementNum(nElement),operationNum(nOperation), pst(pmst)
    {
	}

	void* run() {
	    int i = 0;
        typename SMR<T, K>::HP_Rec * hp = pst->mm->getHPRec();
         
        Logger log(stderr);
        log.log(	"INFO: class:	%s\tThread: 2 \t nElement:\t%d\ttestName:\t%s\t\n",
					"SMRTest", this->elementNum,
					"test SMR");

        for (; i<3; i++)
        {
            std::cout<< *(hp->hp[i])<<std::endl;
            pst->mm->retire(hp, i);
            pst->mm->delNode(hp, *(pst->data+i));
            //std::cout<< *(hp->hp[i])<<std::endl;
        }
        
        return NULL;
	}
};

#define ThreadEmploy_T ThreadEmploy<T,K>
#define ThreadDelete_T ThreadDelete<T,K>

// Start the test case here. Start number of threads for testing.
template<typename T, int K> 
void SMRTest<T,K>::testMT() 
{
    ThreadEmploy_T threadEmploy(BaseTest<T>::NELEMENT, BaseTest<T>::NOPERATION, 0,this);
	threadEmploy.run();

    ThreadDelete_T threadDelete(BaseTest<T>::NELEMENT, BaseTest<T>::NOPERATION, 0,this);
	threadDelete.run();
}

}
#endif /*TEST_SMR_H_*/

/*
 * (c) Copyright 2008, IBM Corporation.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
#include <cstdlib>
#include <iostream>
#include <string.h>

#include "testconfig.h"

using namespace std;
namespace test {

TestConfig TestConfig::tc;

TestConfig::TestConfig() {
	char * element = std::getenv("AMINO_ELEMENTS");
	char * threads = std::getenv("AMINO_THREADS");
	char * operation = std::getenv("AMINO_OPERATION_COUNT");

	if (element == NULL)
		elementNum = 256;
	else {
		elementNum = atoi(element);
		if (elementNum <= 0)
			elementNum = 256;
	}

	if (operation == NULL)
		operationNum = 256;
	else {
		operationNum = atoi(operation);
		if (operationNum <= 0)
			operationNum = 256;
	}

	if (threads == NULL) {
		//warm up
		threadNum.push_back(8);
		for (int i = 1; i < 9; i++)
			threadNum.push_back(i);
	} else {
		char * token = strtok(threads, ",");
		while (token != NULL) {
			threadNum.push_back(atoi(token));
			token = strtok(NULL, ",");
		}
	}
}

const TestConfig * TestConfig::getInstance() {
	return &tc;
}

int TestConfig::getElementNum() const {
	return elementNum;
}

int TestConfig::getOperationNum() const {
	return operationNum;
}
const std::vector<int>& TestConfig::getThreadNum() const {
	return threadNum;
}
}

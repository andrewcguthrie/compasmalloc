/*
 * (c) Copyright 2008, IBM Corporation.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
#include "threadRunner.h"
#include "threadFactory.h"
namespace test {

void ThreadRunner::runThreads(Runnable** runnable, int len, const char* testName) {
	Thread *thread = new Thread[len];

	for (int i = 0; i < len; ++i) {
		thread[i].setRunner(*(runnable + i));
	}

	struct timeval t1, t2;

	for (int i = 0; i < len; ++i) {
		thread[i].start();
	}
	gettimeofday(&t1, NULL);
	for (int i = 0; i < len; ++i)
		thread[i].join();

	gettimeofday(&t2, NULL);

	int takes = (t2.tv_sec - t1.tv_sec)* 1000000 + t2.tv_usec - t1.tv_usec;

	log.log(
			"INFO: class:	%s\tnThread:\t%d\tnOperation:\t%d\ttestName:\t%s\tTakes:\t%d\tmicroseconds\n",
			tclass, nthread, nOperation, testName, takes);
	delete[] thread;
}
}

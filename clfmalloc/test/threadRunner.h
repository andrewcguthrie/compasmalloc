/*
 * (c) Copyright 2008, IBM Corporation.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
#ifndef AMINO_UTIL_THREADRUNNER_H
#define AMINO_UTIL_THREADRUNNER_H

#include <sys/time.h>
#include <stdio.h>
#include <stdarg.h>
#include <amino/thread.h>
#include "testconfig.h"
#include "threadFactory.h"

namespace test {
class Logger {
private:
	FILE *target;
public:
	Logger(FILE *file) :
		target(file) {
	}

	void log(const char* fmt, ...) {
		va_list ap;
		va_start(ap,fmt);
		vfprintf(target, fmt, ap);
		va_end(ap);
	}
};

class ConcurrentRunner {
public:
	virtual void setClassName(const char* name) = 0;
        virtual ~ConcurrentRunner(){};
};

class ThreadRunner: public ConcurrentRunner {
public:
	ThreadRunner(int tn = 8, int on = 100, const char* c = NULL, FILE *logp =
			stderr) :
		nthread(tn), nOperation(on), tclass(c), log(logp) {
	}

	template<typename ParaType> static void runThreads(
			ThreadFactory<ParaType> * tf, const char * className,
			const char * testName, FILE * logp = stderr);

	void setClassName(const char* name) {
		tclass = name;
	}
private:
	int nthread;
	int nOperation;
	const char *tclass;
	Logger log;

public:
	void runThreads(Runnable** runnable, int len, const char* testName);
};

template<typename ParaType> void ThreadRunner::runThreads(ThreadFactory<
		ParaType> * tf, const char * className, const char * testName,
		FILE * logp) {
	const TestConfig * tc = TestConfig::getInstance();
	int elementNum = tc->getElementNum();
	int operationNum = tc->getOperationNum();
	vector<int> threadNum = tc->getThreadNum();

	for (vector<int>::iterator i = threadNum.begin(); i < threadNum.end(); i++) {
		int curThr = *i;
		Runnable ** threads = tf->createThreads(curThr, elementNum, operationNum);
		ThreadRunner * runner =
				new ThreadRunner(curThr, operationNum, className, logp);
		runner->runThreads(threads, curThr, testName);
		tf -> collection(threads, curThr);
		tf -> deleteThreads(threads, curThr);
		tf -> verifyResult(curThr, elementNum);
		delete runner;
	}
}

}
#endif

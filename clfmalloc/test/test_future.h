/*
 * (c) Copyright 2008, IBM Corporation.
 *
 *      Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  Change History:
 *
 *  yy-mm-dd  Developer  Defect     Description
 *  --------  ---------  ------     -----------
 *  08-08-18  ganzhi     N/A        Initial implementation
 */

#ifndef TEST_FUTURE_H
#define TEST_FUTURE_H

#include <cppunit/extensions/HelperMacros.h>
#include <cppunit/TestAssert.h>
#include "baseTest.h"

#include <amino/cstdatomic>
#include <amino/thread.h>
#include <amino/future.h>
#include <amino/ftask.h>

#include <iostream>
#include <assert.h>
#include <unistd.h>

namespace test{
    using namespace amino;

    const int ACCU_COUNT=1000;

    class Accumulate:public Runnable{
        private:
            atomic<int> * count;
        public:
            Accumulate(atomic<int> * arg){
                count = arg;
            }

            void* run(){
                for(int i=0;i<ACCU_COUNT;i++)
                    (*count)++;
                return NULL;
            }
    };

    template<typename Executor>
        class FutureTest :
            public CppUnit::TestFixture, public BaseTest<int> {
                CPPUNIT_TEST_SUITE(FutureTest);
                CPPUNIT_TEST(testAccumulateMassive);
                CPPUNIT_TEST(testAccumulateMassive_another);
                CPPUNIT_TEST_SUITE_END();

                public:
                FutureTest() {
                }

                void setUp() {
                }

                void reset() {
                }

                void tearDown() {
                }

                void testAccumulateMassive(){
                    const int TASK_COUNT=200;
                    FutureTask ** futures= new FutureTask*[TASK_COUNT];
                    Executor executor;
                    atomic<int> sum;
                    sum = 0;

                    Accumulate * ppAcc[TASK_COUNT];
                    for(int i=0;i<TASK_COUNT;i++){
                        ppAcc[i] = new Accumulate(&sum);
                        futures[i] = new FutureTask(ppAcc[i]);
                    }

                    for(int i=0;i<TASK_COUNT;i++){
                        executor.execute(futures[i]);
                    }

                    for(int i=0;i<TASK_COUNT;i++){
                        futures[i]->get();
                    }


                    for(int i=0;i<TASK_COUNT;i++){
                        executor.execute(futures[i]);
                    }

                    executor.shutdown();
                    executor.waitTermination();

                    CPPUNIT_ASSERT(sum.load()==2*ACCU_COUNT*TASK_COUNT);
                    for(int i=0;i<TASK_COUNT;i++){
                        delete ppAcc[i];
                        delete futures[i];
                    }
                    delete [] futures;
                }

                void testAccumulateMassive_another(){
                    const int TASK_COUNT=200;
                    FutureTask ** futures= new FutureTask*[TASK_COUNT];
                    Executor executor;
                    atomic<int> sum;
                    sum = 0;

                    Accumulate * ppAcc[TASK_COUNT];
                    for(int i=0;i<TASK_COUNT;i++){
                        ppAcc[i] = new Accumulate(&sum);
                        futures[i] = new FutureTask(ppAcc[i]);
                    }

                    for(int i=0;i<TASK_COUNT;i++){
                        executor.execute(futures[i]);
                    }

                    for(int i=0;i<TASK_COUNT;i++){
                        futures[i]->get(2000);
                    }


                    for(int i=0;i<TASK_COUNT;i++){
                        executor.execute(futures[i]);
                    }

                    executor.shutdown();
                    executor.waitTermination();

                    CPPUNIT_ASSERT(sum.load()==2*ACCU_COUNT*TASK_COUNT);
                    for(int i=0;i<TASK_COUNT;i++){
                        delete ppAcc[i];
                        delete futures[i];
                    }
                    delete [] futures;
                }
            };
}
#endif /*TEST_EXECUTOR_H_*/

/*
 * (c) Copyright 2008, IBM Corporation.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
#ifndef TEST_AASORT_H_
#define TEST_AASORT_H_
#include <cppunit/extensions/HelperMacros.h>
#include <cppunit/TestAssert.h>
#include "baseTest.h"
#include <amino/cstdatomic>
#include <iostream>

using namespace amino;

namespace test{

    class AASortTest :
        public CppUnit::TestFixture, public BaseTest<int> {
            CPPUNIT_TEST_SUITE(AASortTest);
            CPPUNIT_TEST(testAASort);
            CPPUNIT_TEST_SUITE_END();

            public:
            AASortTest() {
            }

            void setUp() {
            }

            void reset() {
            }

            void tearDown() {
            }

            void testAASort();
        };
}
#endif /*TEST_ATOMIC_H_*/

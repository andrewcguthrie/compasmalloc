/*
 * (c) Copyright 2008, IBM Corporation.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
#ifndef TEST_LIST_H_
#define TEST_LIST_H_
#include <cppunit/extensions/HelperMacros.h>
#include <cppunit/TestAssert.h>
#include "baseTest.h"

namespace test {

using namespace amino;

template<typename ListType, typename ParaType, char const* CLASS_NAME> 
    class ListTest: public CppUnit::TestFixture, public BaseTest<ParaType> {
    CPPUNIT_TEST_SUITE(ListTest);	
    CPPUNIT_TEST(testEmptyST);
	CPPUNIT_TEST(testInsertST);
	CPPUNIT_TEST(testPush_frontST);
	CPPUNIT_TEST(testRemoveST);
	CPPUNIT_TEST(testInsertMT);
	CPPUNIT_TEST(testPush_frontMT);
	CPPUNIT_TEST(testRemoveMT);
	CPPUNIT_TEST(testInsertRemoveMT);
	CPPUNIT_TEST_SUITE_END();
private:
	ListType *list;

public:
	ListTest() {
	}

	ListType * getList() {
		return list;
	}

	void setUp() {
		list = new ListType();
	}

	void reset() {
		delete list;
		list = new ListType();
	}

	void tearDown() {
		delete list;
	}

	void testEmptyST();
	void testInsertST();
	void testPush_frontST();
	void testRemoveST();
	void testInsertMT();
	void testPush_frontMT();
	void testRemoveMT();
	void testInsertRemoveMT();
};

#define ThreadInsert_T ThreadInsert<ListType, ParaType, CLASS_NAME>
#define ThreadRemove_T ThreadRemove<ListType, ParaType, CLASS_NAME>
#define ThreadPush_front_T ThreadPush_front<ListType, ParaType, CLASS_NAME>

template<typename ListType, typename ParaType, char const* CLASS_NAME> 
class ThreadInsert :
public TestThread<ParaType> {
private:
	ListType *list;
	ListTest<ListType, ParaType, CLASS_NAME> *testcase;
	int elementNum;
	int operationNum;

public:

	ThreadInsert(ListType *l, ListTest<ListType, ParaType, CLASS_NAME>* lt,
			int nElement,int nOperation, int threadId = 0) :
	TestThread<ParaType>(threadId), list(l), testcase(lt), elementNum(nElement),operationNum(nOperation)
    {
	}

	void* run() {
		for (int i = 0; i< operationNum; ++i) {
				bool ret = list->insert(0, testcase->data[i + ((this->threadId) * elementNum)]);
				if (ret) {
                    /*REVIEW: This is over simplified. We should test insertion
                     * in the middle of list, and begin of list */
					this->inVec.push_back(testcase->data[i + ((this->threadId) * elementNum)]);
				}
		}
		return NULL;
	}
};

template<typename ListType, typename ParaType, char const* CLASS_NAME> class ThreadPush_front :
public TestThread<ParaType> {
private:
	ListType *list;
	ListTest<ListType, ParaType, CLASS_NAME> *testcase;
	int elementNum;
	int operationNum;

public:
	ThreadPush_front(ListType *l, ListTest<ListType, ParaType, CLASS_NAME>* lt,
			int nElement, int nOperation, int threadId = 0) :
        TestThread<ParaType>(threadId), list(l), testcase(lt), elementNum(nElement),operationNum(nOperation) {
	}

	void *run() {
		for (int i = 0; i < operationNum; ++i) {
			if (list->push_front(testcase->data[i + ((this->threadId) * elementNum)]))
			this->inVec.push_back(testcase->data[i + ((this->threadId) * elementNum)]);
		}
		return NULL;
	}
};

template<typename ListType, typename ParaType, char const* CLASS_NAME> class ThreadRemove :
public TestThread<ParaType> {
private:
	ListType *list;
	ListTest<ListType, ParaType, CLASS_NAME> *testcase;
	int elementNum;
	int operationNum;

public:
	ThreadRemove(ListType *l, ListTest<ListType, ParaType, CLASS_NAME>* lt,
			int nElement, int nOperation, int nThread = 1) :
        TestThread<ParaType>(nThread),list(l), testcase(lt), elementNum(nElement), operationNum(nOperation) {
	}

	void *run() {
		for (int i = 0; i < operationNum; ++i) {

			ParaType tmp = testcase->data[i%((this->threadId+1)*elementNum)];
			if (list->remove(tmp)) {
				this->outVec.push_back(tmp);
			}
		}

		return NULL;
	}
};

template<typename ListType, typename ParaType, char const* CLASS_NAME> 
class ThreadInsertFactory : public ThreadFactory<ParaType> {
private:
	ListType *list;
	ListTest<ListType, ParaType, CLASS_NAME> *testcase;
public:
	ThreadInsertFactory(ListType * l,
            ListTest<ListType, ParaType, CLASS_NAME> * lt) :
        list(l), testcase(lt) {
	}

	virtual Runnable ** createThreads(int threadNum, int elementNum, int operationNum) {
		testcase->reset();
		list = testcase->getList();
		this->inVec.clear();
		this->outVec.clear();

		Runnable ** threads;
		threads = new Runnable*[threadNum];
		for (int i = 0; i < threadNum; ++i)
            threads[i] = new ThreadInsert<ListType, ParaType, CLASS_NAME>(list, 
                    testcase, elementNum, operationNum, i);

		return threads;
	}

	virtual void verifyResult(int threadNum, int elementNum) {
        // REVIEW, need to explain why this is necessary
		ParaType tmp;
		while (!list->empty()) {
			list->front(tmp);
			this->outVec.push_back(tmp);
			list->remove(tmp);
		}

		ThreadFactory<ParaType>::verifyResult(threadNum, elementNum);
	}
};

template<typename ListType, typename ParaType, char const* CLASS_NAME> class ThreadPush_frontFactory :
public ThreadFactory<ParaType> {
private:
	ListType *list;
	ListTest<ListType, ParaType, CLASS_NAME> *testcase;
public:
	ThreadPush_frontFactory(ListType * l,
			ListTest<ListType, ParaType, CLASS_NAME> * lt) :
	list(l), testcase(lt) {
	}

	virtual Runnable ** createThreads(int threadNum, int elementNum, int operationNum) {
		testcase->reset();
		list = testcase->getList();
		this->inVec.clear();
		this->outVec.clear();

		Runnable ** threads;
		threads = new Runnable*[threadNum];
		for (int i = 0; i < threadNum; ++i)
            threads[i] = new ThreadPush_front<ListType, ParaType, CLASS_NAME>(list, testcase, elementNum, i);

		return threads;
	}

	virtual void verifyResult(int threadNum, int elementNum) {
		ParaType tmp;
		while (!list->empty()) {
			list->front(tmp);
			this->outVec.push_back(tmp);
			list->remove(tmp);
		}
		ThreadFactory<ParaType>::verifyResult(threadNum, elementNum);
	}
};

template<typename ListType, typename ParaType, char const* CLASS_NAME>
class ThreadRemoveFactory :
public ThreadFactory<ParaType> {
private:
	ListType *list;
	ListTest<ListType, ParaType, CLASS_NAME> *testcase;
public:
	ThreadRemoveFactory(ListType * l,
			ListTest<ListType, ParaType, CLASS_NAME> * lt) :
	list(l), testcase(lt) {
	}

	virtual Runnable ** createThreads(int threadNum, int elementNum, int operationNum) {
		testcase->reset();
		list = testcase->getList();
		this->inVec.clear();
		this->outVec.clear();

		for (int i = 0; i < threadNum; ++i) {
			ThreadInsert_T threadInsert(list, testcase, elementNum, i);
			threadInsert.run();

			copy(threadInsert.inVec.begin(), threadInsert.inVec.end(),
					back_inserter(this->inVec));
		}

		Runnable ** threads;
		threads = new Runnable*[threadNum];
		for (int i = 0; i < threadNum; ++i)
		threads[i] = new ThreadRemove<ListType, ParaType, CLASS_NAME>(list, testcase, elementNum,operationNum, i);

		return threads;
	}

	virtual void verifyResult(int threadNum, int elementNum) {
		ParaType tmp;
		while (!list->empty()) {
			list->front(tmp);
			this->outVec.push_back(tmp);
			list->remove(tmp);
		}

		ThreadFactory<ParaType>::verifyResult(threadNum, elementNum);
	}
};
template<typename ListType, typename ParaType, char const* CLASS_NAME> 
class ThreadInsertRemoveFactory : public ThreadFactory<ParaType> {
private:
	ListType *list;
	ListTest<ListType, ParaType, CLASS_NAME> *testcase;
public:
	ThreadInsertRemoveFactory(ListType * l,
			ListTest<ListType, ParaType, CLASS_NAME> * lt) :
	list(l), testcase(lt) {
	}

	virtual Runnable ** createThreads(int threadNum, int elementNum, int operationNum) {
		testcase->reset();
		list = testcase->getList();
		this->inVec.clear();
		this->outVec.clear();

		Runnable ** threads;
		threads = new Runnable*[threadNum];

		for (int i = 0; i < threadNum; ++i) {
			ThreadInsert_T threadInsert(list, testcase, elementNum, i);
			threadInsert.run();

			copy(threadInsert.inVec.begin(), threadInsert.inVec.end(),
					back_inserter(this->inVec));
		}

		for (int i = 0; i < threadNum; i += 2) {
			threads[i] = new ThreadInsert<ListType, ParaType, CLASS_NAME>(list, testcase, 
                    elementNum,operationNum, i);

			if (i+1 < threadNum)
                threads[i+1] = new ThreadRemove<ListType, ParaType, CLASS_NAME>(list, testcase,
                        elementNum,operationNum, i+1);
		}
		return threads;
	}

	virtual void verifyResult(int threadNum, int elementNum) {
		ParaType tmp;
		while (!list->empty()) {
			list->front(tmp);
			this->outVec.push_back(tmp);
			list->remove(tmp);
		}
		ThreadFactory<ParaType>::verifyResult(threadNum, elementNum);
	}
};

template<typename ListType, typename ParaType, char const* CLASS_NAME> void ListTest<
ListType, ParaType, CLASS_NAME>::testEmptyST() {
	CPPUNIT_ASSERT( true == list->empty());
}

template<typename ListType, typename ParaType, char const* CLASS_NAME> void ListTest<
ListType, ParaType, CLASS_NAME>::testInsertST() {
	ThreadInsert_T threadInsert(list, this, BaseTest<ParaType>::NELEMENT, BaseTest<ParaType>::NOPERATION);
	threadInsert.run();

	vector<ParaType> outVec;
	ParaType tmp;
	while (!list->empty()) {
		list->front(tmp);
		outVec.push_back(tmp);
		list->remove(tmp);
	}

	sort(threadInsert.inVec.begin(), threadInsert.inVec.end());
	sort(outVec.begin(), outVec.end());

	CPPUNIT_ASSERT(threadInsert.inVec == outVec);
}

template<typename ListType, typename ParaType, char const* CLASS_NAME> void ListTest<
ListType, ParaType, CLASS_NAME>::testPush_frontST() {
	ThreadPush_front_T threadPush_front(list, this, BaseTest<ParaType>::NELEMENT, 
            BaseTest<ParaType>::NOPERATION);
	threadPush_front.run();

	vector<ParaType> outVec;
	ParaType tmp;
	while (!list->empty()) {
		list->front(tmp);
		outVec.push_back(tmp);
		list->remove(tmp);
	}

	sort(threadPush_front.inVec.begin(), threadPush_front.inVec.end());
	sort(outVec.begin(), outVec.end());

	CPPUNIT_ASSERT(threadPush_front.inVec == outVec);
}

template<typename ListType, typename ParaType, char const* CLASS_NAME> void ListTest<
ListType, ParaType, CLASS_NAME>::testRemoveST() {
	ThreadPush_front_T threadPush_front(list, this, BaseTest<ParaType>::NELEMENT,
            BaseTest<ParaType>::NOPERATION);

	ThreadRemove_T threadRemove(list, this, BaseTest<ParaType>::NELEMENT,
            BaseTest<ParaType>::NOPERATION);

	threadPush_front.run();
	threadRemove.run();

	ParaType tmp;
	while (!list->empty()) {
		list->front(tmp);
		threadRemove.outVec.push_back(tmp);
		list->remove(tmp);
	}

	sort(threadPush_front.inVec.begin(), threadPush_front.inVec.end());
	sort(threadRemove.outVec.begin(), threadRemove.outVec.end());

	CPPUNIT_ASSERT(threadPush_front.inVec == threadRemove.outVec);
}

template<typename ListType, typename ParaType, char const* CLASS_NAME> void ListTest<
ListType, ParaType, CLASS_NAME>::testInsertMT() {
	ThreadInsertFactory<ListType, ParaType, CLASS_NAME> factory(list, this);
	ThreadRunner::runThreads(&factory, CLASS_NAME, "testInsertMT");
}

template<typename ListType, typename ParaType, char const* CLASS_NAME> void ListTest<
ListType, ParaType, CLASS_NAME>::testPush_frontMT() {
	ThreadPush_frontFactory<ListType, ParaType, CLASS_NAME> factory(list, this);
	ThreadRunner::runThreads(&factory, CLASS_NAME, "testPush_frontMT");
}

template<typename ListType, typename ParaType, char const* CLASS_NAME> void ListTest<
ListType, ParaType, CLASS_NAME>::testRemoveMT() {
	ThreadRemoveFactory<ListType, ParaType, CLASS_NAME> factory(list, this);
	ThreadRunner::runThreads(&factory, CLASS_NAME, "testRemoveMT");
}

template<typename ListType, typename ParaType, char const* CLASS_NAME> void ListTest<
ListType, ParaType, CLASS_NAME>::testInsertRemoveMT() {
	ThreadInsertRemoveFactory<ListType, ParaType, CLASS_NAME>
	factory(list, this);
	ThreadRunner::runThreads(&factory, CLASS_NAME, "testInsertRemoveMT");
}

}
#endif /*TEST_LIST_H_*/

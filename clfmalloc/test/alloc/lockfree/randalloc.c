//-----------------------------------------------------------------------------
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <sys/time.h>
//-----------------------------------------------------------------------------
#define ASSERT(x)	assert(x)
/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/
#if defined(X64)

#define CAS32(ret,addr,expval,newval) \
 __asm__ __volatile__( \
    "lock; cmpxchg %2, %1; setz %0;\n" \
    : "=a"(ret), "=m"(*(addr)) \
    : "r" (newval), "a"(expval) \
    : "cc")


char cas64(unsigned long long volatile * addr,
	   unsigned long long oval,
	   unsigned long long nval);

#define CAS64(ret,addr,expval,newval) \
	ret = cas64((unsigned long long volatile *)addr, \
		    (unsigned long long)expval, \
		    (unsigned long long)newval)

#elif defined(X86)

#define CAS32(ret,addr,expval,newval) \
 __asm__ __volatile__( \
    "lock; cmpxchg %2, %1; setz %0;\n" \
    : "=a"(ret), "=m"(*(addr)) \
    : "r" (newval), "a"(expval) \
    : "cc")
#define CAS64(ret,addr,expval,newval) \
 __asm__ __volatile__( \
    "pushl %%ebx;\n" \
    "mov 4+%1,%%ecx;\n" \
    "mov %1,%%ebx;\n" \
    "lock; cmpxchg8b (%3);\n" \
    "popl %%ebx;\n" \
    : "=A" (ret) : "o" (newval), "A" (expval), "r" (addr) \
    : "%ebx", "%ecx", "memory")

#elif defined(SPARC32)

#define CAS32(ret,addr,expval,newval) \
	ret = cas32((unsigned volatile *)addr,(unsigned)expval,(unsigned)newval)
static char cas32(unsigned volatile * addr,unsigned expval,unsigned newval) {
    asm volatile ( \
       "casa  %0 0x80, %2, %1\n" \
       : "=m"(*(addr)), "+r"(newval) \
       : "r" (expval), "r"(addr) \
       );
  return (newval == expval);
}

#else

#error

#endif
/*---------------------------------------------------------------------------*/
#ifdef MODE32
#define LL(ret,addr)			LL32(ret,addr)
#define SC(ret,addr,newval)		SC32(ret,addr,newval)
#define CAS(ret,addr,expval,newval)	CAS32(ret,addr,expval,newval)
#define ptrsize_t			unsigned
#else /* MODE32 */
#define LL(ret,addr)			LL64(ret,addr)
#define SC(ret,addr,newval)		SC64(ret,addr,newval)
#define CAS(ret,addr,expval,newval)	CAS64(ret,addr,expval,newval)
#define ptrsize_t			unsigned long long
#endif /* MODE32 */
//-----------------------------------------------------------------------------
static unsigned seed = 7;
static unsigned objects = 100;
static unsigned min_size = 11;
static unsigned max_size = 14;
static unsigned runtime = 10;
//-----------------------------------------------------------------------------
struct lran2_st { long x, y, v[97];};
struct lran2_st rgen;
static void lran2_init(struct lran2_st* d, long seed) ;
static long lran2(struct lran2_st* d);
//-----------------------------------------------------------------------------
static unsigned range;
static char ** blkp;
typedef struct {
    struct lran2_st rgen;
    volatile unsigned finished;
    unsigned allocs;
    unsigned pad[32];
} thread_data;
static thread_data * perthr;
static unsigned sum_allocs;
static volatile unsigned stop = 0;
//-----------------------------------------------------------------------------
void randalloc_parse_args(int argc,char **argv)
{
    extern char * optarg; 
    int c;

    while((c=getopt(argc,argv,"m:n:t:x:"))!=EOF)
	switch(c){
	    case 'm': min_size = atoi(optarg); break;
	    case 'n': objects = atoi(optarg); break;
	    case 't': runtime = atoi(optarg); break;
	    case 'x': max_size = atoi(optarg); break;
	    default: ASSERT(0);
	}
    if (max_size < min_size)
	max_size = min_size;
    range = max_size - min_size + 1;
}
//-----------------------------------------------------------------------------
void randalloc_print_args()
{
    printf("t%d n%d s%d m%d x%d - ",runtime,objects,seed,min_size,max_size);
    printf("a%d - ",sum_allocs);
}
//-----------------------------------------------------------------------------
void randalloc_init(unsigned procs,unsigned multi)
{
    unsigned i;
    unsigned sz;
    unsigned nthreads;
    char * tmp;

    nthreads = procs*multi;
    perthr = malloc(nthreads*sizeof(thread_data));
    ASSERT(perthr);
    blkp = malloc(objects*sizeof(char *));
    ASSERT(blkp);
//printf("perthr %x  blkp %x  nthreads %x (%x)\n",perthr,blkp,nthreads,&nthreads);
    lran2_init(&rgen,seed);
    for (i=0;i<nthreads;i++) {
//printf("bb %x %x %x\n",i,nthreads,&nthreads);
	lran2_init(&perthr[i].rgen,lran2(&rgen));
//printf("bb %x 2\n",i);
	perthr[i].allocs = 0;
//printf("bb %x 3\n",i);
	perthr[i].finished = 0;
//if (i>10) exit;
    }
//printf("bb\n");

    // initialize
    for (i=0;i<objects;i++) {
	blkp[i] = NULL;
    }
//printf("bb\n");
}
//-----------------------------------------------------------------------------
void randalloc(unsigned procs,unsigned multi,unsigned tid)
{
    unsigned count = 0;
    unsigned nthreads;
    struct timezone tz;
    struct timeval tv;
    unsigned t1,t2;
    unsigned i;
    unsigned sz;
    unsigned victim;
    thread_data * pdea;
    char * oldblk;
    char * newblk;
    ret_t ret;

    nthreads = procs*multi;
    pdea = &perthr[tid];
    
    if (tid==0) {
	gettimeofday(&tv,&tz);
	t1 = tv.tv_sec * 1000000 + tv.tv_usec;
    }
    
//printf("start %x \n",tid);
    while (!stop) {
	for (i=0;i<1000;i++) {
	    victim = lran2(&pdea->rgen)%objects;
	    sz = (1<<(min_size+lran2(&pdea->rgen)%range))-8;
	    newblk = malloc(sz);
	    /* Write something! */
	    newblk[0] = 'a';
	    LWSYNC;
	    while (1) {
		oldblk = blkp[victim];
		CAS(ret,&blkp[victim],oldblk,newblk);
		if (ret)
		    break;
	    }
	    free(oldblk);
	}
	count += 1000;
//printf("%x-%d ",tid,count);
	if (tid==0) {
	    gettimeofday(&tv,&tz);
	    t2 = tv.tv_sec * 1000000 + tv.tv_usec;
	    if ((t2-t1) >= runtime*1000000)
		stop = 1;
	}
    } while (!stop);
    pdea->allocs = count;
    pdea->finished = 1;
    if (tid == 0) {
	sum_allocs = 0;
	for (i=0;i<nthreads;i++) {
	    while (!perthr[i].finished);
	    sum_allocs += perthr[i].allocs;
	}
    }
}
//-----------------------------------------------------------------------------
// =======================================================

/* lran2.h
 * by Wolfram Gloger 1996.
 *
 * A small, portable pseudo-random number generator.
 */

#ifndef _LRAN2_H
#define _LRAN2_H

#define LRAN2_MAX 714025l /* constants for portable */
#define IA	  1366l	  /* random number generator */
#define IC	  150889l /* (see e.g. `Numerical Recipes') */

//struct lran2_st {
//    long x, y, v[97];
//};

static void lran2_init(struct lran2_st* d, long seed)
{
  long x;
  int j;

  x = (IC - seed) % LRAN2_MAX;
  if(x < 0) x = -x;
  for(j=0; j<97; j++) {
    x = (IA*x + IC) % LRAN2_MAX;
    d->v[j] = x;
  }
  d->x = (IA*x + IC) % LRAN2_MAX;
  d->y = d->x;
}

static long lran2(struct lran2_st* d)
{
  int j = (d->y % 97);

  d->y = d->v[j];
  d->x = (IA*d->x + IC) % LRAN2_MAX;
  d->v[j] = d->x;
  return d->y;
}

#undef IA
#undef IC

#endif

//-----------------------------------------------------------------------------
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
//-----------------------------------------------------------------------------
#define ASSERT(x)	assert(x)
//-----------------------------------------------------------------------------
static unsigned iterations = 1;
static unsigned size = 1;
static unsigned extra = 0;
//-----------------------------------------------------------------------------
void linux_scalability_parse_args(int argc,char **argv)
{
    extern char * optarg; 
    int c;

    while((c=getopt(argc,argv,"ei:s:"))!=EOF)
	switch(c){
	    case 'e': extra = 1; break;
	    case 'i': iterations = atoi(optarg); break;
	    case 's': size = atoi(optarg); break;
	    default: ASSERT(0);
	}
}
//-----------------------------------------------------------------------------
void linux_scalability_print_args()
{
    printf("i%d s%d - ",iterations,size);
}
//-----------------------------------------------------------------------------
void linux_scalability(unsigned procs, unsigned multi)
{
    unsigned i;
    void * buf;
    void * a;

    if (extra)
	a = malloc(size);
    for (i=iterations/multi;i;i--) {
	buf = malloc(size);
#ifdef NEVER
	if (i+3 > iterations)
	    printf("%4d scalability: 0x%llx\n",pthread_self(),buf);
#endif
	free(buf);
    }
    if (extra)
	free(a);
}
//-----------------------------------------------------------------------------

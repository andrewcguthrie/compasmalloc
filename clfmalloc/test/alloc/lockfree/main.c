//-----------------------------------------------------------------------------
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <string.h>
#include <sys/time.h>
#include <sys/utsname.h>
#include <pthread.h>
//-----------------------------------------------------------------------------
//#define ASSERT(x)	assert(x)
#define ASSERT(x)
#if defined(MODE64)
#define ptrsize_int_t	unsigned long long
#else
#define ptrsize_int_t	unsigned
#endif
//-----------------------------------------------------------------------------
typedef struct {
    pthread_t pthr;
    unsigned volatile started;
    unsigned volatile done;
    unsigned pad[29];
} thr_st;
static thr_st perthr[64];
//-----------------------------------------------------------------------------
static char name[40];
//-----------------------------------------------------------------------------
static unsigned bench = 7;
static unsigned multi = 1;
static unsigned procs = 1;
static unsigned print_stats = 0;
static unsigned fakethr = 1;
//-----------------------------------------------------------------------------
static void init_functions()
{
    strcpy(name,"mmm"); // the default
#ifdef LIBCMALLOC
    strcpy(name,"lbc");
#endif
#ifdef MMM
    strcpy(name,"mmm");
#endif
#ifdef HOARD
    strcpy(name,"hrd");
#endif
#ifdef PTMALLOC
    strcpy(name,"pt ");
#endif
}
//-----------------------------------------------------------------------------
static void parse_args(int argc,char **argv)
{
    extern char * optarg; 
    int c;

    while((c=getopt(argc,argv,"b:m:p:sf"))!=EOF)
	switch(c){
	    case 'b': bench = atoi(optarg); break;
	    case 'm': multi = atoi(optarg); break;
	    case 'p': procs = atoi(optarg); break;
	    case 's': print_stats = 1; break;
	    case 'f': fakethr = 0; break;
	    default: ASSERT(0);
	}
}
//-----------------------------------------------------------------------------
static void malloc_parse_args(int argc,char **argv)
{
#ifdef MMM
    //mmmalloc_parse_args(argc,argv);
#endif
#ifdef HOARD
    hoard_parse_args(argc,argv);
#endif
}
//-----------------------------------------------------------------------------
static void allocator_init()
{

}
//-----------------------------------------------------------------------------
static void bench_parse_args(int argc,char **argv)
{
    switch (bench) {
	case 1: linux_scalability_parse_args(argc,argv); break;
	case 2: threadtest_parse_args(argc,argv); break;
	case 3: cache_thrash_parse_args(argc,argv); break;
	case 4: cache_scratch_parse_args(argc,argv); break;
	case 5: larson_parse_args(argc,argv); break;
	case 6: work_parse_args(argc,argv); break;
	case 7: randalloc_parse_args(argc,argv); break;
    }
}
//-----------------------------------------------------------------------------
static void bench_print_args()
{
    switch (bench) {
	case 1: linux_scalability_print_args(); break;
	case 2: threadtest_print_args(); break;
	case 3: cache_thrash_print_args(); break;
	case 4: cache_scratch_print_args(); break;
	case 5: larson_print_args(); break;
	case 6: work_print_args(); break;
	case 7: randalloc_print_args(); break;
    }
}
//-----------------------------------------------------------------------------
static void benchmark_init()
{
    switch (bench) {
	case 4: cache_scratch_init(procs,multi); break;
	case 5: larson_init(procs,multi); break;
	case 6: work_init(procs,multi); break;
	case 7: randalloc_init(procs,multi); break;
    }
}
//-----------------------------------------------------------------------------
static void benchmark(unsigned tid)
{
    switch (bench) {
	case 1: linux_scalability(procs,multi); break;
	case 2: threadtest(procs,multi,tid); break;
	case 3: cache_thrash(procs,multi,tid); break;
	case 4: cache_scratch(procs,multi,tid); break;
	case 5: larson(procs,multi,tid); break;
	case 6: work(procs,multi,tid); break;
	case 7: randalloc(procs,multi,tid); break;
    }
}
//-----------------------------------------------------------------------------
static void print_time()
{
    time_t tim;
    char date[40];

    tim = time (0);
    strcpy (date, ctime(&tim));
    date[19] = '\0';
    printf ("%s",&date[11]);
}
//-----------------------------------------------------------------------------
static void parent_start()
{
    unsigned tid;
    unsigned i,j;
    struct timezone tz;
    struct timeval tv;
    unsigned t1,t2;
    struct utsname utsname;
    char *machine_name;

    tid = 0;
#ifdef AIX
    if (bindprocessor(BINDTHREAD,thread_self(),tid%procs) == -1)
        perror("error in bindprocessor"), exit (1);
#endif
//    srandom(tid*tid);
    for (i=1;i<multi*procs;i++)
	while (!perthr[i].started);

    perthr[0].started = 1;
    gettimeofday(&tv,&tz);
    t1 = tv.tv_sec * 1000000 + tv.tv_usec;

    benchmark(0);

    for (i=1;i<multi*procs;i++)
	while (!perthr[i].done);
    gettimeofday(&tv,&tz);
    t2 = tv.tv_sec * 1000000 + tv.tv_usec;
    if (uname (&utsname) < 0)
	machine_name = "(unknown)";
    else
	machine_name = utsname.nodename;
#ifdef NEVER
#endif
    printf("%s ",name);
    printf("%7d ",t2-t1);
    printf("%s  ", machine_name);
    printf("f%d ",fakethr);
    printf("b%d m%d p%d ",bench,multi,procs);
    printf(" - ");
    bench_print_args();
#ifdef MMM
    //mmmalloc_print_args();
#endif
#ifdef HOARD
    //hoard_print_used_mem();
#endif
    print_time();
    printf("\n");
    if (print_stats) {
#ifdef MMM
	//	mmmalloc_print_stats();
#endif
#ifdef PTMALLOC
	malloc_stats();
#endif
    }
    fflush(stdout);
}
//-----------------------------------------------------------------------------
static void * child_start(void * arg)
{
    unsigned tid;

    tid = (ptrsize_int_t) arg;
#ifdef AIX
    if (bindprocessor(BINDTHREAD,thread_self(),tid%procs) == -1)
        perror("error in bindprocessor"), exit (1);
#endif
    srandom(tid*tid);
    perthr[tid].started = 1;
    while (!perthr[0].started);

    benchmark(tid);

    perthr[tid].done = 1;
}
//-----------------------------------------------------------------------------
static void * foo(void * arg)
{
    unsigned i;
    void * a;

    fakethr = 2;
 }
#ifdef NEVER
#endif
//-----------------------------------------------------------------------------
int main(int argc, char **argv)
{
    int i;
    pthread_attr_t attr;
    ptrsize_int_t arg;

    // initialization
    parse_args(argc,argv);
    bench_parse_args(argc,argv);
    init_functions();
    malloc_parse_args(argc,argv);
    allocator_init();
    benchmark_init();

    // create threads
    pthread_attr_init(&attr);
    pthread_setconcurrency(procs);
    pthread_attr_setscope(&attr,PTHREAD_SCOPE_SYSTEM);
    for (i=0;i<multi*procs;i++)
	perthr[i].started = perthr[i].done = 0;
    for (i=1;i<multi*procs;i++) {
	arg = i;
	pthread_create(&perthr[i].pthr,&attr,&child_start,(void *)arg);
    }
#ifdef LIBCMALLOC
    if (fakethr)
	pthread_create(&perthr[multi*procs].pthr,&attr,&foo,(void *)arg);
#endif
    parent_start();
}
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <sys/time.h>
//-----------------------------------------------------------------------------
#define ASSERT(x)	assert(x)
/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/
#if defined(X64)

#define CAS32(ret,addr,expval,newval) \
 __asm__ __volatile__( \
    "lock; cmpxchg %2, %1; setz %0;\n" \
    : "=a"(ret), "=m"(*(addr)) \
    : "r" (newval), "a"(expval) \
    : "cc")


char cas64(unsigned long long volatile * addr,
	   unsigned long long oval,
	   unsigned long long nval);

#define CAS64(ret,addr,expval,newval) \
	ret = cas64((unsigned long long volatile *)addr, \
		    (unsigned long long)expval, \
		    (unsigned long long)newval)

#elif defined(X86)

#define CAS32(ret,addr,expval,newval) \
 __asm__ __volatile__( \
    "lock; cmpxchg %2, %1; setz %0;\n" \
    : "=a"(ret), "=m"(*(addr)) \
    : "r" (newval), "a"(expval) \
    : "cc")
#define CAS64(ret,addr,expval,newval) \
 __asm__ __volatile__( \
    "pushl %%ebx;\n" \
    "mov 4+%1,%%ecx;\n" \
    "mov %1,%%ebx;\n" \
    "lock; cmpxchg8b (%3);\n" \
    "popl %%ebx;\n" \
    : "=A" (ret) : "o" (newval), "A" (expval), "r" (addr) \
    : "%ebx", "%ecx", "memory")

#elif defined(SPARC32)

#define CAS32(ret,addr,expval,newval) \
	ret = cas32((unsigned volatile *)addr,(unsigned)expval,(unsigned)newval)
static char cas32(unsigned volatile * addr,unsigned expval,unsigned newval) {
    asm volatile ( \
       "casa  %0 0x80, %2, %1\n" \
       : "+m"(*(addr)), "+r"(newval) \
       : "r" (expval) \
       );
  return (newval == expval);
}

#else

#error

#endif
/*---------------------------------------------------------------------------*/
#ifdef MODE32
#define LL(ret,addr)			LL32(ret,addr)
#define SC(ret,addr,newval)		SC32(ret,addr,newval)
#define CAS(ret,addr,expval,newval)	CAS32(ret,addr,expval,newval)
#define ptrsize_t			unsigned
#else /* MODE32 */
#define LL(ret,addr)			LL64(ret,addr)
#define SC(ret,addr,newval)		SC64(ret,addr,newval)
#define CAS(ret,addr,expval,newval)	CAS64(ret,addr,expval,newval)
#define ptrsize_t			unsigned long long
#endif /* MODE32 */
/*---------------------------------------------------------------------------*/
#ifdef MODE32
/*---------------------------------------------------------------------------*/
#define LL(ret,addr)			LL32(ret,addr)
#define SC(ret,addr,newval)		SC32(ret,addr,newval)
#define CAS(ret,addr,expval,newval)	CAS32(ret,addr,expval,newval)
#define FAA(oldval,addr,val)		FAA32(oldval,addr,val)
#define FAI(oldval,addr)		FAI32(oldval,addr)
#define ptrsize_int_t			unsigned
/*---------------------------------------------------------------------------*/
#else /* MODE32 */
/*---------------------------------------------------------------------------*/
#define LL(ret,addr)			LL64(ret,addr)
#define SC(ret,addr,newval)		SC64(ret,addr,newval)
#define CAS(ret,addr,expval,newval)	CAS64(ret,addr,expval,newval)
#define FAA(oldval,addr,val)		FAA64(oldval,addr,val)
#define FAI(oldval,addr)		FAI64(oldval,addr)
#define ptrsize_int_t			unsigned long long
/*---------------------------------------------------------------------------*/
#endif /* MODE32 */
/*---------------------------------------------------------------------------*/
#define INC(oldval,addr) { \
    ret_t ret; \
    do { \
	oldval = *(addr); \
	CAS32(ret,addr,oldval,oldval+1); \
    } while (!ret); \
}
//-----------------------------------------------------------------------------
static unsigned min_size = 10;
static unsigned max_size = 20;
static unsigned values = 100;
static unsigned runtime = 10;
static unsigned morework = 0;
//-----------------------------------------------------------------------------
typedef struct job {
    unsigned * list;
    unsigned size;
    unsigned jobid;
}job_t;
typedef struct node {
    struct node * volatile Next;
    job_t * job;
} node_t;
typedef struct hprec {
    node_t * hp1;
    node_t * hp2;
    struct hprec * Next;
    void * rlist;
    unsigned rcount;
} hprec_t;
//-----------------------------------------------------------------------------
struct lran2_st { long x, y, v[97];};
struct lran2_st rgen;
static void lran2_init(struct lran2_st* d, long seed) ;
static long lran2(struct lran2_st* d);
//-----------------------------------------------------------------------------
static hprec_t * HPlist = NULL;
static unsigned * data;
static unsigned * volatile finished;
static unsigned produced;
static unsigned range;
static unsigned seed = 7;
static volatile unsigned stop = 0;
static unsigned population = 1000000;
static unsigned numthreads;
static unsigned pad1[30];
static node_t * volatile Head;
static unsigned pad2[30];
static volatile unsigned consumed = 0;
//-----------------------------------------------------------------------------
void work_parse_args(int argc,char **argv)
{
    extern char * optarg; 
    int c;

    while((c=getopt(argc,argv,"m:t:v:w:x:"))!=EOF)
	switch(c){
	    case 'm': min_size = atoi(optarg); break;
	    case 't': runtime = atoi(optarg); break;
	    case 'v': values = atoi(optarg); break;
	    case 'w': morework = atoi(optarg); break;
	    case 'x': max_size = atoi(optarg); break;
	    default: ASSERT(0);
	}
    if (max_size < min_size)
	max_size = min_size;
    range = max_size - min_size + 1;
}
//-----------------------------------------------------------------------------
void work_print_args()
{
    printf("t%d v%d m%d x%d w%d - ",
	   runtime,values,min_size,max_size,morework);
    printf("p%d j%d - ",produced,consumed);
}
//-----------------------------------------------------------------------------
void work_init(unsigned procs,unsigned multi)
{
    unsigned i;
    unsigned nthreads;
    node_t * dummy;

    nthreads = procs*multi;
    numthreads = nthreads;
    data = malloc(population*sizeof(unsigned));
    ASSERT(data);
    lran2_init(&rgen,seed);
    for (i=0;i<population;i++) {
	data[i] = lran2(&rgen) % values;
    }
    
    finished = malloc(nthreads*sizeof(unsigned));
    ASSERT(finished);
    for (i=0;i<nthreads;i++) {
	finished[i] = 0;
    }

    dummy = malloc(sizeof(node_t));
    ASSERT(dummy);
    dummy->Next = NULL;
    Head = dummy;
}
//-----------------------------------------------------------------------------
static int partition(unsigned first,unsigned last,void ** pl)
{
    int i,j;
    void * item;
    void * tmp;

    item = pl[first];
    i = first;
    j = last;
    while (1) {
	while (pl[i] < item) i++;
	while (pl[j] > item) j--;
	if (i >= j) return j;
	tmp = pl[i]; pl[i] = pl[j]; pl[j] = tmp; i++; j--;
    }
}
//-----------------------------------------------------------------------------
static void quicksort(unsigned first,unsigned last,void ** pl)
{
    int mid;
    
    if (first < last) {
	mid = partition(first,last,pl);
	quicksort(first,mid,pl);
	quicksort(mid+1,last,pl);
    }
}
//-----------------------------------------------------------------------------
static int binary_search(void * key,unsigned num,void ** pl)
{
    unsigned l,r,m;

    l = 1;
    r = num-1;
    while (l <= r) {
	m = (l+r)>>1;
	if (pl[m] == key) return m;
	if (pl[m] > key) r = m-1; else l = m+1;
    }
    return 0;
}
//-----------------------------------------------------------------------------
static void scan(hprec_t * hprec)
{
    int num;
    hprec_t * hptr;
    node_t * node;
    node_t * next;
    void * rlist = NULL;
    unsigned rcount = 0;
    void ** pl;

    ISYNC;
    hprec->hp1 = NULL;
    hprec->hp2 = NULL;
    pl = malloc(2*numthreads*sizeof(void *));
    ASSERT(pl);
    // stage 1
    for (num=1,hptr=HPlist;hptr;hptr=hptr->Next) {
	if (node = hptr->hp1) pl[num++] = node;
	if (node = hptr->hp2) pl[num++] = node;
    }
    quicksort(1,num-1,pl);
    // stage 2
    node = hprec->rlist;
    for (node=hprec->rlist;node;node=next) {
	next = node->Next;
	if (binary_search(node,num,pl)) {
	    node->Next = rlist;
	    rlist = node;
	    rcount++;
	} else {
	    free(node);
	}
    }
    hprec->rlist = rlist;
    hprec->rcount = rcount;
    free(pl);
}
//-----------------------------------------------------------------------------
static void retire_node(hprec_t * hprec,node_t * node)
{
    node->Next = hprec->rlist;
    hprec->rlist = node;
    hprec->rcount++;
    if (hprec->rcount >= 100) {
	scan(hprec);
    }
}
//-----------------------------------------------------------------------------
static void do_job(job_t * job)
{
    unsigned * histo;
    unsigned i;
    volatile int d;
    volatile int f;

    histo = malloc(values*sizeof(unsigned));
    ASSERT(histo);
    for (i=0;i<values;i++)
	histo[i] = 0;
    for (d=0;d<morework;d++) {
	f = i;
	f = f + f;
	f = f * f;
	f = f + f;
	f = f * f;
    }
    for (i=0;i<job->size;i++)
	histo[data[job->list[i]]]++;
    for (i=0;i<values;i++) {
	if (histo[i] >= 7)
	    printf("hit on job %d\n",job->jobid);
    }
    free(histo);
    free(job->list);
    free(job);
}
//-----------------------------------------------------------------------------
static void consume(hprec_t * hprec)
{
    job_t * job;
    node_t * node;
    node_t * next;
    ret_t ret;

    // Get a job
    node = Head;
    hprec->hp1 = node;
    SYNC;
    if (Head != node)
	return;
    ISYNC;
    next = node->Next;
    if (!next)
	return;
    hprec->hp2 = next;
    SYNC;
    if (Head != node)
	return;
    CAS(ret,&Head,node,next);
    if (ret) {
	job = next->job;
	retire_node(hprec,node);
	do_job(job);
	INC(ret,&consumed);
    }
}
//-----------------------------------------------------------------------------
static void producer(unsigned nthreads,hprec_t * hprec)
{
    node_t * Tail;
    unsigned i,j;
    struct timezone tz;
    struct timeval tv;
    unsigned t1,t2;
    job_t * job;
    node_t * node;

    Tail = Head;
    gettimeofday(&tv,&tz);
    t1 = tv.tv_sec * 1000000 + tv.tv_usec;
    for(i=0;!stop;i++) {
	// generate job
	job = malloc(sizeof(job_t));
	ASSERT(job);
	job->size = min_size + lran2(&rgen) % range;
	job->list = malloc(job->size * sizeof(unsigned));
	ASSERT(job->list);
	job->jobid = i;
	for (j=0;j<job->size;j++) {
	    job->list[j] = lran2(&rgen) % population;
	}
	produced++;
	// queue job
	node = malloc(sizeof(node_t));
	ASSERT(node);
	node->job = job;
	node->Next = NULL;
	SYNC;
	Tail->Next = node;
	Tail = node;
	// check if need to help
	while (produced - consumed > 1000) {
	    consume(hprec);
	    gettimeofday(&tv,&tz);
	    t2 = tv.tv_sec * 1000000 + tv.tv_usec;
	    if ((t2-t1) >= runtime*1000000) {
		stop = 1;
	    }
	}
	// check time
	if (i % 1000 == 999) {
	    gettimeofday(&tv,&tz);
	    t2 = tv.tv_sec * 1000000 + tv.tv_usec;
	    if ((t2-t1) >= runtime*1000000) {
		stop = 1;
	    }
	}
    }
    produced = i;
    finished[0] = 1;
    for (i=0;i<nthreads;i++)
	while (!finished[i]);
}
//-----------------------------------------------------------------------------
void work(unsigned procs,unsigned multi,unsigned tid)
{
    hprec_t * hprec;
    hprec_t * head;
    ret_t ret;
    unsigned nthreads;

    nthreads = procs*multi;
    // Get hazard pointer
    hprec = malloc(sizeof(hprec_t));
    ASSERT(hprec);
    hprec->hp1 = NULL;
    hprec->hp2 = NULL;
    hprec->rlist = NULL;
    hprec->rcount = 0;
    do {
	head = HPlist;
	hprec->Next = HPlist;
	LWSYNC;
	CAS(ret,&HPlist,head,hprec);
    } while (!ret);

    
    // producer
    if (tid==0) {
	producer(nthreads,hprec);
	return;
    }
    // consumer
    while (!stop) {
	consume(hprec);
    }
    finished[tid] = 1;
}
//-----------------------------------------------------------------------------
// =======================================================

/* lran2.h
 * by Wolfram Gloger 1996.
 *
 * A small, portable pseudo-random number generator.
 */

#ifndef _LRAN2_H
#define _LRAN2_H

#define LRAN2_MAX 714025l /* constants for portable */
#define IA	  1366l	  /* random number generator */
#define IC	  150889l /* (see e.g. `Numerical Recipes') */

//struct lran2_st {
//    long x, y, v[97];
//};

static void lran2_init(struct lran2_st* d, long seed)
{
  long x;
  int j;

  x = (IC - seed) % LRAN2_MAX;
  if(x < 0) x = -x;
  for(j=0; j<97; j++) {
    x = (IA*x + IC) % LRAN2_MAX;
    d->v[j] = x;
  }
  d->x = (IA*x + IC) % LRAN2_MAX;
  d->y = d->x;
}

static long lran2(struct lran2_st* d)
{
  int j = (d->y % 97);

  d->y = d->v[j];
  d->x = (IA*d->x + IC) % LRAN2_MAX;
  d->v[j] = d->x;
  return d->y;
}

#undef IA
#undef IC

#endif

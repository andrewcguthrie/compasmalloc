#  %W% %I%

# BASE #

prodBASEFILES= \
	active_false$(objsuff) \
	larson$(objsuff) \
	linux-scalability$(objsuff) \
	main$(objsuff) \
	passive_false$(objsuff) \
	randalloc$(objsuff) \
	threadtest$(objsuff) \
	work$(objsuff)

symBASEFILES=$(prodBASEFILES)
debugBASEFILES=$(prodBASEFILES)

###################### x86 files #######################
x86FILES=../../../../src/alloc/lockfree/x86/cas64$(objsuff)

###################### PPC files #######################
ppcFILES=

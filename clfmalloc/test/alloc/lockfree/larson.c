//-----------------------------------------------------------------------------
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <sys/time.h>
//-----------------------------------------------------------------------------
#define ASSERT(x)	assert(x)
//-----------------------------------------------------------------------------
static unsigned seed = 7;
static unsigned objects = 1024;
static unsigned min_size = 16;
static unsigned max_size = 80;
static unsigned runtime = 10;
//-----------------------------------------------------------------------------
struct lran2_st { long x, y, v[97];};
struct lran2_st rgen;
static void lran2_init(struct lran2_st* d, long seed) ;
static long lran2(struct lran2_st* d);
//-----------------------------------------------------------------------------
static unsigned range;
static char ** blkp;
static unsigned * blksz;
typedef struct {
    struct lran2_st rgen;
    volatile unsigned finished;
    unsigned allocs;
    char ** array;
    unsigned * blksz;
    unsigned pad[32];
} thread_data;
static thread_data * perthr;
static unsigned sum_allocs;
static volatile unsigned stop = 0;
//-----------------------------------------------------------------------------
void larson_parse_args(int argc,char **argv)
{
    extern char * optarg; 
    int c;

    while((c=getopt(argc,argv,"m:n:t:x:"))!=EOF)
	switch(c){
	    case 'm': min_size = atoi(optarg); break;
	    case 'n': objects = atoi(optarg); break;
	    case 't': runtime = atoi(optarg); break;
	    case 'x': max_size = atoi(optarg); break;
	    default: ASSERT(0);
	}
    if (max_size < min_size)
	max_size = min_size;
    range = max_size - min_size + 1;
}
//-----------------------------------------------------------------------------
void larson_print_args()
{
    printf("t%d n%d s%d m%d x%d - ",runtime,objects,seed,min_size,max_size);
    printf("a%d - ",sum_allocs);
}
//-----------------------------------------------------------------------------
void larson_init(unsigned procs,unsigned multi)
{
    unsigned i;
    unsigned sz;
    unsigned victim;
    unsigned nthreads;
    char * tmp;

    nthreads = procs*multi;
    perthr = malloc(nthreads*sizeof(thread_data));
    ASSERT(perthr);
    blkp = malloc(nthreads*objects*sizeof(char *));
    ASSERT(blkp);
    blksz = malloc(nthreads*objects*sizeof(unsigned));
    ASSERT(blksz);
    lran2_init(&rgen,seed);
    for (i=0;i<nthreads;i++) {
	lran2_init(&perthr[i].rgen,lran2(&rgen));
	perthr[i].allocs = 0;
	perthr[i].finished = 0;
	perthr[i].array = &blkp[i*objects];
	perthr[i].blksz = &blksz[i*objects];
    }

    // warmup
    for (i=0;i<nthreads*objects;i++) {
	sz = min_size+lran2(&rgen)%range;
	blkp[i] = (char *) malloc(sz);
	ASSERT(blkp[i]);
	blksz[i] = sz;
    }
    
    /* generate a random permutation of the chunks */
    for(i=nthreads*objects;i>0;i--){
	victim = lran2(&rgen)%i;
	tmp = blkp[victim];
	blkp[victim]  = blkp[i-1] ;
	blkp[i-1] = tmp ;
    }

    for(i=0;i<4*nthreads*objects;i++){
	victim = lran2(&rgen)%objects;
	free(blkp[victim]);	
	sz = min_size+lran2(&rgen)%range;
	blkp[victim] = (char *) malloc(sz);
	ASSERT(blkp[victim]);
	blksz[victim] = sz;
    }
}
//-----------------------------------------------------------------------------
void larson(unsigned procs,unsigned multi,unsigned tid)
{
    unsigned count = 0;
    unsigned nthreads;
    struct timezone tz;
    struct timeval tv;
    unsigned t1,t2;
    unsigned i;
    unsigned sz;
    unsigned victim;
    thread_data * pdea;

    nthreads = procs*multi;
    pdea = &perthr[tid];
    
    if (tid==0) {
	gettimeofday(&tv,&tz);
	t1 = tv.tv_sec * 1000000 + tv.tv_usec;
    }
    
    while (!stop) {
	for (i=0;i<1000;i++) {
	    victim = lran2(&pdea->rgen)%objects;
	    free(pdea->array[victim]);
	    sz = min_size+lran2(&pdea->rgen)%range;
	    pdea->array[victim] = (char *) malloc(sz) ;
	    assert(pdea->array[victim]);
	    pdea->blksz[victim] = sz;

	    /* Write something! */
    
	    volatile char * chptr = ((char *) pdea->array[victim]);
	    *chptr++ = 'a';
	    volatile char ch = *((char *) pdea->array[victim]);
	    *chptr = 'b';
	}
	count += 1000;
	if (tid==0) {
	    gettimeofday(&tv,&tz);
	    t2 = tv.tv_sec * 1000000 + tv.tv_usec;
	    if ((t2-t1) >= runtime*1000000)
		stop = 1;
	}
    } while (!stop);
    pdea->allocs = count;
    pdea->finished = 1;
    if (tid == 0) {
	sum_allocs = 0;
	for (i=0;i<nthreads;i++) {
	    while (!perthr[i].finished);
	    sum_allocs += perthr[i].allocs;
	}
    }
}
//-----------------------------------------------------------------------------
// =======================================================

/* lran2.h
 * by Wolfram Gloger 1996.
 *
 * A small, portable pseudo-random number generator.
 */

#ifndef _LRAN2_H
#define _LRAN2_H

#define LRAN2_MAX 714025l /* constants for portable */
#define IA	  1366l	  /* random number generator */
#define IC	  150889l /* (see e.g. `Numerical Recipes') */

//struct lran2_st {
//    long x, y, v[97];
//};

static void lran2_init(struct lran2_st* d, long seed)
{
  long x;
  int j;

  x = (IC - seed) % LRAN2_MAX;
  if(x < 0) x = -x;
  for(j=0; j<97; j++) {
    x = (IA*x + IC) % LRAN2_MAX;
    d->v[j] = x;
  }
  d->x = (IA*x + IC) % LRAN2_MAX;
  d->y = d->x;
}

static long lran2(struct lran2_st* d)
{
  int j = (d->y % 97);

  d->y = d->v[j];
  d->x = (IA*d->x + IC) % LRAN2_MAX;
  d->v[j] = d->x;
  return d->y;
}

#undef IA
#undef IC

#endif

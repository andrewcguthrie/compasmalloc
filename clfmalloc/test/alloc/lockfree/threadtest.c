//-----------------------------------------------------------------------------
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
//-----------------------------------------------------------------------------
#define ASSERT(x)	assert(x)
//-----------------------------------------------------------------------------
typedef struct {
    int x;
    int y;
} foo;
//-----------------------------------------------------------------------------
static unsigned iterations = 1;
static unsigned objects = 1;
static unsigned work = 0;
static unsigned size = 1;
//-----------------------------------------------------------------------------
void threadtest_parse_args(int argc,char **argv)
{
    extern char * optarg; 
    int c;

    while((c=getopt(argc,argv,"i:n:s:w:"))!=EOF)
	switch(c){
	    case 'i': iterations = atoi(optarg); break;
	    case 'n': objects = atoi(optarg); break;
	    case 's': size = atoi(optarg); break;
	    case 'w': work = atoi(optarg); break;
	    default: ASSERT(0);
	}
}
//-----------------------------------------------------------------------------
void threadtest_print_args()
{
    printf("i%d n%d s%d w%d - ",iterations,objects,size,work);
}
//-----------------------------------------------------------------------------
void threadtest(unsigned procs,unsigned multi,unsigned tid)
{
    int i,j;
    foo ** a;
    volatile int d;
    volatile int f;

    objects = objects/multi;
    a = malloc(objects*sizeof(foo *));
    for (j=iterations;j;j--) {
	for (i=0;i<objects;i++) {
	    a[i] = malloc(size*sizeof(foo));
	    for (d=0;d<work;d++) {
		f = i;
		f = f + f;
		f = f * f;
		f = f + f;
		f = f * f;
	    }
	    ASSERT(a[i]);
	}
	for (i=0;i<objects;i++) {
	    free(a[i]);
	    for (d=0;d<work;d++) {
		f = i;
		f = f + f;
		f = f * f;
		f = f + f;
		f = f * f;
	    }
	}
    }
    free(a);
}
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
//-----------------------------------------------------------------------------
#define ASSERT(x)	assert(x)
//-----------------------------------------------------------------------------
static unsigned iterations = 1;
static unsigned repetitions = 1;
static unsigned size = 8;
//-----------------------------------------------------------------------------
void cache_thrash_parse_args(int argc,char **argv)
{
    extern char * optarg; 
    int c;

    while((c=getopt(argc,argv,"i:r:s:"))!=EOF)
	switch(c){
	    case 'i': iterations = atoi(optarg); break;
	    case 'r': repetitions = atoi(optarg); break;
	    case 's': size = atoi(optarg); break;
	    default: ASSERT(0);
	}
}
//-----------------------------------------------------------------------------
void cache_thrash_print_args()
{
    printf("i%d r%d s%d - ",iterations,repetitions,size);
}
//-----------------------------------------------------------------------------
void cache_thrash(unsigned procs,unsigned multi,unsigned tid)
{
    unsigned i,j,k;
    char * obj;
    volatile char ch;
    
    // Repeatedly do the following:
    //   malloc a given-sized object,
    //   repeatedly write on it,
    //   then free it.

    for (i=0;i<(iterations+tid)/multi;i++) {
	// Allocate the object.
	obj = malloc(size);
	// Write into it a bunch of times.
	for (j=0;j<repetitions;j++) {
	    for (k=0;k<size;k++) {
		obj[k] = (char) k;
		ch = obj[k];
		ch++;
	    }
	}
	// Free the object.
	free(obj);
    }
}
//-----------------------------------------------------------------------------

/*
 * (c) Copyright 2008, IBM Corporation.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
#ifndef DEQUETEST_H_
#define DEQUETEST_H_
#include <cppunit/extensions/HelperMacros.h>
#include <cppunit/TestAssert.h>
#include <stdexcept>
#include <sstream>

#include "baseTest.h"

namespace test {
using namespace amino;

template<typename CounterType, typename ParaType, char const* CLASS_NAME>
class CounterTest: public CppUnit::TestFixture, public BaseTest<ParaType> {
CPPUNIT_TEST_SUITE(CounterTest)
		;
		CPPUNIT_TEST(testCounter);
	CPPUNIT_TEST_SUITE_END()
	;

private:
	CounterType *counter;
public:
	CounterTest() {
	}

	CounterType * getCounter() {
		return counter;
	}

	void setUp() {
		counter = new CounterType();
	}

	void reset() {
		delete counter;
		counter = new CounterType();
	}

	void tearDown() {
		delete counter;
	}

	void testCounter();
};

template<typename CounterType, typename ParaType, char const* CLASS_NAME>
class Thread4Dir: public TestThread<ParaType> {
private:
	CounterType *counter;
	CounterTest<CounterType, ParaType, CLASS_NAME> *testcase;
	int operationNum;

public:
	Thread4Dir(CounterType * q,
			CounterTest<CounterType, ParaType, CLASS_NAME> * qt, int nOperation) :
		counter(q), testcase(qt), operationNum(nOperation) {
	}

	void* run() {
		for (int i = 0; i < operationNum; ++i) {
			counter->increment((ParaType) i);
			counter->increment((ParaType) i + 1);
			counter->decrement((ParaType) i + 2);
		}
        return NULL;
	}
};

template<typename CounterType, typename ParaType, char const* CLASS_NAME>
class Thread4DirFactory: public ThreadFactory<ParaType> {
private:
	CounterType *counter;
	CounterTest<CounterType, ParaType, CLASS_NAME> *testcase;

public:
	Thread4DirFactory(CounterType * q, CounterTest<CounterType, ParaType,
			CLASS_NAME> * qt) :
		counter(q), testcase(qt) {
	}

    int opNum;

	virtual Runnable ** createThreads(int threadNum, int elementNum,
			int operationNum) {
        opNum=operationNum;
		testcase->reset();
		counter = testcase->getCounter();

		this->inVec.clear();
		this->outVec.clear();

		Runnable ** threads;
		threads = new Runnable*[threadNum];
		for (int i = 0; i < threadNum; ++i)
			threads[i]
					= new Thread4Dir<CounterType, ParaType, CLASS_NAME> (counter, testcase, operationNum);

		return threads;
	}

	virtual void verifyResult(int threadNum, int elementNum) {
        stringstream output;
        output<<"Expecting "<<threadNum*elementNum<<" but get "<<counter->load()<<endl;
		CPPUNIT_ASSERT_MESSAGE(output.str(), counter->load()==threadNum*opNum);
	}
};

template<typename CounterType, typename ParaType, const char * CLASS_NAME>
void CounterTest<CounterType, ParaType, CLASS_NAME>::testCounter() {
	Thread4DirFactory<CounterType, ParaType, CLASS_NAME> dequeueFactory(
			counter, this);
	ThreadRunner::runThreads(&dequeueFactory, CLASS_NAME, "testCounterMT");
}
}
#endif /*DEQUETEST_H_*/

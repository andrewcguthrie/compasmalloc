/*
 * (c) Copyright 2008, IBM Corporation.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
#ifndef BASETEST_H_
#define BASETEST_H_

#include <iostream>
#include <vector>
#include <algorithm>
#include <stdexcept>
#include "testThread.h"
#include "threadRunner.h"
#include "randArrayGenerator.h"

using namespace amino;
using namespace test;


/**
 * These macros are used to register a cppunit test more conveniently.
 *
 * @author Hui Rui
 */
#define STR(X) #X
#define DEFINITION(DS,TYPE) extern const char DS##TYPE[] = STR(DS##_##TYPE)
#define NAME(DS,TYPE) DS##TYPE
#define TYPEDEF(TESTCASE,DS,TYPE,NAME) typedef TESTCASE<DS<TYPE >,TYPE, NAME > DS##_##TYPE
#define TYPEDEF_2_PARA(TESTCASE,DS,TYPE1,TYPE2,NAME) typedef TESTCASE<DS<TYPE1,TYPE2 >, TYPE2, NAME > DS##_##TYPE2

#define REGISTRATION_TEST(TESTCASE,DS,TYPE) DEFINITION(DS,TYPE);\
											TYPEDEF(TESTCASE,DS,TYPE,NAME(DS,TYPE));\
                                            DS<TYPE > DS##_##TYPE##var;\
                                            DS##_##TYPE DS##_##TYPE##temp;\
											CPPUNIT_TEST_SUITE_REGISTRATION( DS##_##TYPE );

#define REGISTRATION_TEST_2_PARA(TESTCASE,DS,TYPE1,TYPE2) DEFINITION(DS,TYPE2);\
											TYPEDEF_2_PARA(TESTCASE,DS,TYPE1,TYPE2,NAME(DS,TYPE2));\
											CPPUNIT_TEST_SUITE_REGISTRATION( DS##_##TYPE2 );

template<typename DataType> class BaseTest {
public:
	int NELEMENT;
	int NOPERATION;
	int MAXTHREADN;
	ConcurrentRunner* runner;

	DataType* data;

	BaseTest() {
		NELEMENT = TestConfig::getInstance()->getElementNum();
		NOPERATION = TestConfig::getInstance()->getOperationNum();
		vector<int> thread_v = TestConfig::getInstance()->getThreadNum();

        if(0 == thread_v.size()) {
            MAXTHREADN = 0;
        } else {
            MAXTHREADN = *max_element(thread_v.begin(), thread_v.end());
        }
		data = new DataType[NELEMENT * MAXTHREADN];
		RandArrayGenerator<DataType>::getRandArray(data, NELEMENT
				* MAXTHREADN);
	}

	/**
	 * This function will be called by multi-thread test. It should
	 * clean the internal state. It should ensure that the test with i
	 * threads should NOT affect the execution with (i+1) threads.
	 */
	virtual void reset(){};

        virtual	~BaseTest() {
		delete[] data;
	}

	DataType& getData(int index) {
		if (index < 0 || index >= NELEMENT * MAXTHREADN) {
			throw range_error();
		}
		return data[index];
	}
};
#endif /*BASETEST_H_*/

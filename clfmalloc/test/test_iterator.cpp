/*
 * (c) Copyright 2008, IBM Corporation.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
/*
 * test_iterator.cpp
 *
 *  Created on: Oct 14, 2008
 *      Author: daixj
 */

#include <cppunit/ui/text/TestRunner.h>
#include <cppunit/extensions/HelperMacros.h>
#include <cppunit/extensions/TestFactoryRegistry.h>

//*************** include head files of test case here ***********************
#include "test_iterator.h"
#include <amino/list.h>
#include <amino/ordered_list.h>
#include <amino/sync_list.h>
//************************ end of include head files ************************

using namespace test;

REGISTRATION_TEST(IteratorTest,List,int);
REGISTRATION_TEST(IteratorTest,List,char);
REGISTRATION_TEST(IteratorTest,List,long);
REGISTRATION_TEST(IteratorTest,List,double);
REGISTRATION_TEST(IteratorTest,List,string);

REGISTRATION_TEST(IteratorTest,OrderedList,int);
REGISTRATION_TEST(IteratorTest,OrderedList,char);
REGISTRATION_TEST(IteratorTest,OrderedList,long);
REGISTRATION_TEST(IteratorTest,OrderedList,double);
REGISTRATION_TEST(IteratorTest,OrderedList,string);

REGISTRATION_TEST(IteratorTest,SyncList,int);
REGISTRATION_TEST(IteratorTest,SyncList,char);
REGISTRATION_TEST(IteratorTest,SyncList,long);
REGISTRATION_TEST(IteratorTest,SyncList,double);
REGISTRATION_TEST(IteratorTest,SyncList,string);

int main() {
	CppUnit::TextUi::TestRunner runner;
	CppUnit::TestFactoryRegistry &registry =
			CppUnit::TestFactoryRegistry::getRegistry();
	runner.addTest(registry.makeTest() );
	bool wasSuccessful = runner.run("", false);
	return !wasSuccessful;
}

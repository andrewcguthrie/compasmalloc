/*
 * (c) Copyright 2008, IBM Corporation.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  Change History:
 *
 *  yy-mm-dd  Developer  Defect     Description
 *  --------  ---------  ------     -----------
 *  08-07-21  huirui     N/A        Initial implementation
 */
#ifndef TESTTHREAD_H
#define TESTTHREAD_H

#include <vector>
#include <amino/thread.h>

/**
 * TestThread is a template class public derived from Thread class. It is used as a base class
 * in unnitest and perf_test for collecting elements operated in every thread.it's template parameter "ParaType"
 * is just the same to the type of tested data structure.
 *
 * @author Hui Rui (huirui2009@gmail.com)
 *
 */
using namespace amino;

namespace test {
template<typename ParaType> class TestThread: public Thread {
public:
	//inVec and outVec holds elements witch is operated by "in" and "out" liked operator.
	std::vector<ParaType> inVec;
	std::vector<ParaType> outVec;

	TestThread() {
		Thread();
	}
	explicit TestThread(int id) :
		Thread(id) {
	}
	explicit TestThread(std::string n, Runnable *r = NULL) :
		Thread(n, r) {
	}
	explicit TestThread(Runnable *r) :
		Thread(r) {
	}
	virtual ~TestThread() {
	}
};
}
#endif

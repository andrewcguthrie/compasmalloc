/*
 * (c) Copyright 2008, IBM Corporation.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
#include <sys/time.h>
#include <iostream>
#include <algorithm>
#include <amino/aasort.h>
#include <amino/foreach.h>

#include <test_pattern.h>

#include <cppunit/ui/text/TestRunner.h>
#include <cppunit/extensions/HelperMacros.h>
#include <cppunit/extensions/TestFactoryRegistry.h>

#include <vector>

using namespace std;

using namespace test;

using namespace amino;

extern const char strI[] = "PatternTest_I";

//typedef PatternTest<vector<int>, int, strI> PatternVectorIntT

typedef PatternTest<int, strI> PatternVectorIntTest;
CPPUNIT_TEST_SUITE_REGISTRATION(PatternVectorIntTest) ;

int main() {
	CppUnit::TextUi::TestRunner runner;
	CppUnit::TestFactoryRegistry &registry =
			CppUnit::TestFactoryRegistry::getRegistry();
	runner.addTest(registry.makeTest());
	bool wasSuccessful = runner.run("", false);
	return !wasSuccessful;
}

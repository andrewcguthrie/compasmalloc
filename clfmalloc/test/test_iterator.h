/*
 * (c) Copyright 2008, IBM Corporation.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
/*
 * test_iterator.h
 *
 *  Created on: Oct 14, 2008
 *      Author: daixj
 */

#ifndef TEST_ITERATOR_H_
#define TEST_ITERATOR_H_

#include <cppunit/extensions/HelperMacros.h>
#include <cppunit/TestAssert.h>
#include "baseTest.h"

namespace test {

using namespace amino;

template<typename ListType, typename ParaType, char const* CLASS_NAME> class IteratorTest: public CppUnit::TestFixture,
		public BaseTest<ParaType> {
            CPPUNIT_TEST_SUITE(IteratorTest);	
            CPPUNIT_TEST(testIteratorST);
            CPPUNIT_TEST(testConstIteratorST);
            CPPUNIT_TEST_SUITE_END();
private:
	ListType *list;

public:
	IteratorTest() {
	}

	ListType * getList() {
		return list;
	}

	void setUp() {
		list = new ListType();
	}

	void reset() {
		delete list;
		list = new ListType();
	}

	void tearDown() {
		delete list;
	}

	void testIteratorST();
	void testConstIteratorST();
};

#define ThreadInsert_T ThreadInsert<ListType, ParaType, CLASS_NAME>

template<typename ListType, typename ParaType, char const* CLASS_NAME> class ThreadInsert :
public TestThread<ParaType> {
private:
	ListType *list;
	IteratorTest<ListType, ParaType, CLASS_NAME> *testcase;
	int elementNum;
	int operationNum;

public:

	ThreadInsert(ListType *l, IteratorTest<ListType, ParaType, CLASS_NAME>* lt,
			int nElement,int nOperation, int threadId = 0) :
	TestThread<ParaType>(threadId), list(l), testcase(lt), elementNum(nElement),operationNum(nOperation)
	{
	}

	void* run() {
		for (int i = 0; i< operationNum; ++i) {
				bool ret = list->insert(0, testcase->data[i%((this->threadId+1)*elementNum)]);
				if (ret) {
					this->inVec.push_back(testcase->data[i%((this->threadId+1)*elementNum)]);
				}
		}
		return NULL;
	}
};

template<typename ListType, typename ParaType, char const* CLASS_NAME> class ThreadInsertFactory :
public ThreadFactory<ParaType> {
private:
	ListType *list;
	IteratorTest<ListType, ParaType, CLASS_NAME> *testcase;
public:
	ThreadInsertFactory(ListType * l,
			IteratorTest<ListType, ParaType, CLASS_NAME> * lt) :
	list(l), testcase(lt) {
	}

	virtual Runnable ** createThreads(int threadNum, int elementNum, int operationNum) {
		testcase->reset();
		list = testcase->getList();
		this->inVec.clear();
		this->outVec.clear();

		Runnable ** threads;
		threads = new Runnable*[threadNum];
		for (int i = 0; i < threadNum; ++i)
		threads[i] = new ThreadInsert<ListType, ParaType, CLASS_NAME>(list, testcase, elementNum, operationNum, i);

		return threads;
	}

	virtual void verifyResult(int threadNum, int elementNum) {
		ParaType tmp;
		while (!list->empty()) {
			list->front(tmp);
			this->outVec.push_back(tmp);
			list->remove(tmp);
		}

		ThreadFactory<ParaType>::verifyResult(threadNum, elementNum);
	}
};

template<typename ListType, typename ParaType, char const* CLASS_NAME> void IteratorTest<
ListType, ParaType, CLASS_NAME>::testIteratorST() {
	ThreadInsert_T threadInsert(list, this, BaseTest<ParaType>::NELEMENT, BaseTest<ParaType>::NOPERATION);
	threadInsert.run();

	vector<ParaType> outVec;
	typename ListType::iterator ite = list->begin();
	for (ite = list->begin(); ite != list->end(); ++ite) {
		outVec.push_back(*ite);
	}

	sort(threadInsert.inVec.begin(), threadInsert.inVec.end());
	sort(outVec.begin(), outVec.end());

	CPPUNIT_ASSERT(threadInsert.inVec == outVec);
}

template<typename ListType, typename ParaType, char const* CLASS_NAME> void IteratorTest<
ListType, ParaType, CLASS_NAME>::testConstIteratorST() {
	ThreadInsert_T threadInsert(list, this, BaseTest<ParaType>::NELEMENT, BaseTest<ParaType>::NOPERATION);
	threadInsert.run();

	vector<ParaType> outVec;
	typename ListType::const_iterator ite = list->begin();
	for (ite = list->begin(); ite != list->end(); ++ite) {
		outVec.push_back(*ite);
	}

	sort(threadInsert.inVec.begin(), threadInsert.inVec.end());
	sort(outVec.begin(), outVec.end());

	CPPUNIT_ASSERT(threadInsert.inVec == outVec);
}

}
#endif /* TEST_ITERATOR_H_ */

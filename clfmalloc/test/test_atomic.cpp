/*
 * (c) Copyright 2008, IBM Corporation.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
#include <cppunit/ui/text/TestRunner.h>
#include <cppunit/extensions/HelperMacros.h>
#include <cppunit/extensions/TestFactoryRegistry.h>

//*************** include head files of test case here ***********************
#include <string>
#include <amino/cstdatomic>
#include <test_atomic.h>
//************************ end of include head files ************************

using namespace test;

class Temp{
};

CPPUNIT_TEST_SUITE_REGISTRATION(AtomicTest<bool>) ;
CPPUNIT_TEST_SUITE_REGISTRATION(AtomicTest<char>) ;
CPPUNIT_TEST_SUITE_REGISTRATION(AtomicTest<signed char>) ;
CPPUNIT_TEST_SUITE_REGISTRATION(AtomicTest<unsigned char>) ;
CPPUNIT_TEST_SUITE_REGISTRATION(AtomicTest<short>) ;
CPPUNIT_TEST_SUITE_REGISTRATION(AtomicTest<unsigned short>) ;
CPPUNIT_TEST_SUITE_REGISTRATION(AtomicTest<Temp*>) ;

extern const char atomicIntStr[] = "AtomicNumTest_Int";
typedef AtomicNumTest<int,atomicIntStr> AtomicNumTest_Int;
CPPUNIT_TEST_SUITE_REGISTRATION(AtomicNumTest_Int);

extern const char atomicLongStr[] = "AtomicNumTest_Long";
typedef AtomicNumTest<long,atomicLongStr> AtomicNumTest_Long;
CPPUNIT_TEST_SUITE_REGISTRATION(AtomicNumTest_Long);

extern const char atomicBoolStr[] = "AtomicBoolTest_Bool";
typedef AtomicBoolTest<atomicBoolStr> AtomicBoolTest_Bool;
CPPUNIT_TEST_SUITE_REGISTRATION(AtomicBoolTest_Bool);
/*
extern const char atomicCharStr[] = "AtomicNumTest_Char";
typedef AtomicNumTest<char,atomicCharStr> AtomicNumTest_Char;
CPPUNIT_TEST_SUITE_REGISTRATION(AtomicNumTest_Char);

extern const char atomicSCharStr[] = "AtomicNumTest_SChar";
typedef AtomicNumTest<signed char,atomicSCharStr> AtomicNumTest_SChar;
CPPUNIT_TEST_SUITE_REGISTRATION(AtomicNumTest_SChar);

extern const char atomicUCharStr[] = "AtomicNumTest_UChar";
typedef AtomicNumTest<unsigned char,atomicUCharStr> AtomicNumTest_UChar;
CPPUNIT_TEST_SUITE_REGISTRATION(AtomicNumTest_UChar);

extern const char atomicShortStr[] = "AtomicNumTest_Short";
typedef AtomicNumTest<short,atomicShortStr> AtomicNumTest_Short;
CPPUNIT_TEST_SUITE_REGISTRATION(AtomicNumTest_Short);

extern const char atomicUShortStr[] = "AtomicNumTest_UShort";
typedef AtomicNumTest<unsigned short,atomicUShortStr> AtomicNumTest_UShort;
CPPUNIT_TEST_SUITE_REGISTRATION(AtomicNumTest_UShort);
*/
int main() {
	  CppUnit::TextUi::TestRunner runner;
	  CppUnit::TestFactoryRegistry &registry = CppUnit::TestFactoryRegistry::getRegistry();
	  runner.addTest( registry.makeTest() );
	  bool wasSuccessful = runner.run( "", false );
	  return !wasSuccessful;
}

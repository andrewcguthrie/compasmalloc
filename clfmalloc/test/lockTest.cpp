/*
 * (c) Copyright 2008, IBM Corporation.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
#include "lockTest.h"
#include "testconfig.h"
#include <time.h>
#include <iostream>

using namespace std;

namespace test{
    void LockTest::testLock(){
        LockThread thread1(fLock);
        LockThread thread2(fLock);
        struct timeval tv, tv1;
        gettimeofday(&tv, NULL);
        thread1.start();
        thread2.start();
        thread1.join();
        thread2.join();
        gettimeofday(&tv1, NULL);
        CPPUNIT_ASSERT(tv1.tv_sec - tv.tv_sec >= 6);
    }

    void LockTest::testRecursiveLock(){
        unique_lock<recursive_mutex> rLock;
        RecursiveLockThread thread1(&rLock);
        RecursiveLockThread thread2(&rLock);
        struct timeval tv, tv1;
        gettimeofday(&tv, NULL);
        thread1.start();
        thread2.start();
        thread1.join();
        thread2.join();
        gettimeofday(&tv1, NULL);
        CPPUNIT_ASSERT(tv1.tv_sec - tv.tv_sec >= 6);
    }
}

/*
 * (c) Copyright 2008, IBM Corporation.
 *
 *      Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  Change History:
 *
 *  yy-mm-dd  Developer  Defect     Description
 *  --------  ---------  ------     -----------
 *  08-08-03  ganzhi     N/A        Initial implementation
 *  08-08-26  Hui Rui    N/A        Add testAccumulate and testTransform
 */

#ifndef TEST_PATTERN_H
#define TEST_PATTERN_H
#include <cppunit/extensions/HelperMacros.h>
#include <cppunit/TestAssert.h>
#include "baseTest.h"

#include <amino/cstdatomic>
#include <amino/thread.h>
#include <amino/foreach.h>
#include <amino/tp_exec.h>
#include <amino/accumulate.h>
#include <amino/transform.h>

#include <iostream>
#include <assert.h>
#include <unistd.h>

namespace test {
using namespace amino;

//template<typename ContainerT>
//class UnaryFunc {
//public:
//	void operator()(typename ContainerT::reference element) {
//		element *= 2;
//	}
//};

template<typename ParaType>
class UnaryFunc {
public:
	ParaType operator()(ParaType element) {
		return 2 * element;
	}
};
template<typename ParaType>
class BinaryFunc {
public:
	ParaType operator()(ParaType ele1, ParaType ele2) {
		return ele1 * ele2;
	}
};
template<typename ElementT, const char * CLASS_NAME>
class PatternTest: public CppUnit::TestFixture, public BaseTest<ElementT> {
    CPPUNIT_TEST_SUITE(PatternTest);
	CPPUNIT_TEST(testForeach);
	CPPUNIT_TEST(testAccumulate);
	CPPUNIT_TEST(testTransform);
	CPPUNIT_TEST_SUITE_END()
	;
public:
	PatternTest() {
	}

	void setUp() {
	}

	void reset() {
	}

	void tearDown() {
	}

	void testForeach() {
		UnaryFunc<ElementT> uf;
		Logger log(stderr);
		vector<int> thread_v = TestConfig::getInstance()->getThreadNum();
		for (unsigned int i = 0; i < thread_v.size(); i++) {
			ThreadPoolExecutor exec;
			ElementT* verify = new ElementT[this->NELEMENT * thread_v[i]];
			std::copy(this->data, this->data + this->NELEMENT * thread_v[i],
					verify);

			struct timeval t1, t2, t3;
			gettimeofday(&t1, NULL);

			for_each(exec, thread_v[i], this->data, this->data + this->NELEMENT
					* thread_v[i], uf);
			exec.shutdown();
			exec.waitTermination();

			gettimeofday(&t2, NULL);

			for_each(verify, verify + this->NELEMENT * thread_v[i], uf);

			gettimeofday(&t3, NULL);

			int takes1 = (t2.tv_sec - t1.tv_sec) * 1000000 + t2.tv_usec
			- t1.tv_usec;
			log.log(
					"INFO: class:	%s\tnThread:\t%d\tnElement:\t%d\ttestName:\t%s\tTakes:\t%d\tmicroseconds\n",
					"ParallelSort", thread_v[i], this->NELEMENT,
					"testSort", takes1);

			int takes2 = (t3.tv_sec - t2.tv_sec) * 1000000 + t3.tv_usec
			- t2.tv_usec;
			log.log(
					"INFO: class:	%s\tnThread:\t%d\tnElement:\t%d\ttestName:\t%s\tTakes:\t%d\tmicroseconds\n",
					"SerialSort", 1, this->NELEMENT * thread_v[i],
					"testSort", takes2);

			for (int i = 0; i < this->NELEMENT * thread_v[i]; i++) {
				CPPUNIT_ASSERT(this->data[i]==verify[i]);
			}

			delete[] verify;
		}
	}

	void testAccumulate() {
		Logger log(stderr);
		vector<int> thread_v = TestConfig::getInstance()->getThreadNum();
		for (unsigned int i = 0; i < thread_v.size(); i++) {
			ThreadPoolExecutor exec;

			struct timeval t1, t2, t3, t4, t5;
			gettimeofday(&t1, NULL);

			ElementT myResult1 = accumulate<ElementT*, ElementT,
			ThreadPoolExecutor> (exec, thread_v[i], this->data,
					this->data + this->NELEMENT * thread_v[i]);
			//			exec.shutdown();
			//			exec.waitTermination();

			gettimeofday(&t2, NULL);

			ElementT verifyResult1 = accumulate(this->data + 1, this->data
					+ this->NELEMENT * thread_v[i], *(this->data));

			gettimeofday(&t3, NULL);

			ElementT myResult2 = accumulate<ElementT*, ElementT,BinaryFunc<ElementT>,
			ThreadPoolExecutor> (exec, thread_v[i], this->data,
					this->data + this->NELEMENT * thread_v[i],BinaryFunc<ElementT>());
			exec.shutdown();
			exec.waitTermination();
			gettimeofday(&t4, NULL);

			ElementT verifyResult2 = accumulate(this->data + 1, this->data
					+ this->NELEMENT * thread_v[i], *(this->data),BinaryFunc<ElementT>());

			gettimeofday(&t5, NULL);

			int takes1 = (t2.tv_sec - t1.tv_sec) * 1000000 + t2.tv_usec
			- t1.tv_usec;
			log.log(
					"INFO: class:	%s\tnThread:\t%d\tnElement:\t%d\ttestName:\t%s\tTakes:\t%d\tmicroseconds\n",
					"ParallelAccumulate", thread_v[i], this->NELEMENT,
					"testAccumulate", takes1);

			int takes2 = (t3.tv_sec - t2.tv_sec) * 1000000 + t3.tv_usec
			- t2.tv_usec;
			log.log(
					"INFO: class:	%s\tnThread:\t%d\tnElement:\t%d\ttestName:\t%s\tTakes:\t%d\tmicroseconds\n",
					"SerialAccumulate", 1, this->NELEMENT * thread_v[i],
					"testAccumulate", takes2);

			int takes3 = (t4.tv_sec - t3.tv_sec) * 1000000 + t4.tv_usec
			- t3.tv_usec;
			log.log(
					"INFO: class:	%s\tnThread:\t%d\tnElement:\t%d\ttestName:\t%s\tTakes:\t%d\tmicroseconds\n",
					"ParallelAccumulateFunc", thread_v[i], this->NELEMENT,
					"testAccumulateFunc", takes3);

			int takes4 = (t5.tv_sec - t4.tv_sec) * 1000000 + t5.tv_usec
			- t4.tv_usec;
			log.log(
					"INFO: class:	%s\tnThread:\t%d\tnElement:\t%d\ttestName:\t%s\tTakes:\t%d\tmicroseconds\n",
					"SerialAccumulateFunc", 1, this->NELEMENT * thread_v[i],
					"testAccumulateFunc", takes4);

			for (int i = 0; i < this->NELEMENT * thread_v[i]; i++) {
				CPPUNIT_ASSERT(myResult1==verifyResult1);
				CPPUNIT_ASSERT(myResult2==verifyResult2);
			}

		}
	}

	void testTransform() {
		Logger log(stderr);
		vector<int> thread_v = TestConfig::getInstance()->getThreadNum();
		for (unsigned int i = 0; i < thread_v.size(); i++) {
			ThreadPoolExecutor exec;

			ElementT* result1 = new ElementT[this->NELEMENT * thread_v[i]];
			ElementT* result2 = new ElementT[this->NELEMENT * thread_v[i]];
			ElementT* result3 = new ElementT[this->NELEMENT * thread_v[i]];
			ElementT* result4 = new ElementT[this->NELEMENT * thread_v[i]];

			struct timeval t1, t2, t3, t4, t5;
			gettimeofday(&t1, NULL);

			transform(exec, thread_v[i], this->data, this->data+this->NELEMENT*thread_v[i], result1, UnaryFunc<ElementT>());
			//			exec.shutdown();
			//			exec.waitTermination();

			gettimeofday(&t2, NULL);

			transform(this->data, this->data + this->NELEMENT * thread_v[i], result2, UnaryFunc<ElementT>());

			gettimeofday(&t3, NULL);

			transform(exec, thread_v[i], this->data, this->data+this->NELEMENT*thread_v[i], result1, result3, BinaryFunc<ElementT>());
			exec.shutdown();
			exec.waitTermination();

			gettimeofday(&t4, NULL);

			std::transform(this->data, this->data + this->NELEMENT * thread_v[i], result1, result4, BinaryFunc<ElementT>());

			gettimeofday(&t5, NULL);

			int takes1 = (t2.tv_sec - t1.tv_sec) * 1000000 + t2.tv_usec
			- t1.tv_usec;
			log.log(
					"INFO: class:	%s\tnThread:\t%d\tnElement:\t%d\ttestName:\t%s\tTakes:\t%d\tmicroseconds\n",
					"ParallelTransformUnary", thread_v[i], this->NELEMENT,
					"testTransformUnary", takes1);

			int takes2 = (t3.tv_sec - t2.tv_sec) * 1000000 + t3.tv_usec
			- t2.tv_usec;
			log.log(
					"INFO: class:	%s\tnThread:\t%d\tnElement:\t%d\ttestName:\t%s\tTakes:\t%d\tmicroseconds\n",
					"SerialTransformUnary", 1, this->NELEMENT * thread_v[i],
					"testTransformUnary", takes2);
			int takes3 = (t4.tv_sec - t3.tv_sec) * 1000000 + t4.tv_usec
			- t3.tv_usec;
			log.log(
					"INFO: class:	%s\tnThread:\t%d\tnElement:\t%d\ttestName:\t%s\tTakes:\t%d\tmicroseconds\n",
					"ParallelTransformBinary", thread_v[i], this->NELEMENT,
					"testTransformBinary", takes3);

			int takes4 = (t5.tv_sec - t4.tv_sec) * 1000000 + t5.tv_usec
			- t4.tv_usec;
			log.log(
					"INFO: class:	%s\tnThread:\t%d\tnElement:\t%d\ttestName:\t%s\tTakes:\t%d\tmicroseconds\n",
					"SerialTransformBinary", 1, this->NELEMENT * thread_v[i],
					"testTransformBinary", takes4);

			for (int i = 0; i < this->NELEMENT * thread_v[i]; i++) {
				CPPUNIT_ASSERT(result1[i] == result2[i]);
				CPPUNIT_ASSERT(result3[i] == result4[i]);
			}

			delete[] result1;
			delete[] result2;
			delete[] result3;
			delete[] result4;
		}

	}
};
}
#endif /*TEST_PATTERN_H*/

/*
 * (c) Copyright 2008, IBM Corporation.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
#ifndef RANDARRAYGENERATOR_H_
#define RANDARRAYGENERATOR_H_

#include <cstdlib>
#include <ctime>
#include <string>
#include <algorithm>

namespace test{

    template<typename DataType> class RandArrayGenerator {
        public:
            static DataType* getRandArray(DataType* a, int n);
            static DataType* getRandUniqueArray(DataType* a, int n);
    };

    template<typename DataType> DataType* RandArrayGenerator<DataType>::getRandArray(
            DataType* a, int n) {
        for (int i = 0; i < n; ++i) {
            new (a+i) DataType(); //placement new
        }
        return a;
    }

    template<typename DataType> DataType* RandArrayGenerator<DataType>::getRandUniqueArray(
            DataType* a, int n) {
    	/*this is just a template. use specialized functions*/
        for (int i = 0; i < n; ++i) {
            new (a+i) DataType(); //placement new
        }
        return a;
    }

    template<typename DataType> class RandArrayGenerator<DataType *> {
        public:
            static DataType** getRandArray(DataType** a, int n){
                srand(time(NULL));
                for (int i = 0; i < n; ++i) {
                    int tmp = rand() % n;
                    a[i] = (DataType *) tmp;
                }
		return a;
            }

            static DataType** getRandUniqueArray(DataType** a, int n){
            	/*this is just a template. use specialized functions*/
                srand(time(NULL));
                for (int i = 0; i < n; ++i) {
                    int tmp = rand() % n;
                    a[i] = (DataType *) tmp;
                }
		return a;
            }
    };

    template<> int* RandArrayGenerator<int>::getRandArray(int* a, int n);
    template<> char* RandArrayGenerator<char>::getRandArray(char* a, int n);
    template<> long* RandArrayGenerator<long>::getRandArray(long* a, int n);
    template<> double* RandArrayGenerator<double>::getRandArray(double* a, int n);
    template<> std::string* RandArrayGenerator<std::string>::getRandArray(
            std::string* a, int n);
    template<> int* RandArrayGenerator<int>::getRandUniqueArray(int* a, int n);
    template<> long* RandArrayGenerator<long>::getRandUniqueArray(long* a, int n);
    template<> double* RandArrayGenerator<double>::getRandUniqueArray(double* a, int n);
    template<> std::string* RandArrayGenerator<std::string>::getRandUniqueArray(
            std::string* a, int n);
}
#endif /*RANDARRAYGENERATOR_H_*/

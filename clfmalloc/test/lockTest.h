/*
 * (c) Copyright 2008, IBM Corporation.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
#ifndef LOCKTEST_H_
#define LOCKTEST_H_
#include <cppunit/extensions/HelperMacros.h>
#include <cppunit/TestAssert.h>
#include "baseTest.h"
#include "threadFactory.h"
#include <amino/lock.h>
#include <amino/mutex.h>
#include <amino/thread.h>

using namespace amino;

namespace test{

    /**
     * Test case for mutex class
     * @author Zhi Gan (ganzhi@gmail.com)
     */
    class LockTest : public CppUnit::TestFixture{
        CPPUNIT_TEST_SUITE(LockTest);
        CPPUNIT_TEST(testLock);
        CPPUNIT_TEST(testRecursiveLock);
        CPPUNIT_TEST_SUITE_END();

        private:
        unique_lock<mutex> * fLock;

        public:
        void setUp() {
            fLock = new unique_lock<mutex>();
        }

        void reset(){
            delete fLock;
            fLock = new unique_lock<mutex>();
        }

        /**
         * This test case will start 2 threads. Each thread will
         * try to acquire a lock and then sleep three seconds. So
         * if the mutex work correctly, the whole test case will
         * need longer than 6 seconds.
         */
        void testLock();
        void testRecursiveLock();

        void tearDown() {
            delete fLock;
        }
    };

    class LockThread : public Thread {
        private:
            unique_lock<mutex> * fLock;
        public:
            LockThread(unique_lock<mutex> * lock) {
                fLock = lock;
            }

            void* run() {
                fLock->lock();
                sleep(3);
                fLock->unlock();
                return NULL;
            }
    };

    class RecursiveLockThread : public Thread {
        private:
            unique_lock<recursive_mutex> * fLock;
        public:
            RecursiveLockThread(unique_lock<recursive_mutex> * lock) {
                fLock = lock;
            }

            void* run() {
                fLock->lock();
                fLock->lock();
                sleep(3);
                fLock->unlock();
                fLock->unlock();
                return NULL;
            }
    };
}
#endif /*MUTEXTEST_H_*/

/*
 * (c) Copyright 2008, IBM Corporation.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
#ifdef X86
#include <amino/x86/cpuid.h>
#include <iostream>
using namespace std;

#endif

int main(int argc, char * argv[]){
#ifdef X86
    int res[4];
    cpuid(1,res);
    cout<<hex<<res[0]<<" "<<res[1]<<" "<<res[2]<<" "<<res[3]<<endl;
    cout<<"Supporting MMX:";
    if((res[3] & 0x00800000)!=0)
        cout<<" ON\n";
    else
        cout<<" OFF\n";

    cout<<"Supporting SSE:";
    if((res[3] & 0x02000000)!=0)
        cout<<" ON\n";
    else
        cout<<" OFF\n";

    cout<<"Supporting SSE2:";
    if((res[3] & 0x04000000)!=0)
        cout<<" ON\n";
    else
        cout<<" OFF\n";

    cout<<"Supporting SSE3:";
    if((res[2] & 0x00000001)!=0)
        cout<<" ON\n";
    else
        cout<<" OFF\n";

    cout<<"Supporting SSSE3:";
    if((res[2] & (1<<8))!=0)
        cout<<" ON\n";
    else
        cout<<" OFF\n";

    cout<<"Supporting SSE4_1:";
    if((res[2] & 0x00080000)!=0)
        cout<<" ON\n";
    else
        cout<<" OFF\n";

    cout<<"Supporting SSE4_2:";
    if((res[2] & 0x00100000)!=0)
        cout<<" ON\n";
    else
        cout<<" OFF\n";

    cout<<"Supporting CMPXCHG8B:";
    if((res[3] & 1<<7)!=0)
        cout<<" ON\n";
    else
        cout<<" OFF\n";

    cout<<"Supporting CMPXCHG16B:";
    if((res[2] & 0x00002000)!=0)
        cout<<" ON\n";
    else
        cout<<" OFF\n";

    cout<<"Supporting HHT:";
    if((res[3] & 0x10000000)!=0)
        cout<<" ON\n";
    else
        cout<<" OFF\n";

    cout<<"Supporting VMX:";
    if((res[2] & (1<<4))!=0)
        cout<<" ON\n";
    else
        cout<<" OFF\n";

    cpuid(2, res);
    cout<<hex<<res[0]<<" "<<res[1]<<" "<<res[2]<<" "<<res[3]<<endl;

#endif
    return 0;
}

/*
 * (c) Copyright 2008, IBM Corporation.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
/*
 * test_dictionary.h
 *
 *  Created on: Oct 24, 2008
 *      Author: daixj
 */

#ifndef TEST_DICTIONARY_H_
#define TEST_DICTIONARY_H_

#include <cppunit/extensions/HelperMacros.h>
#include <cppunit/TestAssert.h>
#include "baseTest.h"

namespace test {

using namespace amino;

template<typename DictType, typename ParaType, char const* CLASS_NAME> class DictionaryTest: public CppUnit::TestFixture,
		public BaseTest<ParaType> {
CPPUNIT_TEST_SUITE(DictionaryTest)
;	//		CPPUNIT_TEST(testEmptyST);
	CPPUNIT_TEST(testInsertST);
	CPPUNIT_TEST(testDeleteKeyST);
	CPPUNIT_TEST(testFindValueST);
	CPPUNIT_TEST(testDeleteValueST);
	CPPUNIT_TEST(testInsertMT);
	CPPUNIT_TEST(testFindValueMT);
	CPPUNIT_TEST(testFindKeyMT);
	CPPUNIT_TEST(testDeleteKeyMT);
	CPPUNIT_TEST(testDeleteValueMT);
//	CPPUNIT_TEST(testInsertRemoveMT);
	CPPUNIT_TEST_SUITE_END ()
	;
private:
	DictType *dict;

public:
	DictionaryTest() {
	}

	DictType * getDict() {
		return dict;
	}

	void setUp() {
		dict = new DictType();
	}

	void reset() {
		delete dict;
		dict = new DictType();
	}

	void tearDown() {
		delete dict;
	}

	//	void testEmptyST();
	void testInsertST();
	void testDeleteKeyST();
	void testFindValueST();
	void testDeleteValueST();
	void testInsertMT();
	void testFindValueMT();
	void testFindKeyMT();
	void testDeleteKeyMT();
	void testDeleteValueMT();
	void testInsertRemoveMT();
};

#define ThreadInsert_T ThreadInsert<DictType, ParaType, CLASS_NAME>
#define ThreadDeleteKey_T ThreadDeleteKey<DictType, ParaType, CLASS_NAME>
#define ThreadDeleteValue_T ThreadDeleteValue<DictType, ParaType, CLASS_NAME>

template<typename DictType, typename ParaType, char const* CLASS_NAME> class ThreadInsert :
public TestThread<ParaType> {
private:
	DictType *dict;
	DictionaryTest<DictType, ParaType, CLASS_NAME> *testcase;
	int elementNum;
	int operationNum;

public:

	ThreadInsert(DictType *l, DictionaryTest<DictType, ParaType, CLASS_NAME>* lt,
			int nElement,int nOperation, int threadId = 0) :
	TestThread<ParaType>(threadId), dict(l), testcase(lt), elementNum(nElement),operationNum(nOperation)
	{
	}

	void* run() {
		cout << "#operation of insert: " << operationNum << endl;
		for (int i = 0; i< this->operationNum; ++i) {
			ParaType tmp = testcase->data[i + ((this->threadId) * elementNum)];
			//			cout << "threadID: " << this->threadId << ", insert key: " << tmp << endl;
			if (dict->insert(tmp, tmp)) {
				this->inVec.push_back(tmp);
			} else {
				cout << "....................insert error." << endl;
			}
		}
		return NULL;
	}
};

template<typename DictType, typename ParaType, char const* CLASS_NAME> class ThreadDeleteKey :
public TestThread<ParaType> {
private:
	DictType *dict;
	DictionaryTest<DictType, ParaType, CLASS_NAME> *testcase;
	int elementNum;
	int operationNum;

public:
	ThreadDeleteKey(DictType *l, DictionaryTest<DictType, ParaType, CLASS_NAME>* lt,
			int nElement, int nOperation, int nThread = 0) :
	TestThread<ParaType>(nThread),dict(l), testcase(lt), elementNum(nElement), operationNum(nOperation) {
	}

	void *run() {
		cout << "#operation of delete: " << operationNum << endl;
		for (int i = 0; i < operationNum; ++i) {
			ParaType tmp = testcase->data[i + ((this->threadId) * elementNum)];
			ParaType tmpValue;

			if (dict->deleteKey(tmp, tmpValue)) {
//				cout << "threadID: " << this->threadId << ", delete key: " << tmp << endl;
//				cout << "success!" << endl;
				this->outVec.push_back(tmpValue);
			}
		}

		return NULL;
	}
};

template<typename DictType, typename ParaType, char const* CLASS_NAME> class ThreadDeleteValue :
public TestThread<ParaType> {
private:
	DictType *dict;
	DictionaryTest<DictType, ParaType, CLASS_NAME> *testcase;
	int elementNum;
	int operationNum;

public:
	ThreadDeleteValue(DictType *l, DictionaryTest<DictType, ParaType, CLASS_NAME>* lt,
			int nElement, int nOperation, int nThread = 0) :
	TestThread<ParaType>(nThread), dict(l), testcase(lt), elementNum(nElement), operationNum(nOperation){
	}

	void *run() {
		for (int i = 0; i < operationNum; ++i) {
			ParaType tmp = testcase->data[i + ((this->threadId) * elementNum)];
			ParaType tmpKey;
			//			cout << "threadID: " << this->threadId << ", delete key: " << tmp << endl;

			if (dict->deleteValue(tmp, tmpKey)) {
				this->outVec.push_back(tmpKey);
			}
		}

		return NULL;
	}
};

template<typename DictType, typename ParaType, char const* CLASS_NAME> class ThreadFindValue :
public TestThread<ParaType> {
private:
	DictType *dict;
	DictionaryTest<DictType, ParaType, CLASS_NAME> *testcase;
	int elementNum;
	int operationNum;

public:
	ThreadFindValue(DictType *l, DictionaryTest<DictType, ParaType, CLASS_NAME>* lt,
			int nElement, int nOperation, int nThread = 0) :
	TestThread<ParaType>(nThread), dict(l), testcase(lt), elementNum(nElement), operationNum(nOperation) {
	}

	void *run() {
		for (int i = 0; i < operationNum; ++i) {
			ParaType tmp = testcase->data[i + ((this->threadId) * elementNum)];
			ParaType tmpKey;
			//			cout << "threadID: " << this->threadId << ", delete key: " << tmp << endl;

			if (dict->findValue(tmp, tmpKey)) {
				this->outVec.push_back(tmpKey);
			}
		}

		return NULL;
	}
};

template<typename DictType, typename ParaType, char const* CLASS_NAME> class ThreadFindKey :
public TestThread<ParaType> {
private:
	DictType *dict;
	DictionaryTest<DictType, ParaType, CLASS_NAME> *testcase;
	int elementNum;
	int operationNum;

public:
	ThreadFindKey(DictType *l, DictionaryTest<DictType, ParaType, CLASS_NAME>* lt,
			int nElement, int nOperation, int nThread = 0) :
	TestThread<ParaType>(nThread), dict(l), testcase(lt), elementNum(nElement), operationNum(nOperation) {
	}

	void *run() {
		for (int i = 0; i < operationNum; ++i) {
			ParaType tmp = testcase->data[i + ((this->threadId) * elementNum)];
			ParaType tmpKey;
			//			cout << "threadID: " << this->threadId << ", delete key: " << tmp << endl;

			if (dict->findKey(tmp, tmpKey)) {
				this->outVec.push_back(tmpKey);
			}
		}

		return NULL;
	}
};

template<typename DictType, typename ParaType, char const* CLASS_NAME> class ThreadInsertFactory :
public ThreadFactory<ParaType> {
private:
	DictType *dict;
	DictionaryTest<DictType, ParaType, CLASS_NAME> *testcase;
public:
	ThreadInsertFactory(DictType * l,
			DictionaryTest<DictType, ParaType, CLASS_NAME> * lt) :
	dict(l), testcase(lt) {
	}

	virtual Runnable ** createThreads(int threadNum, int elementNum, int operationNum) {
		testcase->reset();
		dict = testcase->getDict();
		this->inVec.clear();
		this->outVec.clear();

		Runnable ** threads;
		threads = new Runnable*[threadNum];
		for (int i = 0; i < threadNum; ++i) {
			threads[i] = new ThreadInsert<DictType, ParaType, CLASS_NAME>(dict, testcase, elementNum, operationNum, i);
		}

		return threads;
	}

	virtual void verifyResult(int threadNum, int elementNum) {
		//		dict->dumpQueue();
		ParaType tmp;
		for(int id = 0; id < threadNum; ++id) {
			for (int i = 0; i< testcase->BaseTest<ParaType>::NOPERATION; ++i) {
				//				cout << "find key: " << testcase->data[i + (id * elementNum)] << endl;
				if (dict->findKey(testcase->data[i + (id * elementNum)], tmp)) {
					this->outVec.push_back(tmp);
				}
			}
		}

		ThreadFactory<ParaType>::verifyResult(threadNum, elementNum);
	}
};

template<typename DictType, typename ParaType, char const* CLASS_NAME> class ThreadDeleteKeyFactory :
public ThreadFactory<ParaType> {
private:
	DictType *dict;
	DictionaryTest<DictType, ParaType, CLASS_NAME> *testcase;
public:
	ThreadDeleteKeyFactory(DictType * l,
			DictionaryTest<DictType, ParaType, CLASS_NAME> * lt) :
	dict(l), testcase(lt) {
	}

	virtual Runnable ** createThreads(int threadNum, int elementNum, int operationNum) {
		testcase->reset();
		dict = testcase->getDict();
		this->inVec.clear();
		this->outVec.clear();

		for (int i = 0; i < threadNum; ++i) {
			ThreadInsert_T threadInsert(dict, testcase, elementNum, operationNum, i);
			threadInsert.run();

			copy(threadInsert.inVec.begin(), threadInsert.inVec.end(),
					back_inserter(this->inVec));
		}

		//		cout << "after insert ................" << endl;
		//		dict->dumpQueue();
		Runnable ** threads;
		threads = new Runnable*[threadNum];
		for (int i = 0; i < threadNum; ++i) {
			threads[i] = new ThreadDeleteKey<DictType, ParaType, CLASS_NAME>(dict, testcase, elementNum,operationNum, i);
		}
		return threads;
	}

	virtual void verifyResult(int threadNum, int elementNum) {

		sort(this->inVec.begin(), this->inVec.end());
		this->inVec.erase(unique(this->inVec.begin(),this->inVec.end()),this->inVec.end());

		ThreadFactory<ParaType>::verifyResult(threadNum, elementNum);
	}
};

template<typename DictType, typename ParaType, char const* CLASS_NAME> class ThreadDeleteValueFactory :
public ThreadFactory<ParaType> {
private:
	DictType *dict;
	DictionaryTest<DictType, ParaType, CLASS_NAME> *testcase;
public:
	ThreadDeleteValueFactory(DictType * l,
			DictionaryTest<DictType, ParaType, CLASS_NAME> * lt) :
	dict(l), testcase(lt) {
	}

	virtual Runnable ** createThreads(int threadNum, int elementNum, int operationNum) {
		testcase->reset();
		dict = testcase->getDict();
		this->inVec.clear();
		this->outVec.clear();

		for (int i = 0; i < threadNum; ++i) {
			ThreadInsert_T threadInsert(dict, testcase, elementNum, operationNum, i);
			threadInsert.run();

			copy(threadInsert.inVec.begin(), threadInsert.inVec.end(),
					back_inserter(this->inVec));
		}

		//		cout << "after insert ................" << endl;
		//		dict->dumpQueue();
		Runnable ** threads;
		threads = new Runnable*[threadNum];
		for (int i = 0; i < threadNum; ++i) {
			threads[i] = new ThreadDeleteValue<DictType, ParaType, CLASS_NAME>(dict, testcase, elementNum,operationNum, i);
		}
		return threads;
	}

	virtual void verifyResult(int threadNum, int elementNum) {

		sort(this->inVec.begin(), this->inVec.end());
		this->inVec.erase(unique(this->inVec.begin(),this->inVec.end()),this->inVec.end());

		ThreadFactory<ParaType>::verifyResult(threadNum, elementNum);
	}
};

template<typename DictType, typename ParaType, char const* CLASS_NAME> class ThreadFindValueFactory :
public ThreadFactory<ParaType> {
private:
	DictType *dict;
	DictionaryTest<DictType, ParaType, CLASS_NAME> *testcase;
public:
	ThreadFindValueFactory(DictType * l,
			DictionaryTest<DictType, ParaType, CLASS_NAME> * lt) :
	dict(l), testcase(lt) {
	}

	virtual Runnable ** createThreads(int threadNum, int elementNum, int operationNum) {
		testcase->reset();
		dict = testcase->getDict();
		this->inVec.clear();
		this->outVec.clear();

		for (int i = 0; i < threadNum; ++i) {
			ThreadInsert_T threadInsert(dict, testcase, elementNum, operationNum, i);
			threadInsert.run();

			copy(threadInsert.inVec.begin(), threadInsert.inVec.end(),
					back_inserter(this->inVec));
		}

		//		cout << "after insert ................" << endl;
		//		dict->dumpQueue();
		Runnable ** threads;
		threads = new Runnable*[threadNum];
		for (int i = 0; i < threadNum; ++i) {
			threads[i] = new ThreadFindValue<DictType, ParaType, CLASS_NAME>(dict, testcase, elementNum,operationNum, i);
		}
		return threads;
	}

	virtual void verifyResult(int threadNum, int elementNum) {

		//		sort(this->inVec.begin(), this->inVec.end());
		//		this->inVec.erase(unique(this->inVec.begin(),this->inVec.end()),this->inVec.end());

		ThreadFactory<ParaType>::verifyResult(threadNum, elementNum);
	}
};

template<typename DictType, typename ParaType, char const* CLASS_NAME> class ThreadFindKeyFactory :
public ThreadFactory<ParaType> {
private:
	DictType *dict;
	DictionaryTest<DictType, ParaType, CLASS_NAME> *testcase;
public:
	ThreadFindKeyFactory(DictType * l,
			DictionaryTest<DictType, ParaType, CLASS_NAME> * lt) :
	dict(l), testcase(lt) {
	}

	virtual Runnable ** createThreads(int threadNum, int elementNum, int operationNum) {
		testcase->reset();
		dict = testcase->getDict();
		this->inVec.clear();
		this->outVec.clear();

		for (int i = 0; i < threadNum; ++i) {
			ThreadInsert_T threadInsert(dict, testcase, elementNum, operationNum, i);
			threadInsert.run();

			copy(threadInsert.inVec.begin(), threadInsert.inVec.end(),
					back_inserter(this->inVec));
		}

		//		cout << "after insert ................" << endl;
		//		dict->dumpQueue();
		Runnable ** threads;
		threads = new Runnable*[threadNum];
		for (int i = 0; i < threadNum; ++i) {
			threads[i] = new ThreadFindKey<DictType, ParaType, CLASS_NAME>(dict, testcase, elementNum,operationNum, i);
		}
		return threads;
	}

	virtual void verifyResult(int threadNum, int elementNum) {

		//		sort(this->inVec.begin(), this->inVec.end());
		//		this->inVec.erase(unique(this->inVec.begin(),this->inVec.end()),this->inVec.end());

		ThreadFactory<ParaType>::verifyResult(threadNum, elementNum);
	}
};

template<typename DictType, typename ParaType, char const* CLASS_NAME> class ThreadInsertRemoveFactory :
public ThreadFactory<ParaType> {
private:
	DictType *dict;
	DictionaryTest<DictType, ParaType, CLASS_NAME> *testcase;
public:
	ThreadInsertRemoveFactory(DictType * l,
			DictionaryTest<DictType, ParaType, CLASS_NAME> * lt) :
	dict(l), testcase(lt) {
	}

	virtual Runnable ** createThreads(int threadNum, int elementNum, int operationNum) {
		testcase->reset();
		dict = testcase->getDict();
		this->inVec.clear();
		this->outVec.clear();

		Runnable ** threads;
		threads = new Runnable*[threadNum];

		//		ParaType data = new ParaType[NELEMENT * MAXTHREADN];
		//		RandArrayGenerator<DataType>::getRandArray(data, NELEMENT
		//				* MAXTHREADN);
		//
		//		for (int i = 0; i< operationNum; ++i) {
		//			ParaType tmp = testcase->data[i + ((this->threadId) * elementNum)];
		//			//			cout << "threadID: " << this->threadId << ", insert key: " << tmp << endl;
		//			if (dict->insert(tmp, tmp)) {
		//				this->inVec.push_back(tmp);
		//			} else {
		//				cout << "....................insert error." << endl;
		//			}
		//		}


//		for (int i = 0; i < threadNum; ++i) {
//			ThreadInsert_T threadInsert(dict, testcase, elementNum, operationNum, i);
//			threadInsert.run();
//
//			copy(threadInsert.inVec.begin(), threadInsert.inVec.end(),
//					back_inserter(this->inVec));
//		}

		for (int i = 0; i < threadNum; i += 2) {
			cout << "create insert thread" << endl;
			threads[i] = new ThreadInsert<DictType, ParaType, CLASS_NAME>(dict, testcase, elementNum,operationNum, i);
			if (i+1 < threadNum) {
				cout << "create insert thread" << endl;
				threads[i+1] = new ThreadDeleteKey<DictType, ParaType, CLASS_NAME>(dict, testcase, elementNum,operationNum, i+1);
			}
		}
		return threads;
	}

	virtual void verifyResult(int threadNum, int elementNum) {
//		ParaType tmp;
		//		while (!dict->empty()) {
		//			dict->front(tmp);
		//			this->outVec.push_back(tmp);
		//			dict->remove(tmp);
		//		}
//		ThreadFactory<ParaType>::verifyResult(threadNum, elementNum);
	}
};

//template<typename DictType, typename ParaType, char const* CLASS_NAME> void DictionaryTest<
//DictType, ParaType, CLASS_NAME>::testEmptyST() {
//	CPPUNIT_ASSERT( true == dict->empty());
//}

template<typename DictType, typename ParaType, char const* CLASS_NAME> void DictionaryTest<
DictType, ParaType, CLASS_NAME>::testInsertST() {
	ThreadInsert_T threadInsert(dict, this, BaseTest<ParaType>::NELEMENT, BaseTest<ParaType>::NOPERATION);
	threadInsert.run();

	//	dict->dumpQueue();
	vector<ParaType> outVec;
	ParaType tmp;
	for (int i = 0; i< BaseTest<ParaType>::NOPERATION; ++i) {
		if (dict->findKey(this->data[i], tmp)) {
			//				cout << "find key " << this->data[i] << ":" << tmp << endl;
			outVec.push_back(tmp);
		}
	}

	sort(threadInsert.inVec.begin(), threadInsert.inVec.end());
	//	threadInsert.inVec.erase(unique(threadInsert.inVec.begin(),threadInsert.inVec.end()),threadInsert.inVec.end());

	sort(outVec.begin(), outVec.end());

	//	cout << "inVec .... size: " << threadInsert.inVec.size() << endl;
	//	for (typename vector<ParaType>::iterator ite = threadInsert.inVec.begin(); ite
	//			!= threadInsert.inVec.end(); ++ite) {
	//		cout << *ite << " : ";
	//	}
	//	cout << endl;
	//
	//	cout << "outVec .... size: " << outVec.size() << endl;
	//	for (typename vector<ParaType>::iterator ite = outVec.begin(); ite
	//			!= outVec.eValuend(); ++ite) {
	//		cout << *ite << " : ";
	//	}
	//	cout << endl;

	CPPUNIT_ASSERT(threadInsert.inVec == outVec);
}

template<typename DictType, typename ParaType, char const* CLASS_NAME> void DictionaryTest<
DictType, ParaType, CLASS_NAME>::testDeleteKeyST() {
	ThreadInsert_T threadInsert(dict, this, BaseTest<ParaType>::NELEMENT, BaseTest<ParaType>::NOPERATION);
	ThreadDeleteKey_T threadDeleteKey(dict, this, BaseTest<ParaType>::NELEMENT, BaseTest<ParaType>::NOPERATION);

	threadInsert.run();
	threadDeleteKey.run();

	//	ParaType tmp;
	//	while (!dict->empty()) {
	//		dict->front(tmp);
	//		threadDeleteKey.outVec.push_back(tmp);
	//		dict->remove(tmp);
	//	}

	sort(threadInsert.inVec.begin(), threadInsert.inVec.end());
	threadInsert.inVec.erase(unique(threadInsert.inVec.begin(),threadInsert.inVec.end()),threadInsert.inVec.end());

	sort(threadDeleteKey.outVec.begin(), threadDeleteKey.outVec.end());

	//	cout << "inVec .... size: " << threadInsert.inVec.size() << endl;
	//	for (typename vector<ParaType>::iterator ite = threadInsert.inVec.begin(); ite
	//			!= threadInsert.inVec.end(); ++ite) {
	//		cout << *ite << " : ";
	//	}
	//	cout << endl;
	//
	//	cout << "outVec .... size: " << threadDeleteKey.outVec.size() << endl;
	//	for (typename vector<ParaType>::iterator ite = threadDeleteKey.outVec.begin(); ite
	//			!= threadDeleteKey.outVec.end(); ++ite) {
	//		cout << *ite << " : ";
	//	}
	//	cout << endl;

	CPPUNIT_ASSERT(threadInsert.inVec == threadDeleteKey.outVec);
}

template<typename DictType, typename ParaType, char const* CLASS_NAME> void DictionaryTest<
DictType, ParaType, CLASS_NAME>::testFindValueST() {
	ThreadInsert_T threadInsert(dict, this, BaseTest<ParaType>::NELEMENT, BaseTest<ParaType>::NOPERATION);
	threadInsert.run();

	vector<ParaType> outVec;
	ParaType tmp;
	for (int i = 0; i< BaseTest<ParaType>::NOPERATION; ++i) {
		if (dict->findValue(this->data[i], tmp)) {
			//				cout << "find key " << this->data[i] << ":" << tmp << endl;
			outVec.push_back(tmp);
		}
	}

	sort(threadInsert.inVec.begin(), threadInsert.inVec.end());
	//	threadInsert.inVec.erase(unique(threadInsert.inVec.begin(),threadInsert.inVec.end()),threadInsert.inVec.end());

	sort(outVec.begin(), outVec.end());

	//	cout << "inVec .... size: " << threadInsert.inVec.size() << endl;
	//	for (typename vector<ParaType>::iterator ite = threadInsert.inVec.begin(); ite
	//			!= threadInsert.inVec.end(); ++ite) {
	//		cout << *ite << " : ";
	//	}
	//	cout << endl;
	//
	//	cout << "outVec .... size: " << outVec.size() << endl;
	//	for (typename vector<ParaType>::iterator ite = outVec.begin(); ite
	//			!= outVec.end(); ++ite) {
	//		cout << *ite << " : ";
	//	}
	//	cout << endl;

	CPPUNIT_ASSERT(threadInsert.inVec == outVec);
}

template<typename DictType, typename ParaType, char const* CLASS_NAME> void DictionaryTest<
DictType, ParaType, CLASS_NAME>::testDeleteValueST() {
	ThreadInsert_T threadInsert(dict, this, BaseTest<ParaType>::NELEMENT, BaseTest<ParaType>::NOPERATION);
	ThreadDeleteValue_T threadDeleteValue(dict, this, BaseTest<ParaType>::NELEMENT, BaseTest<ParaType>::NOPERATION);

	threadInsert.run();
	threadDeleteValue.run();

	//	ParaType tmp;
	//	while (!dict->empty()) {
	//		dict->front(tmp);
	//		threadDeleteKey.outVec.push_back(tmp);
	//		dict->remove(tmp);
	//	}

	sort(threadInsert.inVec.begin(), threadInsert.inVec.end());
	threadInsert.inVec.erase(unique(threadInsert.inVec.begin(),threadInsert.inVec.end()),threadInsert.inVec.end());

	sort(threadDeleteValue.outVec.begin(), threadDeleteValue.outVec.end());

	//	cout << "inVec .... size: " << threadInsert.inVec.size() << endl;
	//	for (typename vector<ParaType>::iterator ite = threadInsert.inVec.begin(); ite
	//			!= threadInsert.inVec.end(); ++ite) {
	//		cout << *ite << " : ";
	//	}
	//	cout << endl;
	//
	//	cout << "outVec .... size: " << threadDeleteKey.outVec.size() << endl;
	//	for (typename vector<ParaType>::iterator ite = threadDeleteKey.outVec.begin(); ite
	//			!= threadDeleteKey.outVec.end(); ++ite) {
	//		cout << *ite << " : ";
	//	}
	//	cout << endl;

	CPPUNIT_ASSERT(threadInsert.inVec == threadDeleteValue.outVec);
}

template<typename DictType, typename ParaType, char const* CLASS_NAME> void DictionaryTest<
DictType, ParaType, CLASS_NAME>::testInsertMT() {
	ThreadInsertFactory<DictType, ParaType, CLASS_NAME> factory(dict, this);
	ThreadRunner::runThreads(&factory, CLASS_NAME, "testInsertMT");
}

template<typename DictType, typename ParaType, char const* CLASS_NAME> void DictionaryTest<
DictType, ParaType, CLASS_NAME>::testDeleteKeyMT() {
	ThreadDeleteKeyFactory<DictType, ParaType, CLASS_NAME> factory(dict, this);
	ThreadRunner::runThreads(&factory, CLASS_NAME, "testDeleteKeyMT");
}

template<typename DictType, typename ParaType, char const* CLASS_NAME> void DictionaryTest<
DictType, ParaType, CLASS_NAME>::testDeleteValueMT() {
	ThreadDeleteValueFactory<DictType, ParaType, CLASS_NAME> factory(dict, this);
	ThreadRunner::runThreads(&factory, CLASS_NAME, "testDeleteValueMT");
}

template<typename DictType, typename ParaType, char const* CLASS_NAME> void DictionaryTest<
DictType, ParaType, CLASS_NAME>::testFindValueMT() {
	ThreadFindValueFactory<DictType, ParaType, CLASS_NAME> factory(dict, this);
	ThreadRunner::runThreads(&factory, CLASS_NAME, "testFindValueMT");
}

template<typename DictType, typename ParaType, char const* CLASS_NAME> void DictionaryTest<
DictType, ParaType, CLASS_NAME>::testFindKeyMT() {
	ThreadFindKeyFactory<DictType, ParaType, CLASS_NAME> factory(dict, this);
	ThreadRunner::runThreads(&factory, CLASS_NAME, "testFindKeyMT");
}

template<typename DictType, typename ParaType, char const* CLASS_NAME> void DictionaryTest<
DictType, ParaType, CLASS_NAME>::testInsertRemoveMT() {
	ThreadInsertRemoveFactory<DictType, ParaType, CLASS_NAME> factory(dict, this);
	ThreadRunner::runThreads(&factory, CLASS_NAME, "testInsertRemoveMT");
}
}

#endif /* TEST_DICTIONARY_H_ */

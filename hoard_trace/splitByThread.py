import sys
import os


def main():
	inputFiles = []
	for arg in sys.argv:
		inputFiles.append(arg)
	
	if len(inputFiles) != 2:
		print 'Well shit'
		return -1
	
	# InputFile should be of the form threadtest_Tc_It_NO_Wd_Os.out
	# where Tc = ThreadCount, It = Iterations, NO = Num Objects, Wd = Work Done, Os = Object Size
	
	inputFile = open(inputFiles[1], 'r')
	inputText = inputFile.readlines()
	inputFile.close()

	# print '[InputText] %s' % (inputText)
	
	# Any output that is not part of a malloc() or free() call from threadTest
	#generalOutput = {} # {tid -> [(file, function, lock?)]}
	# Any output that is part of a malloc() from threadtest (between marked print statements)
	#mallocOutput = {} # {tid -> [[(file, function, lock?)]]} value is a list of list, where each top-level list corresponds to a malloc() call and the low-level list is the instruction trace
	# Any output that is part of a free() from threadtest (between marked print statements)
	#freeOutput = {} # {tid -> [[(file, function, lock?)]]} value is a list of list, where each top-level list corresponds to a free() call and the low-level list is the instruction trace
	# This is a map so that we can have a "stateful" parser. Since each threads output will be sequential, we can make some assumptions
	#threadState = {} # {tid -> ("state", [])} state can be either 'malloc', 'free', or 'general', tupled with a list of traces mapped to that particular call (None for general)
	
	# for all of the above, lock? can be either '', 'LOCK', or 'UNLOCK'
	
	# First things first, we need to instantiate the above dicts with all known threads
	#allTids = getAllTids(inputText)
	#allTids = []
	#for tid in allTids:
	#	print tid
	#	generalOutput[tid] 	= []
	#	mallocOutput[tid] 	= []
	#	freeOutput[tid] 	= []
	#	threadState[tid] 	= ('general', []) # init state is general (since can't possible begin trace in middle of malloc/free)

	#print '[InputText-LEN] %s' % (len(inputText))
	#i = 0
	#while i < len(inputText):
	#	print '[LINE-1] %s' % (inputText[i])	
	#	i = i + 1
	for line in inputText:
		# ignore line if either of these are present
		if 'Setting Thread' in line:
			continue
		elif 'Running threadtest' in line:
			continue
		tid = getThreadID(line)
		if tid not in allTids:
			allTids.append(tid)
			
		
		# Now we write these to a file
		# Folder to write to is same as input file (minus 'threadtest_', and '.out')
		folderName = inputFiles[1].split('threadtest_')[1].split('.out')[0]
		if os.path.exists(folderName):
			# if this directory already exists, then remove it and all of its contents recusively
			os.system('rm -r %s' % (folderName))
		# Now create the directory again
		os.makedirs(folderName)
		os.chdir(folderName)
		for tid in allTids:
			os.makedirs('thread_%s' % (tid))
			os.chdir('thread_%s' % (tid))
			# Write general output to general.txt
			generalOut = ''
			for entry in generalOutput[tid]:
				# print '[ENTRY] 0:%s, 1:%s' % (entry[0], entry[1])
				if entry[2] == 'LOCK':
					generalOut += '[LOCK]\tFile: %s\t\tFunction: %s\n' % (entry[0], entry[1])
				elif entry[2] == 'UNLOCK':
					generalOut += '[UNLOCK]\tFile: %s\t\tFunction: %s\n' % (entry[0], entry[1])
				else:
					generalOut += 'File: %s\t\tFunction: %s\n' % (entry[0], entry[1])
			writeFile(generalOut, 'general.txt')
			# Now write malloc output to malloc.txt
			mallocOut = ''
			callNum = 1
			print '[LEN] %s' % (len(mallocOutput[tid]))
			for mallocCall in mallocOutput[tid]:
				mallocOut += 'Malloc #%s:\n' % (callNum)
				callNum = callNum + 1
				for entry in mallocCall:
					if entry[2] == 'LOCK':
						mallocOut += '\t[LOCK]\tFile: %s\t\tFunction: %s\n' % (entry[0], entry[1])
					elif entry[2] == 'UNLOCK':
						mallocOut += '\t[UNLOCK]\tFile: %s\t\tFunction: %s\n' % (entry[0], entry[1])
					else:
						mallocOut += '\tFile: %s\t\tFunction: %s\n' % (entry[0], entry[1])
			writeFile(mallocOut, 'malloc.txt')
			# Finally write free output to free.txt
			freeOut = ''
			callNum = 1
			for freeCall in freeOutput[tid]:
				freeOut = '%sFree #%s:\n' % (freeOut, callNum)
				callNum = callNum + 1
				for entry in freeCall:
					if entry[2] == 'LOCK':
						freeOut += '\t[LOCK]\tFile: %s\t\tFunction: %s\n' % (entry[0], entry[1])
					elif entry[2] == 'UNLOCK':
						freeOut += '\t[UNLOCK]\tFile: %s\t\tFunction: %s\n' % (entry[0], entry[1])
					else:
						freeOut += '\tFile: %s\t\tFunction: %s\n' % (entry[0], entry[1])
			writeFile(freeOut, 'free.txt')
			os.chdir('..') # Return to parent directory
		# Return to the parent directory
		os.chdir('..')
		# Ca C'est Fini!
		return 0
	
def getThreadID(line):
	return line.split('<tid:')[1].split('>')[0]
		
def getAllTids(inputText):
	allTids = []
	for line in inputText:
		if 'Setting Thread' in line:
                        continue
                elif 'Running threadtest' in line:
                        continue
		elif '<tid' not in line:
			continue
		if getThreadID(line) not in allTids:
			allTids.append(getThreadID(line))
	return allTids
	
def getFile(line):
	return line.split('[')[1].split(']')[0].strip()
	
def getFunction(line):
	return line.split(']')[1].split('()')[0].strip()
		
		

def writeFile(text, fileName):
	print text
	outFile = open(fileName, 'w+') # Create this file
	outFile.write(text)
	outFile.close()
	return	

		

if __name__ == '__main__':
	main()

// -*- C++ -*-

#ifndef HL_ONEHEAP_H
#define HL_ONEHEAP_H

#include "utility/singleton.h"
#include <thread>

namespace HL {

  template <class TheHeap>
  class OneHeap : public singleton<TheHeap> {
  public:
    
    enum { Alignment = TheHeap::Alignment };
    
    static inline void * malloc (size_t sz) {
		printf("[Heap-Layers/heaps/OneHeap] malloc() <tid:%lu>\n", pthread_self());
      return singleton<TheHeap>::getInstance().malloc (sz);
    }
    
    static inline bool free (void * ptr) {
		printf("[Heap-Layers/heaps/OneHeap] free() <tid:%lu>\n", pthread_self());
      return singleton<TheHeap>::getInstance().free (ptr);
    }
    
    static inline size_t getSize (void * ptr) {
		printf("[Heap-Layers/heaps/OneHeap] getSize() <tid:%lu>\n", pthread_self());
      return singleton<TheHeap>::getInstance().getSize (ptr);
    }
  };

}

#endif

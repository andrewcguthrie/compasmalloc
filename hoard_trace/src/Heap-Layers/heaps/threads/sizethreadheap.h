/* -*- C++ -*- */

#ifndef HL_SIZETHREADHEAP_H
#define HL_SIZETHREADHEAP_H

#include <assert.h>
#include <thread>
#include "threads/cpuinfo.h"

namespace HL {

  template <class Super>
  class SizeThreadHeap : public Super {
  public:
  
    inline void * malloc (size_t sz) {
		printf("[Heap-Layers/heaps/threads/SizeThreadHeap] malloc() <tid:%lu>\n", pthread_self());
      // Add room for a size field & a thread field.
      // Both of these must fit in a double.
      assert (sizeof(st) <= sizeof(double));
      st * ptr = (st *) Super::malloc (sz + sizeof(double));
      // Store the requested size.
      ptr->size = sz;
      assert (getOrigPtr(ptr + 1) == ptr);
      return (void *) (ptr + 1);
    }
  
    inline void free (void * ptr) {
		printf("[Heap-Layers/heaps/threads/SizeThreadHeap] free() <tid:%lu>\n", pthread_self());
      void * origPtr = (void *) getOrigPtr(ptr);
      Super::free (origPtr);
    }

    static inline size_t& size (void * ptr) {
		printf("[Heap-Layers/heaps/threads/SizeThreadHeap] size() <tid:%lu>\n", pthread_self());
      return getOrigPtr(ptr)->size;
    }

    static inline int& thread (void * ptr) {
		printf("[Heap-Layers/heaps/threads/SizeThreadHeap] thread() <tid:%lu>\n", pthread_self());
      return getOrigPtr(ptr)->tid;
    }

  private:

    typedef struct _st {
      size_t size;
      int tid;
    } st;

    static inline st * getOrigPtr (void * ptr) {
      return (st *) ((double *) ptr - 1);
    }

  };

}

#endif

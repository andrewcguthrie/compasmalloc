// -*- C++ -*-

#ifndef _FREESLLIST_H_
#define _FREESLLIST_H_

#include <assert.h>
#include <thread>

/**
 * @class FreeSLList
 * @brief A "memory neutral" singly-linked list,
 *
 * Uses the free space in objects to store
 * the pointers.
 */


class FreeSLList {
public:

  inline void clear (void) {
	printf("[Heap-Layers/utility/freeslllist] clear()  <tid:%lu>\n", pthread_self());
    head.next = NULL;
  }

  class Entry;
  
  /// Get the head of the list.
  inline Entry * get (void) {
	printf("[Heap-Layers/utility/freeslllist] get()  <tid:%lu>\n", pthread_self());
    const Entry * e = head.next;
    if (e == NULL) {
      return NULL;
    }
    head.next = e->next;
    return const_cast<Entry *>(e);
  }

  inline Entry * remove (void) {
	printf("[Heap-Layers/utility/freeslllist] remove()  <tid:%lu>\n", pthread_self());
    const Entry * e = head.next;
    if (e == NULL) {
      return NULL;
    }
    head.next = e->next;
    return const_cast<Entry *>(e);
  }
  
  inline void insert (void * e) {
	printf("[Heap-Layers/utility/freeslllist] insert()  <tid:%lu>\n", pthread_self());
    Entry * entry = reinterpret_cast<Entry *>(e);
    entry->next = head.next;
    head.next = entry;
  }

  class Entry {
  public:
    Entry (void)
      : next (NULL)
    {}
    Entry * next;
  private:
    Entry (const Entry&);
    Entry& operator=(const Entry&);
  };
  
private:
  Entry head;
};


#endif





// -*- C++ -*-

/*

  The Hoard Multiprocessor Memory Allocator
  www.hoard.org

  Author: Emery Berger, http://www.cs.umass.edu/~emery
 
  Copyright (c) 1998-2012 Emery Berger
  
  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

*/

#ifndef HOARD_THREADPOOLHEAP_H
#define HOARD_THREADPOOLHEAP_H

#include <cassert>
#include <thread>

#include "heaplayers.h"
#include "array.h"
//#include "cpuinfo.h"

namespace Hoard {

  template <int NumThreads,
	    int NumHeaps,
	    class PerThreadHeap_>
  class ThreadPoolHeap : public PerThreadHeap_ {
  public:
    
    typedef PerThreadHeap_ PerThreadHeap;
    
    enum { MaxThreads = NumThreads };
    enum { NumThreadsMask = NumThreads - 1};
    enum { NumHeapsMask = NumHeaps - 1};
    
    HL::sassert<((NumHeaps & NumHeapsMask) == 0)> verifyPowerOfTwoHeaps;
    HL::sassert<((NumThreads & NumThreadsMask) == 0)> verifyPowerOfTwoThreads;
    
    enum { MaxHeaps = NumHeaps };
    
    ThreadPoolHeap (void)
    {
		printf("[include/util/ThreadPoolHeap] ThreadPoolHeap() <tid:%lu>\n", pthread_self());
      // Note: The tidmap values should be set externally.
      int j = 0;
      for (int i = 0; i < NumThreads; i++) {
	setTidMap(i, j % NumHeaps);
	j++;
      }
    }
    
    inline PerThreadHeap& getHeap (void) {
		printf("[include/util/ThreadPoolHeap] getHeap() <tid:%lu>\n", pthread_self());
      int tid = HL::CPUInfo::getThreadId();
      int heapno = _tidMap(tid & NumThreadsMask);
      return _heap(heapno);
    }
    
    inline void * malloc (size_t sz) {
		printf("[include/util/ThreadPoolHeap] malloc()  <tid:%lu>\n", pthread_self());
      return getHeap().malloc (sz);
    }
    
    inline void free (void * ptr) {
		printf("[include/util/ThreadPoolHeap] free()  <tid:%lu>\n", pthread_self());
      getHeap().free (ptr);
    }
    
    inline void clear (void) {
		printf("[include/util/ThreadPoolHeap] clear() <tid:%lu>\n", pthread_self());
      getHeap().clear();
    }
    
    inline size_t getSize (void * ptr) {
		printf("[include/util/ThreadPoolHeap] getSize()  <tid:%lu>\n", pthread_self());
      return PerThreadHeap::getSize (ptr);
    }
    
    void setTidMap (int index, int value) {
		printf("[include/util/ThreadPoolHeap] setTidMap() <tid:%lu>\n", pthread_self());
      assert ((value >= 0) && (value < MaxHeaps));
      _tidMap(index) = value;
    }
    
    int getTidMap (int index) const {
		printf("[include/util/ThreadPoolHeap] getTidMap() <tid:%lu>\n", pthread_self());
      return _tidMap(index); 
    }
    
    void setInusemap (int index, int value) {
		printf("[include/util/ThreadPoolHeap] setInusemap() <tid:%lu>\n", pthread_self());
      _inUseMap(index) = value;
    }
    
    int getInusemap (int index) const {
		printf("[include/util/ThreadPoolHeap] getInusemap() <tid:%lu>\n", pthread_self());
      return _inUseMap(index);
    }
    
    
  private:
    
    /// Which heap is assigned to which thread, indexed by thread.
    Array<MaxThreads, int> _tidMap;
    
    /// Which heap is in use (a reference count).
    Array<MaxHeaps, int> _inUseMap;
    
    /// The array of heaps we choose from.
    Array<MaxHeaps, PerThreadHeap> _heap;
    
  };
  
}

#endif

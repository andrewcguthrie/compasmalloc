import sys
import os

"""
	This script is used to trace the output from all threads in a trace
	in the root directory of the trace folder, outputs malloc.txt and free.txt
	each containing basic statistics about the particular code paths
"""

def main():
	inputFiles = []
	for arg in sys.argv:
		inputFiles.append(arg)
	
	if len(inputFiles) != 2:
		print 'Well shit'
		return -1
	
	# InputFiles[1] should be the dir that contains all of the thread outputs
	if not os.path.exists(inputFiles[1]):
		print 'Dir doesn\' exist'
		return -1
	
	os.chdir(inputFiles[1])
	
	mallocCodePaths = {} # {threadID -> {'codePath' -> count}}
	freeCodePaths = {} # {threadID -> {'codePath' -> count}}
	
	threads = os.listdir('.')
	for thread in threads:
		print 'Thread #%s' % (thread)
		mallocCodePaths[thread] = {}
		freeCodePaths[thread] = {}
		generalFile = open(os.path.join(thread, 'general.txt'), 'r')
		generalText = generalFile.readlines()
		generalFile.close()
		inMalloc = False
		mallocCodePath = ''
		inFree = False
		freeCodePath = ''
		for line in generalText:
			if inMalloc:
				if 'MALLOC-END' in line:
					inMalloc = False
					if mallocCodePath in mallocCodePaths[thread].keys():
						mallocCodePaths[thread][mallocCodePath] = mallocCodePaths[thread][mallocCodePath] + 1
						mallocCodePath = ''
					else:
						# print 'MALLOC CODE PATH:\n%s' % (mallocCodePath)
						mallocCodePaths[thread][mallocCodePath] = 1
						mallocCodePath = ''
				else:
					mallocCodePath += line 
					mallocCodePath += '\n'
			elif inFree:
				if 'FREE-END' in line:
					inFree = False
					if freeCodePath in freeCodePaths[thread].keys():
						freeCodePaths[thread][freeCodePath] = freeCodePaths[thread][freeCodePath] + 1
						freeCodePath = ''
					else:
						freeCodePaths[thread][freeCodePath] = 1
						freeCodePath = ''
				else:
					freeCodePath += line 
					freeCodePath += '\n'
			else:
				if 'MALLOC-START' in line:
					inMalloc = True
				elif 'FREE-START' in line:
					inFree = True
	print 'Finished binning the code paths'
	# Now we need to "invert" the dicts so that it becomes codePath -> thread -> count
	invertedMallocCodePaths = {}
	invertedFreeCodePaths = {}
	for thread in mallocCodePaths.keys():
		for codePath in mallocCodePaths[thread]:
			if codePath in invertedMallocCodePaths.keys():
				if thread in invertedMallocCodePaths[codePath].keys():
					invertedMallocCodePaths[codePath][thread] = mallocCodePaths[thread][codePath] 
				else:
					invertedMallocCodePaths[codePath][thread] = mallocCodePaths[thread][codePath]
			else:
				invertedMallocCodePaths[codePath] = {}
				invertedMallocCodePaths[codePath][thread] = mallocCodePaths[thread][codePath]
	
	for thread in freeCodePaths.keys():
		for codePath in freeCodePaths[thread]:
			if codePath in invertedFreeCodePaths.keys():
				if thread in invertedFreeCodePaths[codePath].keys():
					invertedFreeCodePaths[codePath][thread] = freeCodePaths[thread][codePath] 
				else:
					invertedFreeCodePaths[codePath][thread] = freeCodePaths[thread][codePath]
			else:
				invertedFreeCodePaths[codePath] = {}
				invertedFreeCodePaths[codePath][thread] = freeCodePaths[thread][codePath]
	mallocCodePaths = {}
	freeCodePaths = {}
	print 'Finished inverting the dicts'
	# Build the output files' text
	mallocOutput = ''
	for codePath in invertedMallocCodePaths.keys():
		mallocOutput += 'CODE PATH:\n%s\nOccurrence Count by Thread:\n' % (codePath)
		for thread in invertedMallocCodePaths[codePath].keys():
			mallocOutput += '\tThread #%s:\n\tCount: %s\n' % (thread, invertedMallocCodePaths[codePath][thread])
		mallocOutput += '\n\n'
	invertedMallocCodePaths = {}
	print 'Generated malloc.txt'
	freeOutput = ''
	for codePath in invertedFreeCodePaths.keys():
		freeOutput += 'CODE PATH:\n%s\nOccurrence Count by Thread:\n' % (codePath)
		for thread in invertedFreeCodePaths[codePath].keys():
			freeOutput += '\tThread #%s:\n\tCount: %s\n' % (thread, invertedFreeCodePaths[codePath][thread])
		freeOutput += '\n\n'
	invertedFreeCodePaths = {}
	print 'Generated free.txt'
	writeFile(mallocOutput, 'malloc.txt')
	writeFile(freeOutput, 'free.txt')
	
	# Return back to parent directory
	os.chdir('..')
	# Ca C'est Fini!
	return 0
	
		
		

def writeFile(text, fileName):
	# If the file already exists, delete it
	if os.path.exists('%s' % (fileName)):
		os.system('rm %s' % (fileName))
	outFile = open(fileName, 'w+') # Create this file
	outFile.write(text)
	outFile.close()
	return	

		

if __name__ == '__main__':
	main()

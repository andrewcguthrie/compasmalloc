// -*- C++ -*-

/*

  The Hoard Multiprocessor Memory Allocator
  www.hoard.org

  Author: Emery Berger, http://www.cs.umass.edu/~emery
 
  Copyright (c) 1998-2012 Emery Berger
  
  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

*/


#ifndef HOARD_LOCKMALLOCHEAP_H
#define HOARD_LOCKMALLOCHEAP_H

// Just lock malloc (unlike LockedHeap, which locks both malloc and
// free). Meant to be combined with something like RedirectFree, which will
// implement free.

namespace Hoard {

  template <typename Heap>
    class LockMallocHeap : public Heap {
  public:
    MALLOC_FUNCTION INLINE void * malloc (size_t sz) {
      //HL::Guard<Heap> l (*this);
	// __asm__ __volatile__("mfence": : : ); // HARDWARE FENCE
        // __asm__ __volatile__("": : :"memory"); // COMPILER FENCE
        // __asm__ __volatile__("mfence": : :"memory"); // BOTH FENCES
	 int temp = 5;
         __sync_bool_compare_and_swap(&temp, 0, 1); // CAS
	// printf("[Hoard] Lock #6\n");
      // __asm__ __volatile__("DMB": : :"memory"); // ARM FENCE-1
      // __asm__ __volatile__("DSB": : :"memory"); // ARM FENCE-2
      // __asm__ __volatile__("ISB": : :"memory"); // ARM FENCE-3


      return Heap::malloc (sz);
    }
  };

}

#endif

Building and Running COMPASMalloc
---------------------------------

Prerequisites:
NDK for Android build to test on ARM boards 
	android -- armv7-a
	aarch64 -- armv8-a
(see here for former: https://developer.android.com/ndk/downloads/index.html)
	

In the COMPASMalloc folder, there is a python script called buildScript.py. Before this is run, a few paths need to be changed.
Wherever the NDK was installed, that path needs to be substituted into BuildCommands for ARM-32 and ARM-64, in two places each:
	In the compiler specification
	In the sysroot specification
Only mutate the high-level path (ie: everything before NDK/)

Then simply running 'python buildScript.py' will compile every test for every platform for every COMPASMalloc variant,
both with statistics and without statistics enabled, neatly sorted in the newly created bin/ folder.  


Running COMPASMalloc is very simple: use the provided shell script of runme_multi.sh. This can be edited to your preference to
run whatever configuration and however many iterations are desired. Note that this only works on x86 platforms, for testing on ARM
platforms, adb is required (must be installed on your machine), the target device needs to be connected via USB and then rooted. This
can be done by going to the android sdk installation directory, to where adb is installed (or appending the dir to PATH) and then running
'adb reboot'
to make sure that you can connect as root, and then
'adb root' 
to connect as root. Once you do so, create the subdirectories desired to store the binaries you wish to push on, then from the target
machine (not the adb shell, Ctrl-C out of that), run 'adb push $targetFile $targetLocation'. Connect back as root and then you can execute
the binary file via './', but shell scripts will not work. 
To check that the device is connected, run 'adb devices', or use the help option of adb to learn all of the commands/options.

Running each test individually requires specifying the particular parameters, which are fully documented in each file. 
The first two parameters will always be the number of iterations run, and the number of objects allocated per iteration. 
The third parameter is always the size of the object to be allocated each iteration.
Other possible parameters (can always be explicitly checked by going into the target file) include:
	Number of Producers/Number of Consumers (Semi-Synchronous case)
	Delay time in ms between start of producer and start of consumer 
	Amount of work (in units of 1 iteration of the same arithmetic calculation) done between iterations
	Whether to use the "Big" processors (ARM-64 case)

There are four "flavors" of COMPASMalloc, each defined by the type of synchronization utilized:
	Pthread -- Standard Pthread mutexes used 
	CAS -- Atomic Compare-and-Swap operations used
	PoisNA -- Usage of a poison bit with non-atomic tuples (ie: relaxed constraint version)
	NoSynch -- No synchronization at all utilized (Use at your own risk -- almost always crashes in non-trivial cases)
	
	
If you wish to manually build your own binary with COMPASMalloc, one of these variants must be specified on compilation
via: '-DCM_CAS', '-DCM_PTHREAD', '-DCM_POIS_NONATOMIC', '-DCM_NOSYNC'
If you wish to have statistics generated (internal to COMPASMalloc), the '-DUSE_STATISTICS' compiler flag will trigger 
that. (they won't be generated otherwise for performance reasons).
The most effective way to build your own code using COMPASMalloc is to include it in buildScript.py, and then run
that script.

To use COMPASMalloc in your code, you must first add '#include "COMPASMalloc.h"' at the top/header, and then call
COMPASMalloc::_malloc & COMPASMalloc::_free to perform malloc and free. 

One last caveat is that all of the supplied test files pin the various threads (one per producer/consumer) to their own
cores on the target machine. Before running, make sure that the number of producers & consumers does not exceed the 
capacity of your machine, and that all of the cores are enabled and running, or it won't work. 
	
	
Expected Output (sample):
aguthrie@dogfishhead:~/Research/temp/lib/COMPASMalloc/try_2$ sh runme_multi.sh
[INFO] Running asynchronous producer consumer for 1 producers, 1 consumers, 10000 iterations, 10000 objects, 6 size and 0 work loops  with 2ms delay...
Setting Thread #140693146625792 to CPU #0
Setting Thread #140693062285056 to CPU #2
Time elapsed = 7.6173
[INFO] Running asynchronous producer consumer for 1 producers, 1 consumers, 10000 iterations, 10000 objects, 6 size and 0 work loops  with 2ms delay...
Setting Thread #140699315816192 to CPU #0
Setting Thread #140699236300544 to CPU #2
Time elapsed = 7.88209

Expected Output (sample with statistics):
aguthrie@dogfishhead:~/Research/temp/lib/COMPASMalloc/try_2/bin/x86-64/Stats$ ./asynchPC_cas_x86-64_stats 10000 10000 6 1 1 2500 0
[INFO] Running asynchronous producer consumer for 1 producers, 1 consumers, 10000 iterations, 10000 objects, 6 size and 0 work loops  with 2ms delay...
Setting Thread #140580827260672 to CPU #0
Setting Thread #140580816770816 to CPU #2
Time elapsed = 4.10978
Time elapsed = 4.10978
Work Done: 0
./asynchPC_cas_x86-64_stats, 10000, 10000, 6, 1, 1, 2500, 0
Malloc Count: 100000000
Local-Free Count: 0
Remote-Free Count: 100000000
Recovered-Remote Count: 0
Swap Count: 814512
Superblocks Allocated: 240
BENCHMARK END ------------------------------




COMPASMalloc Code 
-----------------

The high-level structure of COMPASMalloc is the same across all 4 variants: Each thread has its own heap, stored entirely
on thread local storage. This heap consists of a linked list of superblocks, where each superblock is an 8kB chuck of 
memory returned by mmap. The Superblocks themselves are an intrinsic (in that the values at the mem locations are used)
linked list of blocks, by size class, where the size classes are the powers of two up to a page (4kB). The heaps also
have a Thread Local Allocation Buffer, which is a concept borrowed from Hoard. In truth, the blocks of a superblock are
placed on the TLAB when initially allocated, and on malloc, the head of this list (sorted by size class) is popped off 
and returned. 

If the TLAB is empty, then we traverse that thread's superblock list until we either find a superblock whose
free list (of blocks freed remotely -- by other threads) is non-empty, at which point we perform a SWAP operation with
our empty TLAB and the non-empty free list of this superblock. This is the major synchronization point, accessing this 
list. Either No Synchronization is used (often resulting in crashes/infinite-loops/indeterminate behavior when runs are 
long enough, atomic CAS is used, pthread mutexs are used, or our home-brewed relaxed constraint operations that rely on 
a poison bit and intuition about the nature of potential re-ordering of operations. 

When a thread wishes to free a block of memory, it first checks if it is the owner of the corresponding superblock by
masking out the low 13 bits and checking against it's own superblocks, at which point it can then prepend to it's TLAB. 
If it is not the owner, it must return the block to the owning superblock, which keeps a pointer to the free list in the
meta-data at the end. 

All of these operations are tracked if statistics are in place, and the aggregate counts are printed out at the very 
end to provide insight into the inner-workings of the allocator, and possibly evidence of pathological behavior.
	
	


Building and Running modified Hoard with benchmarks
---------------------------------------------------

Prerequisites
Same as for COMPASMalloc

In the Hoard folder are multiple versions of Hoard, the variance being where the presence of mutual exclusion 
locking was either removed (for synchronous ProducerConsumer & threadtest), replaced with CAS, or replaced with fences. There are different
types of fences depending on the target architecture, and these need to be manually toggled with Hoard re-compiled and
re-built into a static archive. Once the static library (as a '.a' file) is generated, it is recommended that you rename
it to something meaningful (ie: indicative of the target architecture). Commands to build the static library for ARM-32,
compile the individual benchmarks, and link them to the static library are below (ARM-64 commands can be built by using correct
cross compiler, --sysroot, -march, and adding in required flags as specified in the python build script) 

Compile a single benchmark: (must be done from the hoard_malloc/src/ directory)
(use your NDK install directory)
/var/services/homes/aguthrie/Research/NDK/toolchains/android/arm/bin/arm-linux-androideabi-g++ -std=c++11 --sysroot=/var/services/homes/aguthrie/Research/NDK/toolchains/android/arm/sysroot/ -O3 -DNDEBUG -c benchmarks/ProducerConsumer.cpp -o benchmarks/ProducerConsumer.o

Create a static library of hoard:
/var/services/homes/aguthrie/Research/NDK/toolchains/android/arm/bin/arm-linux-androideabi-g++ -std=c++11 --sysroot=/var/services/homes/aguthrie/Research/NDK/toolchains/android/arm/sysroot/ -O3 -fno-builtin-malloc -march=armv7-a -fPIC -finline-limit=20000 -finline-functions  -DNDEBUG  -I. -Iinclude -Iinclude/util -Iinclude/hoard -Iinclude/superblocks -IHeap-Layers -D_REENTRANT=1 -c source/unixtls.cpp source/libhoard.cpp -Bsymbolic; ar cr libhoard.a libhoard.o unixtls.o

Link the benchmark with Hoard
/var/services/homes/aguthrie/Research/NDK/toolchains/android/arm/bin/arm-linux-androideabi-g++ -std=c++11 --sysroot=/var/services/homes/aguthrie/Research/NDK/toolchains/android/arm/sysroot/ benchmarks/ProducerConsumer.o -Wl,-whole-archive libhoard.a -Wl,-no-whole-archive -o ProducerConsumer

The resultant binary file can then be pushed to the android device and run. 


On x86, it is MUCH simpler. Building the hoard static library can be done via the provided make option:
make linux-gcc-x86-64-static

We can build a single benchmark like so:
g++ -std=c++11 -O3 -DNDEBUG -c benchmarks/simple_affinity.cpp -o benchmarks/simple_affinity.o

And we can then link the two together to run:
g++ -std=c++11 benchmarks/simple_affinity.o -Wl,-whole-archive libhoard.a -Wl,-no-whole-archive -o simple_affinity -lpthread -Wl,--no-as-needed -ldl


This is one method of testing your own code with Hoard. The other (far simpler) way is via the LD_PRELOAD system variable,
which should point to the build shared library of Hoard (use the provided Makefile to generate correct shared library), and then 
simply calling malloc/free will invoke Hoard's malloc/free.



There are three variants of Hoard provided: Standard Hoard (with COMPASMalloc benchmarks linked to use Hoard 
in src/benchmarks/ folder), Modified Hoard, and Traced Hoard.

Standard Hoard is the hoard_malloc/ directory
Modified Hoard is the hoard_malloc_lock_free/ directory
Traced Hoard is the hoard_trace/ directory

Standard Hoard is fine as is, no need to muck with the internal code.

Modified Hoard has several places where the specific type of synchronization used can be altered (only applicable to synchronous 
producer-consumer and threadtest benchmarks):
	include/hoard/hoardmanager.h in two places (~line 100 and ~line 135)
	include/hoard/hoardsuperblock.h (~line 125)
	include/hoard/redirectfree.h in two places (~line 80 and ~line 105)
	include/util/lockmallocheap.h (~line 40)
	
All variants of fences and compare-and-swap are in place, and must be explicitly toggled (commenting out all others, uncommenting desired
op) before re-building hoard to get the particular variant desired. 

Traced Hoard has trace points in the entire program structure, per function (and sometimes multiple, like in the case of identifying 
mutex variable usage). This output SHOULD be redirected to a file for later examination. To aid in parsing the output, multiple Python
scripts have been written that will sort by thread, and pull out useful information. 


CLFMalloc
---------

Nothing fancy was ever tested with this malloc. It can be built into a shared library using the provided makefile, configured to be used
by setting LD_PRELOAD, and then invoked in exeution when normal malloc/free ops are performed.

All of it is stored in the clfmalloc directory.


